package Parche

import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.CarpetasServidor
import topsystems.actualizador.parche.Parche

class Parche_NUBANKIBR_565 extends Parche{

	private isSicredi = false;
	
	void definicionDeParametros(){
		isSicredi = this.isSicredi();
		}
		
	@Override
	protected void comandos() throws TopazException {
		if(!isSicredi){
		agregarAtivoProblematicoAlHistOracle();
		agregarAtivoProblematicoAlHistSQL();
		agregarAttrAEntidad();
		cambiarQueryEntradaHistorico();
		}
	}
	
	public void cambiarQueryEntradaHistorico(){
		// Agregar o sustituir una query (o varias) de DataMapping
def archivoMapp = "interesesdeudoresmapping.xml"
def queries = """
<query>
        <class-name>topsystems.automaticprocess.interesesvista.interesesdeudores.objectsdomain.HistoricoChequeEspecial</class-name>
		<query-name>query.HistoricoChequeEspecialBR</query-name>
		<database-name>TOP/CLIENTES</database-name>
		<sentence>		
			SELECT 
			s.JTS_OID AS SALDO_JTS_OID,
			s.C1822 AS SCC_C1822,
			s.C1813 AS SCC_C1813,
			s.C1820 AS SCC_C1820,
			s.C1821 AS SCC_C1821,
			s.C1806 AS SCC_C1806,
			s.ULT_FECHA_VIGENTE AS SCC_ULT_FECHA_VIGENTE,
			s.FECHA_COBRO_IOF_SOBREGIROS AS SCC_FECHA_COBRO_IOF_SOBREGIRO,
			s.C2627 AS SCC_C2627,
			s.C1604 AS SCC_C1604,
			s.C1729 as SCC_C1729,
			s.FCAMBIOESTADOSUSPENSO as SCC_FCAMBIOESTADOSUSPENSO,
			s.ATIVO_PROBLEMATICO as SCC_ATIVO_PROBLEMATICO,
			VDS.FECHA_COBRO_MULTA AS 	VDS_FECHA_COBRO_MULTA,
			VDS.FECHA_DEVENGAMIENTO_AD AS 	VDS_FECHA_DEVENGAMIENTO_AD,
			VDS.FECHA_CONTABILIZADO_COBRO_AD AS 	VDS_FECHA_CONTABILIZADO_COBRO_AD,
			VDS.DEVENGADO_COBRO_AD AS 	VDS_DEVENGADO_COBRO_AD,
			VDS.DEVENGADO_ANTERIOR_COBRO_AD	AS VDS_DEVENGADO_ANTERIOR_COBRO_AD,
			VDS.DEVENGADO_59_DIAS_AD AS 	VDS_DEVENGADO_59_DIAS_AD,
			VDS.DIAS_SOBREGIRO AS 	VDS_DIAS_SOBREGIRO,
			VDS.DIAS_AD AS 	VDS_DIAS_AD,
			--VDS.JTS_SALDO_DEUDA AS 	VDS_JTS_SALDO_DEUDA,
			VDS.FECHA_INGRESO_CHEQUE_ESPECIAL	AS VDS_FECH_INGR_CHEQ_ESPECIAL,
			VDS.FECHA_INGRESO_AD  AS 	VDS_FECHA_INGRESO_AD,
			VDS.SUSPENSO_POR_CALIF AS VDS_SUSPENSO_POR_CALIF,
			VDS.FECHA_SUSPENSO_CALIF AS VDS_FECHA_SUSPENSO_CALIF,
			VDS.FECHA_SUSPENSO_ATRASO AS VDS_FECHA_SUSPENSO_ATRASO,																							   
			VS.FECHA_ALTA  AS 	VS_FECHA_ALTA,
			VS.FECHA_VENCIMIENTO  AS 	VS_FECHA_VENCIMIENTO,
			VS.ESTADO AS VS_ESTADO,
			VS.PERIODO AS 	VS_PERIODO,
			VS.IMPORTE AS 	VS_IMPORTE,
			VS.DIA_COBRO   AS  	VS_DIA_COBRO,
			VS.TIPO_DIA_COBRO  AS 	VS_TIPO_DIA_COBRO,
			VS.DIAS_CARENCIA   AS  	VS_DIAS_CARENCIA,
			VS.JTS_SALDO_DEUDA AS 	VS_JTS_SALDO_DEUDA,
			VS.NRO_AUTORIZACION	AS VS_NRO_AUTORIZACION,
			
			VDS.INT_VENCIDO_NO_COBRADO AS VDS_INT_VENCIDO_NO_COBRADO,
			VDS.INT_VIGENTE_NO_COBRADO AS VDS_INT_VIGENTE_NO_COBRADO,
			VDS.INT_VENCIDO_NO_COBRADO_AD AS VDS_INT_VENCIDO_NO_COBRADO_AD,
			VDS.INT_VIGENTE_NO_COBRADO_AD AS VDS_INT_VIGENTE_NO_COBRADO_AD,
			
			CI.IMPORTE	 AS IOF_SUSPENSO_PENDIENTE_COBRO
			
			FROM (SELECT * FROM SALDOS WHERE C1785 = 2  AND C1651 = ' ' AND tz_lock=0) s
			LEFT JOIN 
			VTA_SOBREGIROS vs ON s.JTS_OID = vs.JTS_OID_SALDO AND vs.TZ_LOCK = 0
			LEFT JOIN 
			VTA_DATOS_SOBREGIROS vds ON s.JTS_OID = vds.JTS_OID_SALDO AND vds.TZ_LOCK = 0
			LEFT JOIN 
			(SELECT  SALDO_JTS_OID, SUM(IMPORTE) AS IMPORTE FROM CI_SOLICITUD WHERE tz_lock=0  AND ID_EVENTO = ( SELECT valor FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'COBRO_IOF_SOBREGIROS' AND PARAMETRO = 'EVENTO_IOF_SUSPENSO')
			AND ESTADO = 0
			GROUP BY SALDO_JTS_OID) ci
			ON s.JTS_OID = ci.SALDO_JTS_OID
	</sentence>
        <attribute>
            <attribute-name>saldoJtsOid</attribute-name>
            <column-name>SALDO_JTS_OID</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaContabilizadoCobroSCC</attribute-name>
            <column-name>SCC_C1822</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaDevengamientoSCC</attribute-name>
            <column-name>SCC_C1813</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>devengadoCobroSCC</attribute-name>
            <column-name>SCC_C1820</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>devengadoAntCobroSCC</attribute-name>
            <column-name>SCC_C1821</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>interesesHasta59DiasSCC</attribute-name>
            <column-name>SCC_C1806</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>ultFechaVigente</attribute-name>
            <column-name>SCC_ULT_FECHA_VIGENTE</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaUltPagoIOF</attribute-name>
            <column-name>SCC_FECHA_COBRO_IOF_SOBREGIRO</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>reservaNegocio</attribute-name>
            <column-name>SCC_C2627</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>saldoActual</attribute-name>
            <column-name>SCC_C1604</column-name>
			<column-type>double</column-type>
        </attribute>
		<attribute>
            <attribute-name>estadoSupensoSCC</attribute-name>
            <column-name>SCC_C1729</column-name>
			<column-type>String</column-type>
        </attribute>
		<attribute>
            <attribute-name>fechaSupensoSCC</attribute-name>
            <column-name>SCC_FCAMBIOESTADOSUSPENSO</column-name>
			<column-type>Date</column-type>
        </attribute>	
		<attribute>
            <attribute-name>ativoProblematicoSCC</attribute-name>
            <column-name>SCC_ATIVO_PROBLEMATICO</column-name>
			<column-type>String</column-type>
        </attribute>	
        <attribute>
            <attribute-name>fechaCobroMulta</attribute-name>
            <column-name>VDS_FECHA_COBRO_MULTA</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaDevengamientoAD</attribute-name>
            <column-name>VDS_FECHA_DEVENGAMIENTO_AD</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaContabilizadoCobroAD</attribute-name>
            <column-name>VDS_FECHA_CONTABILIZADO_COBRO_AD</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>devengadoCobroAD</attribute-name>
            <column-name>VDS_DEVENGADO_COBRO_AD</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>devengadoAnteriorCobroAD</attribute-name>
            <column-name>VDS_DEVENGADO_ANTERIOR_COBRO_AD</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>devengado59DiasAD</attribute-name>
            <column-name>VDS_DEVENGADO_59_DIAS_AD</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>diasSobregiro</attribute-name>
            <column-name>VDS_DIAS_SOBREGIRO</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>diasAD</attribute-name>
            <column-name>VDS_DIAS_AD</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaIngresoChequeEspecial</attribute-name>
            <column-name>VDS_FECH_INGR_CHEQ_ESPECIAL</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaIngresoAD</attribute-name>
            <column-name>VDS_FECHA_INGRESO_AD</column-name>
			<column-type>Date</column-type>
        </attribute>
		<attribute>
            <attribute-name>suspensoPorCalificacion</attribute-name>
            <column-name>VDS_SUSPENSO_POR_CALIF</column-name> 
            <column-type>String</column-type>
        </attribute>
		<attribute>
            <attribute-name>fechaSuspensoPorCalif</attribute-name>
            <column-name>VDS_FECHA_SUSPENSO_CALIF</column-name> 
            <column-type>Date</column-type>
        </attribute>
		<attribute>
            <attribute-name>fechaSuspensoAtraso</attribute-name>
            <column-name>VDS_FECHA_SUSPENSO_ATRASO</column-name> 
            <column-type>Date</column-type>
        </attribute>  
        <attribute>
            <attribute-name>fechaAltaCheqEsp</attribute-name>
            <column-name>VS_FECHA_ALTA</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaVencimientoCheqEsp</attribute-name>
            <column-name>VS_FECHA_VENCIMIENTO</column-name>
			<column-type>Date</column-type>
        </attribute>
		<attribute>
            <attribute-name>estadoSobregiro</attribute-name>
            <column-name>VS_ESTADO</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>periodo</attribute-name>
            <column-name>VS_PERIODO</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>montoSobregiro</attribute-name>
            <column-name>VS_IMPORTE</column-name>
			<column-type>double</column-type>
        </attribute>
		<attribute>
            <attribute-name>diaCobro</attribute-name>
            <column-name>VS_DIA_COBRO</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>tipoDiaCobro</attribute-name>
            <column-name>VS_TIPO_DIA_COBRO</column-name>
			<column-type>String</column-type>
        </attribute>
		<attribute>
            <attribute-name>diasCadencia</attribute-name>
            <column-name>VS_DIAS_CARENCIA</column-name>
			<column-type>long</column-type>
        </attribute>
		<attribute>
            <attribute-name>jtsSaldoDeudaVS</attribute-name>
            <column-name>VS_JTS_SALDO_DEUDA</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>autorization</attribute-name>
            <column-name>VS_NRO_AUTORIZACION</column-name>
			<column-type>long</column-type>
        </attribute>
		<attribute>
            <attribute-name>intVencidoNoCobrado</attribute-name>
            <column-name>VDS_INT_VENCIDO_NO_COBRADO</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>intVigenteNoCobrado</attribute-name>
            <column-name>VDS_INT_VIGENTE_NO_COBRADO</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>intVencidoNoCobradoAD</attribute-name>
            <column-name>VDS_INT_VENCIDO_NO_COBRADO_AD</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>intVigenteNoCobradoAD</attribute-name>
            <column-name>VDS_INT_VIGENTE_NO_COBRADO_AD</column-name>
			<column-type>double</column-type>
        </attribute>
		<attribute>
            <attribute-name>IOFSuspensoPendienteCobro</attribute-name>
            <column-name>IOF_SUSPENSO_PENDIENTE_COBRO</column-name>
			<column-type>double</column-type>
        </attribute>
</query>
"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))

	}
	
	public void agregarAttrAEntidad(){
		// Agregar o sustituir uno (o varios) atributos de una entidad de DataMapping
def archivoMapp = "interesesdeudoresmapping.xml"
def entidad = "core.vo_HistoricoChequeEspecial"
def atributo = """
<attribute>
	  <attribute-name>ativoProblematicoSCC</attribute-name>
 	  <column-name>SCC_ATIVO_PROBLEMATICO</column-name>
</attribute>
"""
this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))

	}

	
	public void agregarAtivoProblematicoAlHistOracle(){
	def sentencia = """
ALTER TABLE VTA_HIST_CHEQUE_ESPECIAL ADD SCC_ATIVO_PROBLEMATICO VARCHAR2 (1) DEFAULT (' ')
"""
	def descripcion = "Sobre la sentencia" // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	
	}
	
	public void agregarAtivoProblematicoAlHistSQL(){
		def sentencia = """
ALTER TABLE VTA_HIST_CHEQUE_ESPECIAL ADD SCC_ATIVO_PROBLEMATICO CHAR(1) DEFAULT ' '
 """
	def descripcion = "Sobre la sentencia" // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
	
	}	
	
	private boolean isSicredi(){
		def jarName = "sic_br_tb";
		def directoryLib = new File(CarpetasServidor.SERVER_LIB.getExpandedPath());
	
		if(directoryLib.exists() && directoryLib.isDirectory()){
			File[] filesJars = directoryLib.listFiles({ File dir, String file ->
				file.startsWith(jarName) && file.endsWith(".jar")
			} as FilenameFilter
			)
			if( filesJars.length > 0 ){
				return true;
			}
		}
		return false;
	}
}
