import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.dataserver.BorrarAtributoEntidad
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche;

class Parche_NUBANKIBR_464 extends Parche{

	@Override
	protected void comandos() {

	//----------------------------------------------------------------------------------------------------------------//
	// Actualización de la Base de Datos en Oracle
	//----------------------------------------------------------------------------------------------------------------//
	def sentencia = """
		alter session set ddl_lock_timeout=120;

		alter table GIO_DEFINICIONES add INGRESO_O_EGRESO varchar2(1) default NULL;
		alter table GIO_DEFINICIONES add DESCRIPCION varchar2(50) default NULL;
	"""
	def descripcion = "Se hacen modificaciones en GIO_DEFINICIONES."
	this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	//----------------------------------------------------------------------------------------------------------------//	


	//----------------------------------------------------------------------------------------------------------------//
	// Actualización de la Base de Datos en SQLServer
	//----------------------------------------------------------------------------------------------------------------//
	sentencia = """
		alter table GIO_DEFINICIONES add INGRESO_O_EGRESO varchar(1);
		alter table GIO_DEFINICIONES add DESCRIPCION varchar(50);
	"""
	descripcion = "Se hacen modificaciones en GIO_DEFINICIONES."
	this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
	//----------------------------------------------------------------------------------------------------------------//

	}
}