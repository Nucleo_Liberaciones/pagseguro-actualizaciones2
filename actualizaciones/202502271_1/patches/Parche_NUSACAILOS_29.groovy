import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.parche.Parche;

class Parche_NUSACAILOS_29 extends Parche {

	@Override
	protected void comandos() {

		//----------------------------------------------------------------------------------------------------------------//
		// Agregar o sustituir uno (o varios) atributos de una entidad de DataMapping
		def archivoMapp = "systemdatamapping.xml"
		def entidad = "topsystems.vo.core_EventosInteresPorTramo"
		def atributo = """
			<attribute>
            	<attribute-name>a21InteresPendiente</attribute-name>
            	<column-name>A21_INTERES_PENDIENTE</column-name>
        	</attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
		//----------------------------------------------------------------------------------------------------------------//
		
		//----------------------------------------------------------------------------------------------------------------//
		// Actualización de la Base de Datos en Oracle
		def sentencia = """
			ALTER TABLE BS_EVENTOS_INTERES_POR_TRAMO ADD A21_INTERES_PENDIENTE NUMBER (15, 2) DEFAULT (0);
        """
		def descripcion = "Se agrega IOF y GASTOS a CRE_DEUDA_DEVENGADA_POR_CUOTA."
		this.addCommand(new ActualizarBaseDeDatosOracle(sentencia, descripcion))
		//----------------------------------------------------------------------------------------------------------------//
		
		//----------------------------------------------------------------------------------------------------------------//
		// Actualización de la Base de Datos en SQLServer
		sentencia = """
			ALTER TABLE BS_EVENTOS_INTERES_POR_TRAMO ADD A21_INTERES_PENDIENTE NUMERIC (15, 2) DEFAULT (0) WITH VALUES;
        """
		descripcion = "Se agrega Interés Pendiente por Algoritmo 21 en la Tabla de Intereses Por Tramo."
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
		//----------------------------------------------------------------------------------------------------------------//
	}
}