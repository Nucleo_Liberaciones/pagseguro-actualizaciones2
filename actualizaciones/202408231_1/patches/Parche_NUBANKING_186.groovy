import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoQuery
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.command.processmanager.BorrarPropiedadesProceso
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.configuration.CarpetasServidor
import topsystems.actualizador.parche.Parche

class Parche_NUBANKING_186 extends Parche {	
	int batchSize = 1000
	
	void definicionDeParametros(){
		addParameter("CON_SALDOS_DIARIOS", "CENTROCOSTO");
		addParameter("CON_SALDOS_DIARIOS_ACTUALIZAR", "CENTROCOSTO");
	}
	
	@Override
	protected void comandos() throws TopazException {
		modificarColumna("CON_SALDOS_DIARIOS", "CENTROCOSTO")
		modificarColumna("CON_SALDOS_DIARIOS_ACTUALIZAR", "CENTROCOSTO")
		modificarQuerySDContabilidadActualizar()
		modificarQueryRehacerConSaldosDiarios()
		actualizarProcesoInsertSaldosDiariosContabilidad()
	}	
	
	void modificarQuerySDContabilidadActualizar() {
		String aFileName = "HistoricoMapping.xml"
		String aQuery = "query.QuerySDContabilidadActualizar"
		String anAttribute = """
	        <attribute>
	            <attribute-name>centroCosto</attribute-name>
	            <column-name>CENTROCOSTO</column-name>
	            <column-type>String</column-type>
	        </attribute>
			"""
		this.addCommand( new AgregarModificarAtributoQuery(aFileName, aQuery, anAttribute))
	}
	
	void  modificarQueryRehacerConSaldosDiarios(){
		def aFileName = "HistoricoMapping.xml"
		def columns
		if(isOracle()) {
			columns = """SIGNO,
                TIPO,
                FECHAPROCESO, 
                RUBRO, 
                SUCURSAL, 
                MONEDA, 
                CENTROCOSTO, 
                EMPRESA, 
                TIPOLIBRO,
                SUCURSAL_EMPRESA, 
                NVL(SALDO_AL_CORTE, 0) AS SALDO_AL_CORTE,
                NVL(SALDO_AL_CORTE_MN, 0) AS SALDO_AL_CORTE_MN,                 
                NVL(SALDO_AJUSTADO, 0) AS SALDO_AJUSTADO, 
                NVL(SALDO_AJUSTADO_MN, 0) AS SALDO_AJUSTADO_MN, 
                NVL( SALDO_FECHA_VALOR, 0) AS SALDO_FECHA_VALOR, 
                NVL(SALDO_FECHA_VALOR_MN, 0) AS SALDO_FECHA_VALOR_MN, 
               	NVL(DEBITOS, 0) AS DEBITOS, 
                NVL(CREDITOS, 0) AS CREDITOS, 
                NVL(DEBITOS_MN, 0) AS DEBITOS_MN, 
                NVL(CREDITOS_MN, 0) AS CREDITOS_MN, 
                NVL(DEBITOS_FECHAVALOR, 0) AS DEBITOS_FECHAVALOR, 
                NVL(DEBITOS_FECHAVALOR_MN, 0) AS DEBITOS_FECHAVALOR_MN, 
                NVL(DEBITOS_AJUSTE, 0) AS DEBITOS_AJUSTE, 
                NVL(DEBITOS_AJUSTE_MN, 0) AS DEBITOS_AJUSTE_MN, 
                NVL(CREDITOS_FECHAVALOR, 0) AS CREDITOS_FECHAVALOR, 
                NVL(CREDITOS_FECHAVALOR_MN, 0) AS CREDITOS_FECHAVALOR_MN, 
                NVL(CREDITOS_AJUSTE, 0) AS CREDITOS_AJUSTE, 
                NVL(CREDITOS_AJUSTE_MN, 0) AS CREDITOS_AJUSTE_MN,
                MARCAAJUSTE,
                FECHA_VALOR,
                ESTADOFECHAVALOR"""
		}else if(isSqlServer()){
			columns = """SIGNO,
                TIPO,
                FECHAPROCESO, 
                RUBRO, 
                SUCURSAL, 
                MONEDA, 
                CENTROCOSTO, 
                EMPRESA, 
                TIPOLIBRO,
                SUCURSAL_EMPRESA, 
                ISNULL(SALDO_AL_CORTE, 0) AS SALDO_AL_CORTE,
                ISNULL(SALDO_AL_CORTE_MN, 0) AS SALDO_AL_CORTE_MN,                 
                ISNULL(SALDO_AJUSTADO, 0) AS SALDO_AJUSTADO, 
                ISNULL(SALDO_AJUSTADO_MN, 0) AS SALDO_AJUSTADO_MN, 
                ISNULL( SALDO_FECHA_VALOR, 0) AS SALDO_FECHA_VALOR, 
                ISNULL(SALDO_FECHA_VALOR_MN, 0) AS SALDO_FECHA_VALOR_MN, 
               	ISNULL(DEBITOS, 0) AS DEBITOS, 
                ISNULL(CREDITOS, 0) AS CREDITOS, 
                ISNULL(DEBITOS_MN, 0) AS DEBITOS_MN, 
                ISNULL(CREDITOS_MN, 0) AS CREDITOS_MN, 
                ISNULL(DEBITOS_FECHAVALOR, 0) AS DEBITOS_FECHAVALOR, 
                ISNULL(DEBITOS_FECHAVALOR_MN, 0) AS DEBITOS_FECHAVALOR_MN, 
                ISNULL(DEBITOS_AJUSTE, 0) AS DEBITOS_AJUSTE, 
                ISNULL(DEBITOS_AJUSTE_MN, 0) AS DEBITOS_AJUSTE_MN, 
                ISNULL(CREDITOS_FECHAVALOR, 0) AS CREDITOS_FECHAVALOR, 
                ISNULL(CREDITOS_FECHAVALOR_MN, 0) AS CREDITOS_FECHAVALOR_MN, 
                ISNULL(CREDITOS_AJUSTE, 0) AS CREDITOS_AJUSTE, 
                ISNULL(CREDITOS_AJUSTE_MN, 0) AS CREDITOS_AJUSTE_MN,
                MARCAAJUSTE,
                FECHA_VALOR,
                ESTADOFECHAVALOR"""
		}

		
		def queries = """
	    <query>
        <class-name>topsystems.automaticprocess.historicocliente.calculodiario.QueryRehacerConSaldosDiarios</class-name>
        <query-name>query.QueryRehacerConSaldosDiarios</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
            SELECT ${columns} 
            FROM (
                SELECT  1 AS TIPO,
                'I' AS SIGNO,
                FECHA AS FECHAPROCESO, 
                RUBRO, 
                SUCURSAL, 
                MONEDA, 
                CENTROCOSTO, 
				${isMultiempresa() ? "EMPRESA" : "0"} AS EMPRESA,
                TIPOLIBRO,
				${isMultiempresa() ? "SUCURSAL_EMPRESA" : "' '"} AS SUCURSAL_EMPRESA, 
                SALDO_AL_CORTE, 
                SALDO_AJUSTADO, 
                SALDO_AJUSTADO_MN, 
                SALDO_FECHA_VALOR, 
                SALDO_FECHA_VALOR_MN, 
                DEBITOS, 
                CREDITOS, 
                DEBITOS_MN, 
                CREDITOS_MN, 
                SALDO_AL_CORTE_MN, 
                DEBITOS_FECHAVALOR, 
                DEBITOS_FECHAVALOR_MN, 
                DEBITOS_AJUSTE, 
                DEBITOS_AJUSTE_MN, 
                CREDITOS_FECHAVALOR, 
                CREDITOS_FECHAVALOR_MN, 
                CREDITOS_AJUSTE, 
                CREDITOS_AJUSTE_MN,
                NULL AS MARCAAJUSTE,
                NULL AS FECHA_VALOR,
                NULL AS ESTADOFECHAVALOR
                FROM CON_SALDOS_DIARIOS 
                WHERE FECHA = ?  
            UNION ALL
                SELECT 2 AS TIPO,
                M.DEBITOCREDITO AS SIGNO,
                M.FECHAPROCESO,
                M.RUBROCONTABLE AS RUBRO,
                M.SUCURSAL,
                M.MONEDA,  
				'0' AS CENTROCOSTO,
                ${isMultiempresa() ? "M.EMPRESA" : "0"} AS EMPRESA,
                1 AS TIPOLIBRO,
                ${isMultiempresa() ? "M.SUCURSAL_EMPRESA" : "' '"} AS SUCURSAL_EMPRESA,
                0 AS SALDO_AL_CORTE,
                0 AS SALDO_AJUSTADO,
                0 AS SALDO_AJUSTADO_MN,
                0 as SALDO_FECHA_VALOR,
                0 as SALDO_FECHA_VALOR_MN,
                CASE WHEN (M.DEBITOCREDITO = 'D') THEN ROUND(M.CAPITALREALIZADO, 2) ELSE 0 END as DEBITOS,
                CASE WHEN (M.DEBITOCREDITO = 'C') THEN ROUND(M.CAPITALREALIZADO, 2) ELSE 0 END as CREDITOS, 
                CASE WHEN (M.DEBITOCREDITO = 'D') THEN ROUND(M.EQUIVALENTEMN, 2) ELSE 0 END as DEBITOS_MN, 
                CASE WHEN (M.DEBITOCREDITO = 'C') THEN ROUND(M.EQUIVALENTEMN, 2) ELSE 0 END as CREDITOS_MN, 
                0 AS SALDO_AL_CORTE_MN,
                0 AS DEBITOS_FECHAVALOR,
                0 AS DEBITOS_FECHAVALOR_MN,
                0 AS DEBITOS_AJUSTE,
                0 as DEBITOS_AJUSTE_MN,
                0 AS CREDITOS_FECHAVALOR,
                0 AS CREDITOS_FECHAVALOR_MN,
                0 AS CREDITOS_AJUSTE,
                0 AS CREDITOS_AJUSTE_MN,
                M.MARCAAJUSTE AS MARCAAJUSTE,
                M.FECHAVALOR AS FECHA_VALOR,
                M.ESTADOFECHAVALOR
                FROM MOVIMIENTOS_CONTABLES M
                INNER JOIN ASIENTOS A
                ON M.FECHAPROCESO = A.FECHAPROCESO
                AND M.SUCURSAL = A.SUCURSAL
                AND M.ASIENTO = A.ASIENTO
                WHERE (M.FECHAPROCESO BETWEEN ? AND ? )
                AND A.ESTADO = 77
                AND M.DEBITOCREDITO IN ('C','D')
            UNION ALL
                SELECT  3 AS TIPO,
                'F' AS SIGNO,
                FECHA AS FECHAPROCESO, 
                RUBRO, 
                SUCURSAL, 
                MONEDA, 
                CENTROCOSTO, 
				${isMultiempresa() ? "EMPRESA" : "0"} AS EMPRESA,
                TIPOLIBRO,
				${isMultiempresa() ? "SUCURSAL_EMPRESA" : "' '"} AS SUCURSAL_EMPRESA, 
                SALDO_AL_CORTE, 
                SALDO_AJUSTADO, 
                SALDO_AJUSTADO_MN, 
                SALDO_FECHA_VALOR, 
                SALDO_FECHA_VALOR_MN, 
                DEBITOS, 
                CREDITOS, 
                DEBITOS_MN, 
                CREDITOS_MN, 
                SALDO_AL_CORTE_MN, 
                DEBITOS_FECHAVALOR, 
                DEBITOS_FECHAVALOR_MN, 
                DEBITOS_AJUSTE, 
                DEBITOS_AJUSTE_MN, 
                CREDITOS_FECHAVALOR, 
                CREDITOS_FECHAVALOR_MN, 
                CREDITOS_AJUSTE, 
                CREDITOS_AJUSTE_MN,
                NULL AS MARCAAJUSTE,
                NULL AS FECHA_VALOR,
                NULL AS ESTADOFECHAVALOR
                FROM CON_SALDOS_DIARIOS 
                WHERE FECHA = ? 
            ) T
            ORDER BY RUBRO, 
                SUCURSAL, 
                MONEDA, 
                CENTROCOSTO, 
                EMPRESA, 
                SUCURSAL_EMPRESA,
                TIPOLIBRO
        </sentence>
        <attribute>
            <attribute-name>signo</attribute-name>
            <column-name>SIGNO</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaProceso</attribute-name>
            <column-name>FECHAPROCESO</column-name>
            <column-type>Timestamp</column-type>
        </attribute>       
        <attribute>
            <attribute-name>rubro</attribute-name>
            <column-name>RUBRO</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>sucursal</attribute-name>
            <column-name>SUCURSAL</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>moneda</attribute-name>
            <column-name>MONEDA</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>centroCosto</attribute-name>
            <column-name>CENTROCOSTO</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>empresa</attribute-name>
            <column-name>EMPRESA</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>tipoLibro</attribute-name>
            <column-name>TIPOLIBRO</column-name>
            <column-type>long</column-type>
        </attribute>
         <attribute>
            <attribute-name>sucursalEmpresa</attribute-name>
            <column-name>SUCURSAL_EMPRESA</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>saldoAlCorte</attribute-name>
            <column-name>SALDO_AL_CORTE</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>saldoAlCorteMN</attribute-name>
            <column-name>SALDO_AL_CORTE_MN</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>saldoAjustado</attribute-name>
            <column-name>SALDO_AJUSTADO</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>saldoAjustadoMN</attribute-name>
            <column-name>SALDO_AJUSTADO_MN</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>saldoFechaValor</attribute-name>
            <column-name>SALDO_FECHA_VALOR</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>saldoFechaValorMN</attribute-name>
            <column-name>SALDO_FECHA_VALOR_MN</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>        
        <attribute>
            <attribute-name>debitos</attribute-name>
            <column-name>DEBITOS</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>creditos</attribute-name>
            <column-name>CREDITOS</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>debitosMN</attribute-name>
            <column-name>DEBITOS_MN</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>creditosMN</attribute-name>
            <column-name>CREDITOS_MN</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>debitosFechaValor</attribute-name>
            <column-name>DEBITOS_FECHAVALOR</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>debitosFechaValorMN</attribute-name>
            <column-name>DEBITOS_FECHAVALOR_MN</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>                
        <attribute>
            <attribute-name>debitosAjuste</attribute-name>
            <column-name>DEBITOS_AJUSTE</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>debitosAjusteMN</attribute-name>
            <column-name>DEBITOS_AJUSTE_MN</column-name>
            <column-type>BigDecimal</column-type>
        </attribute> 
        <attribute>
            <attribute-name>creditosFechaValor</attribute-name>
            <column-name>CREDITOS_FECHAVALOR</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>creditosFechaValorMN</attribute-name>
            <column-name>CREDITOS_FECHAVALOR_MN</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>creditosAjuste</attribute-name>
            <column-name>CREDITOS_AJUSTE</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>creditosAjusteMN</attribute-name>
            <column-name>CREDITOS_AJUSTE_MN</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>marcaAjuste</attribute-name>
            <column-name>MARCAAJUSTE</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaValor</attribute-name>
            <column-name>FECHA_VALOR</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>estadoFechaValor</attribute-name>
            <column-name>ESTADOFECHAVALOR</column-name>
            <column-type>long</column-type>
        </attribute>
    </query>
		"""
		this.addCommand( new AgregarModificarQuery(aFileName, queries))
	}
	
	void actualizarProcesoInsertSaldosDiariosContabilidad() {
		def fileName = "processes.py"
		def nombreProceso = "SaldosDiariosContabilidad"
		def constParamProc = """
				pdef.addConstant("insertSaldosDiariosContabilidad"
				"""
		this.addCommand( new BorrarPropiedadesProceso(fileName, nombreProceso, constParamProc))
		if(isMultiempresa()){
			def newConstParamProc = """
				pdef.addConstant("insertSaldosDiariosContabilidad","insert into con_saldos_diarios (FECHA,TZ_LOCK,RUBRO,SUCURSAL,MONEDA,CENTROCOSTO,EMPRESA,SUCURSAL_EMPRESA,TIPOLIBRO,SALDO_AL_CORTE,SALDO_AL_CORTE_MN,SALDO_AJUSTADO,SALDO_AJUSTADO_MN,SALDO_FECHA_VALOR,SALDO_FECHA_VALOR_MN,DEBITOS,CREDITOS,DEBITOS_MN,CREDITOS_MN,DEBITOS_FECHAVALOR,DEBITOS_FECHAVALOR_MN,DEBITOS_AJUSTE,DEBITOS_AJUSTE_MN,CREDITOS_FECHAVALOR,CREDITOS_FECHAVALOR_MN,CREDITOS_AJUSTE,CREDITOS_AJUSTE_MN) select ? AS FECHA,0 as Tz_Lock,RUBRO,SUCURSAL,MONEDA,CENTROCOSTO,EMPRESA,SUCURSAL_EMPRESA,TIPOLIBRO,SUM(SALDO_AL_CORTE) AS SALDO_AL_CORTE,SUM(SALDO_AL_CORTE_MN) AS SALDO_AL_CORTE_MN,SUM(SALDO_AJUSTADO) AS SALDO_AJUSTADO,SUM(SAL_AJUSTADO_MN) AS SAL_AJUSTADO_MN,SUM(SALDO_FECHA_VALOR) AS SALDO_FECHA_VALOR,SUM(SALDO_FECHA_VALOR_MN) AS SALDO_FECHA_VALOR_MN,SUM(DEBITOS) AS DEBITOS,SUM(CREDITOS) AS CREDITOS,SUM(DEBITOS_MN) AS DEBITOS_MN,SUM(CREDITOS_MN) AS CREDITOS_MN,0 AS DEBITOS_FECHAVALOR,0 AS DEBITOS_FECHAVALOR_MN,0 AS DEBITOS_AJUSTE,0 AS DEBITOS_AJUSTE_MN,0 AS CREDITOS_FECHAVALOR,0 AS CREDITOS_FECHAVALOR_MN,0 AS CREDITOS_AJUSTE,0 AS CREDITOS_AJUSTE_MN from ((select SUCURSAL_CUENTA SUCURSAL,MONEDA,1 as TIPOLIBRO,'0' as CentroCosto,0 as SALDO_AL_CORTE,0 as SALDO_AL_CORTE_MN,0 as SALDO_AJUSTADO,0 as SAL_AJUSTADO_MN,0 as SALDO_FECHA_VALOR,0 as SALDO_FECHA_VALOR_MN,SUM(CASE WHEN (M.DEBITOCREDITO = 'D') THEN ROUND(M.CAPITALREALIZADO, 2) ELSE 0 END) as DEBITOS, SUM(CASE WHEN (M.DEBITOCREDITO = 'C') THEN ROUND(M.CAPITALREALIZADO, 2) ELSE 0 END) as CREDITOS, SUM(CASE WHEN (M.DEBITOCREDITO = 'D') THEN ROUND(M.EQUIVALENTEMN, 2) ELSE 0 END) as DEBITOS_MN, SUM(CASE WHEN (M.DEBITOCREDITO = 'C') THEN ROUND(M.EQUIVALENTEMN, 2) ELSE 0 END) as CREDITOS_MN, RUBROCONTABLE RUBRO, 0 TZ_LOCK, M.EMPRESA_CUENTA EMPRESA, M.SUCURSAL_EMPRESA_CUENTA SUCURSAL_EMPRESA FROM MOVIMIENTOS_CONTABLES M INNER JOIN ASIENTOS A ON A.FECHAPROCESO = ? AND M.FECHAPROCESO = A.FECHAPROCESO AND M.SUCURSAL = A.SUCURSAL AND M.ASIENTO = A.ASIENTO AND A.ESTADO = 77 GROUP BY M.RUBROCONTABLE, M.SUCURSAL_CUENTA, M.MONEDA, M.CENTROCOSTO, M.EMPRESA_CUENTA, M.SUCURSAL_EMPRESA_CUENTA ) UNION (select SUCURSAL,MONEDA,1 as TIPOLIBRO,'0' as CentroCosto,sum(C1604) as SALDO_AL_CORTE,sum(C3958) as SALDO_AL_CORTE_MN,sum(C1604) as SALDO_AJUSTADO,sum(C3958) as SAL_AJUSTADO_MN,sum(C1604) as SALDO_FECHA_VALOR,sum(C3958) as SALDO_FECHA_VALOR_MN,0 DEBITOS,0 CREDITOS,0 DEBITOS_MN,0 CREDITOS_MN,C1730 as RUBRO,0 TZ_LOCK,EMPRESA,SUCURSAL_EMPRESA FROM SALDOS S WHERE S.TZ_LOCK = 0 GROUP BY C1730,SUCURSAL,MONEDA,EMPRESA,SUCURSAL_EMPRESA)) D GROUP BY RUBRO,SUCURSAL,MONEDA,CENTROCOSTO,EMPRESA,SUCURSAL_EMPRESA,TIPOLIBRO"); 			   
				"""
			this.addCommand( new AgregarModificarPropiedadesProceso(fileName, nombreProceso, newConstParamProc))
		} else {
			def newConstParamProc = """
			pdef.addConstant("insertSaldosDiariosContabilidad","insert into con_saldos_diarios (FECHA,TZ_LOCK,RUBRO,SUCURSAL,MONEDA,CENTROCOSTO,EMPRESA,TIPOLIBRO,SALDO_AL_CORTE,SALDO_AL_CORTE_MN,SALDO_AJUSTADO,SALDO_AJUSTADO_MN,SALDO_FECHA_VALOR,SALDO_FECHA_VALOR_MN,DEBITOS,CREDITOS,DEBITOS_MN,CREDITOS_MN,DEBITOS_FECHAVALOR,DEBITOS_FECHAVALOR_MN,DEBITOS_AJUSTE,DEBITOS_AJUSTE_MN,CREDITOS_FECHAVALOR,CREDITOS_FECHAVALOR_MN,CREDITOS_AJUSTE,CREDITOS_AJUSTE_MN) select ? AS FECHA,0 as Tz_Lock,RUBRO,SUCURSAL,MONEDA,CENTROCOSTO,EMPRESA,TIPOLIBRO,SUM(SALDO_AL_CORTE) AS SALDO_AL_CORTE,SUM(SALDO_AL_CORTE_MN) AS SALDO_AL_CORTE_MN,SUM(SALDO_AJUSTADO) AS SALDO_AJUSTADO,SUM(SAL_AJUSTADO_MN) AS SAL_AJUSTADO_MN,SUM(SALDO_FECHA_VALOR) AS SALDO_FECHA_VALOR,SUM(SALDO_FECHA_VALOR_MN) AS SALDO_FECHA_VALOR_MN,SUM(DEBITOS) AS DEBITOS,SUM(CREDITOS) AS CREDITOS,SUM(DEBITOS_MN) AS DEBITOS_MN,SUM(CREDITOS_MN) AS CREDITOS_MN,0 AS DEBITOS_FECHAVALOR,0 AS DEBITOS_FECHAVALOR_MN,0 AS DEBITOS_AJUSTE,0 AS DEBITOS_AJUSTE_MN,0 AS CREDITOS_FECHAVALOR,0 AS CREDITOS_FECHAVALOR_MN,0 AS CREDITOS_AJUSTE,0 AS CREDITOS_AJUSTE_MN from ((select SUCURSAL_CUENTA SUCURSAL,MONEDA,1 as TIPOLIBRO,'0' as CentroCosto,0 as SALDO_AL_CORTE,0 as SALDO_AL_CORTE_MN,0 as SALDO_AJUSTADO,0 as SAL_AJUSTADO_MN,0 as SALDO_FECHA_VALOR,0 as SALDO_FECHA_VALOR_MN,SUM(CASE WHEN (M.DEBITOCREDITO = 'D') THEN ROUND(M.CAPITALREALIZADO, 2) ELSE 0 END) as DEBITOS,SUM(CASE WHEN (M.DEBITOCREDITO = 'C') THEN ROUND(M.CAPITALREALIZADO, 2) ELSE 0 END) as CREDITOS,SUM(CASE WHEN (M.DEBITOCREDITO = 'D') THEN ROUND(M.EQUIVALENTEMN, 2) ELSE 0 END) as DEBITOS_MN,SUM(CASE WHEN (M.DEBITOCREDITO = 'C') THEN ROUND(M.EQUIVALENTEMN, 2) ELSE 0 END) as CREDITOS_MN,RUBROCONTABLE RUBRO,0 TZ_LOCK,0 AS EMPRESA FROM MOVIMIENTOS_CONTABLES M INNER JOIN ASIENTOS A ON A.FECHAPROCESO = ? AND M.FECHAPROCESO = A.FECHAPROCESO AND M.SUCURSAL = A.SUCURSAL AND M.ASIENTO = A.ASIENTO AND A.ESTADO = 77 GROUP BY M.RUBROCONTABLE,M.SUCURSAL_CUENTA,M.MONEDA,M.CENTROCOSTO) UNION (select SUCURSAL,MONEDA,1 as TIPOLIBRO,'0' as CentroCosto,sum(C1604) as SALDO_AL_CORTE,sum(C3958) as SALDO_AL_CORTE_MN,sum(C1604) as SALDO_AJUSTADO,sum(C3958) as SAL_AJUSTADO_MN,sum(C1604) as SALDO_FECHA_VALOR,sum(C3958) as SALDO_FECHA_VALOR_MN,0 DEBITOS,0 CREDITOS,0 DEBITOS_MN,0 CREDITOS_MN,C1730 as RUBRO,0 TZ_LOCK,0 AS EMPRESA FROM SALDOS S WHERE S.TZ_LOCK = 0 GROUP BY C1730,SUCURSAL,MONEDA,EMPRESA)) D GROUP BY RUBRO,SUCURSAL,MONEDA,CENTROCOSTO,EMPRESA,TIPOLIBRO"); 			   
			"""
			this.addCommand( new AgregarModificarPropiedadesProceso(fileName, nombreProceso, newConstParamProc))
		}
	}
	
	void addParameter(String nombreTabla, String nombreColumna){
		def cambiarColumnaQuery =""
		if(isSqlServer()) {
			cambiarColumnaQuery = """
			SELECT CASE WHEN COUNT(COLUMN_NAME) = 0 THEN 'S' ELSE 'N' END
			FROM INFORMATION_SCHEMA.COLUMNS
			WHERE TABLE_NAME = '${nombreTabla}' 
			AND COLUMN_NAME = '${nombreColumna}'
			AND UPPER(DATA_TYPE) = 'VARCHAR'
			AND CHARACTER_MAXIMUM_LENGTH = 5
			"""
		}
		else if(isOracle()){
			cambiarColumnaQuery = """
			SELECT  CASE WHEN COUNT(COLUMN_NAME) = 0 THEN 'S' ELSE 'N' END
			FROM USER_TAB_COLUMNS 
			WHERE TABLE_NAME = '${nombreTabla}'
			AND COLUMN_NAME = '${nombreColumna}'
			AND UPPER(DATA_TYPE) = 'VARCHAR2'
			AND DATA_LENGTH = 5
			"""			
		}
		
		addParameterQuery("${nombreTabla}.${nombreColumna}", "Cambiar columna '${nombreColumna}' en la tabla '${nombreTabla}' S(SI) o N(No)", "STRING", cambiarColumnaQuery)
	}
	
	void modificarColumna(String nombreTabla, String nombreColumna){
		String param = getStringParam("${nombreTabla}.${nombreColumna}")
		if(param != null && param.toUpperCase() == 'S') {
			def sentencia = ""
			def descripcion = "Crear nueva columna"
			if(isSqlServer()) {
				sentencia = """
				-- HM begin sentence
				BEGIN
					DECLARE @sql_query NVARCHAR(MAX);
					IF EXISTS (
					    SELECT 1
					    FROM INFORMATION_SCHEMA.COLUMNS
					    WHERE TABLE_NAME = '${nombreTabla}'
					      AND COLUMN_NAME = '${nombreColumna}_TMP'
					)
					BEGIN
					   SET @sql_query = N'UPDATE ${nombreTabla} SET ${nombreColumna}_TMP = ''0''';
					   EXEC sp_executesql @sql_query;
					END
					ELSE
					BEGIN
					   SET @sql_query = N'ALTER TABLE ${nombreTabla} ADD ${nombreColumna}_TMP VARCHAR(5) NOT NULL DEFAULT ''0''';
					   EXEC sp_executesql @sql_query;
					END
				END
                -- HM end sentence"""			
				this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
				
				sentencia = """
				-- HM begin sentence
				BEGIN
					DECLARE @constraint_name NVARCHAR(128);
				 	DECLARE @rowsAffected INT = 1; -- Contador de filas procesadas	
				    DECLARE @keys NVARCHAR(MAX) = NULL;
				    DECLARE @sql_query NVARCHAR(MAX);
					DECLARE @current_index_name NVARCHAR(128);
					DECLARE @indexes_columns_names NVARCHAR(MAX) = NULL;
					DECLARE @create_index_queries NVARCHAR(MAX) = '';
				
				    	
					-- Copiar los datos a la nueva columna usando lotes
					WHILE (@rowsAffected > 0)
					BEGIN
						BEGIN TRANSACTION;
						UPDATE TOP (${batchSize}) ${nombreTabla}
						SET ${nombreColumna}_TMP = CAST(${nombreColumna} AS VARCHAR(5)) 
						WHERE ${nombreColumna}_TMP = '0' 
						AND ${nombreColumna} IS NOT NULL 
						AND CAST(${nombreColumna} AS VARCHAR(5)) <> '0';
						SET @rowsAffected = @@ROWCOUNT
						COMMIT TRANSACTION;		
				  	END
							
					    
					--Obtener primary keys
					SELECT @keys = STUFF((
			        SELECT ',' + c.name
			        FROM sys.key_constraints kc
			        JOIN sys.indexes i ON kc.parent_object_id = i.object_id AND kc.unique_index_id = i.index_id
			        JOIN sys.index_columns ic ON i.object_id = ic.object_id AND i.index_id = ic.index_id
			        JOIN sys.columns c ON ic.object_id = c.object_id AND ic.column_id = c.column_id
			        WHERE kc.type = 'PK' 
			        AND kc.parent_object_id = OBJECT_ID('${nombreTabla}')
			        FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '');
					
				    -- Eliminar restricciones DEFAULT y primary key para la columna
				    DECLARE c_constraints CURSOR FOR
					    SELECT df.name
							FROM sys.default_constraints df
							INNER JOIN sys.columns col
							ON col.object_id = df.parent_object_id AND col.column_id = df.parent_column_id
							WHERE df.parent_object_id = OBJECT_ID('${nombreTabla}') 
							AND col.name = '${nombreColumna}'
						UNION
						SELECT name 
							FROM sys.key_constraints 
							WHERE type = 'PK' 
							AND parent_object_id = OBJECT_ID('${nombreTabla}');
					
					OPEN c_constraints;
					WHILE 1=1
					BEGIN
						FETCH NEXT FROM c_constraints INTO @constraint_name;	
						IF @@FETCH_STATUS <> 0 BREAK;
					    SET @sql_query = N'ALTER TABLE ${nombreTabla} DROP CONSTRAINT ' + @constraint_name;
					    EXEC sp_executesql @sql_query;		
					END
					CLOSE c_constraints;
					DEALLOCATE c_constraints;

					-- Indices de la tabla para la columna
					DECLARE c_indexes CURSOR FOR
					SELECT i.name
					FROM sys.index_columns ic
					JOIN sys.indexes i 
						ON ic.object_id = i.object_id 
						AND ic.index_id = i.index_id
					JOIN sys.columns c 
						ON ic.object_id = c.object_id 
						AND ic.column_id = c.column_id
					WHERE ic.object_id = OBJECT_ID('${nombreTabla}') 
					    AND c.name = '${nombreColumna}';
					   
				   	OPEN c_indexes;
					WHILE 1=1
					BEGIN
						FETCH NEXT FROM c_indexes INTO @current_index_name;	
						IF @@FETCH_STATUS <> 0 BREAK;
					
						-- Obtener indices
						SELECT @indexes_columns_names = STUFF((
							SELECT ',' + c.name
							FROM sys.index_columns ic
							JOIN sys.indexes i 
								ON ic.object_id = i.object_id 
								AND ic.index_id = i.index_id
							JOIN sys.columns c 
								ON ic.object_id = c.object_id 
								AND ic.column_id = c.column_id
							WHERE i.name = @current_index_name
							FOR XML PATH(''), TYPE
							).value('.', 'NVARCHAR(MAX)'), 1, 1, '');
						
						-- Elimino indice 		
						SET @sql_query = N'DROP INDEX ' + @current_index_name + N' ON ${nombreTabla}';
						EXEC sp_executesql @sql_query;	
					
					    -- Guardo indices para crear luego
					    SET @create_index_queries =  @create_index_queries + N'CREATE INDEX ' + @current_index_name + N' ON ${nombreTabla} ('  + @indexes_columns_names + N');';
					END
					CLOSE c_indexes;
					DEALLOCATE c_indexes;
				    
					-- Eliminar la columna
					ALTER TABLE ${nombreTabla} DROP COLUMN ${nombreColumna};
					
					--Renombrar nueva columna
					EXEC sp_rename  '${nombreTabla}.${nombreColumna}_TMP' , '${nombreColumna}', 'COLUMN';
				
					-- Establezco la llave primaria
					IF @keys IS NOT NULL
				    BEGIN				    	
						SET @sql_query = N'ALTER TABLE ${nombreTabla} ADD CONSTRAINT ${nombreTabla}_PK PRIMARY KEY (' + @keys  + N')';
						EXEC sp_executesql @sql_query;
				    END	

					--Crear indices
					BEGIN TRY					
						IF @create_index_queries <> ''
						BEGIN
							EXEC sp_executesql @create_index_queries;
						END
					END TRY
					BEGIN CATCH
						IF ERROR_NUMBER() <> 1913
						BEGIN
					    	PRINT ERROR_MESSAGE();
					    END
					END CATCH					
				END	
                -- HM end sentence"""
				this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
			}
			else if(isOracle()) {
				sentencia = """
				-- HM begin sentence
				DECLARE
					column_exists NUMERIC(1);
				BEGIN
					SELECT CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END
						INTO column_exists
				        FROM USER_TAB_COLUMNS
				        WHERE TABLE_NAME = '${nombreTabla}'
				        AND COLUMN_NAME = '${nombreColumna}_TMP';     
					
				   IF column_exists = 1 THEN
				        EXECUTE IMMEDIATE 'UPDATE ${nombreTabla} SET ${nombreColumna}_TMP = ''0''';
				    ELSE
				    	EXECUTE IMMEDIATE 'ALTER TABLE ${nombreTabla} ADD ${nombreColumna}_TMP VARCHAR2(5) DEFAULT ''0'' NOT NULL';
				    END IF;	
				END;
                -- HM end sentence"""			
				this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
				
				sentencia = """
				-- HM begin sentence
				DECLARE
					constraint_name user_cons_columns.constraint_name%TYPE;
					rowsAffected INT; -- Contador de filas procesadas
					keys VARCHAR2(4000);
					sql_query VARCHAR2(4000);
                    current_index_name VARCHAR2(128);
					indexes_columns_names VARCHAR2(4000);
					-- Declarar una variable de tipo tabla anidada
					create_index_queries_count INT := 0;
					TYPE VARCHAR_ARRAY IS TABLE OF VARCHAR2(500);     				
    				create_index_queries VARCHAR_ARRAY := VARCHAR_ARRAY();
				
					CURSOR c_constraints IS
				    	SELECT a.constraint_name 
						    FROM user_cons_columns a
						    INNER JOIN user_constraints b
						    ON a.constraint_name = b.constraint_name
						    AND a.table_name = b.table_name
						    WHERE b.table_name = '${nombreTabla}'
						    AND a.column_name = '${nombreColumna}'
						    AND constraint_type <> 'P'
					    UNION
							SELECT constraint_name
							FROM user_constraints
							WHERE table_name = '${nombreTabla}'
							AND constraint_type = 'P';

					CURSOR c_indexes IS
				        SELECT  ui.index_name
						FROM user_indexes ui
						INNER JOIN user_ind_columns uic
							ON ui.table_name = uic.table_name 
							AND ui.index_name = uic.index_name
					    WHERE ui.table_name = '${nombreTabla}'
					    AND uic.column_name = '${nombreColumna}';

				
				BEGIN	
					-- Copiar los datos a la nueva columna usando lotes
					rowsAffected := 1;
					WHILE rowsAffected > 0 LOOP
						UPDATE ${nombreTabla}
					    SET ${nombreColumna}_TMP = CAST(${nombreColumna} AS VARCHAR2(5))
					    WHERE ROWNUM <= ${batchSize}
					    AND ${nombreColumna}_TMP = '0'
					    AND ${nombreColumna} IS NOT NULL
					    AND CAST(${nombreColumna} AS VARCHAR2(5)) <> '0';
					    rowsAffected := SQL%ROWCOUNT;
						COMMIT;
					END LOOP;	
					
					--Obtener primary keys
				    SELECT LISTAGG(a.COLUMN_NAME, ', ') WITHIN GROUP (ORDER BY a.COLUMN_NAME) AS COLUMN_NAMES 
				    INTO keys    
				    FROM user_cons_columns a
				    INNER JOIN user_constraints b
				    ON a.constraint_name = b.constraint_name
				    AND a.table_name = b.table_name
				    WHERE b.table_name = '${nombreTabla}'
				    AND b.constraint_type = 'P';
				
					-- Eliminar restricciones DEFAULT y primary key para la columna
					OPEN c_constraints;
				   	LOOP
				    	FETCH c_constraints INTO constraint_name;
				      	EXIT WHEN c_constraints%NOTFOUND;
				      	sql_query := 'ALTER TABLE ${nombreTabla} DROP CONSTRAINT ' || constraint_name;
				      	EXECUTE IMMEDIATE sql_query;
				   	END LOOP;
				   	CLOSE c_constraints;

                    -- Indices de la tabla para la columna
				    OPEN c_indexes;
				    LOOP
				        FETCH c_indexes INTO current_index_name;
				        EXIT WHEN c_indexes%NOTFOUND;
				
				        -- Obtener indices
				        SELECT LISTAGG(column_name, ', ') WITHIN GROUP (ORDER BY column_position)
				        INTO indexes_columns_names
				        FROM user_ind_columns
				        WHERE index_name = current_index_name;
				
				        -- Elimino indice
				        sql_query := 'DROP INDEX ' || current_index_name;
				        EXECUTE IMMEDIATE sql_query;
				
				        -- Guardo indices para crear luego
						create_index_queries_count := create_index_queries_count + 1;
				        create_index_queries.EXTEND; -- Aumenta el tama�o de la colecci�n
    					create_index_queries(create_index_queries_count) := 'CREATE INDEX ' || current_index_name || ' ON CON_SALDOS_DIARIOS (' || indexes_columns_names || ')';
				    END LOOP;
				
				    CLOSE c_indexes;
				
				   	-- Eliminar la columna
					sql_query := 'ALTER TABLE ${nombreTabla} DROP COLUMN ${nombreColumna}';
					EXECUTE IMMEDIATE sql_query;
					
				
				
					--Renombrar nueva columna
					sql_query := 'ALTER TABLE ${nombreTabla} RENAME COLUMN ${nombreColumna}_TMP TO ${nombreColumna}';
					EXECUTE IMMEDIATE sql_query;
				
					-- Establezco la llave primaria
					IF keys IS NOT NULL THEN						
						sql_query := 'ALTER TABLE ${nombreTabla} ADD CONSTRAINT ${nombreTabla}_PK PRIMARY KEY (' || keys  || ')';
				    	EXECUTE IMMEDIATE sql_query;
					END IF;	
					
					--Crear indices
					FOR i IN 1 .. create_index_queries.COUNT LOOP
						BEGIN
						    EXECUTE IMMEDIATE create_index_queries(i);
						EXCEPTION
						    WHEN OTHERS THEN
						        IF SQLCODE = -955 THEN
						            DBMS_OUTPUT.PUT_LINE(SQLERRM);
						        END IF;
						END;
    				END LOOP;
			  
				END;
                -- HM end sentence"""
				this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
			}
		}
		else {
			System.out.println("* No se ejecutan tareas. La columna ${nombreColumna} de la tabla ${nombreTabla} ya tiene el tipo de dato varchar(5). *" );
		}
	}
	
	private boolean isMultiempresa(){
		Properties chargeProperty = loadPropetyFile("jts.properties");
		String multiempresa = chargeProperty.getProperty("topaz.jts.use.multiempresa");
		return "true".equalsIgnoreCase(multiempresa);
		return true;
	}
	
	
	private Properties loadPropetyFile(String fileName){
						
		String filePath = CarpetasServidor.TOPAZ_PROPS.getExpandedPath() + File.separator + fileName;
		InputStream fileInputStream = null;
		try{
			fileInputStream = new FileInputStream(filePath);
			Properties prop = new Properties();
			prop.load(fileInputStream);
			return prop;

		} catch (Exception ex) {
			throw new RuntimeException("Can't read file:"+ filePath +" Error: "+ ex.getMessage(), ex);
		} finally{
			if(fileInputStream != null){
				try{
					fileInputStream.close();
				}catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
	
}

