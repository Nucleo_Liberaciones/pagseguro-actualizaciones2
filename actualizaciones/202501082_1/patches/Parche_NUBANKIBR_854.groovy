import topsystems.TopazException
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Parche_NUBANKIBR_854 extends Parche{

	@Override
	protected void comandos() throws TopazException {
		updateColumnVS_NRO_AUTORIZACION()
	}
	
	private void updateColumnVS_NRO_AUTORIZACION(){
		if(isOracle()){
			def sentencia = """
	-- HM begin sentence
		DECLARE
    v_data_precision NUMBER;
    v_data_scale NUMBER;
BEGIN
    SELECT DATA_PRECISION, DATA_SCALE INTO v_data_precision, v_data_scale
    FROM USER_TAB_COLUMNS
    WHERE TABLE_NAME = 'VTA_HIST_CHEQUE_ESPECIAL'
    AND COLUMN_NAME = 'VS_NRO_AUTORIZACION';

    -- Verificar si la precisi�n es menor que 7
    IF v_data_precision < 7 THEN
        EXECUTE IMMEDIATE 'ALTER TABLE VTA_HIST_CHEQUE_ESPECIAL MODIFY VS_NRO_AUTORIZACION NUMBER(8, 0)';
        DBMS_OUTPUT.PUT_LINE('Columna modificada a NUMBER(8, 0)');
    ELSE
        DBMS_OUTPUT.PUT_LINE('La columna ya tiene precisi�n suficiente: ' || v_data_precision);
    END IF;
END;	
	-- HM end sentence """
	def descripcion = "Actualizar campo VS_NRO_AUTORIZACION a largo 8 en Oracle" // La descripci�n es opcional
	this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	
		}else if(isSqlServer()){
		def sentencia = """
	-- HM begin sentence
		DECLARE @precision INT;
DECLARE @scale INT;

-- Obtener la precisi�n y escala de la columna
SELECT 
    @precision = NUMERIC_PRECISION, 
    @scale = NUMERIC_SCALE
FROM 
    INFORMATION_SCHEMA.COLUMNS
WHERE 
    TABLE_NAME = 'VTA_HIST_CHEQUE_ESPECIAL'
    AND COLUMN_NAME = 'VS_NRO_AUTORIZACION';

-- Verificar si la precisi�n es menor que 7
IF @precision < 7
BEGIN
    -- Modificar la columna a NUMBER(8,0) equivalente en SQL Server, que es NUMERIC(8,0)
    EXEC('ALTER TABLE VTA_HIST_CHEQUE_ESPECIAL ALTER COLUMN VS_NRO_AUTORIZACION NUMERIC(8, 0)');
    PRINT 'Columna modificada a DECIMAL(8, 0)';
END
ELSE
BEGIN
    PRINT 'La columna ya tiene precisi�n suficiente: ' + CAST(@precision AS VARCHAR(10));
END;
	-- HM end sentence """
	def descripcion = "Sobre la sentencia" // La descripci�n es opcional
	this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
	
		}
	}
}
