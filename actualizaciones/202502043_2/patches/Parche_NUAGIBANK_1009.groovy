package agibank

import topsystems.TopazException
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.parche.Parche

class Parche_NUAGIBANK_1009 extends Parche {

	@Override
	protected void comandos() throws TopazException {
		addProperty()		
	}
	
	private void addProperty(){
		// Agregar una propiedad o cambiar el valor a una existente
		def archivo = "loans.properties" // Si el archivo no existe, se crea.
		def propiedad = "topaz.ifrs.devenga.TEJ.como.producto"
		def valor = "N"
		def comentario = "Propiedad que indica si toma el comportamiento del producto para calcular el devengado a TEJ. Opciones validas (S/N). Default (N)"
		
		this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, valor, comentario))
	}

}
