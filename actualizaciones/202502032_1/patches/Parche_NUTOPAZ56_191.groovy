import topsystems.TopazException
import topsystems.actualizador.command.dataserver.ModificarConsultaQuery
import topsystems.actualizador.parche.Parche

class Parche_NUTOPAZ56_191 extends Parche{

	@Override
	protected void comandos() throws TopazException {
		updateQueryRegeneracionSaldosDyM()
		updateQueryRegeneracionSaldosDyMConFiltro()
	}
	
	private void updateQueryRegeneracionSaldosDyM(){
		// Sustituir consulta SQL de query de DataMapping
def archivoMapp = "HistoricoMapping.xml"
def query = "query.QueryRegeneracionSaldosDyM"
def consultaSQL = """
SELECT SALDOS_JTS_OID,
													       FECHA,
													       SALDO_AL_CORTE,
													       SALDO_AL_CORTE_MN,
													       SIGNO,
													       FECHA_VALOR,
													       MONTO,
													       AJUSTE,
													       MONTOMN,
													       RUBROCONTABLE,
													       EQRESULTADOME,
													       ESTADOFECHAVALOR,
														   SUCURSAL,
                                                           MONEDA
													  FROM (SELECT 1 AS TIPO, SALDOS_JTS_OID AS SALDOS_JTS_OID,
														       FECHA,
														       SALDO_AL_CORTE,
														       SALDO_AL_CORTE_MN,
														       'I' AS SIGNO,
														       NULL AS FECHA_VALOR,
														       0 AS MONTO,
														       ' ' AS AJUSTE,
														       0 AS MONTOMN,
														       RUBROCONTABLE,
														       NULL AS HORAFIN,
														       NULL AS TRREAL,
														       0 AS EQRESULTADOME,
															   0 AS ESTADOFECHAVALOR,
															   SUCURSAL,
                                                               MONEDA
														  FROM GRL_SALDOS_DIARIOS
														 WHERE FECHA = ?
														UNION ALL
														SELECT 2 AS TIPO, S.JTS_OID AS SALDOS_JTS_OID,
														       M.FECHAPROCESO AS FECHA,
														       0 AS SALDO_AL_CORTE,
														       0 AS SALDO_AL_CORTE_MN,
														       M.DEBITOCREDITO AS SIGNO,
														       M.FECHAVALOR AS FECHA_VALOR,
														       ROUND(M.CAPITALREALIZADO, 2) AS MONTO,
														       -- C7844 Campo con concepto 820
														       M.MARCAAJUSTE AS AJUSTE,
														       ROUND(M.EQUIVALENTEMN, 2) AS MONTOMN,
														       M.RUBROCONTABLE AS RUBROCONTABLE,
														       HORAFIN,
														       M.TRREAL,
														       ROUND(M.EQUIVALENTEME, 2) AS EQRESULTADOME,
															   M.ESTADOFECHAVALOR,
															   M.SUCURSAL_CUENTA,
                                                               S.MONEDA
														  FROM SALDOS S, MOVIMIENTOS_CONTABLES M, ASIENTOS A
														 WHERE M.FECHAPROCESO = A.FECHAPROCESO
														   AND M.FECHAVALOR &lt;= ?
														   AND M.SUCURSAL = A.SUCURSAL
														   AND M.ASIENTO = A.ASIENTO
														   AND A.ESTADO = 77
														    AND M.DEBITOCREDITO IN ('C', 'D')
														   AND M.FECHAPROCESO &gt;= ?
														   AND M.SALDO_JTS_OID = S.JTS_OID
														UNION ALL
														   SELECT 3 AS TIPO, JTS_OID AS SALDOS_JTS_OID,
															  NULL AS FECHA,
															  C1604 AS SALDO_AL_CORTE,
															  -- CAMPO CON CONCEPTO 633 SALDOMN
															  C3958 AS SALDO_AL_CORTE_MN,
															  'F' AS SIGNO,
															  NULL AS FECHA_VALOR,
															  0 AS MONTO,
															  ' ' AS AJUSTE,
															  0 AS MONTOMN,
															  C1730 AS RUBROCONTABLE,
															  NULL AS HORAFIN,
															  NULL AS TRREAL,
															  0 AS EQRESULTADOME,
															  0 AS ESTADOFECHAVALOR,
															  SUCURSAL,
                                                              MONEDA
														     FROM SALDOS
														     WHERE TZ_LOCK = 0 AND ? = ?
														  UNION ALL
														     SELECT 3 AS TIPO, SALDOS_JTS_OID AS SALDOS_JTS_OID,
															       FECHA,
															       SALDO_CON_FECHA_VALOR,
															       SALDO_FECHA_VALOR_MN,
															       'F' AS SIGNO,
															       NULL AS FECHA_VALOR,
															       0 AS MONTO,
															       ' ' AS AJUSTE,
															       0 AS MONTOMN,
															       RUBROCONTABLE,
															       NULL AS HORAFIN,
															       NULL AS TRREAL,
															       0 AS EQRESULTADOME,
															       0 AS ESTADOFECHAVALOR,
																   SUCURSAL,
                                                                   MONEDA
															  FROM GRL_SALDOS_DIARIOS
															WHERE FECHA = ?  AND ? &lt;&gt; ?) T
													 WHERE ((SALDOS_JTS_OID &gt;= ? AND SALDOS_JTS_OID &lt;= ?) OR - 1 = ?) 
													 ORDER BY SALDOS_JTS_OID,TIPO, FECHA, HORAFIN, TRREAL
"""
this.addCommand( new ModificarConsultaQuery(archivoMapp, query, consultaSQL))

	}
	
	private void updateQueryRegeneracionSaldosDyMConFiltro(){
		// Sustituir consulta SQL de query de DataMapping
def archivoMapp = "HistoricoMapping.xml"
def query = "query.QueryRegeneracionSaldosDyMConFiltro"
def consultaSQL = """
SELECT SALDOS_JTS_OID,
										       FECHA,
										       SALDO_AL_CORTE,
										       SALDO_AL_CORTE_MN,
										       SIGNO,
										       FECHA_VALOR,
										       MONTO,
										       AJUSTE,
										       MONTOMN,
										       RUBROCONTABLE,
										       EQRESULTADOME,
										       ESTADOFECHAVALOR,
											   SUCURSAL,
                                               MONEDA
										  FROM (SELECT 1 AS TIPO, SALDOS_JTS_OID AS SALDOS_JTS_OID,
											       FECHA,
											       SALDO_AL_CORTE,
											       SALDO_AL_CORTE_MN,
											       'I' AS SIGNO,
											       NULL AS FECHA_VALOR,
											       0 AS MONTO,
											       ' ' AS AJUSTE,
											       0 AS MONTOMN,
											       RUBROCONTABLE,
											       NULL AS HORAFIN,
											       NULL AS TRREAL,
											       0 AS EQRESULTADOME,
												   0 AS ESTADOFECHAVALOR,
												   SUCURSAL, 
                                                   MONEDA
											  FROM GRL_SALDOS_DIARIOS S INNER JOIN JTSSALDOS_DIARIOS J ON 
											      S.SALDOS_JTS_OID = J.JTS_OID
											 WHERE FECHA = ?
											UNION ALL
											SELECT 2 AS TIPO, S.JTS_OID AS SALDOS_JTS_OID,
											       M.FECHAPROCESO AS FECHA,
											       0 AS SALDO_AL_CORTE,
											       0 AS SALDO_AL_CORTE_MN,
											       M.DEBITOCREDITO AS SIGNO,
											       M.FECHAVALOR AS FECHA_VALOR,
											       ROUND(M.CAPITALREALIZADO, 2) AS MONTO,
											       -- C7844 Campo con concepto 820
											       M.MARCAAJUSTE AS AJUSTE,
											       ROUND(M.EQUIVALENTEMN, 2) AS MONTOMN,
											       M.RUBROCONTABLE AS RUBROCONTABLE,
											       HORAFIN,
											       M.TRREAL,
											       ROUND(M.EQUIVALENTEME, 2) AS EQRESULTADOME,
												   M.ESTADOFECHAVALOR,
												   M.SUCURSAL_CUENTA,
                                                   S.MONEDA 
											  FROM SALDOS S, MOVIMIENTOS_CONTABLES M, ASIENTOS A, JTSSALDOS_DIARIOS J  
											 WHERE M.FECHAPROCESO = A.FECHAPROCESO
											   AND M.FECHAVALOR &lt;= ?
											   AND M.SUCURSAL = A.SUCURSAL
											   AND M.ASIENTO = A.ASIENTO
											   AND A.ESTADO = 77
											    AND M.DEBITOCREDITO IN ('C', 'D')
											   AND M.FECHAPROCESO &gt;= ?
											   AND M.SALDO_JTS_OID = S.JTS_OID
											   AND S.JTS_OID = J.JTS_OID
											UNION ALL
											   SELECT 3 AS TIPO, S.JTS_OID AS SALDOS_JTS_OID,
												  NULL AS FECHA,
												  C1604 AS SALDO_AL_CORTE,
												  -- CAMPO CON CONCEPTO 633 SALDOMN
												  C3958 AS SALDO_AL_CORTE_MN,
												  'F' AS SIGNO,
												  NULL AS FECHA_VALOR,
												  0 AS MONTO,
												  ' ' AS AJUSTE,
												  0 AS MONTOMN,
												  C1730 AS RUBROCONTABLE,
												  NULL AS HORAFIN,
												  NULL AS TRREAL,
												  0 AS EQRESULTADOME,
												  0 AS ESTADOFECHAVALOR,
												  SUCURSAL,
                                                  MONEDA
											     FROM SALDOS S INNER JOIN JTSSALDOS_DIARIOS J ON 
											      S.JTS_OID = J.JTS_OID
											     WHERE TZ_LOCK = 0 AND ? = ?
											  UNION ALL
											     SELECT 3 AS TIPO, SALDOS_JTS_OID AS SALDOS_JTS_OID,
												       FECHA,
												       SALDO_CON_FECHA_VALOR,
												       SALDO_FECHA_VALOR_MN,
												       'F' AS SIGNO,
												       NULL AS FECHA_VALOR,
												       0 AS MONTO,
												       ' ' AS AJUSTE,
												       0 AS MONTOMN,
												       RUBROCONTABLE,
												       NULL AS HORAFIN,
												       NULL AS TRREAL,
												       0 AS EQRESULTADOME,
													   0 AS ESTADOFECHAVALOR,
													   SUCURSAL,
                                                       S.MONEDA
												  FROM GRL_SALDOS_DIARIOS S INNER JOIN JTSSALDOS_DIARIOS J ON 
											      S.SALDOS_JTS_OID = J.JTS_OID
												WHERE FECHA = ?  AND ? &lt;&gt; ?) t
										 ORDER BY SALDOS_JTS_OID,TIPO, FECHA, HORAFIN, TRREAL
"""
this.addCommand( new ModificarConsultaQuery(archivoMapp, query, consultaSQL))

    }
}
