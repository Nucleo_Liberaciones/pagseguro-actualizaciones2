import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Parche_NUBANKIBR_634 extends Parche {

	@Override
	protected void comandos() throws TopazException {
		crearTabla()
		adicionarDatamapping()
		adicionarServicio()
	}
	
	private crearTabla() {
		if(isOracle()) {
			def sentencia = """
				CREATE TABLE DE_PARA_COSIF_IFRS9
				(
					TZ_LOCK NUMBER(15,0) NOT NULL ENABLE, 
					CODIGO_ATUAL NUMBER(12,0) NOT NULL ENABLE, 
					CODIGO_NOVO NUMBER(12,0) NOT NULL ENABLE, 
					CONSTRAINT PK_DE_PARA_COSIF_IFRS9 PRIMARY KEY (CODIGO_ATUAL)
				)	
			"""
			def descripcion = "Crea tabla"
			this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
		}
		else if(isSqlServer()) {
			def sentencia = """
	    		CREATE TABLE DE_PARA_COSIF_IFRS9 (
				    TZ_LOCK NUMERIC(15,0) NOT NULL, 
				    CODIGO_ATUAL NUMERIC(12,0) NOT NULL,
				    CODIGO_NOVO NUMERIC(12,0) NOT NULL, 
				    CONSTRAINT PK_DE_PARA_COSIF_IFRS9 PRIMARY KEY (CODIGO_ATUAL)
				)	
			"""
			def descripcion = "Crea tabla"
			this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
		}
	}
	
	private adicionarDatamapping(){
		def archivoMapp = "RastreabilidadeIFRS9Mapping.xml"
		def clase = """
		<class>
		    <class-name>topsystems.topaz.server.businesslogic.accountplan.DeParaCosifIFRS9</class-name>
		    <entity-name>core.vo_DeParaCosifIFRS9</entity-name>
		    <table-name>DE_PARA_COSIF_IFRS9</table-name>
		    <database-name>TOP/CLIENTES</database-name>
			<attribute>
		        <attribute-name>tz_lock</attribute-name>
		        <column-name>TZ_LOCK</column-name>
		    </attribute>
		    <attribute>
		        <attribute-name>codigoAtual</attribute-name>
		        <column-name>CODIGO_ATUAL</column-name>
		        <key>primary</key>
		    </attribute>	
		    <attribute>
		        <attribute-name>codigoNovo</attribute-name>
		        <column-name>CODIGO_NOVO</column-name>
			    </attribute>    
	    </class>
		"""
		this.addCommand( new AgregarModificarEntidad(archivoMapp, clase))
	}
	
	private adicionarServicio(){
		String fileName = "AccountPlanDeploy.xml"
		String servicios = """
			<service>
				<name>topsystems:service=AccountPlan</name>
				<code>topsystems.topaz.server.businesslogic.accountplan.AccountPlanIFRS9</code>
			</service>
		"""		
		this.addCommand( new AgregarModificarServicios(fileName, servicios))
	}

}
