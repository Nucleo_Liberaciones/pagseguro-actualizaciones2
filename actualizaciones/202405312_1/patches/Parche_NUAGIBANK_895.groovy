import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche

class Parche_NUAGIBANK_895 extends Parche{

	@Override
	protected void comandos() throws TopazException {
		
		agregarTablaHistorico();
		agregarTablaTipoPrepago();
		queryEntrada();
		queryEntradaAjustesPrepagos();
		agregarVoPrepagoAgenda();
		agregarVoPrepago();
		agregarVoHistorico();
		agregarQueryTipoPrepago();		
		agregarGroup();
		agregarGroupAjustes()
		agregarProcesess();
		agregarProcesessAjustes();
		agregarService();
		agregarServiceAjustes();
		agregarFuncionalidad();
		agregarFuncionalidadAjustes();
		agregarOperacion();
	}
	
	
	protected void definicionDeParametros() {
		this.addParameterNewOperationNumber("OPERACION_PREPAGO_AGENDA", "Numero de operación para el proceso: Prepago Agenda")
		this.addParameterPATitleOpe("TITULO_OPERACION", "TITULO OPERACION_PREPAGO_AGENDA")
		this.addParameterNewOperationNumber("OPERACION_AJUSTES_PREPAGO", "Numero de operación para el proceso: Ajustes Agenda")
		this.addParameterPATitleOpe("TITULO_OPERACION_AJUSTES", "TITULO OPERACION_AJUSTES_PREPAGO")
	}
	
	
	protected void agregarTablaHistorico(){
		if(isOracle()){
			def sentencia = """
			CREATE TABLE SCL_HISTORICO_MOV_PREPAGOS(
			IDPREPAGO NUMBER(10),
			LOTE NUMBER(15),
			ORDINAL NUMBER(10),
			ESTADO_INICIAL VARCHAR2(2),
			ESTADO_FINAL VARCHAR2(2),
			FECHA_PROCESO DATE,
			ASIENTO NUMBER(10),
			SUCURSAL NUMBER(10),
			ID_EJECUCION NUMBER(10),
			CONSTRAINT PK_SCL_HISTORICO PRIMARY KEY (IDPREPAGO,LOTE,ORDINAL,ESTADO_FINAL)
			);
        """
			def descripcion = "Se inserta script del historico en oracle" // La descripción es opcional
			this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))	
		}else if(isSqlServer()){
			def sentencia = """
			CREATE TABLE SCL_HISTORICO_MOV_PREPAGOS(
			IDPREPAGO NUMERIC(10),
			LOTE NUMERIC(15),
			ORDINAL NUMERIC(10),
			ESTADO_INICIAL VARCHAR(2),
			ESTADO_FINAL VARCHAR(2),
			FECHA_PROCESO DATETIME,
			ASIENTO NUMERIC(10),
			SUCURSAL NUMERIC(10),
			ID_EJECUCION NUMERIC(10),
			CONSTRAINT PK_SCL_HISTORICO PRIMARY KEY (IDPREPAGO,LOTE,ORDINAL,ESTADO_FINAL)
			);
        """
			def descripcion = "Se inserta script del historico en sql" // La descripción es opcional
			this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
		}
	}
	
	protected void agregarTablaTipoPrepago(){
		if(isOracle()){
			def sentencia = """
			CREATE TABLE SCL_TIPO_PREPAGOS(
			TIPO_PREPAGO VARCHAR2(1) PRIMARY KEY,
			SALDO_TRANSITORIA_PREPAGO NUMBER(15),
			SALDO_TRANSITORIA_PREVISION NUMBER(15),
			SALDO_TRANSITORIA_PERDIDAS NUMBER(15)
			);
        """
			def descripcion = "Se inserta script del historico en oracle" // La descripción es opcional
			this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
		}else if(isSqlServer()){
			def sentencia = """
			CREATE TABLE SCL_TIPO_PREPAGOS(
			TIPO_PREPAGO VARCHAR(1) PRIMARY KEY,
			SALDO_TRANSITORIA_PREPAGO NUMERIC(15),
			SALDO_TRANSITORIA_PREVISION NUMERIC(15),
			SALDO_TRANSITORIA_PERDIDAS NUMERIC(15)
			);
        """
			def descripcion = "Se inserta script del historico en sql" // La descripción es opcional
			this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))	
		}
	}
	
	
	
	protected void agregarFuncionalidad(){
		
		def sentencia = """
            INSERT INTO PARAMETROS_JTS(FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
            VALUES('ACTUALIZA_PREPAGO_AGENDA','DESCRIPCION','Actualiza Agenda Prepago',0);
        
            INSERT INTO PARAMETROS_JTS(FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)        
            VALUES('ACTUALIZA_PREPAGO_AGENDA','OPERACION','${this.P("OPERACION_PREPAGO_AGENDA")}', 0);

            INSERT INTO PARAMETROS_JTS(FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)        
            VALUES('ACTUALIZA_PREPAGO_AGENDA','CANAL','0', 0);
        """
		def descripcion = "Se inserta nuevo proceso PREPAGO_AGENDA" // La descripción es opcional
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	protected void agregarFuncionalidadAjustes(){		
		def sentencia = """
	INSERT INTO PARAMETROS_JTS(FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
	VALUES('AJUSTES_PREPAGOS','DESCRIPCION','Ajusta Prepagos',0);

	INSERT INTO PARAMETROS_JTS(FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
	VALUES('AJUSTES_PREPAGOS','OPERACION','${this.P("OPERACION_AJUSTES_PREPAGO")}', 0);

	INSERT INTO PARAMETROS_JTS(FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
	VALUES('AJUSTES_PREPAGOS','CANAL','0', 0);
"""
		def descripcion = "Se inserta nuevo parametro AJUSTES PREPAGOS" // La descripción es opcional
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	private void agregarOperacion(){
		def sentencia = """
        INSERT INTO OPERACIONES (TITULO, IDENTIFICACION, NOMBRE, DESCRIPCION, MNEMOTECNICO, AUTORIZACION, FORMULARIOPRINCIPAL, PROXOPERACION, ESTADO, TZ_LOCK, COPIAS, SUBOPERACION, PERMITEBAJA, COMPORTAMIENTOENCIERRE, REQUIERECONTRASENA, PERMITECONCURRENTE, PERMITEESTADODIFERIDO)
        VALUES (${this.P("TITULO_OPERACION")}, ${this.P("OPERACION_PREPAGO_AGENDA")}, 'Operacion Prepago Agenda', 'Operacion Prepago Agenda', ${this.P("OPERACION_PREPAGO_AGENDA")}, 'N', NULL, NULL, 'P', 0, NULL, 0, 'S', 'N', 'N', 'S', 'S');
		
		INSERT INTO OPERACIONES (TITULO, IDENTIFICACION, NOMBRE, DESCRIPCION, MNEMOTECNICO, AUTORIZACION, FORMULARIOPRINCIPAL, PROXOPERACION, ESTADO, TZ_LOCK, COPIAS, SUBOPERACION, PERMITEBAJA, COMPORTAMIENTOENCIERRE, REQUIERECONTRASENA, PERMITECONCURRENTE, PERMITEESTADODIFERIDO)
        VALUES (${this.P("TITULO_OPERACION_AJUSTES")}, ${this.P("OPERACION_AJUSTES_PREPAGO")}, 'Operacion Ajustes Prepago', 'Operacion Ajustes Prepago', ${this.P("OPERACION_AJUSTES_PREPAGO")}, 'N', NULL, NULL, 'P', 0, NULL, 0, 'S', 'N', 'N', 'S', 'S');
		"""
		def descripcion = "Se inserta OPE para este proceso" // La descripción es opcional
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	protected void queryEntrada(){
		// Agregar o sustituir una query (o varias) de DataMapping
def archivoMapp = "PagosAutomaticosMapping.xml"
def queries = """
	<query>
        <class-name>topsystems.automaticprocess.automaticcredit.prepagosinss.QueryPrepagoAgenda</class-name> 
        <query-name>query.ActualizarPrepagoAgenda</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
					SELECT 
					    PA.IDPREPAGO,
						PA.LOTE,
						PA.ORDINAL
					FROM 
					    SCL_PREPAGOS_AGENDA PA
					WHERE
						PA.FECHA_APLICACION &lt;= ?										
					    AND PA.ESTADO IS NULL
		</sentence>
        <attribute>
            <attribute-name>prepagoId</attribute-name>
            <column-name>IDPREPAGO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>lote</attribute-name>
            <column-name>LOTE</column-name>
            <column-type>long</column-type>
        </attribute>
		<attribute>
            <attribute-name>ordinal</attribute-name>
            <column-name>ORDINAL</column-name>
            <column-type>long</column-type>
        </attribute>
    </query>
"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))

	}
	
	protected void queryEntradaAjustesPrepagos(){
		// Agregar o sustituir una query (o varias) de DataMapping
def archivoMapp = "PagosAutomaticosMapping.xml"
def queries = """
	<query>
        <class-name>topsystems.automaticprocess.automaticcredit.prepagosinss.QueryAjustesPrepagos</class-name> 
        <query-name>query.AjustesPrepagos</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
					SELECT SP.IDPREPAGO,SP.LOTE,SP.ORDINAL, 
					CASE  
						WHEN SP.TIPO_PREPAGO=0 THEN SD.MONTO_PREPAGO_DEBITADO 
						ELSE SP.MONTO_PREPAGO_DEBITADO  
						END AS MONTO_PREPAGO_DEBITADO
					FROM SCL_PREPAGOS SP 
					LEFT JOIN SCL_DETALLE_CREDITOS SD 
					ON  SP.LOTE = SD.LOTE AND SP.ORDINAL = SD.ORDINAL
					WHERE 
					(SP.ESTADO IN ('A', 'P') 
					AND ( (SP.TIPO_PREPAGO &lt;&gt; 0 AND SP.MONTO_PREPAGO_DEBITADO &gt; 0) OR (SP.TIPO_PREPAGO=0 AND SD.MONTO_PREPAGO_DEBITADO &gt; 0) ) ) 
					OR (SP.ESTADO = 'A' AND SP.FECHA_LIQUIDACION &lt;= ? ) 
					OR (SP.ESTADO = 'P' AND SP.FECHA_REEMBOLSO &lt;= ? )
		</sentence>
        <attribute>
            <attribute-name>idPrepago</attribute-name>
            <column-name>IDPREPAGO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>lote</attribute-name>
            <column-name>LOTE</column-name>
            <column-type>long</column-type>
        </attribute>
		<attribute>
            <attribute-name>ordinal</attribute-name>
            <column-name>ORDINAL</column-name>
            <column-type>long</column-type>
        </attribute>
		<attribute>
            <attribute-name>montoPrepagoDebitado</attribute-name>
            <column-name>MONTO_PREPAGO_DEBITADO</column-name>
            <column-type>double</column-type>
        </attribute>
    </query>
"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))

	}
	
	//AGREGO LOS VO
	
	protected void agregarVoPrepagoAgenda(){
		// Agregar o sustituir una entidad de DataMapping
def archivoMapp = "PagosAutomaticosMapping.xml"
def entidades = """
<class>
        <class-name>topsystems.automaticprocess.automaticcredit.valueobjects.PrepagoAgenda</class-name>
        <entity-name>core.vo_PrepagoAgenda</entity-name>
        <table-name>SCL_PREPAGOS_AGENDA</table-name>
        <database-name>TOP/CLIENTES</database-name>
        <attribute>
            <attribute-name>prepagoId</attribute-name>
            <column-name>IDPREPAGO</column-name>
            <key>primary</key>
        </attribute>
		<attribute>
            <attribute-name>lote</attribute-name>
            <column-name>LOTE</column-name>
            <key>primary</key>
        </attribute>
        <attribute>
            <attribute-name>ordinal</attribute-name>
            <column-name>ORDINAL</column-name>
            <key>primary</key>
        </attribute>
        <attribute>
            <attribute-name>estado</attribute-name>
            <column-name>ESTADO</column-name>
        </attribute>
        <attribute>
            <attribute-name>tipoPrepago</attribute-name>
            <column-name>TIPO_PREPAGO</column-name>
        </attribute>
		<attribute>
            <attribute-name>importe</attribute-name>
            <column-name>IMPORTE</column-name>
        </attribute>
		<attribute>
            <attribute-name>fechaAplicacion</attribute-name>
            <column-name>FECHA_APLICACION</column-name>
        </attribute>
		<attribute>
            <attribute-name>fechaProcesado</attribute-name>
            <column-name>FECHA_PROCESADO</column-name>
        </attribute>
		<attribute>
            <attribute-name>fechaLiquidacion</attribute-name>
            <column-name>FECHA_LIQUIDACION</column-name>
        </attribute>
		<attribute>
            <attribute-name>fechaReembolso</attribute-name>
            <column-name>FECHA_REEMBOLSO</column-name>
        </attribute>
		<attribute>
            <attribute-name>saldoAcreditar</attribute-name>
            <column-name>SALDO_ACREDITAR</column-name>
        </attribute>
</class>
"""
this.addCommand( new AgregarModificarEntidad(archivoMapp, entidades))

	}
	
	protected void agregarVoPrepago(){
		// Agregar o sustituir una entidad de DataMapping
def archivoMapp = "PagosAutomaticosMapping.xml"
def entidades = """
<class>
        <class-name>topsystems.automaticprocess.automaticcredit.valueobjects.Prepago</class-name>
        <entity-name>core.vo_Prepago</entity-name>
        <table-name>SCL_PREPAGOS</table-name>
        <database-name>TOP/CLIENTES</database-name>
        <attribute>
            <attribute-name>idPrepago</attribute-name>
            <column-name>IDPREPAGO</column-name>
            <key>primary</key>
        </attribute>
        <attribute>
            <attribute-name>tipoPrepago</attribute-name>
            <column-name>TIPO_PREPAGO</column-name>
        </attribute>
		<attribute>
            <attribute-name>lote</attribute-name>
            <column-name>LOTE</column-name>
        </attribute>
        <attribute>
            <attribute-name>ordinal</attribute-name>
            <column-name>ORDINAL</column-name>
        </attribute>
		<attribute>
            <attribute-name>fechaLiquidacion</attribute-name>
            <column-name>FECHA_LIQUIDACION</column-name>
        </attribute>
		<attribute>
            <attribute-name>fechaReembolso</attribute-name>
            <column-name>FECHA_REEMBOLSO</column-name>
        </attribute>
        <attribute>
            <attribute-name>estado</attribute-name>
            <column-name>ESTADO</column-name>
    	</attribute>
		<attribute>
            <attribute-name>montoPrepagoDebitado</attribute-name>
            <column-name>MONTO_PREPAGO_DEBITADO</column-name>
    	</attribute>
		<attribute>
            <attribute-name>montoPrepago</attribute-name>
            <column-name>MONTO_PREPAGO</column-name>
    	</attribute>
</class>
"""
this.addCommand( new AgregarModificarEntidad(archivoMapp, entidades))

	}
	
	protected void agregarVoHistorico(){
		// Agregar o sustituir una entidad de DataMapping
def archivoMapp = "PagosAutomaticosMapping.xml"
def entidades = """
<class>
        <class-name>topsystems.automaticprocess.automaticcredit.valueobjects.HistoricoMovimientosPrepagos</class-name>
        <entity-name>core.vo_HistoricoMovimientosPrepago</entity-name>
        <table-name>SCL_HISTORICO_MOV_PREPAGOS</table-name>
        <database-name>TOP/CLIENTES</database-name>
        <attribute>
            <attribute-name>prepagoId</attribute-name>
            <column-name>IDPREPAGO</column-name>
            <key>primary</key>
        </attribute>
		<attribute>
            <attribute-name>lote</attribute-name>
            <column-name>LOTE</column-name>
			<key>primary</key>
        </attribute>
        <attribute>
            <attribute-name>ordinal</attribute-name>
            <column-name>ORDINAL</column-name>
			<key>primary</key>
        </attribute>
        <attribute>
            <attribute-name>estadoInicial</attribute-name>
            <column-name>ESTADO_INICIAL</column-name>
        </attribute>
		<attribute>
            <attribute-name>estadoFinal</attribute-name>
            <column-name>ESTADO_FINAL</column-name>
			<key>primary</key>
        </attribute>
		<attribute>
            <attribute-name>fechaProceso</attribute-name>
            <column-name>FECHA_PROCESO</column-name>			
        </attribute>
        <attribute>
            <attribute-name>asiento</attribute-name>
            <column-name>ASIENTO</column-name>
        </attribute>
		<attribute>
            <attribute-name>sucursal</attribute-name>
            <column-name>SUCURSAL</column-name>
        </attribute>
		<attribute>
            <attribute-name>idEjecucion</attribute-name>
            <column-name>ID_EJECUCION</column-name>
        </attribute>

</class>
"""
this.addCommand( new AgregarModificarEntidad(archivoMapp, entidades))

	}
	
	protected void agregarQueryTipoPrepago(){
		// Agregar o sustituir una query (o varias) de DataMapping
def archivoMapp = "PagosAutomaticosMapping.xml"
def queries = """
	<query>
        <class-name>topsystems.automaticprocess.automaticcredit.valueobjects.TipoPrepago</class-name>
        <query-name>query.TipoPrepago</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
					SELECT 
					    TIPO_PREPAGO,
						SALDO_TRANSITORIA_PREPAGO,
						SALDO_TRANSITORIA_PREVISION,
						SALDO_TRANSITORIA_PERDIDAS
					FROM 
					    SCL_TIPO_PREPAGOS
		</sentence>
		<attribute>
            <attribute-name>tipoPrepago</attribute-name>
            <column-name>TIPO_PREPAGO</column-name>
            <column-type>String</column-type>
        </attribute>
		<attribute>
            <attribute-name>saldoTransitoriaPrepago</attribute-name>
            <column-name>SALDO_TRANSITORIA_PREPAGO</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>saldoTransitoriaPrevision</attribute-name>
            <column-name>SALDO_TRANSITORIA_PREVISION</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>saldoTransitoriaPerdida</attribute-name>
            <column-name>SALDO_TRANSITORIA_PERDIDAS</column-name>
			<column-type>long</column-type>
        </attribute>
    </query>
"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))

	}
	
	
	// AGREGO EL GROUPS
	
	protected void agregarGroup(){
		// Agregar o sustituir un grupo
def archivo = "groups.py"
def grupo = """
gdef = GroupDefinition("VTA - Actualizar Agenda Prepago") 
gdef.registerProcess("Actualizar Agenda Prepago")
gdefs.addGroup(gdef)
"""
this.addCommand( new AgregarModificarGrupo(archivo, grupo))

	}
	
	protected void agregarGroupAjustes(){
		// Agregar o sustituir un grupo
def archivo = "groups.py"
def grupo = """
gdef = GroupDefinition("VTA - Ajustes Prepago") 
gdef.registerProcess("Ajustes Prepago")
gdefs.addGroup(gdef)
"""
this.addCommand( new AgregarModificarGrupo(archivo, grupo))

	}
	
	// AGREGO EL PROCESESS
	
	protected void agregarProcesess(){
		// Agregar o sustituir un proceso
def archivo = "processes.py"
def procesos = """
pdef = ProcessDefinition("Actualizar Agenda Prepago", "topsystems.automaticprocess.processmanager.WorkManager")
pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WorkDescriptorActualizaAgendaPrepago");
pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BusinessWorkerActualizaAgendaPrepago");
pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
pdef.addConstant("rangoCommit","500");
pdef.addConstant("cantidadHilos","10");
pdef.addConstant("applyCodigoTransaccion","true");
pdefs.addProcess(pdef)
"""
this.addCommand(new AgregarModificarProceso(archivo, procesos))

	}
	
	protected void agregarProcesessAjustes(){
		// Agregar o sustituir un proceso
def archivo = "processes.py"
def procesos = """
pdef = ProcessDefinition("Ajustes Prepago", "topsystems.automaticprocess.processmanager.WorkManager")
pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WDAjustesPrepagos");
pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BWAjustesPrepagos");
pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
pdef.addConstant("rangoCommit","500");
pdef.addConstant("cantidadHilos","10");
pdef.addConstant("applyCodigoTransaccion","true");
pdefs.addProcess(pdef)
"""
this.addCommand(new AgregarModificarProceso(archivo, procesos))

	}
	//AGREGO EL SERVICES
	
	protected void agregarService(){
		// Agregar o sustituir uno o varios servicios en un mismo archivo
def archivo = "pagosAutomaticos/PagosAutomaticos.xml"
def servicios = """
<service>
      <name>topsystems:processManager:WorkDescriptor=WorkDescriptorActualizaAgendaPrepago</name>
      <code>topsystems.automaticprocess.automaticcredit.prepagosinss.WorkDescriptorActualizaAgendaPrepago</code>
   </service>
   <service>
      <name>topsystems:processManager:BusinessWorker=BusinessWorkerActualizaAgendaPrepago</name>
      <code>topsystems.automaticprocess.automaticcredit.prepagosinss.BusinessWorkerActualizaAgendaPrepago</code>
   </service>
"""
this.addCommand( new AgregarModificarServicios(archivo, servicios))

	}
	
	protected void agregarServiceAjustes(){
		// Agregar o sustituir uno o varios servicios en un mismo archivo
def archivo = "pagosAutomaticos/PagosAutomaticos.xml"
def servicios = """
   <service>
      <name>topsystems:processManager:WorkDescriptor=WDAjustesPrepagos</name>
      <code>topsystems.automaticprocess.automaticcredit.prepagosinss.WDAjustesPrepagos</code>
   </service>
   <service>
      <name>topsystems:processManager:BusinessWorker=BWAjustesPrepagos</name>
      <code>topsystems.automaticprocess.automaticcredit.prepagosinss.BWAjustesPrepagos</code>
   </service>	
"""
this.addCommand( new AgregarModificarServicios(archivo, servicios))

	}
}


