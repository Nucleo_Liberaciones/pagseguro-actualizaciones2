import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle;
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer;
import topsystems.actualizador.parche.Parche;

class Parche_NUSICREDBR_3481 extends Parche{
	
	void definicionDeParametros(){
		this.addParameterNewFreeField("FECHA_CALCULADO_SUSPENSO", "N�mero de Campo en el Diccionario para la Fecha de C�lculo Suspenso")
	}
	
	@Override
	protected void comandos() {
		
		//--------------------------------------------------------------------------------------------------------------------------------//
		// Agregar una propiedad o cambiar el valor a una existente
		def archivo = "jts.properties"
		def propiedad = "topaz.campo.saldos.fecha.calculado.suspenso"
		def valor = this.P("FECHA_CALCULADO_SUSPENSO").toString()
		def comentario = """Nro.Campo de Saldos para la Fecha Calculado Suspenso"""
		this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, valor, comentario))
		//--------------------------------------------------------------------------------------------------------------------------------//	
	
		//--------------------------------------------------------------------------------------------------------------------------------//
		// Agregar constantes y parametros a un proceso
		archivo = "processes.py"
		def nombreProceso = "Devengamiento Calculo y Actualizacion"
		def constParamProc = """
		   pdef.addConstant("Pasa a Suspenso en Feriado","True");
		"""
		this.addCommand( new AgregarModificarPropiedadesProceso(archivo, nombreProceso, constParamProc))
		//--------------------------------------------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------------------------------------------//
		def archivoMapp = "DevengamientoMapping.xml"
		def entidad = "core.vo_SaldoDevengar"
		def atributo = """
			<attribute>
            	<attribute-name>FECHA_CALCULADO_SUSPENSO</attribute-name>
            	<column-name>FECHA_CALCULADO_SUSPENSO</column-name>
        	</attribute>
		"""
		this.addCommand(new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
		//--------------------------------------------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------------------------------------------//
		// SQL Server Agrega Atributo en la tabla Saldos
		def sentencia = """
			ALTER TABLE SALDOS ADD FECHA_CALCULADO_SUSPENSO DATETIME NULL;

			INSERT INTO diccionario (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
				VALUES ("""+this.P("FECHA_CALCULADO_SUSPENSO")+""", ' ', null, 'Fecha Calculado Suspenso', 'F.Calc.Susp.', 8, 'F', 0, 'F', 0, 0, null, null, null, null, 0, 21, 'FECHA_CALCULADO_SUSPENSO', 0, ' ');

		"""
		def descripcion = "Agrega Atributo en Saldos de la fecha de Calculado el Suspenso"
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
		
		//--------------------------------------------------------------------------------------------------------------------------------//
		
		// Oracle Agrega Atributo en la tabla Saldos
		sentencia = """
			ALTER SESSION SET DDL_LOCK_TIMEOUT=120;

			ALTER TABLE SALDOS ADD FECHA_CALCULADO_SUSPENSO DATE NULL;

			INSERT INTO diccionario (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
				VALUES ("""+this.P("FECHA_CALCULADO_SUSPENSO")+""", ' ', null, 'Fecha Calculado Suspenso', 'F.Calc.Susp', 8, 'F', 0, 'F', 0, 0, null, null, null, null, 0, 21, 'FECHA_CALCULADO_SUSPENSO', 0, ' ');

		"""
		descripcion = "Agrega Atributo en Saldos de la fecha de Calculado el Suspenso"
		this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	
	}

}