package Base

import topsystems.actualizador.command.ActualizacionCommand
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.parche.Parche

class Parche_NUAGIBANK_952 extends Parche {

	void sobreElParche() {
		this.notas_del_autor = "Parche_NUAGIBANK_952"
		this.opcional = false
	}
	
	void definicionDeParametros(){
		this.addParameterQuery("nombreTabla", "Nombre de la tabla", "STRING", "SELECT d2.NOMBREFISICO FROM DICCIONARIO D JOIN DESCRIPTORES d2 ON d2.IDENTIFICACION = D.TABLA WHERE D.CONCEPTO  = 270")
	}
	
	@Override
	protected void comandos() {
		modificacionDatamapping()
	}

	private void modificacionDatamapping(){
		def archivoMapping = "ActualizacionAtrasoMapping.xml"
		def query = """   
		<query>
			<class-name>topsystems.automaticprocess.calificacion.diasatrasocuotas.CuotasAtrasadas</class-name>
			<query-name>query.CuotasVencidas</query-name>
			<database-name>TOP/CLIENTES</database-name>
			<sentence>
				SELECT SALDO_JTS_OID, C2300, C2302
				FROM ${this.P("nombreTabla")}
				WHERE saldo_jts_oid = ?
					AND TZ_LOCK = 0
					AND c2300 &gt; ?
					AND c2300 &lt;= ?
					AND c2302 &lt;= ?
			</sentence>
			<attribute>
				<attribute-name>saldoJtsOid</attribute-name>
				<column-name>SALDO_JTS_OID</column-name>
				<column-type>long</column-type>
			</attribute>
			<attribute>
				<attribute-name>numeroCuota</attribute-name>
				<column-name>C2300</column-name>
				<column-type>long</column-type>
			</attribute>
			<attribute>
				<attribute-name>fechaVencimientoCuota</attribute-name>
				<column-name>C2302</column-name>
				<column-type>Date</column-type>
			</attribute>
		</query>
		"""
		this.addCommand(new AgregarModificarQuery(archivoMapping, query))
	}
}
