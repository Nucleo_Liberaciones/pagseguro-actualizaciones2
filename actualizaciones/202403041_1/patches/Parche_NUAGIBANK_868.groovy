
import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche

public class Parche_NUAGIBANK_868 extends Parche {

	@Override
	protected void comandos() throws TopazException {
		// TODO Auto-generated method stub
		agregarQueryMultiHilos();
		agregarProcesoMultiHilos();
		agregarGrupoMultiHilos();
		agregarGrupoDebitosAutomaticos();
		agregarServicesDeployMultiHilos();
		agregarProcesoTransferencias();
		agregarServicesTransferencias();
		agregarQueryTransferencias();
		agregarGrupoTransferenciasCobranzasConvenios();
		addParametrosJts();

	}
	
	private void agregarQueryMultiHilos() {
		// Agregar o sustituir una query (o varias) de DataMapping
def archivoMapp = "CobranzaVistaMapping.xml"
def queries = """
<query>	                 
        <class-name>topsystems.automaticprocess.cobranzavista.objectsdomain.SolicitudDefault</class-name>
        <query-name>query.Solicitud_Recaudacion_MultiHilos</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
				SELECT K.TZ_LOCK,
		               FECHA_INICIO_COBRO,
		               REFERENCIA,
		               IMPORTE_A_COBRAR,
		               IMPORTE_MINIMO,
		               MONEDA_SOLICITUD,
		               K.ESTADO,
		               FECHA_ESTADO,
		               IMPORTE_COBRADO,
		               FECHA_INGRESO,
		               FECHA_EFECTIVA_COBRO,
		               COD_RESULTADO,
		               TXT_RESULTADO,
		               ID_PROCESO,
		               JTS_OID,
		               ASIENTO,
		               SUCURSAL_ASIENTO,
		               FECHA_PROCESO_ASIENTO,
		               JTS_OID_PADRE,
		               SALDO_JTS_OID,
		               K.ID_CONVENIO,
		               SERVICIO_REFERENCIA,
		               NOTIFICADO_PAGO,
		               REGISTRO,
		               TIPO_PROCESO,
		               K.CONCEPTO,
		               IMPORTE_CARGOS,
		               K.GESTION,
		               K.TIPO_GESTION,
		               K.GENERA_RESERVA,
					   K.CODIGO_INTERNO_MOV
		          FROM (SELECT R.TZ_LOCK,
		                       R.FECHA_INICIO_COBRO,
		                       R.REFERENCIA,
		                       R.IMPORTE_A_COBRAR,
		                       R.IMPORTE_MINIMO,
		                       R.MONEDA_SOLICITUD,
		                       R.ESTADO,
		                       R.FECHA_ESTADO,
		                       R.IMPORTE_COBRADO,
		                       R.FECHA_INGRESO,
		                       R.FECHA_EFECTIVA_COBRO,
		                       R.COD_RESULTADO,
		                       R.TXT_RESULTADO,
		                       R.ID_PROCESO,
		                       R.JTS_OID,
		                       R.ASIENTO,
		                       R.SUCURSAL_ASIENTO,
		                       R.FECHA_PROCESO_ASIENTO,
		                       R.JTS_OID_PADRE,
		                       R.SALDO_JTS_OID,
		                       R.ID_CONVENIO,
		                       R.SERVICIO_REFERENCIA,
		                       R.NOTIFICADO_PAGO,
		                       R.REGISTRO,
		                       R.TIPO_PROCESO,
		                       R.CONCEPTO,
		                       R.IMPORTE_CARGOS,
		                       CR.TIPO_GESTION,
		                       CR.PRIORIDAD AS PRIORIDAD_CONVENIO,
		                       C.PRIORIDAD AS PRIORIDAD_CONCEPTO,
		                       R.FECHAVENCIMIENTO,
		                       R.GESTION, 
		                       CR.GENERA_RESERVA,
							   CR.CODIGO_INTERNO_MOV
		                  FROM RCT_RECAUDACIONES R, RCT_CONVENIO_RECAUD CR, RCT_CONCEPTOS C, RCT_SUSCRIPCION_SERVICIO SS--, RCT_DOSIFICACION_COLEGIOS DM  
		                 WHERE R.ESTADO = ?
		                   AND R.FECHA_INICIO_COBRO &lt;= ? 
		                   AND R.FECHA_INICIO_COBRO IS NOT NULL
		                   AND R.ID_CONVENIO = CR.ID_CONVENIO 
		                   AND R.ID_CONVENIO = C.ID_CONVENIO
		                   AND R.CONCEPTO = C.CONCEPTO
		                  AND R.ID_CONVENIO = SS.ID_CONVENIO
		                   AND R.SERVICIO_REFERENCIA = SS.SERVICIO_REFERENCIA
		                   AND SS.ESTADO = 1 AND
					  (CR.OPTANTEOBRIGATORIO='N' OR R.SALDO_JTS_OID IN 
						(SELECT JTS_CONTA FROM RCT_OPTANTES O WHERE O.NUMEROCONTRATO=R.SERVICIO_REFERENCIA AND O.STATUS='A' AND O.DEBITOAUTOMATICO='S')) AND R.TZ_LOCK=0 )K
		          LEFT JOIN RCT_CONTROL_GESTION CG ON CG.ID_CONVENIO = K.ID_CONVENIO
		                                          AND CG.CONCEPTO = K.CONCEPTO
		                                          AND CG.MONEDA = K.MONEDA_SOLICITUD
		                                          AND CG.GESTION = K.GESTION
		                                          AND K.TIPO_GESTION IN (1, 2)
		                                          AND CG.ESTADO = 'A'
		         WHERE (TIPO_GESTION = 0 OR
		               (TIPO_GESTION IN (1, 2) AND CG.GESTION IS NOT NULL))
		         ORDER BY SALDO_JTS_OID,
						  PRIORIDAD_CONVENIO,
		                  K.ID_CONVENIO,
		                  SERVICIO_REFERENCIA,
		                  PRIORIDAD_CONCEPTO,
		                  ORDEN,
		            	TO_NUMBER(REFERENCIA),
		                FECHA_INICIO_COBRO,
		                FECHAVENCIMIENTO,CONCEPTO
		</sentence>
        <attribute>
            <attribute-name>tz_lock</attribute-name>
            <column-name>TZ_LOCK</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaInicioCobro</attribute-name>
            <column-name>FECHA_INICIO_COBRO</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>referencia</attribute-name>
            <column-name>REFERENCIA</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>importeACobrar</attribute-name>
            <column-name>IMPORTE_A_COBRAR</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>importeMinimo</attribute-name>
            <column-name>IMPORTE_MINIMO</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>monedaSolicitud</attribute-name>
            <column-name>MONEDA_SOLICITUD</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>estado</attribute-name>
            <column-name>ESTADO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaEstado</attribute-name>
            <column-name>FECHA_ESTADO</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>importeCobrado</attribute-name>
            <column-name>IMPORTE_COBRADO</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaIngreso</attribute-name>
            <column-name>FECHA_INGRESO</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaEfectivaCobro</attribute-name>
            <column-name>FECHA_EFECTIVA_COBRO</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>codResultado</attribute-name>
            <column-name>COD_RESULTADO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>txtResultado</attribute-name>
            <column-name>TXT_RESULTADO</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>idProceso</attribute-name>
            <column-name>ID_PROCESO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>JTS_OID</attribute-name>
            <column-name>JTS_OID</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>nroAsiento</attribute-name>
            <column-name>ASIENTO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>sucursalAsiento</attribute-name>
            <column-name>SUCURSAL_ASIENTO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaProcesoAsiento</attribute-name>
            <column-name>FECHA_PROCESO_ASIENTO</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>jtsOidPadre</attribute-name>
            <column-name>JTS_OID_PADRE</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>jtsOidSaldo</attribute-name>
            <column-name>SALDO_JTS_OID</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>idConvenio</attribute-name>
            <column-name>ID_CONVENIO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>servicioReferencia</attribute-name>
            <column-name>SERVICIO_REFERENCIA</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>notificadoPago</attribute-name>
            <column-name>NOTIFICADO_PAGO</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>registro</attribute-name>
            <column-name>REGISTRO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>tipoProceso</attribute-name>
            <column-name>TIPO_PROCESO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>concepto</attribute-name>
            <column-name>CONCEPTO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>importe_cargos</attribute-name>
            <column-name>IMPORTE_CARGOS</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>gestion</attribute-name>
            <column-name>GESTION</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>tipoGestion</attribute-name>
            <column-name>TIPO_GESTION</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>generaReserva</attribute-name>
            <column-name>GENERA_RESERVA</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>codigoInternoMov</attribute-name>
            <column-name>CODIGO_INTERNO_MOV</column-name>
            <column-type>long</column-type>
        </attribute>
</query>
"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))

	}
	
	private void agregarProcesoMultiHilos() {
		// Agregar o sustituir un proceso
	def archivo = "processes.py"
	def procesos = """
	    pdef = ProcessDefinition("Cobranza Vista Multihilo", "topsystems.automaticprocess.processmanager.WorkManager")
    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WDCobranzaVistaMultiHilos");
    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BusinessWorkerCobranzaVistaDefault");
    pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerCobranzaVistaDefault");
    pdef.addConstant("rangoCommit","100");
    pdef.addConstant("cantidadHilos","10");
    pdef.addConstant("isStopable","true");
    pdef.addConstant("applySchemes","true");
    pdef.addConstant("isSumarizable","true");
    pdef.addConstant("offLine","true");
    pdef.addConstant("enqueue","false");
    pdef.addConstant("priorityType", "topsystems:service:RequestSorting.ComisionExterna");
    pdef.addConstant("transactionByRequest", "false");
    pdef.addConstant("fechaVencimiento","50027");
    pdef.addConstant("diasAtraso","50026");
    pdef.addConstant("mesesAtraso","50025");
    pdef.addConstant("deuda","50024");
    pdef.addConstant("mora","50023");
    pdef.addConstant("campoNombreConvenio","50022");
    pdef.addConstant("applyInstalationFields","true");
    pdef.addConstant("generaAsientoContable","true");
    pdef.addConstant("applyCodigoTransaccion","true");
    pdef.addConstant("QUERY_NAME_MULTIHILOS","query.Solicitud_Recaudacion_MultiHilos");
	pdefs.addProcess(pdef)
"""
	this.addCommand(new AgregarModificarProceso(archivo, procesos))
	
	}
	
	private void agregarGrupoMultiHilos() {
		// Agregar o sustituir un grupo
def archivo = "groups.py"
def grupo = """
    gdef = GroupDefinition("RCT - Cobranza Vista Multihilo")
    gdef.registerProcess("Cobranza Vista Multihilo")
    gdef.setCanBeRootGroup("true")
    gdef.setSingleton("false")
    gdefs.addGroup(gdef)
"""
this.addCommand( new AgregarModificarGrupo(archivo, grupo))

	}
	
	private void agregarGrupoTransferenciasCobranzasConvenios() {
		// Agregar o sustituir un grupo
def archivo = "groups.py"
def grupo = """
	gdef = GroupDefinition("RCT - Transferencia Cobranzas a Convenios")
	gdef.registerProcess("Transferencia Cobranzas a Convenios")
	gdef.setCanBeRootGroup("true")
	gdef.setSingleton("false")
	gdefs.addGroup(gdef)
"""
this.addCommand( new AgregarModificarGrupo(archivo, grupo))

	}

	private void agregarGrupoDebitosAutomaticos() {
		// Agregar o sustituir un grupo
def archivo = "groups.py"
def grupo = """
	gdef = GroupDefinition("RCT - Debitos Automaticos Multihilo")
	gdef.registerProcess("RCT - Cobranza Vista Multihilo")
	gdef.registerProcess("GRL - Aplicacion Movimientos OffLine")
	gdef.registerProcess("RCT - Transferencia Cobranzas a Convenios")
	gdef.registerProcess("GRL - Aplicacion Movimientos OffLine")
	gdef.setCanBeRootGroup("true")
	gdef.setSingleton("false")
	gdefs.addGroup(gdef)
"""
this.addCommand( new AgregarModificarGrupo(archivo, grupo))

	}
	
	private void agregarServicesDeployMultiHilos() {
		// Agregar o sustituir uno o varios servicios en un mismo archivo
def archivo = "cobranzaVista/cobranzaVistaProcessDeploy.xml"
def servicios = """
	<service>
		<name>topsystems:processManager:WorkDescriptor=WDCobranzaVistaMultiHilos</name>
		<code>topsystems.automaticprocess.cobranzavista.WDCobranzaVistaMultiHilos</code>
	</service>
"""
this.addCommand( new AgregarModificarServicios(archivo, servicios))

	}
	
	private void agregarQueryTransferencias() {
		// Agregar o sustituir una query (o varias) de DataMapping
def archivoMapp = "CobranzaVistaMapping.xml"
def queries = """
<query>	                 
        <class-name>topsystems.automaticprocess.cobranzavista.QueryTransferenciaCobranzasAConvenios</class-name>
        <query-name>query.QueryTransferenciaCobranzasAConvenios</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
				SELECT sum(RR.IMPORTE_COBRADO) AS importe_transferir,
	   RR.ID_CONVENIO,
	   RCS.SALDO_JTS_OID,
	   RCS.SALDO_JTS_OID_ENTE,
	   RR.MONEDA_SOLICITUD,
	   RR.CONCEPTO
FROM 
	RCT_RECAUDACIONES RR
	INNER JOIN RCT_CONVENIO_RECAUD RCR ON RCR.ID_CONVENIO = RR.ID_CONVENIO
							       AND RCR.ESTADO = 'A' AND RCR.TRANSFIERE_ENTE= 'S' AND RR.NOTIFICADO_PAGO = 'N' 
							       AND RR.ESTADO = 9 AND RR.IMPORTE_COBRADO &gt; 0 AND RR.TZ_LOCK=0 AND RCR.TZ_LOCK=0
	INNER JOIN RCT_CONVENIO_SALDOS RCS ON RCS.ID_CONVENIO = RR.ID_CONVENIO  
								   AND RCS.MONEDA = RR.MONEDA_SOLICITUD AND RCS.CONCEPTO = RR.CONCEPTO
								   AND RCS.SALDO_JTS_OID_ENTE IS NOT NULL AND RCS.SALDO_JTS_OID_ENTE &gt; 0 AND RCS.TZ_LOCK=0
GROUP BY RR.ID_CONVENIO, RR.MONEDA_SOLICITUD, RR.CONCEPTO, RCS.SALDO_JTS_OID, RCS.SALDO_JTS_OID_ENTE
		</sentence>
        <attribute>
            <attribute-name>importeTransferir</attribute-name>
            <column-name>importe_transferir</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>idConvenio</attribute-name>
            <column-name>ID_CONVENIO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>saldoJtsOid</attribute-name>
            <column-name>SALDO_JTS_OID</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>saldoJtsOidEnte</attribute-name>
            <column-name>SALDO_JTS_OID_ENTE</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>monedaSolicitud</attribute-name>
            <column-name>MONEDA_SOLICITUD</column-name>
            <column-type>int</column-type>
        </attribute>
        <attribute>
            <attribute-name>concepto</attribute-name>
            <column-name>CONCEPTO</column-name>
            <column-type>int</column-type>
        </attribute>
</query>
"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))

	}
	
	private void agregarServicesTransferencias() {
		// Agregar o sustituir uno o varios servicios en un mismo archivo
def archivo = "cobranzaVista/cobranzaVistaProcessDeploy.xml"
def servicios = """
			<service>
				<name>topsystems:processManager:WorkDescriptor=WDTransferenciasCobranzasAConvenios</name>
				<code>topsystems.automaticprocess.cobranzavista.WDTransferenciasCobranzasAConvenios</code>
			</service>
			<service>
				<name>topsystems:processManager:BusinessWorker=BWTransferenciasCobranzasAConvenios</name>
				<code>topsystems.automaticprocess.cobranzavista.BWTransferenciasCobranzasAConvenios</code>
			</service>
"""
this.addCommand( new AgregarModificarServicios(archivo, servicios))

	}
	

	private void agregarProcesoTransferencias() {
		// Agregar o sustituir un proceso
	def archivo = "processes.py"
	def procesos = """
	    pdef = ProcessDefinition("Transferencia Cobranzas a Convenios", "topsystems.automaticprocess.processmanager.WorkManager")
    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WDTransferenciasCobranzasAConvenios");
    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BWTransferenciasCobranzasAConvenios");
    pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
    pdef.addConstant("rangoCommit","100");
    pdef.addConstant("cantidadHilos","10");
    pdef.addConstant("isStopable","true");
    pdef.addConstant("applySchemes","true");
    pdef.addConstant("isSumarizable","true");
    pdef.addConstant("offLine","true");
    pdef.addConstant("enqueue","false");
	pdefs.addProcess(pdef)
"""
	this.addCommand(new AgregarModificarProceso(archivo, procesos))
		
	}
	
	private void addParametrosJts(){
		def sentencia = """
		INSERT INTO PARAMETROS_JTS(FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
			VALUES('TRANSFERENCIAS_ENTRE_ENTES','DESCRIPCION','Transferencias a convenios.',0);
		
			INSERT INTO PARAMETROS_JTS(FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)		
			VALUES('TRANSFERENCIAS_ENTRE_ENTES','OPERACION','${this.P("OPERACION_TRANSFERENCIAS_ENTES")}', 0);

			INSERT INTO PARAMETROS_JTS(FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)		
			VALUES('TRANSFERENCIAS_ENTRE_ENTES','CANAL','0', 0);

			INSERT INTO OPERACIONES (TITULO, IDENTIFICACION, NOMBRE, DESCRIPCION, MNEMOTECNICO, AUTORIZACION, FORMULARIOPRINCIPAL, PROXOPERACION, ESTADO, TZ_LOCK, COPIAS, SUBOPERACION, PERMITEBAJA, COMPORTAMIENTOENCIERRE, REQUIERECONTRASENA, PERMITECONCURRENTE, PERMITEESTADODIFERIDO, ICONO_TITULO, ESTILO)
			VALUES ( ${this.P("TITULO_OPERACION")}, ${this.P("OPERACION_TRANSFERENCIAS_ENTES")}, 'Transferencias a convenios', 'Transferencias a convenios', '${this.P("OPERACION_TRANSFERENCIAS_ENTES")}', 'N', NULL, NULL, 'P', 0, NULL, 0, 'S', 'N', 'N', 'S', 'S', NULL, NULL);
"""
	def descripcion = "Sobre la sentencia" // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	
	}
	
	void definicionDeParametros() {
		this.addParameterPATitleOpe("TITULO_OPERACION", "TITULO OPE TRANSFERENCIAS_ENTES")
		this.addParameterNewOperationNumber("OPERACION_TRANSFERENCIAS_ENTES", "Numero de operación para el proceso: TRANSFERENCIAS_ENTES")
	}
	
}


