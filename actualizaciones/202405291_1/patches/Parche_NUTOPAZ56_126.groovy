package nucleo

import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.parche.Parche

class Parche_NUTOPAZ56_126 extends Parche{

	@Override
	protected void comandos() throws TopazException {
		addNewAttributeMapping()
	}	

	private void addNewAttributeMapping(){
		def archivoMapp = "categorizacionPorCuotaMapping.xml"
		def entidad = "core.vo_SaldoCategorizar"
		def atributo = """
			<attribute>
	            <attribute-name>SAL_RUBRO_ORIGINAL</attribute-name>
	            <column-name>C1692</column-name>
	        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
	}

}
