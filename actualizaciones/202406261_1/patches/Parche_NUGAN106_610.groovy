import topsystems.TopazException
import topsystems.actualizador.command.actualizarjars.BorrarEntradaEnJnlp
import topsystems.actualizador.command.archivos.BorrarArchivo
import topsystems.actualizador.command.archivos.CopiarDesdeZip
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Parche_NUGAN106_610 extends Parche{

	@Override
	protected void comandos() {
		agregarMapping()
		agregarProperty()
		addDataBaseObjects()
	}
	
	private void addDataBaseObjects(){
		if(isOracle()){
			oracleScript()
		}else{
			sqlServerScript()
		}
	}
	
	void agregarMapping(){
		
		// Copiar un archivo desde el zip del parche al destino que se le indique
		// El .zip debe tener el mismo nombre que el archivo del parche (salvo la extensión)
		this.addCommand new CopiarDesdeZip("TableRequestsMapping.xml", "\${DATAMAPPINGS}/")

	}
	
	void agregarProperty(){
		
		// Agregar una propiedad o cambiar el valor a una existente
		def archivo = "tablerequest.properties" // Si el archivo no existe, se crea.
		def propiedad = "tablerequest.control.server.execution"
		def valor = "false"
		def comentario = "Si es true, antes de que el servidor active una operacion desatendida, se verifica si esta autorizado a hacerlo (tabla TR_SERVERSCONTROL). Si es false (defecto) no se verifica." // Descripción de la propiedad [Obligatorio]
		this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, valor, comentario))


	}

	void oracleScript(){
		def sentencia = """
		CREATE TABLE TR_SERVERSCONTROL ( 
		 REQUEST_NAME		VARCHAR2(150),
		 SERVER_IP			VARCHAR2(100),
		 TZ_LOCK            NUMBER (15) DEFAULT (0)
		);
		"""
		def descripcion = "Tabla para indicar que servidores pueden ejecutar un tablerequest" // La descripción es opcional
		this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	
	}
	
	void sqlServerScript(){
		def sentencia = """
		CREATE TABLE TR_SERVERSCONTROL ( 
		 REQUEST_NAME		VARCHAR(150),
		 SERVER_IP			VARCHAR(100),
		 TZ_LOCK            NUMERIC (15) DEFAULT (0)
		);
		"""
		def descripcion = "Tabla para indicar que servidores pueden ejecutar un tablerequest" // La descripción es opcional
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
	
	}
	
}
