import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.dataserver.BorrarAtributoEntidad
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche;

class Parche_NUBANKIBR_398 extends Parche{

	void definicionDeParametros(){
		this.addParameterNewFreeField("SAL_GIO_TIPO_DEVENGADO", "N�mero de campo para columna SAL_GIO_TIPO_DEVENGADO de la tabla Saldos")
		this.addParameterNewFreeField("SAL_TASA_TEJ", "N�mero de campo para columna SAL_TASA_TEJ de la tabla Saldos")
	}

	@Override
	protected void comandos() {

	//----------------------------------------------------------------------------------------------------------------//
	// Actualizaci�n de la Base de Datos en Oracle
	def sentencia = """
		alter session set ddl_lock_timeout=120;

		alter table SALDOS drop column GIO_CONTABILIZADOS;
		alter table SALDOS drop column GIO_DEVENGADO_VIGENTE;
		alter table SALDOS drop column GIO_DEV_XDIASATRASO;
		alter table SALDOS drop column GIO_TOTAL;

		alter table SALDOS add GIO_TIPO_DEVENGADO varchar2(1) default (' ');
		alter table SALDOS add TASA_TEJ number(11, 7) default (0);
	"""
	def descripcion = "Se hacen modificaciones por GIO."
	this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	//----------------------------------------------------------------------------------------------------------------//	


	//----------------------------------------------------------------------------------------------------------------//
	// Actualizaci�n de la Base de Datos en SQLServer
	//----------------------------------------------------------------------------------------------------------------//
	
	//----------------------------- ELIMINA CAMPOS DE SALDOS ---------------------------------------------------------//
	sentencia = """
			-- HM begin sentence 
			DECLARE @TableName sysname,
			        @Schema sysname,
			        @colname sysname,
			        @sql VARCHAR(1000)
			
			SELECT @Schema = 'dbo',
			       @TableName = 'SALDOS',
			       @colname = 'GIO_CONTABILIZADOS'
			
			IF COL_LENGTH(@Schema+'.'+@TableName, @colname) IS NOT NULL
			BEGIN
			    SET @sql = ''
			    SELECT @sql += N' ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + default_constraints.name + ';'
			    FROM sys.all_columns
			        INNER JOIN sys.tables
			            ON all_columns.object_id = TABLES.object_id
			        INNER JOIN sys.schemas
			            ON TABLES.schema_id = schemas.schema_id
			        INNER JOIN sys.default_constraints
			            ON all_columns.default_object_id = default_constraints.object_id
			    WHERE schemas.name = @Schema
			          AND tables.name = @TableName
			          AND all_columns.name = @colname
			
			    SET @sql += N' ALTER TABLE ' + @TableName + ' DROP COLUMN ' + @colname + ';'
					
			    EXECUTE(@sql)
			
			END;

			SELECT @Schema = 'dbo',
			       @TableName = 'SALDOS',
			       @colname = 'GIO_DEVENGADO_VIGENTE'
			
			IF COL_LENGTH(@Schema+'.'+@TableName, @colname) IS NOT NULL
			BEGIN
			    SET @sql = ''
			    SELECT @sql += N' ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + default_constraints.name + ';'
			    FROM sys.all_columns
			        INNER JOIN sys.tables
			            ON all_columns.object_id = TABLES.object_id
			        INNER JOIN sys.schemas
			            ON TABLES.schema_id = schemas.schema_id
			        INNER JOIN sys.default_constraints
			            ON all_columns.default_object_id = default_constraints.object_id
			    WHERE schemas.name = @Schema
			          AND tables.name = @TableName
			          AND all_columns.name = @colname
			
			    SET @sql += N' ALTER TABLE ' + @TableName + ' DROP COLUMN ' + @colname + ';'
					
			    EXECUTE(@sql)
			
			END;

			SELECT @Schema = 'dbo',
			       @TableName = 'SALDOS',
			       @colname = 'GIO_DEV_XDIASATRASO'
			
			IF COL_LENGTH(@Schema+'.'+@TableName, @colname) IS NOT NULL
			BEGIN
			    SET @sql = ''
			    SELECT @sql += N' ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + default_constraints.name + ';'
			    FROM sys.all_columns
			        INNER JOIN sys.tables
			            ON all_columns.object_id = TABLES.object_id
			        INNER JOIN sys.schemas
			            ON TABLES.schema_id = schemas.schema_id
			        INNER JOIN sys.default_constraints
			            ON all_columns.default_object_id = default_constraints.object_id
			    WHERE schemas.name = @Schema
			          AND tables.name = @TableName
			          AND all_columns.name = @colname
			
			    SET @sql += N' ALTER TABLE ' + @TableName + ' DROP COLUMN ' + @colname + ';'
					
			    EXECUTE(@sql)
			
			END;

			SELECT @Schema = 'dbo',
			       @TableName = 'SALDOS',
			       @colname = 'GIO_TOTAL'
			
			IF COL_LENGTH(@Schema+'.'+@TableName, @colname) IS NOT NULL
			BEGIN
			    SET @sql = ''
			    SELECT @sql += N' ALTER TABLE ' + @TableName + ' DROP CONSTRAINT ' + default_constraints.name + ';'
			    FROM sys.all_columns
			        INNER JOIN sys.tables
			            ON all_columns.object_id = TABLES.object_id
			        INNER JOIN sys.schemas
			            ON TABLES.schema_id = schemas.schema_id
			        INNER JOIN sys.default_constraints
			            ON all_columns.default_object_id = default_constraints.object_id
			    WHERE schemas.name = @Schema
			          AND tables.name = @TableName
			          AND all_columns.name = @colname
			
			    SET @sql += N' ALTER TABLE ' + @TableName + ' DROP COLUMN ' + @colname + ';'
					
			    EXECUTE(@sql)
			
			END;

			-- HM end sentence
		"""
		descripcion = "Se hacen modificaciones por GIO."
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
	//----------------------------------------------------------------------------------------------------------------//
		
		
	//--------------------------------- AGREGAN LOS NUEVOS CAMPOS ----------------------------------------------------//
	sentencia = """
		alter table SALDOS add GIO_TIPO_DEVENGADO varchar(1) default (' ') with values;
		alter table SALDOS add TASA_TEJ numeric(11, 7) default (0) with values;
	"""
	descripcion = "Se hacen modificaciones por GIO."
	this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
	//----------------------------------------------------------------------------------------------------------------//

		
	//----------------------------------------------------------------------------------------------------------------//
	// SQL GEN�RICO
	sentencia = """
		insert into CONCEPTOS (CODIGO, DESCRIPCION, CAMPOV25)
			values (2799, 'Tipo devengado GIO', 0);

		insert into CONCEPTOS (CODIGO, DESCRIPCION, CAMPOV25)
			values (2800, 'Tasa Efectiva Juros', 0);

		insert into DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
			values ('${this.P("SAL_GIO_TIPO_DEVENGADO")}', ' ', 0, 'GIO Tipo Dev.', 'GIO Tipo Dev.', 1, 'A', 0, NULL, 0, 2799, 0, 0, 0, 0, 0, 21, 'GIO_TIPO_DEVENGADO', 0, NULL);

		insert into DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
			values ('${this.P("SAL_TASA_TEJ")}', ' ', 0, 'Tasa Efe.Juros', 'Tasa Efe.Juros', 11, 'N', 7, NULL, 0, 2800, 0, 0, 0, 0, 0, 21, 'TASA_TEJ', 0, NULL);
		"""
	this.addCommand( new ActualizarBaseDeDatos(sentencia))
	//----------------------------------------------------------------------------------------------------------------//
	}

}