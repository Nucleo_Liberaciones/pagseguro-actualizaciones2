import topsystems.TopazException
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.command.dataserver.BorrarEntidad
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProcesoByBusinessWorker
import topsystems.actualizador.parche.Parche

class Parche_NUBANKIBR_488 extends Parche {

	@Override
	protected void comandos() throws TopazException {
		eliminarTablaReservasPendientes()
		eliminarEntidadReservasPendientes()
		agregarParametros()
	}
	
	
	private eliminarTablaReservasPendientes(){		
		def descripcion = "Borrar tabla reservas pendientes"
		if(isOracle()) {
			def sentencia = "DROP TABLE VTA_RESERVAS_PENDIENTES CASCADE CONSTRAINTS;"				    
			this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
		}
		else if(isSqlServer()) {
			def sentencia = "DROP TABLE VTA_RESERVAS_PENDIENTES;"
			this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
		}
	}
	
	
	private eliminarEntidadReservasPendientes(){
		def archivoMapp = "Reservas.xml"
		def entidades = "topsystems.automaticprocess.objectsdomain.ReservasPendientes"
		this.addCommand( new BorrarEntidad(archivoMapp, entidades))
	}
	
	private void agregarParametros(){
		def archivo = "processes.py"
		def businessWorkerName = "topsystems:processManager:BusinessWorker=BusinessWorkerCobranzaAutomatica"
		def constParamProc = """
			pdef.addConstant("disminuirReservaEnCobro","false");
			pdef.addConstant("disponibilidadConReservasMayorPrioridad","false");
        """
		this.addCommand( new AgregarModificarPropiedadesProcesoByBusinessWorker(archivo, businessWorkerName, constParamProc))
	}
	
}
