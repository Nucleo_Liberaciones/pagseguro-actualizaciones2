import topsystems.TopazException
import topsystems.actualizador.parche.Parche
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProcesoByBusinessWorker

class Parche_NUBANKIBR_489_LB extends Parche{

	@Override
	protected void comandos() throws TopazException {
		// TODO Auto-generated method stub		
		addParamToManyProcess()
	}
	
	private void addParamToManyProcess(){
		// Agregar constantes y parametros a un proceso
		// Tener en cuenta que pueden existir varios procesos con el mismo businessWorkName, el cambio se realizará en todos los procesos encontrados.
		def archivo = "processes.py"
		def businessWorkerName = "topsystems:processManager:BusinessWorker=BusinessWorkerCobranzaCargos"
		def constParamProc = """
		pdef.addConstant("disminuirReservaEnCobro","false");
		pdef.addConstant("disponibilidadConReservasMayorPrioridad","false");
		"""
		this.addCommand( new AgregarModificarPropiedadesProcesoByBusinessWorker(archivo, businessWorkerName, constParamProc))
	}
	
	
}
