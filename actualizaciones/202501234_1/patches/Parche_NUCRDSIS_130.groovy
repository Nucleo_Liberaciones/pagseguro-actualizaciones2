import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche
import topsystems.actualizador.configuration.CarpetasServidor

class Parche_NUCRDSIS_130 extends Parche {
	
	def isSicredi = false;

	@Override
	protected void comandos() throws TopazException {
		isSicredi = isSicredi();
		crearTablaPARAMETROS_PREJUIZO()	
		modificarServicio()			
	}
	
	private void crearTablaPARAMETROS_PREJUIZO() {
		def ORDINALINTERES = isSicredi ? 1 : 99
		def ORDINALMULTA = isSicredi ? 2 : 99
		def ORDINALMORA = isSicredi ? 3 : 99
		def createSentence = null
		def insertSentence = null
		
		
		if (isSqlServer()) { 
			/*
			 * Creando estructura
			 */
			createSentence = """
				CREATE TABLE PARAMETROS_PREJUIZO (
				    ORDINALINTERES NUMERIC(6,0),
				    PIVOTCOBRANZAINTERES NUMERIC(5,0),
				    PIVOTCAMBIOSUCURSALINTERES NUMERIC(5,0),
				    ORDINALMORA NUMERIC(6,0),
				    PIVOTCOBRANZAMORA NUMERIC(5,0),
				    PIVOTCAMBIOSUCURSALMORA NUMERIC(5,0),
				    ORDINALMULTA NUMERIC(6,0),
				    PIVOTCOBRANZAMULTA NUMERIC(5,0),
				    PIVOTCAMBIOSUCURSALMULTA NUMERIC(5,0),
				    ORDINALIPF NUMERIC(6,0),
				    PIVOTCOBRANZAIPF NUMERIC(5,0),
				    PIVOTCAMBIOSUCURSALIPF NUMERIC(5,0),
				    ORDINALIPFMORATORIO NUMERIC(6,0),
				    PIVOTCOBRANZAIPFMORATORIO NUMERIC(5,0),
				    PIVOTCAMBIOSUCURSALIPFMORATORIO NUMERIC(5,0),
				    PIVOTCOBRANZATOTAL NUMERIC(5,0),
				    PIVOTCAMBIOSUCURSALTOTAL NUMERIC(5,0)
				);
				"""
			
			/*
			* Creando registro unico
			*/
			insertSentence = """
				INSERT INTO PARAMETROS_PREJUIZO (
				    ORDINALINTERES,
				    PIVOTCOBRANZAINTERES,
				    PIVOTCAMBIOSUCURSALINTERES,
				    ORDINALMORA,
				    PIVOTCOBRANZAMORA,
				    PIVOTCAMBIOSUCURSALMORA,
				    ORDINALMULTA,
				    PIVOTCOBRANZAMULTA,
				    PIVOTCAMBIOSUCURSALMULTA,
				    ORDINALIPF,
				    PIVOTCOBRANZAIPF,
				    PIVOTCAMBIOSUCURSALIPF,
				    ORDINALIPFMORATORIO,
				    PIVOTCOBRANZAIPFMORATORIO,
				    PIVOTCAMBIOSUCURSALIPFMORATORIO,
				    PIVOTCOBRANZATOTAL,
				    PIVOTCAMBIOSUCURSALTOTAL
				) VALUES (
				    ${ORDINALINTERES}, -- ORDINALINTERES
				    COALESCE((SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'COBRANZA_AUTOMATICA' AND PARAMETRO = 'PIVOT_PREVISION_PAGO_INTERESES'), 0), -- PIVOTCOBRANZAINTERES
				    COALESCE((SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'PROCESO_CAMBIO_SUCURSAL' AND PARAMETRO = 'PIVOT_PREJUIZO_INTERESES'), 0), -- PIVOTCAMBIOSUCURSALINTERES
				    ${ORDINALMORA}, -- ORDINALMORA
				    COALESCE((SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'COBRANZA_AUTOMATICA' AND PARAMETRO = 'PIVOT_PREVISION_PAGO_MORA'), 0), -- PIVOTCOBRANZAMORA
				    COALESCE((SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'PROCESO_CAMBIO_SUCURSAL' AND PARAMETRO = 'PIVOT_PREJUIZO_MORA'), 0), -- PIVOTCAMBIOSUCURSALMORA
				    ${ORDINALMULTA}, -- ORDINALMULTA
				    COALESCE((SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'COBRANZA_AUTOMATICA' AND PARAMETRO = 'PIVOT_PREVISION_PAGO_MULTA'), 0), --PIVOTCOBRANZAMULTA
				    COALESCE((SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'PROCESO_CAMBIO_SUCURSAL' AND PARAMETRO = 'PIVOT_PREJUIZO_MULTA'), 0), -- PIVOTCAMBIOSUCURSALMULTA
				    99, -- ORDINALIPF
				    0, -- PIVOTCOBRANZAIPF
				    0, -- PIVOTCAMBIOSUCURSALIPF
				    99, -- ORDINALIPFMORATORIO
				    0, -- PIVOTCOBRANZAIPFMORATORIO
				    0, -- PIVOTCAMBIOSUCURSALIPFMORATORIO
				    COALESCE((SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'COBRANZA_AUTOMATICA' AND PARAMETRO = 'PIVOT_PREVISION_PAGO_TOTAL'), 0), -- PIVOTCOBRANZATOTAL
				    0  -- PIVOTCAMBIOSUCURSALTOTAL
				);
			"""	
			
			this.addCommand( new ActualizarBaseDeDatosSQLServer(createSentence))
			this.addCommand( new ActualizarBaseDeDatosSQLServer(insertSentence))
			
		} else {
			/*
			 * Creando estructura
			 */
			createSentence = """
				CREATE TABLE PARAMETROS_PREJUIZO (
				    ORDINALINTERES NUMBER(6,0),
				    PIVOTCOBRANZAINTERES NUMBER(5,0),
				    PIVOTCAMBIOSUCURSALINTERES NUMBER(5,0),
				    ORDINALMORA NUMBER(6,0),
				    PIVOTCOBRANZAMORA NUMBER(5,0),
				    PIVOTCAMBIOSUCURSALMORA NUMBER(5,0),
				    ORDINALMULTA NUMBER(6,0),
				    PIVOTCOBRANZAMULTA NUMBER(5,0),
				    PIVOTCAMBIOSUCURSALMULTA NUMBER(5,0),
				    ORDINALIPF NUMBER(6,0),
				    PIVOTCOBRANZAIPF NUMBER(5,0),
				    PIVOTCAMBIOSUCURSALIPF NUMBER(5,0),
				    ORDINALIPFMORATORIO NUMBER(6,0),
				    PIVOTCOBRANZAIPFMORATORIO NUMBER(5,0),
				    PIVOTCAMBIOSUCURSALIPFMORATORIO NUMBER(5,0),
				    PIVOTCOBRANZATOTAL NUMBER(5,0),
				    PIVOTCAMBIOSUCURSALTOTAL NUMBER(5,0)
				);
				"""
			
			/*
			* Creando registro unico
			*/
			insertSentence = """
				INSERT INTO PARAMETROS_PREJUIZO (
				    ORDINALINTERES,
				    PIVOTCOBRANZAINTERES,
				    PIVOTCAMBIOSUCURSALINTERES,
				    ORDINALMORA,
				    PIVOTCOBRANZAMORA,
				    PIVOTCAMBIOSUCURSALMORA,
				    ORDINALMULTA,
				    PIVOTCOBRANZAMULTA,
				    PIVOTCAMBIOSUCURSALMULTA,
				    ORDINALIPF,
				    PIVOTCOBRANZAIPF,
				    PIVOTCAMBIOSUCURSALIPF,
				    ORDINALIPFMORATORIO,
				    PIVOTCOBRANZAIPFMORATORIO,
				    PIVOTCAMBIOSUCURSALIPFMORATORIO,
				    PIVOTCOBRANZATOTAL,
				    PIVOTCAMBIOSUCURSALTOTAL
				) VALUES (
				    ${ORDINALINTERES}, -- ORDINALINTERES
				    COALESCE((SELECT TO_NUMBER(VALOR) FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'COBRANZA_AUTOMATICA' AND PARAMETRO = 'PIVOT_PREVISION_PAGO_INTERESES'), 0), -- PIVOTCOBRANZAINTERES
				    COALESCE((SELECT TO_NUMBER(VALOR) FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'PROCESO_CAMBIO_SUCURSAL' AND PARAMETRO = 'PIVOT_PREJUIZO_INTERESES'), 0), -- PIVOTCAMBIOSUCURSALINTERES
				    ${ORDINALMORA}, -- ORDINALMORA
				    COALESCE((SELECT TO_NUMBER(VALOR) FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'COBRANZA_AUTOMATICA' AND PARAMETRO = 'PIVOT_PREVISION_PAGO_MORA'), 0), -- PIVOTCOBRANZAMORA
				    COALESCE((SELECT TO_NUMBER(VALOR) FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'PROCESO_CAMBIO_SUCURSAL' AND PARAMETRO = 'PIVOT_PREJUIZO_MORA'), 0), -- PIVOTCAMBIOSUCURSALMORA
				    ${ORDINALMULTA}, -- ORDINALMULTA
				    COALESCE((SELECT TO_NUMBER(VALOR) FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'COBRANZA_AUTOMATICA' AND PARAMETRO = 'PIVOT_PREVISION_PAGO_MULTA'), 0), --PIVOTCOBRANZAMULTA
				    COALESCE((SELECT TO_NUMBER(VALOR) FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'PROCESO_CAMBIO_SUCURSAL' AND PARAMETRO = 'PIVOT_PREJUIZO_MULTA'), 0), -- PIVOTCAMBIOSUCURSALMULTA
				    99, -- ORDINALIPF
				    0, -- PIVOTCOBRANZAIPF
				    0, -- PIVOTCAMBIOSUCURSALIPF
				    99, -- ORDINALIPFMORATORIO
				    0, -- PIVOTCOBRANZAIPFMORATORIO
				    0, -- PIVOTCAMBIOSUCURSALIPFMORATORIO
				    COALESCE((SELECT TO_NUMBER(VALOR) FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'COBRANZA_AUTOMATICA' AND PARAMETRO = 'PIVOT_PREVISION_PAGO_TOTAL'), 0), -- PIVOTCOBRANZATOTAL
				    0  -- PIVOTCAMBIOSUCURSALTOTAL
				);
			"""	
			
			this.addCommand( new ActualizarBaseDeDatosOracle(createSentence))
			this.addCommand( new ActualizarBaseDeDatosOracle(insertSentence))
		}
		
			
		/*
		 *Mappings 
		 */			
		
		def archivoMapp = "CambioSucursalMapping.xml"
		def entidades = """
			<class>
				    <class-name>topsystems.automaticprocess.cambiosucursal.objectsdomain.ParametrosPrejuizo</class-name>
				    <entity-name>core.vo_ParametrosPrejuizo</entity-name>
				    <table-name>PARAMETROS_PREJUIZO</table-name>
				    <database-name>TOP/CLIENTES</database-name>				
				    <attribute>
				        <attribute-name>ordinalInteres</attribute-name>
				        <column-name>ORDINALINTERES</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>pivotCobranzaInteres</attribute-name>
				        <column-name>PIVOTCOBRANZAINTERES</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>pivotCambioSucursalInteres</attribute-name>
				        <column-name>PIVOTCAMBIOSUCURSALINTERES</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>ordinalMora</attribute-name>
				        <column-name>ORDINALMORA</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>pivotCobranzaMora</attribute-name>
				        <column-name>PIVOTCOBRANZAMORA</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>pivotCambioSucursalMora</attribute-name>
				        <column-name>PIVOTCAMBIOSUCURSALMORA</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>ordinalMulta</attribute-name>
				        <column-name>ORDINALMULTA</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>pivotCobranzaMulta</attribute-name>
				        <column-name>PIVOTCOBRANZAMULTA</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>pivotCambioSucursalMulta</attribute-name>
				        <column-name>PIVOTCAMBIOSUCURSALMULTA</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>ordinalIpf</attribute-name>
				        <column-name>ORDINALIPF</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>pivotCobranzaIpf</attribute-name>
				        <column-name>PIVOTCOBRANZAIPF</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>pivotCambioSucursalIpf</attribute-name>
				        <column-name>PIVOTCAMBIOSUCURSALIPF</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>ordinalIpfMoratorio</attribute-name>
				        <column-name>ORDINALIPFMORATORIO</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>pivotCobranzaIpfMoratorio</attribute-name>
				        <column-name>PIVOTCOBRANZAIPFMORATORIO</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>pivotCambioSucursalIpfMoratorio</attribute-name>
				        <column-name>PIVOTCAMBIOSUCURSALIPFMORATORIO</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>pivotCobranzaTotal</attribute-name>
				        <column-name>PIVOTCOBRANZATOTAL</column-name>
				    </attribute>
				    <attribute>
				        <attribute-name>pivotCambioSucursalTotal</attribute-name>
				        <column-name>PIVOTCAMBIOSUCURSALTOTAL</column-name>
				    </attribute>
				</class>
			"""
		this.addCommand( new AgregarModificarEntidad(archivoMapp, entidades))
	}
	
	private void modificarServicio() {
		
		if (!isSicredi) {
			def archivo = "CambioSucursalDeploy.xml"
			def servicios = """
			   <service>
			      <name>topsystems:processManager:WorkDescriptor=WDCambioSucursal</name>
			      <code>topsystems.automaticprocess.cambiosucursal.WDCambioSucursalBankingBR</code>
			   </service>
			   <service>
			      <name>topsystems:processManager:BusinessWorker=BWCambioSucursal</name>
			      <code>topsystems.automaticprocess.cambiosucursal.BWCambioSucursalBankingBR</code>
			   </service>
			"""
			this.addCommand( new AgregarModificarServicios(archivo, servicios))
		}

	}	
	
	
	private boolean isSicredi(){
		def jarName = "sic_br_tb";
		def directoryLib = new File(CarpetasServidor.SERVER_LIB.getExpandedPath());
	
		if(directoryLib.exists() && directoryLib.isDirectory()){
			File[] filesJars = directoryLib.listFiles({ File dir, String file ->
				file.startsWith(jarName) && file.endsWith(".jar")
			} as FilenameFilter
			)
			if( filesJars.length > 0 ){
				return true;
			}
		}
		return false;
	}

}
