package Parches_ChequeEspecial

import topsystems.actualizador.parche.Parche
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoQuery
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.dataserver.ModificarConsultaQuery
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.configuration.CarpetasServidor

class Parche_NUPAGSEGUR_281 extends Parche{
	
	@Override
	protected void comandos() {
			//305
			agregarQuerys();
			insertParametrosJTS();
			cambiarQueryInteresesDeudores();
			cambiarQueryInteresesAD();
			agregarAttrQueryInteresesDeudores();
			agregarAttrQueryInteresesAD();
			agregarColumnaEnTablaHistorico();
			agregarAttrEntidadHistorico();
			
			// 412
			agregarCamposALabase();
			
			// 478
			cambiarQueryInteresesDeudoresUnicoSaldo();
			cambiarQueryInteresesADUnicoSaldo();
			agregarAttrQueryInteresesDeudoresUnicoSaldo();
			agregarAttrQueryInteresesADUnicoSaldo();
			agregarNuevosCampos();
			agregarNuevosCamposDatosSobregiros();
	}

	
	public void agregarQuerys(){
		// Agregar o sustituir una query (o varias) de DataMapping
	def archivoMapp = "interesesdeudoresmapping.xml"
	def queries = """
	<query>
		<class-name>topsystems.automaticprocess.interesesvista.objectsdomain.Ponderacion</class-name>
		<query-name>queryObtenerPonderacionesDeClasubjetiva</query-name>
		<database-name>TOP/CLIENTES</database-name>
		<sentence>	
			select CATEGORIASUB as CATEGORIA, PONDERACION
			  from CLI_CLASUBJETIVA			    
			 where TZ_LOCK = 0			
		</sentence>
		<attribute>
			<attribute-name>categoria</attribute-name>
			<column-name>categoria</column-name>
			<column-type>String</column-type>
		</attribute>
		<attribute>
			<attribute-name>ponderacion</attribute-name>
			<column-name>ponderacion</column-name>
			<column-type>long</column-type>
		</attribute>
	</query>
	 <query>
        <class-name>topsystems.automaticprocess.interesesvista.interesesdeudores.objectsdomain.HistoricoChequeEspecial</class-name>
        <query-name>query.historicoSuspenso</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>			
			SELECT FECHA_PROCESO, VDS_SUSPENSO_POR_CALIF from VTA_HIST_CHEQUE_ESPECIAL 
			WHERE FECHA_PROCESO &gt; = ? AND FECHA_PROCESO &lt;= ?
			AND SALDO_JTS_OID = ?			
		</sentence>
        <attribute>
            <attribute-name>fechaProceso</attribute-name>
            <column-name>FECHA_PROCESO</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>suspensoPorCalificacion</attribute-name>
            <column-name>VDS_SUSPENSO_POR_CALIF</column-name>
            <column-type>String</column-type>
        </attribute>
    </query>
	<query>
        <class-name>topsystems.automaticprocess.interesesvista.interesesdeudores.objectsdomain.HistoricoChequeEspecial</class-name>
		<query-name>query.HistoricoChequeEspecialBR</query-name>
		<database-name>TOP/CLIENTES</database-name>
		<sentence>		
			SELECT 
			s.JTS_OID AS SALDO_JTS_OID,
			s.C1822 AS SCC_C1822,
			s.C1813 AS SCC_C1813,
			s.C1820 AS SCC_C1820,
			s.C1821 AS SCC_C1821,
			s.C1806 AS SCC_C1806,
			s.ULT_FECHA_VIGENTE AS SCC_ULT_FECHA_VIGENTE,
			s.FECHA_COBRO_IOF_SOBREGIROS AS SCC_FECHA_COBRO_IOF_SOBREGIRO,
			s.C2627 AS SCC_C2627,
			s.C1604 AS SCC_C1604,
			s.C1729 as SCC_C1729,
			s.FCAMBIOESTADOSUSPENSO as SCC_FCAMBIOESTADOSUSPENSO,
			VDS.FECHA_COBRO_MULTA AS 	VDS_FECHA_COBRO_MULTA,
			VDS.FECHA_DEVENGAMIENTO_AD AS 	VDS_FECHA_DEVENGAMIENTO_AD,
			VDS.FECHA_CONTABILIZADO_COBRO_AD AS 	VDS_FECHA_CONTABILIZADO_COBRO_AD,
			VDS.DEVENGADO_COBRO_AD AS 	VDS_DEVENGADO_COBRO_AD,
			VDS.DEVENGADO_ANTERIOR_COBRO_AD	AS VDS_DEVENGADO_ANTERIOR_COBRO_AD,
			VDS.DEVENGADO_59_DIAS_AD AS 	VDS_DEVENGADO_59_DIAS_AD,
			VDS.DIAS_SOBREGIRO AS 	VDS_DIAS_SOBREGIRO,
			VDS.DIAS_AD AS 	VDS_DIAS_AD,
			--VDS.JTS_SALDO_DEUDA AS 	VDS_JTS_SALDO_DEUDA,
			VDS.FECHA_INGRESO_CHEQUE_ESPECIAL	AS VDS_FECH_INGR_CHEQ_ESPECIAL,
			VDS.FECHA_INGRESO_AD  AS 	VDS_FECHA_INGRESO_AD,
			VDS.SUSPENSO_POR_CALIF AS VDS_SUSPENSO_POR_CALIF,
			VDS.FECHA_SUSPENSO_CALIF AS VDS_FECHA_SUSPENSO_CALIF,
			VDS.FECHA_SUSPENSO_ATRASO AS VDS_FECHA_SUSPENSO_ATRASO,																							   
			VS.FECHA_ALTA  AS 	VS_FECHA_ALTA,
			VS.FECHA_VENCIMIENTO  AS 	VS_FECHA_VENCIMIENTO,
			VS.ESTADO AS VS_ESTADO,
			VS.PERIODO AS 	VS_PERIODO,
			VS.IMPORTE AS 	VS_IMPORTE,
			VS.DIA_COBRO   AS  	VS_DIA_COBRO,
			VS.TIPO_DIA_COBRO  AS 	VS_TIPO_DIA_COBRO,
			VS.DIAS_CARENCIA   AS  	VS_DIAS_CARENCIA,
			VS.JTS_SALDO_DEUDA AS 	VS_JTS_SALDO_DEUDA,
			VS.NRO_AUTORIZACION	AS VS_NRO_AUTORIZACION,
			
			VDS.INT_VENCIDO_NO_COBRADO AS VDS_INT_VENCIDO_NO_COBRADO,
			VDS.INT_VIGENTE_NO_COBRADO AS VDS_INT_VIGENTE_NO_COBRADO,
			VDS.INT_VENCIDO_NO_COBRADO_AD AS VDS_INT_VENCIDO_NO_COBRADO_AD,
			VDS.INT_VIGENTE_NO_COBRADO_AD AS VDS_INT_VIGENTE_NO_COBRADO_AD,
			
			CI.IMPORTE	 AS IOF_SUSPENSO_PENDIENTE_COBRO
			
			FROM (SELECT * FROM SALDOS WHERE C1785 = 2  AND C1651 = ' ' AND tz_lock=0) s
			LEFT JOIN 
			VTA_SOBREGIROS vs ON s.JTS_OID = vs.JTS_OID_SALDO AND vs.TZ_LOCK = 0
			LEFT JOIN 
			VTA_DATOS_SOBREGIROS vds ON s.JTS_OID = vds.JTS_OID_SALDO AND vds.TZ_LOCK = 0
			LEFT JOIN 
			(SELECT  SALDO_JTS_OID, SUM(IMPORTE) AS IMPORTE FROM CI_SOLICITUD WHERE tz_lock=0  AND ID_EVENTO = ( SELECT valor FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'COBRO_IOF_SOBREGIROS' AND PARAMETRO = 'EVENTO_IOF_SUSPENSO')
			AND ESTADO = 0
			GROUP BY SALDO_JTS_OID) ci
			ON s.JTS_OID = ci.SALDO_JTS_OID
	</sentence>
        <attribute>
            <attribute-name>saldoJtsOid</attribute-name>
            <column-name>SALDO_JTS_OID</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaContabilizadoCobroSCC</attribute-name>
            <column-name>SCC_C1822</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaDevengamientoSCC</attribute-name>
            <column-name>SCC_C1813</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>devengadoCobroSCC</attribute-name>
            <column-name>SCC_C1820</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>devengadoAntCobroSCC</attribute-name>
            <column-name>SCC_C1821</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>interesesHasta59DiasSCC</attribute-name>
            <column-name>SCC_C1806</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>ultFechaVigente</attribute-name>
            <column-name>SCC_ULT_FECHA_VIGENTE</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaUltPagoIOF</attribute-name>
            <column-name>SCC_FECHA_COBRO_IOF_SOBREGIRO</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>reservaNegocio</attribute-name>
            <column-name>SCC_C2627</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>saldoActual</attribute-name>
            <column-name>SCC_C1604</column-name>
			<column-type>double</column-type>
        </attribute>
		<attribute>
            <attribute-name>estadoSupensoSCC</attribute-name>
            <column-name>SCC_C1729</column-name>
			<column-type>String</column-type>
        </attribute>
		<attribute>
            <attribute-name>fechaSupensoSCC</attribute-name>
            <column-name>SCC_FCAMBIOESTADOSUSPENSO</column-name>
			<column-type>Date</column-type>
        </attribute>	  
        <attribute>
            <attribute-name>fechaCobroMulta</attribute-name>
            <column-name>VDS_FECHA_COBRO_MULTA</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaDevengamientoAD</attribute-name>
            <column-name>VDS_FECHA_DEVENGAMIENTO_AD</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaContabilizadoCobroAD</attribute-name>
            <column-name>VDS_FECHA_CONTABILIZADO_COBRO_AD</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>devengadoCobroAD</attribute-name>
            <column-name>VDS_DEVENGADO_COBRO_AD</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>devengadoAnteriorCobroAD</attribute-name>
            <column-name>VDS_DEVENGADO_ANTERIOR_COBRO_AD</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>devengado59DiasAD</attribute-name>
            <column-name>VDS_DEVENGADO_59_DIAS_AD</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>diasSobregiro</attribute-name>
            <column-name>VDS_DIAS_SOBREGIRO</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>diasAD</attribute-name>
            <column-name>VDS_DIAS_AD</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaIngresoChequeEspecial</attribute-name>
            <column-name>VDS_FECH_INGR_CHEQ_ESPECIAL</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaIngresoAD</attribute-name>
            <column-name>VDS_FECHA_INGRESO_AD</column-name>
			<column-type>Date</column-type>
        </attribute>
		<attribute>
            <attribute-name>suspensoPorCalificacion</attribute-name>
            <column-name>VDS_SUSPENSO_POR_CALIF</column-name> 
            <column-type>String</column-type>
        </attribute>
		<attribute>
            <attribute-name>fechaSuspensoPorCalif</attribute-name>
            <column-name>VDS_FECHA_SUSPENSO_CALIF</column-name> 
            <column-type>Date</column-type>
        </attribute>
		<attribute>
            <attribute-name>fechaSuspensoAtraso</attribute-name>
            <column-name>VDS_FECHA_SUSPENSO_ATRASO</column-name> 
            <column-type>Date</column-type>
        </attribute>  
        <attribute>
            <attribute-name>fechaAltaCheqEsp</attribute-name>
            <column-name>VS_FECHA_ALTA</column-name>
			<column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaVencimientoCheqEsp</attribute-name>
            <column-name>VS_FECHA_VENCIMIENTO</column-name>
			<column-type>Date</column-type>
        </attribute>
		<attribute>
            <attribute-name>estadoSobregiro</attribute-name>
            <column-name>VS_ESTADO</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>periodo</attribute-name>
            <column-name>VS_PERIODO</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>montoSobregiro</attribute-name>
            <column-name>VS_IMPORTE</column-name>
			<column-type>double</column-type>
        </attribute>
		<attribute>
            <attribute-name>diaCobro</attribute-name>
            <column-name>VS_DIA_COBRO</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>tipoDiaCobro</attribute-name>
            <column-name>VS_TIPO_DIA_COBRO</column-name>
			<column-type>String</column-type>
        </attribute>
		<attribute>
            <attribute-name>diasCadencia</attribute-name>
            <column-name>VS_DIAS_CARENCIA</column-name>
			<column-type>long</column-type>
        </attribute>
		<attribute>
            <attribute-name>jtsSaldoDeudaVS</attribute-name>
            <column-name>VS_JTS_SALDO_DEUDA</column-name>
			<column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>autorization</attribute-name>
            <column-name>VS_NRO_AUTORIZACION</column-name>
			<column-type>long</column-type>
        </attribute>
		<attribute>
            <attribute-name>intVencidoNoCobrado</attribute-name>
            <column-name>VDS_INT_VENCIDO_NO_COBRADO</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>intVigenteNoCobrado</attribute-name>
            <column-name>VDS_INT_VIGENTE_NO_COBRADO</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>intVencidoNoCobradoAD</attribute-name>
            <column-name>VDS_INT_VENCIDO_NO_COBRADO_AD</column-name>
			<column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>intVigenteNoCobradoAD</attribute-name>
            <column-name>VDS_INT_VIGENTE_NO_COBRADO_AD</column-name>
			<column-type>double</column-type>
        </attribute>
		<attribute>
            <attribute-name>IOFSuspensoPendienteCobro</attribute-name>
            <column-name>IOF_SUSPENSO_PENDIENTE_COBRO</column-name>
			<column-type>double</column-type>
        </attribute>
		</query>
		 <query>
    			<class-name>topsystems.automaticprocess.interesesvista.interesesdeudores.objectsdomain.QueryUltimoNumeralBr</class-name>
    			<query-name>queryUltFechaHistorico</query-name>
    			<database-name>TOP/CLIENTES</database-name>
    			<sentence>
						SELECT MAX(FECHA_PROCESO) AS FECHA
					 	 FROM VTA_HIST_CHEQUE_ESPECIAL
						 WHERE FECHA_PROCESO &lt; ?
					   	AND SALDO_JTS_OID = ?
				</sentence>
    			<attribute>
       				 <attribute-name>fecha</attribute-name>
        			 <column-name>FECHA</column-name>
        			 <column-type>Date</column-type>
    			</attribute>
		</query>
	"""
	this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}
	
	public void cambiarQueryInteresesDeudores(){
	// Sustituir consulta SQL de query de DataMapping
	def archivoMapp = "interesesdeudoresmapping.xml"
	def query = "query.QuerySaldosCobroDeudoresSG"
	def consultaSQL = """
							SELECT S.C1620 AS FECHA_INICIO,
							   S.C1621 AS FECHA_VALOR,
							   S.C1604 AS SALDO_ACTUAL,
							   S.JTS_OID,
							   S.C1813 AS FECHA_DEVENGADO,
							   S.C1820 AS DEVENGADO_COBRO,
							   S.C1822 AS FECHA_COBRO,
							   V.NRO_AUTORIZACION,
							   V.ORDINAL,
							   V.IMPORTE AS MONTO_ACUERDO,
							   V.JTS_OID_CONTINGENCIA,
							   V.FECHA_VENCIMIENTO,
							   V.JTS_SALDO_DEUDA,
							   V.DIA_COBRO,
							   V.DIAS_CARENCIA,
							   V.FECHA_ALTA,	
					   		   V.TIPO_DIA_COBRO,
					   		   C.CATEGORIARESULTANTE,
					   		   C.FECHACAMBIOCATEGORIA
							FROM SALDOS S
						    INNER JOIN VTA_SOBREGIROS V ON S.JTS_OID = V.JTS_OID_SALDO
							INNER JOIN CLI_CLIENTES C ON C.CODIGOCLIENTE = S.C1803
						  AND S.C1651 = ' ' and S.C1728 NOT IN ('C','U','W')
								 AND S.TZ_LOCK = 0
								 AND V.TZ_LOCK = 0
								 AND V.ESTADO = 1
						 ORDER BY S.JTS_OID
	"""
	this.addCommand( new ModificarConsultaQuery(archivoMapp, query, consultaSQL))
	}

	private void cambiarQueryInteresesAD(){
		// Sustituir consulta SQL de query de DataMapping
		def archivoMapp = "interesesdeudoresmapping.xml"
		def query = "query.QuerySaldosCobroDeudoresAD"
		def consultaSQL = """
				SELECT S.C1620 AS FECHA_INICIO,
					   S.C1621 AS FECHA_VALOR,
					   S.C1604 AS SALDO_ACTUAL,
					   S.JTS_OID,
					   S.C1813 AS FECHA_DEVENGADO,
					   S.C1820 AS DEVENGADO_COBRO,
					   S.C1822 AS FECHA_COBRO,
					   V.NRO_AUTORIZACION,
					   V.ORDINAL,
					   V.IMPORTE AS MONTO_ACUERDO,
					   V.JTS_OID_CONTINGENCIA,
					   V.FECHA_VENCIMIENTO,
					   V.JTS_SALDO_DEUDA,
					   V.DIA_COBRO,
					   V.DIAS_CARENCIA,
					   V.FECHA_ALTA,  
					   V.TIPO_DIA_COBRO,
			   		   C.CATEGORIARESULTANTE,
			   		   C.FECHACAMBIOCATEGORIA
				  FROM SALDOS S             
				  LEFT JOIN VTA_SOBREGIROS V ON S.JTS_OID = V.JTS_OID_SALDO
				        AND V.TZ_LOCK = 0
					INNER JOIN CLI_CLIENTES C ON C.CODIGOCLIENTE = S.C1803
				 WHERE S.PRODUCTO IN ( 102,103,107 ) AND S.EMPRESA = 300
				   AND S.C1651 = ' ' and S.C1728 NOT IN ('C','U','W')
				   AND S.C1785 = 2
				   AND S.TZ_LOCK = 0
				   AND V.ESTADO = 1
				 ORDER BY S.JTS_OID
		"""
		this.addCommand( new ModificarConsultaQuery(archivoMapp, query, consultaSQL))
	}
	
	public void agregarAttrQueryInteresesDeudores(){
		// Agregar o sustituir uno (o varios) atributos de una query de DataMapping
		def archivoMapp = "interesesdeudoresmapping.xml"
		def query = "query.QuerySaldosCobroDeudoresSG"
		def atributo = """
					<attribute>
			            <attribute-name>calificacionResultante</attribute-name>
			            <column-name>CATEGORIARESULTANTE</column-name>
			            <column-type>String</column-type>
			        </attribute>
			        <attribute>
			            <attribute-name>fechaCalifResultante</attribute-name>
			            <column-name>FECHACAMBIOCATEGORIA</column-name>
			            <column-type>Date</column-type>
			        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoQuery(archivoMapp, query, atributo))
	}
	
	public void agregarAttrQueryInteresesAD(){
	// Agregar o sustituir uno (o varios) atributos de una query de DataMapping
	def archivoMapp = "interesesdeudoresmapping.xml"
	def query = "query.QuerySaldosCobroDeudoresAD"
	def atributo = """
				<attribute>
		            <attribute-name>calificacionResultante</attribute-name>
		            <column-name>CATEGORIARESULTANTE</column-name>
		            <column-type>String</column-type>
		        </attribute>
		        <attribute>
		            <attribute-name>fechaCalifResultante</attribute-name>
		            <column-name>FECHACAMBIOCATEGORIA</column-name>
		            <column-type>Date</column-type>
		        </attribute>
	"""
	this.addCommand( new AgregarModificarAtributoQuery(archivoMapp, query, atributo))
	}

	public void insertParametrosJTS(){
		def sentencia = """
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
		VALUES ('COBRO_INTERESES_DEUDORES', 'LIMITE_CALIF_RESULTANTE_DEUDA', ' ' , 0);
		"""
		def descripcion = "Agrega parametro LIMITE_CALIF_RESULTANTE_DEUDA a la funcionlidad:COBRO_INTERESES_DEUDORES en la tabla PARAMETROS_JTS"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	
	public void agregarColumnaEnTablaHistorico(){
		def sentencia = """
		ALTER TABLE VTA_HIST_CHEQUE_ESPECIAL ADD SCC_C1729 VARCHAR2(1) DEFAULT (' ');
		"""
		def descripcion = "Agrega columna SCC_C1729 en la tabla VTA_HIST_CHEQUE_ESPECIAL "
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	public void agregarAttrEntidadHistorico(){
	 // Agregar o sustituir uno (o varios) atributos de una entidad de DataMapping
	def archivoMapp = "interesesdeudoresmapping.xml"
	def entidad = "core.vo_HistoricoChequeEspecial"
	def atributo = """
		<attribute>
            <attribute-name>estadoSupensoSCC</attribute-name>
            <column-name>SCC_C1729</column-name>
        </attribute>
		<attribute>
            <attribute-name>fechaSupensoSCC</attribute-name>
            <column-name>SCC_FCAMBIOESTADOSUSPENSO</column-name>
        </attribute>
		<attribute>
	 		<attribute-name>suspensoPorCalificacion</attribute-name>
	 		<column-name>VDS_SUSPENSO_POR_CALIF</column-name>
		</attribute>
		<attribute>
			 <attribute-name>fechaSuspensoPorCalif</attribute-name>
			 <column-name>VDS_FECHA_SUSPENSO_CALIF</column-name>
		</attribute>
		<attribute>
			 <attribute-name>fechaSuspensoAtraso</attribute-name>
			 <column-name>VDS_FECHA_SUSPENSO_ATRASO</column-name>
		</attribute>
	"""
	this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
	}
	
	public void agregarCamposALabase(){
		def sentencia = """
		ALTER TABLE VTA_HIST_CHEQUE_ESPECIAL ADD SCC_FCAMBIOESTADOSUSPENSO DATE;
		"""
		def descripcion = "Agrega campo FCAMBIOESTADOSUSPENSO A la tabla de historico" // La descripción es opcional
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	public void cambiarQueryInteresesDeudoresUnicoSaldo(){
		// Sustituir consulta SQL de query de DataMapping
		def archivoMapp = "interesesdeudoresmapping.xml"
		def query = "query.QuerySaldosCobroDeudoresSGUnicoSaldo"
		def consultaSQL = """
			  SELECT S.C1620 AS FECHA_INICIO,
				   S.C1621 AS FECHA_VALOR,
				   S.C1604 AS SALDO_ACTUAL,
				   S.JTS_OID,
				   S.C1813 AS FECHA_DEVENGADO,
				   S.C1820 AS DEVENGADO_COBRO,
				   S.C1822 AS FECHA_COBRO,
				   V.NRO_AUTORIZACION,
				   V.ORDINAL,
				   V.IMPORTE AS MONTO_ACUERDO,
				   V.JTS_OID_CONTINGENCIA,
				   V.FECHA_VENCIMIENTO,
				   V.JTS_SALDO_DEUDA,
				   V.DIA_COBRO,
				   V.DIAS_CARENCIA,
				   V.FECHA_ALTA,	
		   		   V.TIPO_DIA_COBRO,
				   C.CATEGORIARESULTANTE,
		   		   C.FECHACAMBIOCATEGORIA
				FROM SALDOS S
			    INNER JOIN VTA_SOBREGIROS V ON S.JTS_OID = V.JTS_OID_SALDO
				INNER JOIN CLI_CLIENTES C ON C.CODIGOCLIENTE = S.C1803
			  		 AND S.C1651 = ' ' and S.C1728 NOT IN ('C','U','W')
					 AND S.TZ_LOCK = 0
					 AND V.TZ_LOCK = 0
					 AND V.ESTADO = 1
					 AND S.JTS_OID = ? 
			 	ORDER BY S.JTS_OID
	"""
	this.addCommand( new ModificarConsultaQuery(archivoMapp, query, consultaSQL))
	}
	public void cambiarQueryInteresesADUnicoSaldo(){
		// Sustituir consulta SQL de query de DataMapping
		def archivoMapp = "interesesdeudoresmapping.xml"
		def query = "query.QuerySaldosCobroDeudoresADUnicoSaldo"
		def consultaSQL = """
			 SELECT S.C1620 AS FECHA_INICIO,
				   S.C1621 AS FECHA_VALOR,
				   S.C1604 AS SALDO_ACTUAL,
				   S.JTS_OID,
				   S.C1813 AS FECHA_DEVENGADO,
				   S.C1820 AS DEVENGADO_COBRO,
				   S.C1822 AS FECHA_COBRO,
				   V.NRO_AUTORIZACION,
				   V.ORDINAL,
				   V.IMPORTE AS MONTO_ACUERDO,
				   V.JTS_OID_CONTINGENCIA,
				   V.FECHA_VENCIMIENTO,
				   V.JTS_SALDO_DEUDA,
				   V.DIA_COBRO,
				   V.DIAS_CARENCIA,
				   V.FECHA_ALTA,  
				   V.TIPO_DIA_COBRO,
				   C.CATEGORIARESULTANTE,
		   		   C.FECHACAMBIOCATEGORIA
			  FROM SALDOS S             
			  LEFT JOIN VTA_SOBREGIROS V ON S.JTS_OID = V.JTS_OID_SALDO
			        AND V.TZ_LOCK = 0
			  INNER JOIN CLI_CLIENTES C ON C.CODIGOCLIENTE = S.C1803
			 WHERE S.PRODUCTO IN ( 102,103,107 ) AND S.EMPRESA = 300
			   AND S.C1651 = ' ' and S.C1728 NOT IN ('C','U','W')
			   AND S.C1785 = 2
			   AND S.TZ_LOCK = 0
			   AND V.ESTADO = 1
			   AND S.JTS_OID = ?
			 ORDER BY S.JTS_OID
	"""
	this.addCommand( new ModificarConsultaQuery(archivoMapp, query, consultaSQL))
	}
	
	public void agregarAttrQueryInteresesDeudoresUnicoSaldo(){
		// Agregar o sustituir uno (o varios) atributos de una query de DataMapping
		def archivoMapp = "interesesdeudoresmapping.xml"
		def query = "query.QuerySaldosCobroDeudoresSGUnicoSaldo"
		def atributo = """
					<attribute>
			            <attribute-name>calificacionResultante</attribute-name>
			            <column-name>CATEGORIARESULTANTE</column-name>
			            <column-type>String</column-type>
			        </attribute>
			        <attribute>
			            <attribute-name>fechaCalifResultante</attribute-name>
			            <column-name>FECHACAMBIOCATEGORIA</column-name>
			            <column-type>Date</column-type>
			        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoQuery(archivoMapp, query, atributo))
	}
	
	public void agregarAttrQueryInteresesADUnicoSaldo(){
		// Agregar o sustituir uno (o varios) atributos de una query de DataMapping
		def archivoMapp = "interesesdeudoresmapping.xml"
		def query = "query.QuerySaldosCobroDeudoresADUnicoSaldo"
		def atributo = """
					<attribute>
			            <attribute-name>calificacionResultante</attribute-name>
			            <column-name>CATEGORIARESULTANTE</column-name>
			            <column-type>String</column-type>
			        </attribute>
			        <attribute>
			            <attribute-name>fechaCalifResultante</attribute-name>
			            <column-name>FECHACAMBIOCATEGORIA</column-name>
			            <column-type>Date</column-type>
			        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoQuery(archivoMapp, query, atributo))
	}
	
	public void agregarNuevosCampos(){
		def sentencia = """
		ALTER TABLE VTA_DATOS_SOBREGIROS ADD SUSPENSO_POR_CALIF VARCHAR2(1);
		ALTER TABLE VTA_DATOS_SOBREGIROS ADD FECHA_SUSPENSO_CALIF DATE;
		ALTER TABLE VTA_DATOS_SOBREGIROS ADD FECHA_SUSPENSO_ATRASO DATE;
		
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
		VALUES ('COBRO_INTERESES_DEUDORES', 'ACTUALIZA_ATIVO_PROBLEMATICO', 'N', 0);
		
		ALTER TABLE VTA_HIST_CHEQUE_ESPECIAL ADD VDS_SUSPENSO_POR_CALIF VARCHAR2(1);
		ALTER TABLE VTA_HIST_CHEQUE_ESPECIAL ADD VDS_FECHA_SUSPENSO_CALIF DATE;
		ALTER TABLE VTA_HIST_CHEQUE_ESPECIAL ADD VDS_FECHA_SUSPENSO_ATRASO DATE;
		 """
		def descripcion = "Sobre la sentencia"
		this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	}
	
	private void agregarNuevosCamposDatosSobregiros() {
		def archivoMapp = "interesesdeudoresmapping.xml"
		def entidad = "core.vo.DatosSobregiros"
		def atributo = """
		<attribute>
		 <attribute-name>suspensoPorCalificacion</attribute-name>
		 <column-name>SUSPENSO_POR_CALIF</column-name>
		</attribute>
		<attribute>
		 <attribute-name>fechaSuspensoPorCalif</attribute-name>
		 <column-name>FECHA_SUSPENSO_CALIF</column-name>
		</attribute>
		<attribute>
		 <attribute-name>fechaSuspensoAtraso</attribute-name>
		 <column-name>FECHA_SUSPENSO_ATRASO</column-name>
		</attribute>
		"""
	this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
	}
	
}
