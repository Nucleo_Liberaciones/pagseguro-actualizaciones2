import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.parche.Parche

class Parche_NUNBCHACO_1365 extends Parche{

	@Override
	protected void comandos() throws TopazException {
		addDataMappingQuery();
	}
	
	private void addDataMappingQuery(){
		// Agregar o sustituir una query (o varias) de DataMapping
		def archivoMapp = "PosMapping.xml"
		def queries = """
		<query>
	        <class-name>topsystems.pos.automaticprocess.conciliacion.QueryResumenTPVO</class-name>
	        <query-name>query.QueryResumenTP</query-name>
	        <database-name>TOP/CLIENTES</database-name>
	        <sentence>
				SELECT  TopazBranch, 
					TopazPostingNumber, 
					FechaMensaje,  
					element3, 
					element4,  
					element13, 
					element37, 
					element39, 
					element41
				FROM TP_TOPAZPOSCONTROL  
				WHERE ELEMENT0  = '0210'
				AND   ELEMENT12 = ?
				AND   ELEMENT13 = ?
				AND   ELEMENT37 = ?
				AND   ELEMENT41 = ?
				AND   ELEMENT66 = '3'
			</sentence>
	        <attribute>
	            <attribute-name>TopazBranch</attribute-name>
	            <column-name>TopazBranch</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>TopazPostingNumber</attribute-name>
	            <column-name>TopazPostingNumber</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>messageDate</attribute-name>
	            <column-name>FechaMensaje</column-name>
	            <column-type>Timestamp</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>element3</attribute-name>
	            <column-name>element3</column-name>
	            <column-type>string</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>element4</attribute-name>
	            <column-name>element4</column-name>
	            <column-type>string</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>element13</attribute-name>
	            <column-name>element13</column-name>
	            <column-type>string</column-type>
	        </attribute>
			<attribute>
	            <attribute-name>element37</attribute-name>
	            <column-name>element37</column-name>
	            <column-type>string</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>element39</attribute-name>
	            <column-name>element39</column-name>
	            <column-type>string</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>element41</attribute-name>
	            <column-name>element41</column-name>
	            <column-type>string</column-type>
	        </attribute>        
	    </query>
		"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))

	}
}