package topsystems.actualizador.parche

import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.servicios.BorrarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.CarpetasServidor
import topsystems.actualizador.configuration.Client
import topsystems.actualizador.configuration.app.ConfigurationAppManager
import topsystems.actualizador.parche.Parche;
import topsystems.TopazException
import topsystems.actualizador.command.ActualizacionCommandResult
import topsystems.actualizador.command.processmanager.ProcessManagerProcessCommand
import topsystems.actualizador.exception.HakunaException
import topsystems.actualizador.util.PathUtil

class Patch_NUTOPAZ56_166 extends Parche{

	public void sobreElParche() {
		this.notas_del_autor = "Patch_NUTOPAZ56_166_DevengamientoUnificado";
		this.opcional = false;
	}

	@Override
	protected void comandos() {
		
		if(!isAGB()){
		
		addNewAutomaticProcess()
		
		def archivoMapp = "DevengamientoMapping.xml"
		def entidad = "core.vo_SaldoDevengar"
		def atributo = """
	    <attribute>
            <attribute-name>SAL_FECHA_CONT_DEVENGADO</attribute-name>
            <column-name>FECHA_CONT_DEVENGADO</column-name>
        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
		
		
		// Agregar o sustituir una query (o varias) de DataMapping
		archivoMapp = "DevengamientoMapping.xml"
		def queries = """
    <query>
        <class-name>topsystems.automaticprocess.devengamiento.calculo.SeleccionDevengamiento</class-name>
        <query-name>query.devengamiento.seleccionDevengamientoDiferencia</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
				select distinct JTS_OID, 'TODOS' AS DIFERENCIA, 0 as NRO_CUOTA -- query original del calculo devengado por extorno
				       from saldos,planctas 
				       where 
				       c1621 &lt;= (select FechaProceso from parametros) and 
				       ((c1604 &lt; 0 and (C1785 = 5 or C1785 = 6)) or 
				       (c1604 &gt; 0 and (C1785 = 4))) and 
				       saldos.Tz_lock = 0 and 
				       (C1813 &lt; (select FechaProceso from parametros) or
				       C1813 is null) AND
				       c6326 = c1730 AND -- saldo con plan de cuentas
				       C6318 &lt;&gt; 'N' -- rubro califica para el devengamiento
		UNION -- parte de la querry para cargar en el historico devengamioento los campos con los pivot en 0 de prestamos cancelados o castigados.
			    select JTS_OID, 'POR_DIFERENCIA' AS DIFERENCIA, hd.NRO_CUOTA AS NRO_CUOTA
				    FROM SALDOS s INNER JOIN historico_devengamiento hd 
					ON (s.jts_oid =hd.SALDO_JTS_OID AND hd.fecha = s.C1813) --saldos devengados
					INNER JOIN planctas p ON (s.C1730 = p.c6326)-- saldo con plan de cuentas		 
					WHERE	C1785 IN (4,5,6)  AND (
					C1604=0 and C1813 &lt; C1625 
					AND C1625 &lt;= (SELECT FECHAPROCESO FROM PARAMETROS)--salods que son de tipo plazo cancelados
					OR
					( p.C6318 = 'N')--salods que son de tipo plazo castigados en la fecha FECULTCATEG 
					)		
					AND --nos fijamos que los pivot no esten en 0 (Puesto que si estan en 0 no es necesario q)
					NOT (hd.INTERES_DEVENG_VIGENTE_CONT= 0 AND hd.INTERES_DEVENG_VENCIDO_CONT = 0 AND INTERES_DEVENG_SUSPENSO_CONT =0
					AND hd.MORA_DEVENG_VIGENTE_CONT =0 AND hd.MORA_DEVENG_VENCIDO_CONT = 0 AND hd.MORA_DEVENG_SUSPENSO_CONT =0 AND
					hd.MORA_TASA_INT_DEVENG_VIGE_CONT = 0 AND hd.MORA_TASA_INT_DEVENG_VENC_CONT= 0 AND hd.MORA_TASA_INT_DEVENG_SUSP_CONT = 0
					AND hd.ICMORA_CONTABILIZADO = 0)			
		</sentence>
        <attribute>
            <attribute-name>JTS_OID</attribute-name>
            <column-name>JTS_OID</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>tipoProcesamientoSaldo</attribute-name>
            <column-name>DIFERENCIA</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>nro_cuota</attribute-name>
            <column-name>NRO_CUOTA</column-name>
            <column-type>long</column-type>
        </attribute>
    </query>
"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
		
		// Agregar o sustituir uno o varios servicios en un mismo archivo
		def archivoServ = "DevengamientoDeploy.xml"
		def servicios = """
   <service>
      <name>topsystems:service:core=DevengamientoPlazoWDLogic</name>
      <code>topsystems.automaticprocess.devengamiento.DevengamientoPlazoWDLogicImpl</code>
      <code-instruction></code-instruction>
      <movement-filler></movement-filler>
   </service>
   <service>
      <name>topsystems:processManager:WorkDescriptor=WorkDescriptorDevengamientoPlazo</name>
      <code>topsystems.automaticprocess.devengamiento.unificado.WorkDescriptorDevengamientoPlazo</code>
      <code-instruction></code-instruction>
      <movement-filler></movement-filler>
   </service>
   <service>
      <name>topsystems:processManager:BusinessWorker=BusinessWorkerDevengamientoPlazo</name>
      <code>topsystems.automaticprocess.devengamiento.unificado.BusinessWorkerDevengamientoPlazo</code>
      <code-instruction></code-instruction>
      <movement-filler></movement-filler>
   </service>
   <service>
      <name>topsystems:processManager:ResultHandler=ResultHandlerDevengamientoPlazo</name>
      <code>topsystems.automaticprocess.devengamiento.unificado.ResultHandlerDevengamientoPlazo</code>
      <code-instruction></code-instruction>
      <movement-filler></movement-filler>
   </service>
"""
		this.addCommand( new AgregarModificarServicios(archivoServ, servicios))
		
		// Agregar o sustituir un grupo
		def archivo = "groups.py"
		def grupo = """
    gdef = GroupDefinition("CBL - Devengamiento Unificado")
    gdef.registerProcess("DevengamientoPlazo")
    gdef.setCanBeRootGroup("true")
    gdef.setSingleton("true")
    gdefs.addGroup(gdef)
"""
		this.addCommand( new AgregarModificarGrupo(archivo, grupo))

		}
	}
	
	private void addNewAutomaticProcess(){
		// Agregar o sustituir un proceso
		def archivo = "processes.py"
		def procesos = """
		pdef = ProcessDefinition("DevengamientoPlazo", "topsystems.automaticprocess.processmanager.WorkManager")
		pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WorkDescriptorDevengamientoPlazo");
		pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BusinessWorkerDevengamientoPlazo");
		pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDevengamientoPlazo");
		${getMergeConstant()}		
		pdefs.addProcess(pdef)
		"""
		System.out.println(procesos);
		
		this.addCommand(new AgregarModificarProceso(archivo, procesos))

	}
	
	private String getMergeConstant(){
		FindAutomaticProcess findAutomaticProcess = new FindAutomaticProcess();
		String automaticProcessContabilizacionFound = findAutomaticProcess.findProcess("Devengamiento Contabilizacion");
		String automaticProcessDevengamientoFound = findAutomaticProcess.findProcess("Devengamiento Calculo y Actualizacion");

		String constants = mergeConstants2(getConstants(automaticProcessContabilizacionFound),
				getConstants(automaticProcessDevengamientoFound));
		
	}
	
	public class FindAutomaticProcess extends ProcessManagerProcessCommand {
		
		public String findProcess(String processName) {
			try {
				String fullPathFileName = getFileName("processes.py", CarpetasServidor.TOPAZ_PROCESS.getExpandedPath());
				String automaticProcessFound = super.findProcess(fullPathFileName, processName);

				if(automaticProcessFound.isEmpty()){
					throw new HakunaException("Process with name: '" + processName +"' not found." );
				}

				return automaticProcessFound;
			} catch (TopazException e) {
				throw new HakunaException(e);
			}
		}

		@Override
		public String getSummary() {
			return "FindAutomaticProcess";
		}

		@Override
		public String getTitulo() {
			return "FindAutomaticProcess";
		}

		@Override
		public ActualizacionCommandResult execute() {
			return new ActualizacionCommandResult();
		}

		@Override
		public ActualizacionCommandResult simulation() {
			return new ActualizacionCommandResult();
		}
		
	}
	
	public class AutomaticProcessConstant {
		private final String key;
		private final String value;

		public AutomaticProcessConstant(String key, String value) {
			super();
			this.key = key;
			this.value = value;
		}

		public String getKey() {
			return key;
		}

		public String getValue() {
			return value;
		}

	}
	
	/**
	 * Realiza merge de ambas listas de constantes
	 * la lista numero uno tiene mas peso que la lista numero dos
	 * @param constantsProcess1
	 * @param constantsProcess2
	 * @return
	 */
	public String mergeConstants2(List<AutomaticProcessConstant> constantsProcess1, List<AutomaticProcessConstant> constantsProcess2){
		List<AutomaticProcessConstant> constantsMerge = new ArrayList<>(constantsProcess1);

		constantsProcess2.each {s ->
			if (!constantsMerge.any {x -> x.key.equalsIgnoreCase(s.key)}) {
				constantsMerge.add(s)
			}
		}

		return constantsMerge.collect { it.value }.join('\n')
	}
	
	public List<AutomaticProcessConstant> getConstants(String process){
		 return process.split("\n")
				  .collect { it.trim() }
				  .findAll { s ->
					  !s.startsWith("#") &&
					  s.startsWith("pdef.addConstant") &&
					  !s.contains("workDescriptorName") &&
					  !s.contains("businessWorkName") &&
					  !s.contains("resultHandlerName")
				  }
				  .collect { x ->
					  new AutomaticProcessConstant(x.split("\"")[1], x)
				  }
	}

	/**
	 * Retorna true si en las lib se encuentra el jar agb_br_tb...jar
	 * @return
	 */
	private boolean isAGB(){
		def jarName = "agb_br_tb";
		def directoryLib = new File(CarpetasServidor.SERVER_LIB.getExpandedPath());

		if(directoryLib.exists() && directoryLib.isDirectory()){
			File[] filesJars = directoryLib.listFiles({ File dir, String file ->
				file.startsWith(jarName) && file.endsWith(".jar")
			} as FilenameFilter
			)
			if( filesJars.length > 0 ){
				return true;
			}
		}
		return false;
	}
	
}