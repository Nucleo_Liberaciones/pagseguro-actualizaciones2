package HAKUNA


import java.io.File
import java.io.FileFilter

import topsystems.TopazException
import topsystems.actualizador.command.ActualizacionCommand
import topsystems.actualizador.command.ActualizacionCommandResult
import topsystems.actualizador.command.archivos.CopiarArchivo
import topsystems.actualizador.command.archivos.CopiarDesdeZip
import topsystems.actualizador.configuration.CarpetasActualizacion
import topsystems.actualizador.configuration.CarpetasActualizador
import topsystems.actualizador.exception.HakunaException
import topsystems.actualizador.parche.Parche
import topsystems.actualizador.simulation.Simulacion
import topsystems.actualizador.undo.UndoManager

class Parche_NUHAKUNAMA_227 extends Parche {
	private static final forderOrigen =  CarpetasActualizacion.ACTUALIZACION_HOME.getExpandedPath() + File.separator + "hakunamatata_launcher";
	private static final forderDestino =  CarpetasActualizador.ACTUALIZADOR_HOME.getExpandedPath() + File.separator + "bin";
	private static newLauncherName
	
	void sobreElParche() {
		this.notas_del_autor = "Parche_NUHAKUNAMA_227"
		this.opcional = false
		this.newLauncherName = findLauncherFileName();
	}
	
	@Override
	protected void comandos() {
		copyNewLauncher()
		updateExecutionFiles()
	}
	
	private void copyNewLauncher(){
		def launcherFolder = forderDestino + File.separator + "launcher"
		new File(launcherFolder).mkdir();
		
		def archivoACopiar = forderOrigen + File.separator + newLauncherName
		this.addCommand new CopiarArchivo(archivoACopiar, launcherFolder + File.separator + newLauncherName)
	}
	
	
	private static String findLauncherFileName(){
		File folder = new File(forderOrigen);
		if(folder.exists() && folder.isDirectory()){
			File[] launchers = folder.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					return pathname.getName().toLowerCase().startsWith("hakunamatatalauncher") && pathname.getName().endsWith(".jar");
				}
			});
			
			if(launchers.length > 0){
				return launchers[0].getName();
			}
		}
		return null;
	}
	
	private void updateExecutionFiles(){
		this.addCommand(new UpdateLauncherConfiguration("run.bat"));
		this.addCommand(new UpdateLauncherConfiguration("run.sh"));
	}
	
	public class UpdateLauncherConfiguration extends ActualizacionCommand {
				
		private final String folderPath = CarpetasActualizador.ACTUALIZADOR_HOME.getExpandedPath()+ File.separator + "bin";
		private final String filePath;
		private final String fileName;
		
		public UpdateLauncherConfiguration(String fileName) {
			this.fileName = fileName;
			this.filePath = folderPath + File.separator + fileName;
		}
		
		@Override
		public ActualizacionCommandResult execute() {
			File executeFile = findExecuteFile();
			
			if(executeFile == null){
				this.getContext().notificar("File "+ filePath +" not found");
				return new ActualizacionCommandResult();
			}
			
			try {
				
				UndoManager.getInstance().registerUpdateAndBackUpFile(this, executeFile.getAbsolutePath());	
				
				
				def modifiedLines = []
				
				executeFile.eachLine { line ->
										
					if (line.contains('$HM_HOME/bin/*') || line.contains('%HM_HOME%/bin/*')) {
						if(this.fileName.endsWith(".bat")){
							line = line.replace('%HM_HOME%/bin/*', '%HM_HOME%/bin/launcher/*')
						}else{
							line = line.replace('$HM_HOME/bin/*', '$HM_HOME/bin/launcher/*')
						}										
					}
					modifiedLines << line
				}
				
				// Escribir el contenido modificado de nuevo al archivo
				executeFile.withWriter { writer ->
					modifiedLines.each { modifiedLine ->
						writer.writeLine(modifiedLine)
					}
				}			
					
				return new ActualizacionCommandResult();
				
			} catch (Throwable e) {
				String mensajeError = String.format("Se produjo un error al intentar modificar el archivo '%s' . Error: '%s'.", executeFile.getAbsolutePath(), e.getMessage());
				this.getContext().grabarLog(mensajeError);
				return new ActualizacionCommandResult(mensajeError);
			}
		}
	
		@Override
		public String getSummary() {
			return String.format("Se va a actualizar el archivo '%s'", filePath);
		}
	
		@Override
		public ActualizacionCommandResult simulation() {
			try {
				Simulacion.getInstance().addTexto(this, String.format("Actualizar el archivo '%s' ", filePath));
	
			} catch (TopazException e) {
				String msg = "Error durante la simulación del comando UpdateLauncherConfiguration.\n Error: " + e.getErrorText();
				this.getContext().grabarLog(msg);
				return new ActualizacionCommandResult(msg);
			}
			
			return new ActualizacionCommandResult();
		}
	
		@Override
		public String getTitulo() {
			return "Actualizando archivo " + filePath;
		}
		
		
		private File findExecuteFile(){
			File executableFile = new File(filePath);
			if(executableFile.exists() && !executableFile.isDirectory()){
				return executableFile;
			 }
			 
			 return null;
		}
		
	}
}
