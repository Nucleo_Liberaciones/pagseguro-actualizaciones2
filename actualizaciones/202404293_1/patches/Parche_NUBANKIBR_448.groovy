package BANKINGBR

import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.configuration.CarpetasServidor
import topsystems.actualizador.parche.Parche;

class Parche_NUBANKIBR_448 extends Parche{
	
	@Override
	protected void comandos() {
		if(!isSicredi()){
			modificaQueryNumerales();
		}
	}
	
	private boolean isSicredi(){
		def jarName = "sic_br_tb";
		def directoryLib = new File(CarpetasServidor.SERVER_LIB.getExpandedPath());
	
		if(directoryLib.exists() && directoryLib.isDirectory()){
			File[] filesJars = directoryLib.listFiles({ File dir, String file ->
				file.startsWith(jarName) && file.endsWith(".jar")
			} as FilenameFilter
			)
			if( filesJars.length > 0 ){
				return true;
			}
		}
		return false;
	}
	
	
	protected void modificaQueryNumerales(){
		// Agregar o sustituir una query (o varias) de DataMapping
		def archivoMapp = "interesesdeudoresmapping.xml"
		def queries = """
		<query>
	        <class-name>topsystems.automaticprocess.interesesvista.interesesdeudores.objectsdomain.SaldoDiarioExtendBr</class-name>
	        <query-name>query.QueryNumerales</query-name>
	        <database-name>TOP/CLIENTES</database-name>
	        <sentence>
						SELECT FECHA, SALDO_CON_FECHA_VALOR_COBRO, CUPO_SOBREGIRO,INTERESESCOBRO_AD,TASAINTERESCOBRO,INTERESESCOBRO
						  FROM GRL_SALDOS_DIARIOS
						 WHERE FECHA &gt;= ? AND FECHA &lt;= ? AND SALDOS_JTS_OID = ?
						 ORDER BY FECHA ASC
					</sentence>
	        <attribute>
	            <attribute-name>fecha</attribute-name>
	            <column-name>FECHA</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>saldo_fecha_valor_cobro</attribute-name>
	            <column-name>SALDO_CON_FECHA_VALOR_COBRO</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>cupoSobregiro</attribute-name>
	            <column-name>CUPO_SOBREGIRO</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>interesesCobroAD</attribute-name>
	            <column-name>INTERESESCOBRO_AD</column-name>
				<column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>tasaInteresCobro</attribute-name>
	            <column-name>TASAINTERESCOBRO</column-name>
				<column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>interesesCobro</attribute-name>
	            <column-name>INTERESESCOBRO</column-name>
				<column-type>double</column-type>
	        </attribute>
	    </query>
		"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}

}