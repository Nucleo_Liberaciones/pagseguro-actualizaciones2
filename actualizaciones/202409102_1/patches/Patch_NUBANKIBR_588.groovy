import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.properties.BorrarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.CarpetasServidor
import topsystems.actualizador.configuration.Client
import topsystems.actualizador.configuration.app.ConfigurationAppManager
import topsystems.actualizador.parche.Parche;

class Patch_NUBANKIBR_588 extends Parche{



	@Override
	protected void comandos() {

		aplicarEnBase();

			def archivoMapp1 = "DevengamientoMapping.xml"
			def queries = """
	<query>
        <class-name>topsystems.automaticprocess.devengamiento.calculo.SeleccionDevengamiento</class-name>
        <query-name>query.devengamiento.seleccionDevengamientoDiferencia</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
				select distinct JTS_OID, 'TODOS' AS DIFERENCIA, 0 as NRO_CUOTA, CATEGORIARESULTANTE -- query original del calculo devengado por extorno
				       from saldos,planctas, cli_clientes 
				       where 
				       c1621 &lt;= (select FechaProceso from parametros) and 
				       (((c1604 &lt; 0 or c1608 &gt; 0) and (C1785 = 5 or C1785 = 6)) or 
				       ((c1604 &gt; 0 or c1608 &gt; 0) and (C1785 = 4))) and 
				       saldos.Tz_lock = 0 and 
				       (C1813 &lt; (select FechaProceso from parametros) or
				       C1813 is null) AND
				       c6326 = c1730 AND -- saldo con plan de cuentas
				       C6318 &lt;&gt; 'N' AND  -- rubro califica para el devengamiento
				       cli_clientes.CODIGOCLIENTE = saldos.C1803 AND cli_clientes.TZ_LOCK =0			
UNION -- parte de la querry para cargar en el historico devengamioento los campos con los pivot en 0 de prestamos cancelados o castigados.			
					select JTS_OID, 'POR_DIFERENCIA' AS DIFERENCIA, hd.NRO_CUOTA AS NRO_CUOTA, c.CATEGORIARESULTANTE 
				FROM SALDOS s INNER JOIN historico_devengamiento hd 
				ON (s.jts_oid =hd.SALDO_JTS_OID AND hd.fecha = s.C1813) --saldos devengados
				INNER JOIN planctas p ON (s.C1730 = p.c6326)-- saldo con plan de cuentas
				INNER JOIN 	cli_clientes  c	ON 	c.CODIGOCLIENTE = s.C1803  AND c.TZ_LOCK =0
				WHERE	C1785 IN (4,5,6)  AND (
				C1604=0 and C1608=0 and C1813&lt; C1625 
				AND C1625&lt;=(SELECT FECHAPROCESO FROM PARAMETROS)--salods que son de tipo plazo cancelados
				OR				
				( p.C6318 = 'N')--salods que son de tipo plazo castigados en la fecha FECULTCATEG 
				)				
				AND --nos fijamos que los pivot no esten en 0 (Puesto que si estan en 0 no es necesario q)
				NOT (hd.INTERES_DEVENG_VIGENTE_CONT= 0 AND hd.INTERES_DEVENG_VENCIDO_CONT = 0 AND INTERES_DEVENG_SUSPENSO_CONT =0
				AND hd.MORA_DEVENG_VIGENTE_CONT =0 AND hd.MORA_DEVENG_VENCIDO_CONT = 0 AND hd.MORA_DEVENG_SUSPENSO_CONT =0 AND
				hd.MORA_TASA_INT_DEVENG_VIGE_CONT = 0 AND hd.MORA_TASA_INT_DEVENG_VENC_CONT= 0 AND hd.MORA_TASA_INT_DEVENG_SUSP_CONT = 0
				AND hd.ICMORA_CONTABILIZADO = 0)			
			</sentence>
        <attribute>
            <attribute-name>JTS_OID</attribute-name>
            <column-name>JTS_OID</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>tipoProcesamientoSaldo</attribute-name>
            <column-name>DIFERENCIA</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>nro_cuota</attribute-name>
            <column-name>NRO_CUOTA</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>calificacionResultanteCliente</attribute-name>
            <column-name>CATEGORIARESULTANTE</column-name>
            <column-type>String</column-type>
        </attribute>
    </query>
			"""
			this.addCommand( new AgregarModificarQuery(archivoMapp1, queries))

			archivoMapp1 = "DevengamientoMapping.xml"
			def queries2 = """
    <query>
        <class-name>topsystems.automaticprocess.devengamiento.calculo.SeleccionDevengamiento</class-name>
        <query-name>query.devengamiento.seleccionDevengamientoExtorno</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
				select distinct JTS_OID, 'TODOS' AS DIFERENCIA, 0 as NRO_CUOTA, c.CATEGORIARESULTANTE 
				       from saldos,planctas, CLI_CLIENTES c
				       where 
				       c1621 &lt;= (select FechaProceso from parametros) and 
				       (((c1604 &lt; 0 or c1608 &gt; 0) and (C1785 = 5 or C1785 = 6)) or 
				       ((c1604 &gt; 0 or c1608 &gt; 0) and (C1785 = 4))) and 
				       saldos.Tz_lock = 0 and 
				       (C1813 &lt; (select FechaProceso from parametros) or
				       C1813 is null) 
					   AND c.CODIGOCLIENTE  = saldos.C1803 AND c.TZ_LOCK =0 AND 
				       c6326 = c1730 AND -- saldo con plan de cuentas
				       C6318 &lt;&gt; 'N' -- rubro califica para el devengamiento 				
			</sentence>
        <attribute>
            <attribute-name>JTS_OID</attribute-name>
            <column-name>JTS_OID</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>tipoProcesamientoSaldo</attribute-name>
            <column-name>DIFERENCIA</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>nro_cuota</attribute-name>
            <column-name>NRO_CUOTA</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>calificacionResultanteCliente</attribute-name>
            <column-name>CATEGORIARESULTANTE</column-name>
            <column-type>String</column-type>
        </attribute>
    </query>
			"""
			this.addCommand( new AgregarModificarQuery(archivoMapp1, queries2))
			
			archivoMapp1 = "DevengamientoMapping.xml"
			def queries3 = """
   <query>
        <class-name>topsystems.automaticprocess.devengamiento.suspenso.SeleccionTratoSuspensoGrupo</class-name>
        <query-name>query.suspenso.grupo.seleccionSaldos</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
			SELECT 
				s.SUCURSAL, 
				s.PRODUCTO, 
				s.CUENTA, 
				s.MONEDA, 
				s.OPERACION, 
				s.ORDINAL,
				s.JTS_OID,
				c.CATEGORIARESULTANTE 
			FROM SALDOS s
				INNER JOIN PRODUCTOS p
					ON s.PRODUCTO = p.C6250
				INNER JOIN planctas pc
					ON pc.C6326 = s.C1730
				INNER JOIN CLI_CLIENTES c
					ON c.CODIGOCLIENTE = s.C1803 AND c.TZ_LOCK =0
			WHERE upper(p.C6800) = 'F'
				AND (s.C1604 &lt; 0)
				AND C1621 &lt;= (SELECT FechaProceso FROM PARAMETROS)
				AND (C1813 &lt; (SELECT FechaProceso FROM PARAMETROS) OR C1813 is null)
				AND s.TZ_LOCK = 0
				AND C6318 &lt;&gt; 'N' -- rubro califica para el devengamiento 
			ORDER BY s.SUCURSAL, s.PRODUCTO, s.CUENTA, s.MONEDA, s.OPERACION, s.ORDINAL
		</sentence>
        <attribute>
            <attribute-name>JTS_OID</attribute-name>
            <column-name>JTS_OID</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>sucursal</attribute-name>
            <column-name>SUCURSAL</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>producto</attribute-name>
            <column-name>PRODUCTO</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>cuenta</attribute-name>
            <column-name>CUENTA</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>moneda</attribute-name>
            <column-name>MONEDA</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>operacion</attribute-name>
            <column-name>OPERACION</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>ordinal</attribute-name>
            <column-name>ORDINAL</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>calificacionResultanteCliente</attribute-name>
            <column-name>CATEGORIARESULTANTE</column-name>
            <column-type>String</column-type>
        </attribute>
    </query>
			"""
					this.addCommand( new AgregarModificarQuery(archivoMapp1, queries3))
					
					archivoMapp1 = "DevengamientoMapping.xml"
					def queries4 = """
   <query>
        <class-name>topsystems.automaticprocess.devengamiento.SaldosDevengaProporcionalPlazo</class-name>
        <query-name>query.DevengaCargosPrestamo</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
					SELECT saldos.jts_oid AS SALDO_JTS_OID,
						   saldos.C1604, -- Saldo
						   saldos.C1627, -- Vencimiento
						   saldos.C1620, -- Alta
						   saldos.C1601,  -- Capital Original
		                   planctas.devengado_por_plazo,
		                   c.CATEGORIARESULTANTE 
					FROM   saldos ,
						   planctas, CLI_CLIENTES c
					WHERE  saldos.C1730 = planctas.C6326 AND
						   saldos.c1604 &lt;&gt; 0 AND
		                   saldos.C1627 IS NOT NULL AND
						   saldos.C1620 IS NOT NULL AND
						   saldos.C1601 &lt;&gt; 0 AND
		                   saldos.tz_lock = 0 AND
		                   c.CODIGOCLIENTE = saldos.C1803 AND c.TZ_LOCK =0 AND 
						   planctas.devengado_por_plazo IN ('C', 'P') AND  
		                   planctas.tz_lock = 0
				</sentence>
        <attribute>
            <attribute-name>saldoJtsOid</attribute-name>
            <column-name>SALDO_JTS_OID</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>C1604</attribute-name>
            <column-name>C1604</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>C1627</attribute-name>
            <column-name>C1627</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>C1620</attribute-name>
            <column-name>C1620</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>C1601</attribute-name>
            <column-name>C1601</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>devengaPorPlazo</attribute-name>
            <column-name>devengado_por_plazo</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>calificacionResultanteCliente</attribute-name>
            <column-name>CATEGORIARESULTANTE</column-name>
            <column-type>String</column-type>
        </attribute>
    </query>
			"""
	this.addCommand( new AgregarModificarQuery(archivoMapp1, queries4))

	archivoMapp1 = "DevengamientoMapping.xml"
	def queries5 = """
 	<query>
		<class-name>topsystems.topaz.server.businesslogic.devengamientoutil.NivelesCodigosEstadoAtraso</class-name>
		<query-name>query.vo_codigosAtrasoCartera</query-name>
		<database-name>TOP/CLIENTES</database-name>
		<sentence>	
			select NIVEL_ATRASO, COD_ATRASO 
			from CR_CODIGOS_ATRASO_CARTERA 
		</sentence>
		<attribute>
			<attribute-name>nivelCodigoEstadoAtraso</attribute-name>
			<column-name>NIVEL_ATRASO</column-name>
			<column-type>long</column-type>
		</attribute>
		<attribute>
			<attribute-name>codigoEstadoAtraso</attribute-name>
			<column-name>COD_ATRASO</column-name>
			<column-type>String</column-type>
		</attribute>
	</query>
			"""
	this.addCommand( new AgregarModificarQuery(archivoMapp1, queries5))
	
	archivoMapp1 = "DevengamientoMapping.xml"
	def queries6 = """
 	<query>
		<class-name>topsystems.topaz.server.businesslogic.devengamientoutil.Ponderacion</class-name>
		<query-name>query.vo_ponderacionesDeClasubjetiva</query-name>
		<database-name>TOP/CLIENTES</database-name>
		<sentence>	
			select CATEGORIASUB as CATEGORIA, PONDERACION
			  from CLI_CLASUBJETIVA			    
			 where TZ_LOCK = 0			
		</sentence>
		<attribute>
			<attribute-name>categoria</attribute-name>
			<column-name>categoria</column-name>
			<column-type>String</column-type>
		</attribute>
		<attribute>
			<attribute-name>ponderacion</attribute-name>
			<column-name>ponderacion</column-name>
			<column-type>long</column-type>
		</attribute>
	</query>
			"""
		this.addCommand( new AgregarModificarQuery(archivoMapp1, queries6))
							
			// Agregar o sustituir uno o varios servicios en un mismo archivo
			def archivoServ = "DevengamientoDeploy.xml"
			def servicios = """
		<service>
			<name>topsystems:service:core=DevengamientoLoanSuspensoUtil</name>
			<code>topsystems.topaz.server.businesslogic.devengamientoutil.DevengamientoSuspensoUtilBrIFRS9</code>
		</service>
"""
			this.addCommand( new AgregarModificarServicios(archivoServ, servicios))


		// Nueva Propiedad para indicar si esta activada la funcionalidad IFRS9
		def archivo = "jts.properties"  
		def propiedad = "topaz.IFRS9.habilitado"
		def valor = "false"
		def comentario = "Para activar la funcionalidad IFRS9"  
		this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, valor, comentario))
					
	}
	
	private void aplicarEnBase(){
	
	if ( esBaseDeDatos("ORACLE")){
		
		first();
		
		second();
		
		def sentenciaORA = """
		MERGE INTO PARAMETROS_JTS p
		USING (
		  SELECT 'DEVENGAMIENTO' AS FUNCIONALIDAD, 'LIMITE_CALIF_RESULTANTE_DEUDA' AS PARAMETRO, ' ' AS VALOR, 0 AS TZ_LOCK FROM dual
		) src
		ON ( upper(p.FUNCIONALIDAD) = src.FUNCIONALIDAD AND upper(p.PARAMETRO) = src.PARAMETRO)
		WHEN MATCHED THEN
		  UPDATE SET p.VALOR = src.VALOR
		WHEN NOT MATCHED THEN
		  INSERT (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
		  VALUES (src.FUNCIONALIDAD, src.PARAMETRO, src.VALOR, src.TZ_LOCK); 
		
		"""
		this.addCommand( new ActualizarBaseDeDatosOracle(sentenciaORA))
	}
	
	}
	
	private void first(){
		def sentencia = """
	-- HM begin sentence
		DECLARE 
		v_cant NUMBER(9);
		v_sql VARCHAR2(2000);
		BEGIN
		SELECT COUNT(*) INTO v_cant
		    FROM user_tables
		    WHERE upper(table_name) = upper('CLI_CLASUBJETIVA');
			  
		IF (v_cant = 0) THEN
		 v_sql:=
		 q'[create table CLI_CLASUBJETIVA
			(
			  TZ_LOCK           NUMBER(15) default (0) not null,
			  CATEGORIASUB      VARCHAR2(3) default (' ') not null,
			  DESCRIP_NO_FINANC VARCHAR2(60) default (' '),
			  DESCRIP_FINANC    VARCHAR2(60) default (' '),
			  PORCENTAJE_SNF    NUMBER(5,2) default (0),
			  CODIGO_VALIDO     VARCHAR2(1) default (' '),
			  PORCENTAJE_SF     NUMBER(5,2) default (0),
			  PONDERACION       NUMBER(3),
			  constraint PK_CLI_CLASUBJETIVA primary key (CATEGORIASUB)
			)]';
		 EXECUTE IMMEDIATE v_sql;
		  END IF;
		END;
	-- HM end sentence """
	def descripcion = "first sentence" // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	
	}

	private void second(){
		def sentencia = """
	-- HM begin sentence
		DECLARE 
		v_cant NUMBER(9);
		v_sql VARCHAR2(2000);
		BEGIN
		
		SELECT COUNT(*) INTO v_cant
		    FROM user_tables
		    WHERE upper(table_name) = upper('CR_CODIGOS_ATRASO_CARTERA');
			  
		IF (v_cant = 0) THEN
		 v_sql:=
		 q'[create table CR_CODIGOS_ATRASO_CARTERA
			(
			  COD_ATRASO   VARCHAR2(1) not null,
			  DESCRIPCION  VARCHAR2(150) not null,
			  NIVEL_ATRASO NUMBER(2) not null,
			  TIPO_ATRASO  VARCHAR2(1) not NULL,
			  CONSTRAINT PK_CODIGOS_ATRASO_CARTERA primary key (COD_ATRASO)
			)]';
		 EXECUTE IMMEDIATE v_sql;
		  END IF;
		END;
	-- HM end sentence """
	def descripcion = "Second sentence" // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	
	}
}