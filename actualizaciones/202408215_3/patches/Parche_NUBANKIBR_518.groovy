import javax.xml.crypto.dsig.spec.HMACParameterSpec

import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche
import topsystems.actualizador.configuration.CarpetasServidor

class Parche_NUBANKIBR_518 extends Parche{

	void definicionDeParametros(){
		if(!isSicredi()){
		addParameterPATitleOpe("NRO_OPERACION_PA"," Numero de titulo de los Procesos Automaticos")
		addParameterNewOperationNumber("OPERACION_PA", "Numero de Operacion para PA: Recalculo Intereses Vigentes IFRS9");
		}
	}	
	

	@Override
	protected void comandos() throws TopazException {
		if(!isSicredi()){
		agregarParametrosJTS();
		crearTablaHistoricoChequeEspecialDiaD();
		agregarQueryRecalculoInteresesVigentesIFRS9();
		agregarEntidadHistoricoDiaD();
		agregarProcesses();
		agregarGroup();
		agregarDeploy();
		}		
	}
	
	public void agregarParametrosJTS(){
		def sentencia = """

INSERT INTO OPERACIONES (TITULO, IDENTIFICACION, NOMBRE, DESCRIPCION, MNEMOTECNICO, AUTORIZACION, FORMULARIOPRINCIPAL, PROXOPERACION, ESTADO, TZ_LOCK, COPIAS, SUBOPERACION, PERMITEBAJA, COMPORTAMIENTOENCIERRE, REQUIERECONTRASENA, PERMITECONCURRENTE, PERMITEESTADODIFERIDO, ICONO_TITULO, ESTILO)
VALUES (${this.P("NRO_OPERACION_PA")}, ${this.P("OPERACION_PA")}, 'Recalculo Int Vigentes IFRS9', 'Recalculo Int Vigentes IFRS9', '${this.P("OPERACION_PA")}', 'N', NULL, NULL, 'P', 0, NULL, 0, 'S', 'I', 'N', 'S', 'S', NULL, NULL);

INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
VALUES ('RECALCULO_INTERESES_VIGENTES_IFRS9', 'CANAL', '0', 0);

INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
VALUES ('RECALCULO_INTERESES_VIGENTES_IFRS9', 'OPERACION', '${this.P("OPERACION_PA")}', 0);

INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
VALUES ('RECALCULO_INTERESES_VIGENTES_IFRS9', 'DESCRIPCION', 'Recalculo de intereses Vigentes IFRS9', 0);

INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
VALUES ('RECALCULO_INTERESES_VIGENTES_IFRS9', 'FECHA_FIN_ANO', 'dd-mm-yyyy', 0);

INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
VALUES ('RECALCULO_INTERESES_VIGENTES_IFRS9', 'APLICA_ANTICIPADO_ATIVO_PROBLEMATICO', 'N', 0);

INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
VALUES ('RECALCULO_INTERESES_VIGENTES_IFRS9', 'RECLASIFICA_ATRASO', 'N', 0);

INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
VALUES ('RECALCULO_INTERESES_VIGENTES_IFRS9', 'PIVOT_SUSPENSO_VIGENTE_CHEQ_ESPECIAL', '0', 0);

INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
VALUES ('RECALCULO_INTERESES_VIGENTES_IFRS9', 'PIVOT_SUSPENSO_VIGENTE_CHEQ_ESPECIAL_AD', '0', 0);

INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
VALUES ('RECALCULO_INTERESES_VIGENTES_IFRS9', 'PIVOT_VIGENTE_CHEQ_ESPECIAL', '0', 0);

INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
VALUES ('RECALCULO_INTERESES_VIGENTES_IFRS9', 'PIVOT_VIGENTE_AD', '0', 0);

"""
	def descripcion = "Agrega ParametrosJTS de la funcionalidad Recalculo de intereses Vigentes" // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	
	}
	
	public void crearTablaHistoricoChequeEspecialDiaD(){
	if(isOracle()){
		def sentencia = """

CREATE TABLE TMP_CHEQUE_ESP_DIA_D
	(
	JTS_OID_CC 								 NUMBER (10) NOT NULL,
	FECHA_PROCESO                            DATE,
    CASO 							 	     NUMBER (2),
	VDS_INTERESES_TOTALES_ANTES              NUMBER (15,2) DEFAULT (0),
	VDS_INTERES_VIGENTE_ANTES				 NUMBER (15,5) DEFAULT (0),
	INT_AD_PERIODO_ANT_VIGENTE_ANTES	 	 NUMBER (15,2) DEFAULT (0),	
	VDS_INTERESES_TOTALES_DESPUES            NUMBER (15,2) DEFAULT (0),
	VDS_INTERES_VIGENTE_DESPUES				 NUMBER (15,2) DEFAULT (0),	
	INT_AD_PERIODO_ANT_VIGENTE_DESPUES	     NUMBER (15,2) DEFAULT (0),
	VDS_CONT_INTERES_RECLASIFICADO   	     NUMBER (15,2) DEFAULT (0),
	VDS_CONT_INTERES_DIAS_NO_UTIL            NUMBER (15,2) DEFAULT (0),	
	CONT_INTERES_AD_RECLAS_PERIODO_ANT    	 NUMBER (15,2) DEFAULT (0),
	SCC_INTERESES_TOTALES_ANTES				 NUMBER (15,2) DEFAULT (0),
	SCC_INTERES_VIGENTE_ANTES                NUMBER (15,2) DEFAULT (0),
	INT_PERIODO_ANT_VIGENTE_ANTES	     	 NUMBER (15,2) DEFAULT (0),	
	SCC_INTERESES_TOTALES_DESPUES			 NUMBER (15,2) DEFAULT (0),	
	SCC_INTERES_VIGENTE_DESPUES	             NUMBER (15,2) DEFAULT (0),
	INT_PERIODO_ANT_VIGENTE_DESPUES	 		 NUMBER (15,2) DEFAULT (0),
	SCC_CONT_INTERES_RECLASIFICADO   	     NUMBER (15,2) DEFAULT (0),
	SCC_CONT_INTERES_DIAS_NO_UTIL            NUMBER (15,2) DEFAULT (0),
	CONT_INTERES_RECLAS_PERIODO_ANT    		 NUMBER (15,2) DEFAULT (0),
    CONSTRAINT PK_TMP_CHEQUE_ESP_DIA_D PRIMARY KEY (JTS_OID_CC,FECHA_PROCESO) 	 
	)

 """
	def descripcion = "Crea la tabla TMP_CHEQUE_ESP_DIA_D" // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	
	}else if(isSqlServer()){
		def sentencia = """
	CREATE TABLE TMP_CHEQUE_ESP_DIA_D
	(
	JTS_OID_CC 								 NUMERIC (10) NOT NULL,
	FECHA_PROCESO                            DATE,
    CASO 							 	     NUMERIC (2),
	VDS_INTERESES_TOTALES_ANTES              NUMERIC (15,2) DEFAULT (0),
	VDS_INTERES_VIGENTE_ANTES				 NUMERIC (15,2) DEFAULT (0),
	INT_AD_PERIODO_ANT_VIGENTE_ANTES		 NUMERIC (15,2) DEFAULT (0),		
	VDS_INTERESES_TOTALES_DESPUES            NUMERIC (15,2) DEFAULT (0),
	VDS_INTERES_VIGENTE_DESPUES				 NUMERIC (15,2) DEFAULT (0),
	INT_AD_PERIODO_ANT_VIGENTE_DESPUES		 NUMERIC (15,2) DEFAULT (0),
	VDS_CONT_INTERES_RECLASIFICADO   	     NUMERIC (15,2) DEFAULT (0),
	VDS_CONT_INTERES_DIAS_NO_UTIL            NUMERIC (15,2) DEFAULT (0),	
	CONT_INTERES_AD_RECLAS_PERIODO_ANT    	 NUMERIC (15,2) DEFAULT (0),
	SCC_INTERESES_TOTALES_ANTES				 NUMERIC (15,2) DEFAULT (0),
	SCC_INTERES_VIGENTE_ANTES                NUMERIC (15,2) DEFAULT (0),
	INT_PERIODO_ANT_VIGENTE_ANTES			 NUMERIC (15,2) DEFAULT (0),	
	SCC_INTERESES_TOTALES_DESPUES			 NUMERIC (15,2) DEFAULT (0),	
	SCC_INTERES_VIGENTE_DESPUES	             NUMERIC (15,2) DEFAULT (0),
	INT_PERIODO_ANT_VIGENTE_DESPUES			 NUMERIC (15,2) DEFAULT (0),
	SCC_CONT_INTERES_RECLASIFICADO   	     NUMERIC (15,2) DEFAULT (0),
	SCC_CONT_INTERES_DIAS_NO_UTIL            NUMERIC (15,2) DEFAULT (0),
	CONT_INTERES_RECLAS_PERIODO_ANT    		 NUMERIC (15,2) DEFAULT (0),
	
	CONSTRAINT PK_TMP_CHEQUE_ESP_DIA_D PRIMARY KEY (JTS_OID_CC,FECHA_PROCESO) 	 
	)
		"""
		def descripcion = "Crea la tabla TMP_CHEQUE_ESP_DIA_D" // La descripción es opcional
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
	}		
}	
	
	public void agregarQueryRecalculoInteresesVigentesIFRS9(){
		// Agregar o sustituir una query (o varias) de DataMapping
def archivoMapp = "interesesdeudoresmapping.xml"
def queries = """
<query>
        <class-name>topsystems.automaticprocess.objectsdomain.JtsOidGenericObject</class-name>
        <query-name>query.RecalculoIFRS9DiaD</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
			SELECT S.JTS_OID 
						FROM SALDOS S
						INNER JOIN VTA_DATOS_SOBREGIROS VDS ON VDS.JTS_OID_SALDO=S.JTS_OID AND VDS.TZ_LOCK = 0
						WHERE  S.C1651 = ' '
						AND S.C1785 = 2
						AND (S.ULT_FECHA_VIGENTE + 60 &lt;= ? OR (S.ULT_FECHA_VIGENTE IS NULL AND (VDS.INT_VENCIDO_NO_COBRADO - VDS.INT_VIGENTE_NO_COBRADO &gt; 0 or VDS.INT_VENCIDO_NO_COBRADO_AD - vds.INT_VIGENTE_NO_COBRADO_AD &gt; 0))) 					
						AND S.TZ_LOCK = 0
						AND (S.ATIVO_PROBLEMATICO &lt;&gt; 'S' OR S.ATIVO_PROBLEMATICO IS NULL)		
        </sentence>
        <attribute>
            <attribute-name>JTS_OID</attribute-name>
            <column-name>JTS_OID</column-name>
            <column-type>long</column-type>
        </attribute>      
    </query>
	"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))

	}
	
	public void agregarGroup(){
		// Agregar o sustituir un grupo
def archivo = "groups.py"
def grupo = """
 gdef = GroupDefinition("VTA - Recalculo Vigente Cheque Especial IFRS9")
    gdef.registerProcess("Recalculo Vigente Cheque Especial IFRS9")
    gdefs.addGroup(gdef)
"""
this.addCommand( new AgregarModificarGrupo(archivo, grupo))

	}
	
	public void agregarProcesses(){
		// Agregar o sustituir un proceso
def archivo = "processes.py"
def procesos = """
 pdef = ProcessDefinition("Recalculo Vigente Cheque Especial IFRS9", "topsystems.automaticprocess.processmanager.WorkManager")
    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WDRecalculoIFRS9DiaD");
    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BWRecalculoIFRS9DiaD");
    pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
    pdef.addConstant("rangoCommit","100");
    pdef.addConstant("cantidadHilos","10");
    pdef.addConstant("isSumarizable","true");
    pdef.addConstant("enqueue","false");
	pdef.addConstant("CAMPO_DEVENGADO_59_DIAS","C1806");
	pdefs.addProcess(pdef)
"""
this.addCommand(new AgregarModificarProceso(archivo, procesos))

	}
	
	public void agregarDeploy(){
		// Agregar o sustituir uno o varios servicios en un mismo archivo
def archivo = "interesesVistaCobroDeudor.xml"
def servicios = """
<service>
 <name>topsystems:processManager:WorkDescriptor=WDRecalculoIFRS9DiaD</name>
 <code>topsystems.automaticprocess.interesesvista.interesesdeudores.ifrs9DiaD.WDRecalculoIFRS9DiaD</code>
</service>
<service>
   <name>topsystems:processManager:BusinessWorker=BWRecalculoIFRS9DiaD</name>
   <code>topsystems.automaticprocess.interesesvista.interesesdeudores.ifrs9DiaD.BWRecalculoIFRS9DiaD</code>
</service>
"""
this.addCommand( new AgregarModificarServicios(archivo, servicios))

	}
	
	public void agregarEntidadHistoricoDiaD(){
		// Agregar o sustituir una entidad de DataMapping
def archivoMapp = "interesesdeudoresmapping.xml"
def entidades = """
<class>
  <class-name>topsystems.automaticprocess.interesesvista.interesesdeudores.ifrs9DiaD.HistoricoRecalculoIRFS9DiaD</class-name>
        <entity-name>core.vo_HistoricoRecalculoIRFS9DiaD</entity-name>
        <table-name>TMP_CHEQUE_ESP_DIA_D</table-name>
        <database-name>TOP/CLIENTES</database-name>
        <attribute>
            <attribute-name>saldoJtsOid</attribute-name>
            <column-name>JTS_OID_CC</column-name>
        	<key>primary</key>
		</attribute>
		<attribute>
            <attribute-name>fechaProceso</attribute-name>
            <column-name>FECHA_PROCESO</column-name>
			<key>primary</key>
        </attribute>
       <attribute>
            <attribute-name>cases</attribute-name>
            <column-name>CASO</column-name>
        </attribute>
		<attribute>
            <attribute-name>devengadoCobroAdAntes</attribute-name>
            <column-name>VDS_INTERESES_TOTALES_ANTES</column-name>
        </attribute>
		<attribute>
            <attribute-name>devengado59DiasAdAntes</attribute-name>
            <column-name>VDS_INTERES_VIGENTE_ANTES</column-name>
        </attribute>
		<attribute>
            <attribute-name>intVigenteADNoCobradoActual</attribute-name>
            <column-name>INT_AD_PERIODO_ANT_VIGENTE_ANTES</column-name>
        </attribute>
		<attribute>		
            <attribute-name>devengadoCobroAdDespues</attribute-name>
            <column-name>VDS_INTERESES_TOTALES_DESPUES</column-name>
		</attribute>
		<attribute>
            <attribute-name>devengado59DiasAdDespues</attribute-name>
            <column-name>VDS_INTERES_VIGENTE_DESPUES</column-name>
        </attribute>		
		<attribute>
            <attribute-name>intVigenteADNoCobradoModificado</attribute-name>
            <column-name>INT_AD_PERIODO_ANT_VIGENTE_DESPUES</column-name>
        </attribute>
		<attribute>
            <attribute-name>vdsInteresesReclasif</attribute-name>
            <column-name>VDS_CONT_INTERES_RECLASIFICADO</column-name>
        </attribute>
		<attribute>
            <attribute-name>vdsInteresesDiasNoUtil</attribute-name>
            <column-name>VDS_CONT_INTERES_DIAS_NO_UTIL</column-name>
        </attribute>
		<attribute>
            <attribute-name>intVigentesADReclasifAnteriores</attribute-name>
            <column-name>CONT_INTERES_AD_RECLAS_PERIODO_ANT</column-name>
        </attribute>
		<attribute>
            <attribute-name>scc_1820_Antes</attribute-name>
            <column-name>SCC_INTERESES_TOTALES_ANTES</column-name>
        </attribute>
		<attribute>
            <attribute-name>scc_1806_Antes</attribute-name>
            <column-name>SCC_INTERES_VIGENTE_ANTES</column-name>
        </attribute>
		<attribute>
            <attribute-name>intVigenteNoCobradoActual</attribute-name>
            <column-name>INT_PERIODO_ANT_VIGENTE_ANTES</column-name>
        </attribute>
		<attribute>
            <attribute-name>scc_1820_Despues</attribute-name>
            <column-name>SCC_INTERESES_TOTALES_DESPUES</column-name>
        </attribute>		
		<attribute>
            <attribute-name>scc_1806_Despues</attribute-name>
            <column-name>SCC_INTERES_VIGENTE_DESPUES</column-name>
        </attribute>
		<attribute>
            <attribute-name>intVigenteNoCobradoModificado</attribute-name>
            <column-name>INT_PERIODO_ANT_VIGENTE_DESPUES</column-name>
        </attribute>
		<attribute>
            <attribute-name>sccInteresesReclasif</attribute-name>
            <column-name>SCC_CONT_INTERES_RECLASIFICADO</column-name>
        </attribute>
		<attribute>
            <attribute-name>sccInteresesDiasNoUtil</attribute-name>
            <column-name>SCC_CONT_INTERES_DIAS_NO_UTIL</column-name>
        </attribute>
		<attribute>
            <attribute-name>intVigentesReclasifAnteriores</attribute-name>
            <column-name>CONT_INTERES_RECLAS_PERIODO_ANT</column-name>
        </attribute>		
	</class>
"""
this.addCommand( new AgregarModificarEntidad(archivoMapp, entidades))

	}
	private boolean isSicredi(){
		def jarName = "sic_br_tb";
		def directoryLib = new File(CarpetasServidor.SERVER_LIB.getExpandedPath());
	
		if(directoryLib.exists() && directoryLib.isDirectory()){
			File[] filesJars = directoryLib.listFiles({ File dir, String file ->
				file.startsWith(jarName) && file.endsWith(".jar")
			} as FilenameFilter
			)
			if( filesJars.length > 0 ){
				return true;
			}
		}
		return false;
	}
	
}
