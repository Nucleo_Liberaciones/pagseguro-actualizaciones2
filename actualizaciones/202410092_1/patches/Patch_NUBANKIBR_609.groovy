import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.Client
import topsystems.actualizador.configuration.app.ConfigurationAppManager
import topsystems.actualizador.parche.Parche;

class Patch_NUBANKIBR_609 extends Parche{

	void definicionDeParametros(){
		this.addParameterNewFreeField("PIVOT_GIO_GT_DEVENGADO_VIGENTE", "Campo pivot Gastos Devengados Vigente")
		this.addParameterNewFreeField("PIVOT_GIO_IN_DEVENGADO_VIGENTE", "Campo pivot Ingresos Devengados Vigente")
	}

	@Override
	protected void comandos() {		
	actualizarBase();
		}
		
		
	private void actualizarBase(){
		
		def sentencia = """
INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK) VALUES('CATEGORIZACION', 'PIVOT_GIO_GT_DEVENGADO_VIGENTE',  '${this.P("PIVOT_GIO_GT_DEVENGADO_VIGENTE")}', 0);

INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK) VALUES('CATEGORIZACION', 'PIVOT_GIO_IN_DEVENGADO_VIGENTE',  '${this.P("PIVOT_GIO_IN_DEVENGADO_VIGENTE")}', 0);

INSERT INTO DICCIONARIO
(NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
VALUES('${this.P("PIVOT_GIO_GT_DEVENGADO_VIGENTE")}', NULL, 0, 'GT Devengado Vigente (Pivot)', 'GTDevVigentePivot', 15, 'N', 2, 'I', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL);

INSERT INTO DICCIONARIO
(NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
VALUES('${this.P("PIVOT_GIO_IN_DEVENGADO_VIGENTE")}', NULL, 0, 'IN Devengado Vigente (Pivot)', 'INDevVigentePivot', 15, 'N', 2, 'I', 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, NULL);

"""
	def descripcion = "Pivotes GIO" 
	this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	
		
	}
}