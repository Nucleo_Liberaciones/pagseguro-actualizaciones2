import topsystems.actualizador.command.archivos.BorrarArchivo
import topsystems.actualizador.command.archivos.CopiarDesdeZip
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.parche.Parche


class Patch_NUTOPAZ56_101 extends Parche {
	
	@Override
	void sobreElParche() { // Este m�todo es opcional
		this.notas_del_autor = """
			Actualizaci�n OPCIONAL de Kettle de la versi�n 8.3 a 9.4.
			Esta actualizaci�n puede ser aplicada en cualquier momento, ya que no depende de otras liberaciones. 
			De esta forma, se puede probar todo lo relacionado a kettle y luego aplicar la misma cuando sea conveniente.
			De momento se podr� optar por mantener la versi�n actual (8.3), por eso el car�cter de opcional.
			En caso de aplicarse, se deber� tener en cuenta, que todos los plugins (ubicados en: jboss/standalone/userlibrary/default/tools/kettle/plugins)
			que cada cliente utilice, y que fueron agregados propiamente por cada cliente, y no liberados por Digital Technology, deber�n ser actualizados siempre
			que corresponda, y esa actualizaci�n queda a cargo del cliente as� como tambi�n los tests de dichos plugins.
		"""
		this.opcional = false
	}
	@Override
	protected void comandos() {
		
		// Borrar archivo
		this.addCommand new BorrarArchivo("\${TOPAZ_HOME}/lib/commons-io-2.2.jar")
		// Copiar un archivo desde el zip del parche al destino que se le indique
		// El .zip debe tener el mismo nombre que el archivo del parche (salvo la extensi�n)
		def path = "\${TOPAZ_HOME}/lib"
		this.addCommand (new CopiarDesdeZip("commons-io-2.11.0.jar", path))
		
		
		this.addCommand (new BorrarArchivo("\${TOPAZ_HOME}/lib/commons-vfs2-2.2.jar"))
		// Copiar un archivo desde el zip del parche al destino que se le indique
		// El .zip debe tener el mismo nombre que el archivo del parche (salvo la extensi�n)
		path = "\${TOPAZ_HOME}/lib"
		this.addCommand (new CopiarDesdeZip("commons-vfs2-2.7.0.jar", path))
		
		
		this.addCommand (new BorrarArchivo("\${TOPAZ_HOME}/lib/kettle-core-8.3.0.0-371.jar"))		
		// Copiar un archivo desde el zip del parche al destino que se le indique
		// El .zip debe tener el mismo nombre que el archivo del parche (salvo la extensi�n)
		path = "\${TOPAZ_HOME}/lib"
		this.addCommand (new CopiarDesdeZip("kettle-core-9.4.0.0-343.jar", path))
		
		
		this.addCommand (new BorrarArchivo("\${TOPAZ_HOME}/lib/kettle-engine-8.3.0.0-371.jar"))
		// Copiar un archivo desde el zip del parche al destino que se le indique
		// El .zip debe tener el mismo nombre que el archivo del parche (salvo la extensi�n)
		path = "\${TOPAZ_HOME}/lib"
		this.addCommand (new CopiarDesdeZip("kettle-engine-9.4.0.0-343.jar", path))
		
		
		this.addCommand (new BorrarArchivo("\${TOPAZ_HOME}/lib/kettle-ui-swt-8.3.0.0-371.jar"))
		// Copiar un archivo desde el zip del parche al destino que se le indique
		// El .zip debe tener el mismo nombre que el archivo del parche (salvo la extensi�n)
		path = "\${TOPAZ_HOME}/lib"
		this.addCommand (new CopiarDesdeZip("kettle-ui-swt-9.4.0.0-343.jar", path))
		
		
		this.addCommand (new BorrarArchivo("\${TOPAZ_HOME}/lib/metastore-8.3.0.0-371.jar"))
		// Copiar un archivo desde el zip del parche al destino que se le indique
		// El .zip debe tener el mismo nombre que el archivo del parche (salvo la extensi�n)
		path = "\${TOPAZ_HOME}/lib"
		this.addCommand (new CopiarDesdeZip("metastore-9.4.0.0-343.jar", path))		
		
		this.addCommand (new BorrarArchivo("\${TOPAZ_HOME}/lib/libformula-8.3.0.0-371.jar"))
		// Copiar un archivo desde el zip del parche al destino que se le indique
		// El .zip debe tener el mismo nombre que el archivo del parche (salvo la extensi�n)
		path = "\${TOPAZ_HOME}/lib"
		this.addCommand (new CopiarDesdeZip("libformula-9.4.0.0-343.jar", path))
		
		this.addCommand (new BorrarArchivo("\${TOPAZ_HOME}/lib/libbase-8.3.0.0-371.jar"))
		// Copiar un archivo desde el zip del parche al destino que se le indique
		// El .zip debe tener el mismo nombre que el archivo del parche (salvo la extensi�n)
		path = "\${TOPAZ_HOME}/lib"
		this.addCommand (new CopiarDesdeZip("libbase-9.4.0.0-343.jar", path))
		
		// Copiar un archivo desde el zip del parche al destino que se le indique
		// El .zip debe tener el mismo nombre que el archivo del parche (salvo la extensi�n)
		path = "\${TOPAZ_HOME}/lib"
		this.addCommand (new CopiarDesdeZip("pentaho-encryption-support-9.4.0.0-343.jar", path))
		
		//KETTLE_PLUGINS
		this.addCommand (new BorrarArchivo("\${KETTLE_PLUGINS}/pdi-core-plugins-impl-8.3.0.0-371.jar"))
		// Copiar un archivo desde el zip del parche al destino que se le indique
		// El .zip debe tener el mismo nombre que el archivo del parche (salvo la extensi�n)
		path = "\${KETTLE_PLUGINS}"
		this.addCommand (new CopiarDesdeZip("pdi-core-plugins-impl-9.4.0.0-343.jar", path))		
		
		
		// Agregar una propiedad o cambiar el valor a una existente
		def archivo = "kettle.properties" // Si el archivo no existe, se crea.
		def propiedad = "KETTLE_PASSWORD_ENCODER_PLUGINS_FILE"
		def valor = "kettle-password-encoder-plugins-sample.xml"
		def comentario = "" // Descripci�n de la propiedad [Obligatorio]
		this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, valor, comentario))
		
	}
}
