package Base

import topsystems.actualizador.command.ActualizacionCommand
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.CarpetasActualizador
import topsystems.actualizador.parche.Parche

class Parche_NUBANKIBR_576 extends Parche {

	void sobreElParche() {
		this.notas_del_autor = "Parche_NUBANKIBR_576"
		this.opcional = false
	}

	@Override
	protected void comandos() {
		addAlterTable()
	}
	
	protected void addAlterTable(){
		def baseOra = esBaseDeDatos("ORACLE")
		if(baseOra){
			def sentencia = """
				ALTER TABLE GIO_DEFINICIONES ADD CONCEPCONT NUMBER(10);			
			"""
			this.addCommand(new ActualizarBaseDeDatosOracle(sentencia))
		}else{
			def sentencia = """
				ALTER TABLE GIO_DEFINICIONES ADD CONCEPCONT NUMERIC(10, 0);
			"""
			this.addCommand(new ActualizarBaseDeDatosSQLServer(sentencia))
		}
	}
}
