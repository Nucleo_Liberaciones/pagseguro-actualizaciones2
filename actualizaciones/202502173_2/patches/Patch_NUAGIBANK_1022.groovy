import topsystems.actualizador.command.dataserver.AgregarModificarAtributoQuery
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.mbeans.AgregarModificarMBeans
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.parche.Parche;

class Patch_NUAGIBANK_1022 extends Parche{
	


	@Override
	protected void comandos() {
		
		
// Agregar constantes y parametros a un proceso
def archivo = "processes.py"
def nombreProceso = "Reservas Sobre Saldos"
def constParamProc = """
pdef.addConstant("cantidadHilos","30");
"""
this.addCommand( new AgregarModificarPropiedadesProceso(archivo, nombreProceso, constParamProc))

		
		

// Agregar o sustituir una query (o varias) de DataMapping
def archivoMapp = "Reservas.xml"
def queries = """
<query>
        <class-name>topsystems.automaticprocess.reservassobresaldos.QueryReservaSobreSaldos</class-name>
        <query-name>query.QueryReservaSobreSaldos</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
						SELECT NRO_RESERVA, SALDO_JTS_OID, TIPO_RESERVA
						  FROM VTA_RESERVAS RES
						 INNER JOIN VTA_TIPO_RESERVAS TPO
							   ON RES.TIPO_RESERVA = TPO.TIPO
								 AND RES.ESTADO IN (0, 1)
								 AND TPO.VTO_AUTOMATICO = 'S'
								 AND RES.TZ_LOCK = 0
								 AND TPO.TZ_LOCK = 0
								 ORDER BY SALDO_JTS_OID
					</sentence>
        <attribute>
            <attribute-name>nroReserva</attribute-name>
            <column-name>NRO_RESERVA</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>saldoJtsOid</attribute-name>
            <column-name>SALDO_JTS_OID</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>tipoReserva</attribute-name>
            <column-name>TIPO_RESERVA</column-name>
            <column-type>long</column-type>
        </attribute>
    </query>
"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))

 
	}

}