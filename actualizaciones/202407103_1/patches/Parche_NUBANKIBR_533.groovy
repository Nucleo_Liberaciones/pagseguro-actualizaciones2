import topsystems.TopazException
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.parche.Parche
import topsystems.actualizador.configuration.CarpetasServidor

class Parche_NUBANKIBR_533 extends Parche {

	boolean isSicredi=false
	
	protected void definicionDeParametros(){
		isSicredi = this.isSicredi();
		if(isOracle()) {
			this.addParameterQuery("CAMPO_FECHA_CANCELACION",
				"obtengo numero de campo del saldo NRO_CAMPO_FCH_CANC_CHEQUE_ESP",
				"INTEGER",
				"""
			SELECT
			(CASE
				WHEN (SELECT COUNT(*)
					  FROM DICCIONARIO d
					  WHERE UPPER(CAMPO) = 'FECHA_CANC_CHEQUE_ESPECIAL'
					  AND tabla IN (SELECT IDENTIFICACION
								   FROM DESCRIPTORES d2
								   WHERE upper(d2.NOMBREFISICO) = 'SALDOS')) > 0
				THEN (SELECT NUMERODECAMPO
					  FROM DICCIONARIO d
					  WHERE UPPER(CAMPO) = 'FECHA_CANC_CHEQUE_ESPECIAL'
					  AND tabla IN (SELECT IDENTIFICACION
								   FROM DESCRIPTORES d2
								   WHERE upper(d2.NOMBREFISICO) = 'SALDOS'))
				ELSE -1
			END) AS NUMERODECAMPO
		FROM dual
			""")
	
		} else if(isSqlServer()) {
			this.addParameterQuery("CAMPO_FECHA_CANCELACION",
				"obtengo numero de campo del saldo NRO_CAMPO_FCH_CANC_CHEQUE_ESP",
				"INTEGER",
				"""
SELECT 
    CASE
        WHEN EXISTS (
            SELECT 1
            FROM DICCIONARIO d
            WHERE UPPER(CAMPO) = 'FECHA_CANC_CHEQUE_ESPECIAL'
            AND tabla IN (
                SELECT IDENTIFICACION
                FROM DESCRIPTORES d2
                WHERE UPPER(NOMBREFISICO) = 'SALDOS'
            )
        ) THEN (
            SELECT NUMERODECAMPO
            FROM DICCIONARIO d
            WHERE UPPER(CAMPO) = 'FECHA_CANC_CHEQUE_ESPECIAL'
            AND tabla IN (
                SELECT IDENTIFICACION
                FROM DESCRIPTORES d2
                WHERE UPPER(NOMBREFISICO) = 'SALDOS'
            )
        )
        ELSE -1
    END AS NUMERODECAMPO;
			""")
	
		}
	}
	
	private boolean isSicredi(){
		def jarName = "sic_br_tb";
		def directoryLib = new File(CarpetasServidor.SERVER_LIB.getExpandedPath());
	
		if(directoryLib.exists() && directoryLib.isDirectory()){
			File[] filesJars = directoryLib.listFiles({ File dir, String file ->
				file.startsWith(jarName) && file.endsWith(".jar")
			} as FilenameFilter
			)
			if( filesJars.length > 0 ){
				return true;
			}
		}
		return false;
	}

	@Override
	protected void comandos() throws TopazException {
		if(!isSicredi) {
			agregarConstanteCobroInteresesAD();
			agregarConstanteCobroIntereses();
		}
	}
	


	protected void agregarConstanteCobroInteresesAD() {
		// Agregar constantes y parametros a un proceso
def archivo = "processes.py"
def nombreProceso = "Cobro Intereses Deudores AD Unico Saldo"
def constParamProc = ""
if(this.P('CAMPO_FECHA_CANCELACION') == -1) {
	constParamProc = """
pdef.addConstant("NRO_CAMPO_FCH_CANC_CHEQUE_ESP","");
"""
		
} else {
	constParamProc = """
pdef.addConstant("NRO_CAMPO_FCH_CANC_CHEQUE_ESP","${this.P('CAMPO_FECHA_CANCELACION')}");
"""
		
}

this.addCommand( new AgregarModificarPropiedadesProceso(archivo, nombreProceso, constParamProc))

	}
		
	protected void agregarConstanteCobroIntereses() {
		// Agregar constantes y parametros a un proceso
def archivo = "processes.py"
def nombreProceso = "Cobro Intereses Deudores Unico Saldo"
def constParamProc = ""
if(this.P('CAMPO_FECHA_CANCELACION') == -1) {
	constParamProc = """
pdef.addConstant("NRO_CAMPO_FCH_CANC_CHEQUE_ESP","");
"""
		
} else {
	constParamProc = """
pdef.addConstant("NRO_CAMPO_FCH_CANC_CHEQUE_ESP","${this.P('CAMPO_FECHA_CANCELACION')}");
"""
		
}

this.addCommand( new AgregarModificarPropiedadesProceso(archivo, nombreProceso, constParamProc))

	}

}
