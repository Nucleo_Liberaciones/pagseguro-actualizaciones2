import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche


class Patch_NUGAN106_574 extends Parche {

	@Override
	protected void comandos() {
		addProperty();
		addDataBaseObjects();
	}	
	
	private void addProperty(){
		// Agregar una propiedad o cambiar el valor a una existente
		def archivo = "personidentification.properties" // Si el archivo no existe, se crea.
		def propiedad = "personidentification.signature.use.permission.addOrPaste"
		def valor = "false"
		def comentario = "Si es false (defecto), se muestran los botones 'Agregar' y 'Paste' en el Mantenimiento de Firmas, si es true, se muestran solo si el Grupo del usuario tiene el permiso 71" // Descripción de la propiedad [Obligatorio]
		this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, valor, comentario))

	}
	
	private void addDataBaseObjects(){
		if(isOracle()){
			oracleScript()
		}else{
			sqlServerScript()
		}
	}
	
	private void oracleScript(){
		def sentencia = """
			INSERT INTO PERMISOS (NUMERO, DESCRIPCION, TIPO) VALUES (71, 'Agregar y Pegar Firmas', 'P');
		"""
		def descripcion = "Permiso para poder agregar y pegar firmas en el Mantenimiento de firmas." // La descripción es opcional
		this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	
	}
	
	private void sqlServerScript(){
		def sentencia = """
			INSERT INTO PERMISOS (NUMERO, DESCRIPCION, TIPO) VALUES (71, 'Agregar y Pegar Firmas', 'P');
		"""
		def descripcion = "Permiso para poder agregar y pegar firmas en el Mantenimiento de firmas." // La descripción es opcional
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
	}
	
}
