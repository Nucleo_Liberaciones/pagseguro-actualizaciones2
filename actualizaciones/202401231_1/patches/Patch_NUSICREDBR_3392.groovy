package Parches

import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Patch_NUSICREDBR_3392 extends Parche {

	
	void sobreElParche() { // Este método es opcional
		 this.notas_del_autor = "Patch NUSICREDBR_3392"
		 this.opcional = false
	 }
	 
	 void comandos() {
	 
 
	
	// Agregar una propiedad o cambiar el valor a una existente
def archivo = "jts.properties" // Si el archivo no existe, se crea.
def propiedad = "topaz.timeout.reverse.enable"
def valor = "false"
def comentario = "" // Descripci�n de la propiedad [Obligatorio]
this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, valor, comentario))

	

}
}
