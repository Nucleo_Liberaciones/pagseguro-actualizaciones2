import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.dataserver.BorrarQuery
import topsystems.actualizador.command.dataserver.ModificarConsultaQuery
import topsystems.actualizador.parche.Parche

class Parche_NUSICREDBR_3682 extends Parche{

	@Override
	protected void comandos() throws TopazException {
		// TODO Auto-generated method stub
		deleteQueryUltFechaHistorico()
		addQueryUltFechaHistorico()
	}

	private void deleteQueryUltFechaHistorico(){
		// Borrar una query de un DataMapping
def fileName = "interesesdeudoresmapping.xml"
def queryName = "queryUltFechaHistorico"
this.addCommand( new BorrarQuery(fileName, queryName))

	}
	
	private void addQueryUltFechaHistorico(){
		// Agregar o sustituir una query (o varias) de DataMapping
def archivoMapp = "interesesdeudoresmapping.xml"
def queries = """
	    <query>
        <class-name>topsystems.automaticprocess.interesesvista.interesesdeudores.objectsdomain.HistoricoChequeEspecial</class-name>
        <query-name>queryUltFechaHistorico</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
				SELECT FECHA_PROCESO, VDS_SUSPENSO_POR_CALIF FROM VTA_HIST_CHEQUE_ESPECIAL 
				WHERE SALDO_JTS_OID = ? 
				AND FECHA_PROCESO = (
   				SELECT MAX(FECHA_PROCESO)
    			FROM VTA_HIST_CHEQUE_ESPECIAL
    			WHERE SALDO_JTS_OID = ?
    			AND FECHA_PROCESO &lt; ?)
			</sentence>
        <attribute>
            <attribute-name>fechaProceso</attribute-name>
            <column-name>FECHA_PROCESO</column-name>
			<column-type>Date</column-type>
        </attribute>
		<attribute>
            <attribute-name>suspensoPorCalificacion</attribute-name>
            <column-name>VDS_SUSPENSO_POR_CALIF</column-name>
			<column-type>String</column-type>
        </attribute>
    </query>
"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))

	}	
}
