package BANKINGBR

import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.parche.Parche

class Parche_NUPAGSEGUR_278 extends Parche{

	@Override
	protected void comandos() {
		creaProcesoSG();
		creaProcesoAD();
		crearGrupoSG();
		crearGrupoAD();
		crearQueryDeudoresADUnicoSaldo();
		crearQueryDeudoresSGUnicoSaldo();
		modificaQueryDeudoresSG();
		modificaQueryDeudoresAD();
	}

	protected void creaProcesoSG(){
		// Agregar o sustituir un proceso
		def archivo = "processes.py"
		def procesos = """
	    pdef = ProcessDefinition("Cobro Intereses Deudores Unico Saldo", "topsystems.automaticprocess.processmanager.WorkManager")
	    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WDInteresesVistaCobroDeudores");
	    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BWInteresesVistaCobroDeudores");
	    pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
	    pdef.addConstant("rangoCommit","1000");
	    pdef.addConstant("cantidadHilos","40");
	    pdef.addConstant("applyCodigoTransaccion","true");
	    pdef.addConstant("isSumarizable","true");
	    pdef.addConstant("enqueue","false");
	    pdef.addConstant("CAMPO_DEVENGADO_59_DIAS","C1806");
	    pdef.addConstant("Tratar Intereses por AD","false");
	    pdef.addConstant("CAMPO_FECHA_INT_DEUDOR_MIGRADO","1622");
	    pdef.addConstant("CAMPO_INT_DEUDOR_VENCIDO","1726");
	    pdef.addConstant("CAMPO_INT_DEUDOR_VIGENTE","1832");
	    pdef.addConstant("Cobro Intereses Por Cargo","False");
	    pdef.addConstant("NRO_CAMPO_FCH_CANC_CHEQUE_ESP","19000");
	    pdef.addConstant("applyInstalationFields","true")
		pdef.addConstant("QUERY_NAME","query.QuerySaldosCobroDeudoresSGUnicoSaldo");
		pdef.addParameter("JTS_OID",1,"",1);
	    pdefs.addProcess(pdef)
	    """
		this.addCommand(new AgregarModificarProceso(archivo, procesos))
	}

	protected void creaProcesoAD(){
		// Agregar o sustituir un proceso
		def archivo = "processes.py"
		def procesos = """
		pdef = ProcessDefinition("Cobro Intereses Deudores AD Unico Saldo", "topsystems.automaticprocess.processmanager.WorkManager")
	    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WDInteresesVistaCobroDeudores");
	    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BWInteresesVistaCobroDeudores");
	    pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
	    pdef.addConstant("rangoCommit","1000");
	    pdef.addConstant("cantidadHilos","40");
	    pdef.addConstant("applyCodigoTransaccion","true");
	    pdef.addConstant("isSumarizable","true");
	    pdef.addConstant("enqueue","false");
	    pdef.addConstant("Tratar Intereses por AD","true");
	    pdef.addConstant("CAMPO_FECHA_INT_DEUDOR_MIGRADO","1622");
	    pdef.addConstant("CAMPO_INT_DEUDOR_VENCIDO","1685");
	    pdef.addConstant("CAMPO_INT_DEUDOR_VIGENTE","1656");
	    pdef.addConstant("Cobro Intereses Por Cargo","False");
	    pdef.addConstant("NRO_CAMPO_FCH_CANC_CHEQUE_ESP","19000");
	    pdef.addConstant("applyInstalationFields","true");
	    pdef.addConstant("condicionDiaAnteriorParaMultaADPf","NO_AD_PURO");
	    pdef.addConstant("condicionDiaAnteriorParaMultaADPj","NO_AD_PURO");
	    pdef.addConstant("validaMultaPorFechaPf","NUEVO_MES");
	    pdef.addConstant("validaMultaPorFechaPj","NUEVO_MES");
		pdef.addConstant("QUERY_NAME","query.QuerySaldosCobroDeudoresADUnicoSaldo");
		pdef.addParameter("JTS_OID",1,"",1);
	    pdefs.addProcess(pdef)
	    """
		this.addCommand(new AgregarModificarProceso(archivo, procesos))
	}

	protected void crearGrupoAD(){
		// Agregar o sustituir un grupo
		def archivo = "groups.py"
		def grupo = """
	    gdef = GroupDefinition("VTA - Cobro Intereses Deudores AD a un Saldo")
	    gdef.registerProcess("Cobro Intereses Deudores AD Unico Saldo")
	    gdefs.addGroup(gdef)
		"""
		this.addCommand( new AgregarModificarGrupo(archivo, grupo))
	}

	protected void crearGrupoSG(){
		// Agregar o sustituir un grupo
		def archivo = "groups.py"
		def grupo = """
	    gdef = GroupDefinition("VTA - Cobro Intereses Deudores a un Saldo")
	    gdef.registerProcess("Cobro Intereses Deudores Unico Saldo")
	    gdefs.addGroup(gdef)
		"""
		this.addCommand( new AgregarModificarGrupo(archivo, grupo))
	}

	protected void crearQueryDeudoresADUnicoSaldo(){
		// Agregar o sustituir una query (o varias) de DataMapping
		def archivoMapp = "interesesdeudoresmapping.xml"
		def queries = """
	    <query>
	        <class-name>topsystems.automaticprocess.interesesvista.interesesdeudores.objectsdomain.QuerySaldosCobroDeudoresBr</class-name>
	        <query-name>query.QuerySaldosCobroDeudoresADUnicoSaldo</query-name>
	        <database-name>TOP/CLIENTES</database-name>
	        <sentence>
						SELECT S.C1620 AS FECHA_INICIO,
							   S.C1621 AS FECHA_VALOR,
							   S.C1604 AS SALDO_ACTUAL,
							   S.JTS_OID,
							   S.C1813 AS FECHA_DEVENGADO,
							   S.C1820 AS DEVENGADO_COBRO,
							   S.C1822 AS FECHA_COBRO,
							   V.NRO_AUTORIZACION,
							   V.ORDINAL,
							   V.IMPORTE AS MONTO_ACUERDO,
							   V.JTS_OID_CONTINGENCIA,
							   V.FECHA_VENCIMIENTO,
							   V.JTS_SALDO_DEUDA,
							   V.DIA_COBRO,
							   V.DIAS_CARENCIA,
							   V.FECHA_ALTA,  
							   V.TIPO_DIA_COBRO
						  FROM SALDOS S             
						  LEFT JOIN VTA_SOBREGIROS V ON S.JTS_OID = V.JTS_OID_SALDO
						        AND V.TZ_LOCK = 0
						 WHERE S.PRODUCTO IN ( 102,103,107 ) AND S.EMPRESA = 300
						   AND S.C1651 = ' ' and S.C1728 NOT IN ('C','U','W')
						   AND S.C1785 = 2
						   AND S.TZ_LOCK = 0
						   AND V.ESTADO = 1
						   AND S.JTS_OID = ?
						 ORDER BY S.JTS_OID
			</sentence>
		    <attribute>
	        	<attribute-name>fechaInicio</attribute-name>
	            <column-name>FECHA_INICIO </column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaValor</attribute-name>
	            <column-name>FECHA_VALOR</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>saldoActual</attribute-name>
	            <column-name>SALDO_ACTUAL</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>saldos_jts_oid</attribute-name>
	            <column-name>JTS_OID</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaDevengado</attribute-name>
	            <column-name>FECHA_DEVENGADO</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>devengadoCobro</attribute-name>
	            <column-name>DEVENGADO_COBRO</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaCobro</attribute-name>
	            <column-name>FECHA_COBRO</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>nroAcuerdo</attribute-name>
	            <column-name>NRO_AUTORIZACION</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>ordinal</attribute-name>
	            <column-name>ORDINAL</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>montoAcuerdo</attribute-name>
	            <column-name>MONTO_ACUERDO</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>jtsOidContingencia</attribute-name>
	            <column-name>JTS_OID_CONTINGENCIA</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaVencimiento</attribute-name>
	            <column-name>FECHA_VENCIMIENTO</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>jtsSaldoDeuda</attribute-name>
	            <column-name>JTS_SALDO_DEUDA</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>diaCobro</attribute-name>
	            <column-name>DIA_COBRO</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>diasCarencia</attribute-name>
	            <column-name>DIAS_CARENCIA</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaALta</attribute-name>
	            <column-name>FECHA_ALTA</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>tipoDiaCobro</attribute-name>
	            <column-name>TIPO_DIA_COBRO</column-name>
	            <column-type>String</column-type>
	        </attribute>
	    </query>
		"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}

	protected void crearQueryDeudoresSGUnicoSaldo(){
		// Agregar o sustituir una query (o varias) de DataMapping
		def archivoMapp = "interesesdeudoresmapping.xml"
		def queries = """
	    <query>
	        <class-name>topsystems.automaticprocess.interesesvista.interesesdeudores.objectsdomain.QuerySaldosCobroDeudoresBr</class-name>
	        <query-name>query.QuerySaldosCobroDeudoresSGUnicoSaldo</query-name>
	        <database-name>TOP/CLIENTES</database-name>
	        <sentence>
						SELECT S.C1620 AS FECHA_INICIO,
							   S.C1621 AS FECHA_VALOR,
							   S.C1604 AS SALDO_ACTUAL,
							   S.JTS_OID,
							   S.C1813 AS FECHA_DEVENGADO,
							   S.C1820 AS DEVENGADO_COBRO,
							   S.C1822 AS FECHA_COBRO,
							   V.NRO_AUTORIZACION,
							   V.ORDINAL,
							   V.IMPORTE AS MONTO_ACUERDO,
							   V.JTS_OID_CONTINGENCIA,
							   V.FECHA_VENCIMIENTO,
							   V.JTS_SALDO_DEUDA,
							   V.DIA_COBRO,
							   V.DIAS_CARENCIA,
							   V.FECHA_ALTA,	
					   		   V.TIPO_DIA_COBRO
							FROM SALDOS S
						  INNER JOIN VTA_SOBREGIROS V ON S.JTS_OID = V.JTS_OID_SALDO
						  AND S.C1651 = ' ' and S.C1728 NOT IN ('C','U','W')
								 AND S.TZ_LOCK = 0
								 AND V.TZ_LOCK = 0
								 AND V.ESTADO = 1
								 AND S.JTS_OID = ? 
						 ORDER BY S.JTS_OID
						  		</sentence>
	        <attribute>
	            <attribute-name>fechaInicio</attribute-name>
	            <column-name>FECHA_INICIO </column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaValor</attribute-name>
	            <column-name>FECHA_VALOR</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>saldoActual</attribute-name>
	            <column-name>SALDO_ACTUAL</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>saldos_jts_oid</attribute-name>
	            <column-name>JTS_OID</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaDevengado</attribute-name>
	            <column-name>FECHA_DEVENGADO</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>devengadoCobro</attribute-name>
	            <column-name>DEVENGADO_COBRO</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaCobro</attribute-name>
	            <column-name>FECHA_COBRO</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>nroAcuerdo</attribute-name>
	            <column-name>NRO_AUTORIZACION</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>ordinal</attribute-name>
	            <column-name>ORDINAL</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>montoAcuerdo</attribute-name>
	            <column-name>MONTO_ACUERDO</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>jtsOidContingencia</attribute-name>
	            <column-name>JTS_OID_CONTINGENCIA</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaVencimiento</attribute-name>
	            <column-name>FECHA_VENCIMIENTO</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>jtsSaldoDeuda</attribute-name>
	            <column-name>JTS_SALDO_DEUDA</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>diaCobro</attribute-name>
	            <column-name>DIA_COBRO</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>diasCarencia</attribute-name>
	            <column-name>DIAS_CARENCIA</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaALta</attribute-name>
	            <column-name>FECHA_ALTA</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>tipoDiaCobro</attribute-name>
	            <column-name>TIPO_DIA_COBRO</column-name>
	            <column-type>String</column-type>
	        </attribute>
	    </query>
		"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}
	
	protected void modificaQueryDeudoresSG(){
		// Agregar o sustituir una query (o varias) de DataMapping
		def archivoMapp = "interesesdeudoresmapping.xml"
		def queries = """
	    <query>
	        <class-name>topsystems.automaticprocess.interesesvista.interesesdeudores.objectsdomain.QuerySaldosCobroDeudoresBr</class-name>
	        <query-name>query.QuerySaldosCobroDeudoresSG</query-name>
	        <database-name>TOP/CLIENTES</database-name>
	        <sentence>
						SELECT S.C1620 AS FECHA_INICIO,
							   S.C1621 AS FECHA_VALOR,
							   S.C1604 AS SALDO_ACTUAL,
							   S.JTS_OID,
							   S.C1813 AS FECHA_DEVENGADO,
							   S.C1820 AS DEVENGADO_COBRO,
							   S.C1822 AS FECHA_COBRO,
							   V.NRO_AUTORIZACION,
							   V.ORDINAL,
							   V.IMPORTE AS MONTO_ACUERDO,
							   V.JTS_OID_CONTINGENCIA,
							   V.FECHA_VENCIMIENTO,
							   V.JTS_SALDO_DEUDA,
							   V.DIA_COBRO,
							   V.DIAS_CARENCIA,
							   V.FECHA_ALTA,	
					   		   V.TIPO_DIA_COBRO
							FROM SALDOS S
						  INNER JOIN VTA_SOBREGIROS V ON S.JTS_OID = V.JTS_OID_SALDO
						  AND S.C1651 = ' ' and S.C1728 NOT IN ('C','U','W')
								 AND S.TZ_LOCK = 0
								 AND V.TZ_LOCK = 0
								 AND V.ESTADO = 1
						 ORDER BY S.JTS_OID
						  		</sentence>
	        <attribute>
	            <attribute-name>fechaInicio</attribute-name>
	            <column-name>FECHA_INICIO </column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaValor</attribute-name>
	            <column-name>FECHA_VALOR</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>saldoActual</attribute-name>
	            <column-name>SALDO_ACTUAL</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>saldos_jts_oid</attribute-name>
	            <column-name>JTS_OID</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaDevengado</attribute-name>
	            <column-name>FECHA_DEVENGADO</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>devengadoCobro</attribute-name>
	            <column-name>DEVENGADO_COBRO</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaCobro</attribute-name>
	            <column-name>FECHA_COBRO</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>nroAcuerdo</attribute-name>
	            <column-name>NRO_AUTORIZACION</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>ordinal</attribute-name>
	            <column-name>ORDINAL</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>montoAcuerdo</attribute-name>
	            <column-name>MONTO_ACUERDO</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>jtsOidContingencia</attribute-name>
	            <column-name>JTS_OID_CONTINGENCIA</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaVencimiento</attribute-name>
	            <column-name>FECHA_VENCIMIENTO</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>jtsSaldoDeuda</attribute-name>
	            <column-name>JTS_SALDO_DEUDA</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>diaCobro</attribute-name>
	            <column-name>DIA_COBRO</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>diasCarencia</attribute-name>
	            <column-name>DIAS_CARENCIA</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaALta</attribute-name>
	            <column-name>FECHA_ALTA</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>tipoDiaCobro</attribute-name>
	            <column-name>TIPO_DIA_COBRO</column-name>
	            <column-type>String</column-type>
	        </attribute>
	    </query>
		"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}
	
	protected void modificaQueryDeudoresAD(){
		// Agregar o sustituir una query (o varias) de DataMapping
		def archivoMapp = "interesesdeudoresmapping.xml"
		def queries = """
	    <query>
	        <class-name>topsystems.automaticprocess.interesesvista.interesesdeudores.objectsdomain.QuerySaldosCobroDeudoresBr</class-name>
	        <query-name>query.QuerySaldosCobroDeudoresAD</query-name>
	        <database-name>TOP/CLIENTES</database-name>
	        <sentence>
						SELECT S.C1620 AS FECHA_INICIO,
							   S.C1621 AS FECHA_VALOR,
							   S.C1604 AS SALDO_ACTUAL,
							   S.JTS_OID,
							   S.C1813 AS FECHA_DEVENGADO,
							   S.C1820 AS DEVENGADO_COBRO,
							   S.C1822 AS FECHA_COBRO,
							   V.NRO_AUTORIZACION,
							   V.ORDINAL,
							   V.IMPORTE AS MONTO_ACUERDO,
							   V.JTS_OID_CONTINGENCIA,
							   V.FECHA_VENCIMIENTO,
							   V.JTS_SALDO_DEUDA,
							   V.DIA_COBRO,
							   V.DIAS_CARENCIA,
							   V.FECHA_ALTA,  
							   V.TIPO_DIA_COBRO
						  FROM SALDOS S             
						  LEFT JOIN VTA_SOBREGIROS V ON S.JTS_OID = V.JTS_OID_SALDO
						        AND V.TZ_LOCK = 0
						 WHERE S.PRODUCTO IN ( 102,103,107 ) AND S.EMPRESA = 300
						   AND S.C1651 = ' ' and S.C1728 NOT IN ('C','U','W')
						   AND S.C1785 = 2
						   AND S.TZ_LOCK = 0
						   AND V.ESTADO = 1
						 ORDER BY S.JTS_OID
			</sentence>
		    <attribute>
	        	<attribute-name>fechaInicio</attribute-name>
	            <column-name>FECHA_INICIO </column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaValor</attribute-name>
	            <column-name>FECHA_VALOR</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>saldoActual</attribute-name>
	            <column-name>SALDO_ACTUAL</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>saldos_jts_oid</attribute-name>
	            <column-name>JTS_OID</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaDevengado</attribute-name>
	            <column-name>FECHA_DEVENGADO</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>devengadoCobro</attribute-name>
	            <column-name>DEVENGADO_COBRO</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaCobro</attribute-name>
	            <column-name>FECHA_COBRO</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>nroAcuerdo</attribute-name>
	            <column-name>NRO_AUTORIZACION</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>ordinal</attribute-name>
	            <column-name>ORDINAL</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>montoAcuerdo</attribute-name>
	            <column-name>MONTO_ACUERDO</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>jtsOidContingencia</attribute-name>
	            <column-name>JTS_OID_CONTINGENCIA</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaVencimiento</attribute-name>
	            <column-name>FECHA_VENCIMIENTO</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>jtsSaldoDeuda</attribute-name>
	            <column-name>JTS_SALDO_DEUDA</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>diaCobro</attribute-name>
	            <column-name>DIA_COBRO</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>diasCarencia</attribute-name>
	            <column-name>DIAS_CARENCIA</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaALta</attribute-name>
	            <column-name>FECHA_ALTA</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>tipoDiaCobro</attribute-name>
	            <column-name>TIPO_DIA_COBRO</column-name>
	            <column-type>String</column-type>
	        </attribute>
	    </query>
		"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}
}
