package TOPAZ_56

import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.parche.Parche

class Parche_NUTOPAZ56_82 extends Parche {

	void sobreElParche() {
		this.notas_del_autor = "Parche_NUTOPAZ56_82"
		this.opcional = false
	}

	@Override
	protected void comandos() {
		addRemoteExecutionService()
		addDatabaseCheckStatusService()
	}
	
	private void addRemoteExecutionService(){
		// Agregar o sustituir uno o varios servicios en un mismo archivo
		def archivo = "AutomaticProcessRemoteExecutionService.xml"
		def servicios = """
		<service>
			<name>topsystems:service:core:=RemoteProcessDispatcherService</name>
			<code>topsystems.processmgr.core.remote.execution.RemoteProcessDispatcherHttpService</code>
			<parameters>
				<parameter>
					<name>connectTimeout</name>
					<value>45000</value>
				</parameter>
				<parameter>
					<name>responseTimeout</name>
					<value>60000</value>
				</parameter>
			</parameters>
		</service>
		"""
		this.addCommand( new AgregarModificarServicios(archivo, servicios))

	}
	
	private void addDatabaseCheckStatusService(){
		// Agregar o sustituir uno o varios servicios en un mismo archivo
		def archivo = "AutomaticProcessRemoteExecutionService.xml"
		def servicios = """
		<service>
			<name>topsystems:service:core:=ProcessRemoteCheckStatusService</name>
			<code>topsystems.processmgr.core.remote.check.status.ProcessRemoteDatabaseCheckStatusService</code>
			<parameters>
				<parameter>
					<name>retryTime</name>
					<value>10000</value>
				</parameter>
			</parameters>
		</service>
		"""
		this.addCommand( new AgregarModificarServicios(archivo, servicios))

	}
	
}
