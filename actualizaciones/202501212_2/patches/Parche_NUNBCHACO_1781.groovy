package chaco

import topsystems.TopazException
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche

class Parche_NUNBCHACO_1781 extends Parche {

	@Override
	protected void comandos() throws TopazException {
		crearParametrosJTS()
	}
	
	private void crearParametrosJTS(){
		def descripcion = "Parametro que indica el evento para el cobro de IIBB CABA si corresponde"
		def sentencia = """
			INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
			VALUES ('COBRANZA_AUTOMATICA', 'EVENTO_IIBB', '0', 0);
		"""
		this.addCommand(new ActualizarBaseDeDatos(sentencia, descripcion))
	}
}
