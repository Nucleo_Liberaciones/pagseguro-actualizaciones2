package banking

import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche

class Parche_NUBANKIBR_454  extends Parche{

	@Override
	protected void comandos() throws TopazException {
		addNewAttributeToMappingEntity()
		insertarParametrosJTS()
	}
	
	private void addNewAttributeToMappingEntity(){
		def archivoMapp = "DevengamientoMapping.xml"
		def entidad = "core.vo_SaldoDevengar"
		def atributo = """
			<attribute>
				<attribute-name>SAL_ATIVO_PROBLEMATICO</attribute-name>
				<column-name>ATIVO_PROBLEMATICO</column-name>
			</attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
	}
	
	private void insertarParametrosJTS(){
		def descripcion = "Parametro que indica si se debe actualizar el activo problematico en IFRS9"
		def sentencia = """
			INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
			VALUES ('DEVENGAMIENTO', 'ACTUALIZA_ACTIVO_PROBLEMATICO', 'N', 0);
		"""
		this.addCommand(new ActualizarBaseDeDatos(sentencia, descripcion))		
	}

}
