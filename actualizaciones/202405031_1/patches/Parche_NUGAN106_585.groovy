package GANADERO

import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.parche.Parche

class Parche_NUGAN106_585 extends Parche {

	void sobreElParche() {
		this.notas_del_autor = "Parche_NUGAN106_585"
		this.opcional = false
	}

	@Override
	protected void comandos() {
		addPropertyCommand()
	}
	
	private void addPropertyCommand(){
		def archivo = "mscl.properties"
		def propiedad = "topaz.login.unique.error.response.enable"
		def valor = "false"
		def comentario = "Habilita mostrar una �nica respuesta cuando el login falla"
		this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, valor, comentario))

	}
}
