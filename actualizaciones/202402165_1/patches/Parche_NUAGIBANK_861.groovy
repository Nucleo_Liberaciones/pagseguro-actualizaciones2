package agibank

import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.parche.Parche

class Parche_NUAGIBANK_861 extends Parche{
	

	void definicionDeParametros(){
		this.addParameterNewFreeField("MONTO_PREPAGO", "Monto del prepago para la tabla SCL_DETALLE_CREDITOS")
		this.addParameterNewFreeField("MONTO_PREPAGO_DEBITADO", "Monto que se logra debitar del prepago para la tabla SCL_DETALLE_CREDITOS")
		this.addParameterNewFreeField("TRANSITORIA_DEBITO_PREPAGO", "Jts oid para realizar el credito del monto prepago para la tabla PCT_CONVENIOS_SALDOS")
		this.addParameterQuery("Descriptor_SCL_DETALLE_CREDITOS", "Obtener descriptor de la tabla SCL_DETALLE_CREDITOS", "INTEGER", "SELECT IDENTIFICACION FROM DESCRIPTORES WHERE UPPER(NOMBREFISICO) LIKE 'SCL_DETALLE_CREDITOS'")
		this.addParameterQuery("Descriptor_PCT_CONVENIOS_SALDOS", "Obtener descriptor de la tabla PCT_CONVENIOS_SALDOS", "INTEGER", "SELECT IDENTIFICACION FROM DESCRIPTORES WHERE UPPER(NOMBREFISICO) LIKE 'PCT_CONVENIOS_SALDOS'")
	}
	
	@Override
	protected void comandos() throws TopazException {
		addEntriesToBD()
		cambiosEstructurasBD()
		addAttributeDetallePago()
		addAttributeConvenioSaldos()
	}
	
	protected void addEntriesToBD(){
		def sentencia = """
			INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
			VALUES (${this.P('MONTO_PREPAGO')}, ' ', 0, 'MONTO PREPAGO', 'MONTO_PREPAGO', 15, 'N', 2, 'I', 0, 0, 0, 0, 0, 0, 0, ${this.P('Descriptor_SCL_DETALLE_CREDITOS')}, 'MONTO_PREPAGO', 0, NULL);

			INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
			VALUES (${this.P('MONTO_PREPAGO_DEBITADO')}, ' ', 0, 'MONTO DEBITADO PREPAGO', 'MONTO_PREPAGO_DEBITADO', 15, 'N', 2, 'I', 0, 0, 0, 0, 0, 0, 0, ${this.P('Descriptor_SCL_DETALLE_CREDITOS')}, 'MONTO_PREPAGO_DEBITADO', 0, NULL);

			INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
			VALUES (${this.P('TRANSITORIA_DEBITO_PREPAGO')}, ' ', 0, 'JTS OID DE CTA TRANSITORIA PREP', 'TRANSITORIA_DEBITO_PREPAGO', 10, 'N', 0, 'I', 0, 0, 0, 0, 0, 0, 0, ${this.P('Descriptor_PCT_CONVENIOS_SALDOS')}, 'TRANSITORIA_DEBITO_PREPAGO', 0, NULL);

			INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
			VALUES ('PAGO_SUELDOS_Y_PROVEEDORES', 'NIVEL_PRIORIDAD_RESERVA', '0', 0);
		"""
		this.addCommand(new ActualizarBaseDeDatos(sentencia))
	}
	
	protected void cambiosEstructurasBD(){
		def sentencia = """
		ALTER TABLE PCT_CONVENIOS_SALDOS ADD TRANSITORIA_DEBITO_PREPAGO NUMBER (10) DEFAULT(0);
		ALTER TABLE SCL_DETALLE_CREDITOS ADD MONTO_PREPAGO NUMBER (15,2) DEFAULT(0);
		ALTER TABLE SCL_DETALLE_CREDITOS ADD MONTO_PREPAGO_DEBITADO NUMBER (15,2) DEFAULT(0);
		"""
		this.addCommand( new ActualizarBaseDeDatosOracle(sentencia))
	}
	
	private void addAttributeDetallePago(){
		def archivoMapp = "PagosAutomaticosMapping.xml"
		def entidad = "core.vo_DetalleDePago"
		def atributo = """
        <attribute>
            <attribute-name>monto_prepago</attribute-name>
            <column-name>MONTO_PREPAGO</column-name>
        </attribute>
        <attribute>
            <attribute-name>monto_prepago_debitado</attribute-name>
            <column-name>MONTO_PREPAGO_DEBITADO</column-name>
        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
	}
		
	private void addAttributeConvenioSaldos(){
		def archivoMapp = "PagosAutomaticosMapping.xml"
		def entidad = "core.vo_ConveniosSaldos"
		def atributo = """
        <attribute>
            <attribute-name>transitoriaDebitoPrepago</attribute-name>
            <column-name>TRANSITORIA_DEBITO_PREPAGO</column-name>
        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
	}
}
