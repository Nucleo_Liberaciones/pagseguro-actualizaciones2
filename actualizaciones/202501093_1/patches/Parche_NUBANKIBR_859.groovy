package brasil

import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.parche.Parche

class Parche_NUBANKIBR_859 extends Parche{
	
	void sobreElParche() {
		this.notas_del_autor = "Parche_NUBANKIBR_859"
		this.opcional = false
	}

	@Override
	protected void comandos() throws TopazException {
		agregarConstanteEnProcesos()
		agregarQueryMapping()
	}
	
	protected void agregarConstanteEnProcesos(){
		def errorMsg = ""
		def archivoPro = "processes.py"
		def nombreProceso = "Reclasificacion Ingresos Renegociados NIIF9"
		def constParamProc = """
				pdef.addConstant("regenerar","true");
			"""	
		this.addCommand( new AgregarModificarPropiedadesProceso(archivoPro, nombreProceso, constParamProc))
	}
	
	protected void agregarQueryMapping(){
		// Agregar o sustituir una query (o varias) de DataMapping
		def archivoMap = "RastreabilidadeIFRS9Mapping.xml"
		def queries = """
	    <query>
	        <class-name>topsystems.automaticprocess.ReclasificacionIngresosNIIF9.RastreabilidadeIFRS9</class-name>
	        <query-name>query.ReclasificacionIngresosNIIF9RenegociadosRegenerarInt</query-name>
	        <database-name>TOP/CLIENTES</database-name>
	        <sentence>
				SELECT 
					rf.FECHA,
					rf.JTS_OID_SALDO,
					rf.TIPO_IMPORTE,
					rf.IMPORTE_A_DEBITAR
				FROM RASTREABILIDADE_IFRS9 rf
				INNER JOIN saldos s ON rf.JTS_OID_SALDO = s.JTS_OID
				WHERE s.PRODUCTO IN (SELECT p.C6250 FROM PRODUCTOS p WHERE p.RENEGOCIADO = 'S' AND p.TZ_LOCK = 0)  
				AND s.TZ_LOCK = 0 
				AND s.c1785 = 5 
				AND upper(rf.TIPO_IMPORTE) = 'INTERES'
				AND (rf.ATIVO_PROBLEMATICO IS NULL OR rf.ATIVO_PROBLEMATICO = ' ')				        
			</sentence>
	        <attribute>
	            <attribute-name>fecha</attribute-name>
	            <column-name>FECHA</column-name>
				<column-type>Timestamp</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>jtsOidSaldo</attribute-name>
	            <column-name>JTS_OID_SALDO</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>tipoImporte</attribute-name>
	            <column-name>TIPO_IMPORTE</column-name>
				<column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>importeADebitar</attribute-name>
	            <column-name>IMPORTE_A_DEBITAR</column-name>
				<column-type>double</column-type>
	        </attribute>       
	    </query>
		"""
		this.addCommand(new AgregarModificarQuery(archivoMap, queries))		
	}


}
