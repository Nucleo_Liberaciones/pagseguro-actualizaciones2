package Parches

import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Patch_NUSICREDBR_3346 extends Parche {

	
	void definicionDeParametros(){

	
	}
	
	
void sobreElParche() { // Este metodo es opcional
	 this.notas_del_autor = "Patch_NUSICREDBR-3346"
	 this.opcional = false
}
	 
void comandos() {

// Agregar o sustituir uno o varios servicios en un mismo archivo
def archivo = "MovimientosOfflineServices.xml"
def servicios = """
   <service>
       <name>topsystems:processManager:WorkDescriptor=WDOfflineMovementApplication</name>
       <code>topsystems.automaticprocess.offlinemovementapplication.WDOfflineMovementApplicationPreparedStatementNew</code>
   </service>
"""
this.addCommand( new AgregarModificarServicios(archivo, servicios))


}
			

		
	
			
			
}
