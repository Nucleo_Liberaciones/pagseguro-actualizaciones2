package CREDISIS

import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche
import topsystems.actualizador.configuration.CarpetasServidor

class Parche_NUCREDISIS_176 extends Parche {
	
	def isSicredi = false
	
	@Override
	protected void comandos() throws TopazException {
		isSicredi = isSicredi()
		if (!isSicredi){
			agregarParametrosJts()
		}
	}
	
	private void agregarParametrosJts() {
		def sentencia = """
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK) VALUES('PROCESO_CAMBIO_SUCURSAL', 'PIVOT_INT_VIGENTE_CH_ESPECIAL', '0', 0);
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK) VALUES('PROCESO_CAMBIO_SUCURSAL', 'PIVOT_INT_SUSPENSOS_CH_ESPECIAL', '0', 0);
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK) VALUES('PROCESO_CAMBIO_SUCURSAL', 'PIVOT_INTERESES_AD_VIGENTES', '0', 0);
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK) VALUES('PROCESO_CAMBIO_SUCURSAL', 'PIVOT_INTERESES_AD_SUSPENSOS', '0', 0);
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK) VALUES('PROCESO_CAMBIO_SUCURSAL', 'NRO_CAMPO_INT_VIGENTES', '0', 0);
	    """
		def descripcion = "Agrego nuevos parametros jts para cheque especial"
		this.addCommand(new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	private boolean isSicredi(){
		def jarName = "sic_br_tb";
		def directoryLib = new File(CarpetasServidor.SERVER_LIB.getExpandedPath());
	
		if(directoryLib.exists() && directoryLib.isDirectory()){
			File[] filesJars = directoryLib.listFiles({ File dir, String file ->
				file.startsWith(jarName) && file.endsWith(".jar")
			} as FilenameFilter
			)
			if( filesJars.length > 0 ){
				return true;
			}
		}
		return false;
	}

}
