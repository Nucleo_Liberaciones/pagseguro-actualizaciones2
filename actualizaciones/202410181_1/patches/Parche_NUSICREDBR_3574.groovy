package Base

import topsystems.actualizador.command.ActualizacionCommand
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.CarpetasActualizador
import topsystems.actualizador.parche.Parche

class Parche_NUSICREDBR_3574 extends Parche {

	void sobreElParche() {
		this.notas_del_autor = "Parche_NUSICREDBR_3574"
		this.opcional = false
	}
	
		
	@Override
	protected void comandos() {
		adicionarQueryDatamapping()
		adicionarProcesoReclasificaciones()
		adicionarServicioReclasificaciones()
	}

	 private adicionarQueryDatamapping(){ 
		 // Agregar o sustituir una query (o varias) de DataMapping
		 def archivoMap = "RastreabilidadeIFRS9Mapping.xml"
		 def baseOra = esBaseDeDatos("ORACLE")
		 if(baseOra){
		 def queries = """
		    <query>
		        <class-name>topsystems.automaticprocess.ReclasificacionIngresosNIIF9.RastreabilidadeIFRS9</class-name>
		        <query-name>query.Reclasificaciones</query-name>
		        <database-name>TOP/CLIENTES</database-name>
		        <sentence>
			 	SELECT
					FECHA , 
					jts_oid AS JTS_OID_SALDO,
					TIPO_IMPORTE,
					SUCURSAL, 
					PRODUCTO,
					CUENTA, 
					MONEDA, 
					OPERACION,
					ORDINAL,
					c1628 AS FECHA_VENCIMIENTO, 
					ATIVO_PROBLEMATICO,
					ESTADO_SUSPENSO, 
					RUBRO_CONTABLE_OPERACION, 
					FECHA_DEVENGADO,
					IMPORTE_DEVENGADO_CON_STOP, 
					IMPORTE_DEVENGADO_SIN_STOP, 
					IMPORTE_DEBITAR,
					rubro_debitar,		
					EMPRESA, 
					SUCURSAL_EMPRESA
				FROM (SELECT 
						hd.FECHA , 
						s.jts_oid,
						'INTERES' AS TIPO_IMPORTE,
						s.SUCURSAL, 
						s.PRODUCTO,
						s.CUENTA, 
						s.MONEDA, 
						s.OPERACION,
						s.ORDINAL,
						s.c1628, 
						s.ATIVO_PROBLEMATICO,
						hd.ESTADO_SUSPENSO, 
						hd.RUBRO_CONTABLE_OPERACION, 
						hd.FECHA_DEVENGADO,
						hd.INTERES_DEVENG_VIGENTE_CONT AS IMPORTE_DEVENGADO_CON_STOP, 
						hd.INTERES_DEVENG_VIGENTE_CONT + hd.INTERES_DEVENG_SUSPENSO_CONT AS IMPORTE_DEVENGADO_SIN_STOP, 
						hd.INTERES_DEVENG_SUSPENSO_CONT AS IMPORTE_DEBITAR,
						e.RUBROSALIDA AS rubro_debitar,		
						s.EMPRESA, 
						s.SUCURSAL_EMPRESA		
					FROM  SALDOS s,
							HISTORICO_DEVENGAMIENTO hd,
							ESQUEMACONTABLE e 
					WHERE s.JTS_OID = hd.SALDO_JTS_OID AND hd.FECHA = s.C1813
					AND s.TZ_LOCK = 0 AND s.c1785 = 5 AND s.c1604 &lt; 0 
					AND s.c1728 &lt;&gt; 'C'
					AND s.c1813 =  ?
					AND ( s.ATIVO_PROBLEMATICO is NULL or s.ATIVO_PROBLEMATICO = ' ' ) 
					AND s.C1729 = 'S'
					AND  ? - s.c1628 BETWEEN 60 AND 90
					AND e.RUBRO = s.c1804 AND e.D_DC = 'C' 
					AND e.CAMPO = (SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_INTERES_DEVENGADO_VIGENTE')
					AND hd.INTERES_DEVENG_SUSPENSO_CONT &gt; 0
					UNION ALL 
					SELECT 
						hd.FECHA , 
						s.jts_oid,
						'MORA' AS TIPO_IMPORTE,
						s.SUCURSAL, 
						s.PRODUCTO,
						s.CUENTA, 
						s.MONEDA, 
						s.OPERACION,
						s.ORDINAL,
						s.c1628, 
						s.ATIVO_PROBLEMATICO,
						hd.ESTADO_SUSPENSO, 
						hd.RUBRO_CONTABLE_OPERACION, 
						hd.FECHA_DEVENGADO,
						hd.MORA_DEVENG_VIGENTE_CONT AS IMPORTE_DEVENGADO_CON_STOP, 
						hd.MORA_DEVENG_VIGENTE_CONT + hd.MORA_DEVENG_SUSPENSO_CONT AS IMPORTE_DEVENGADO_SIN_STOP, 
						hd.MORA_DEVENG_SUSPENSO_CONT AS IMPORTE_DEBITAR,
						e.RUBROSALIDA AS rubro_debitar,		
						s.EMPRESA, 
						s.SUCURSAL_EMPRESA
					FROM  SALDOS s
						, HISTORICO_DEVENGAMIENTO hd 
						, ESQUEMACONTABLE e 
					WHERE s.JTS_OID = hd.SALDO_JTS_OID AND hd.FECHA = s.C1813
					AND s.TZ_LOCK = 0 AND s.c1785 = 5 AND s.c1604 &lt; 0
					AND s.c1728 &lt;&gt; 'C'
					AND s.c1813 =  ?
					AND ( s.ATIVO_PROBLEMATICO is NULL or s.ATIVO_PROBLEMATICO = ' ' ) 
					AND s.C1729 = 'S'
					AND  ? - s.c1628 BETWEEN 60 AND 90 
					AND e.RUBRO = s.c1804 AND e.D_DC = 'C' 
					AND e.CAMPO = (SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_MORA_DEVENGADA_VIGENTE')
					AND hd.MORA_DEVENG_SUSPENSO_CONT &gt; 0
					UNION ALL 
					SELECT 
						hd.FECHA , 
						s.jts_oid,
						'IPF' AS TIPO_IMPORTE,
						s.SUCURSAL, 
						s.PRODUCTO,
						s.CUENTA, 
						s.MONEDA, 
						s.OPERACION,
						s.ORDINAL,
						s.c1628, 
						s.ATIVO_PROBLEMATICO,
						hd.ESTADO_SUSPENSO, 
						hd.RUBRO_CONTABLE_OPERACION, 
						hd.FECHA_DEVENGADO,
						hd.IPF_DEVENG_VIGENTE_CONT AS IMPORTE_DEVENGADO_CON_STOP, 
						hd.IPF_DEVENG_VIGENTE_CONT + hd.IPF_DEVENG_SUSPENSO_CONT AS IMPORTE_DEVENGADO_SIN_STOP, 
						hd.IPF_DEVENG_SUSPENSO_CONT AS IMPORTE_DEBITAR,
						e.RUBROSALIDA AS rubro_debitar,		
						s.EMPRESA, 
						s.SUCURSAL_EMPRESA
					FROM  SALDOS s
						, HISTORICO_DEVENGAMIENTO hd 
						, ESQUEMACONTABLE e 
					WHERE s.JTS_OID = hd.SALDO_JTS_OID AND hd.FECHA = s.C1813
					AND s.TZ_LOCK = 0 AND s.c1785 = 5 AND s.c1604 &lt; 0
					AND s.c1728 &lt;&gt; 'C'
					AND s.c1813 =  ?
					AND ( s.ATIVO_PROBLEMATICO is NULL or s.ATIVO_PROBLEMATICO = ' ' )
					AND s.C1729 = 'S'
					AND  ? - s.c1628 BETWEEN 60 AND 90
					AND e.RUBRO = s.c1804 AND e.D_DC = 'C' 
					AND e.CAMPO = (SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_IPF_DEVENGADO_VIGENTE')
					AND hd.IPF_DEVENG_SUSPENSO_CONT &gt; 0
					UNION ALL 
					SELECT 
						hd.FECHA , 
						s.jts_oid,
						'IPFMORATORIO' AS TIPO_IMPORTE,
						s.SUCURSAL, 
						s.PRODUCTO,
						s.CUENTA, 
						s.MONEDA, 
						s.OPERACION,
						s.ORDINAL,
						s.c1628, 
						s.ATIVO_PROBLEMATICO,
						hd.ESTADO_SUSPENSO, 
						hd.RUBRO_CONTABLE_OPERACION, 
						hd.FECHA_DEVENGADO,
						hd.IPF_MORATORIO_DEV_VIG_CONT AS IMPORTE_DEVENGADO_CON_STOP, 
						hd.IPF_MORATORIO_DEV_VIG_CONT + hd.IPF_MORATORIO_DEV_SUSP_CONT AS IMPORTE_DEVENGADO_SIN_STOP, 
						hd.IPF_MORATORIO_DEV_SUSP_CONT AS IMPORTE_DEBITAR,
						e.RUBROSALIDA AS rubro_debitar,		
						s.EMPRESA, 
						s.SUCURSAL_EMPRESA
					FROM  SALDOS s
						, HISTORICO_DEVENGAMIENTO hd 
						, ESQUEMACONTABLE e 
					WHERE s.JTS_OID = hd.SALDO_JTS_OID AND hd.FECHA = s.C1813
					AND s.TZ_LOCK = 0 AND s.c1785 = 5 AND s.c1604 &lt; 0
					AND s.c1728 &lt;&gt; 'C'
					AND s.c1813 =  ?
					AND ( s.ATIVO_PROBLEMATICO is NULL or s.ATIVO_PROBLEMATICO = ' ')
					AND s.C1729 = 'S'
					AND ? - s.c1628 BETWEEN 60 AND 90
					AND e.RUBRO = s.c1804 AND e.D_DC = 'C' 
					AND e.CAMPO = (SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_IPF_MORATORIO_DEV_VIGENTE')
					AND hd.IPF_MORATORIO_DEV_SUSP_CONT &gt; 0)
			</sentence>
			<attribute>
		        <attribute-name>fecha</attribute-name>
		        <column-name>FECHA</column-name>
		 		<column-type>Timestamp</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>jtsOidSaldo</attribute-name>
		        <column-name>JTS_OID_SALDO</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>tipoImporte</attribute-name>
		        <column-name>TIPO_IMPORTE</column-name>
		 		<column-type>String</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>sucursal</attribute-name>
		        <column-name>SUCURSAL</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>producto</attribute-name>
		        <column-name>PRODUCTO</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>cuenta</attribute-name>
		        <column-name>CUENTA</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>moneda</attribute-name>
		        <column-name>MONEDA</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>operacion</attribute-name>
		        <column-name>OPERACION</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>ordinal</attribute-name>
		        <column-name>ORDINAL</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>fechaVencimiento</attribute-name>
		        <column-name>FECHA_VENCIMIENTO</column-name>
		 		<column-type>Timestamp</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>ativoProblema</attribute-name>
		        <column-name>ATIVO_PROBLEMATICO</column-name>
		 		<column-type>String</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>estadoSuspenso</attribute-name>
		        <column-name>ESTADO_SUSPENSO</column-name>
		 		<column-type>String</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>rubroContableDevengado</attribute-name>
		        <column-name>RUBRO_CONTABLE_OPERACION</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>fechaDevengado</attribute-name>
		        <column-name>FECHA_DEVENGADO</column-name>
		 		<column-type>Timestamp</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>importeDevengadoConStop</attribute-name>
		        <column-name>IMPORTE_DEVENGADO_CON_STOP</column-name>
		 		<column-type>double</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>importeDevengadoSinStop</attribute-name>
		        <column-name>IMPORTE_DEVENGADO_SIN_STOP</column-name>
		 		<column-type>double</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>importeADebitar</attribute-name>
		        <column-name>IMPORTE_DEBITAR</column-name>
		 		<column-type>double</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>rubroADebitar</attribute-name>
		        <column-name>rubro_debitar</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>empresaSaldo</attribute-name>
		        <column-name>EMPRESA</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>sucursalEmpresaSaldo</attribute-name>
		        <column-name>SUCURSAL_EMPRESA</column-name>
		 		<column-type>String</column-type>
		    </attribute>
	    </query>
		"""
		 this.addCommand(new AgregarModificarQuery(archivoMap, queries))
		 }else{
		 def querie = """
	    <query>
	        <class-name>topsystems.automaticprocess.ReclasificacionIngresosNIIF9.RastreabilidadeIFRS9</class-name>
	        <query-name>query.Reclasificaciones</query-name>
	        <database-name>TOP/CLIENTES</database-name>
	        <sentence>
		 	 SELECT 
			        hd.FECHA AS FECHA , 
			        s.jts_oid AS JTS_OID_SALDO,
			        'INTERES' AS TIPO_IMPORTE,
			        s.SUCURSAL, 
			        s.PRODUCTO,
			        s.CUENTA, 
			        s.MONEDA, 
			        s.OPERACION,
			        s.ORDINAL,
			        s.c1628 AS FECHA_VENCIMIENTO, 
			        s.ATIVO_PROBLEMATICO,
			        hd.ESTADO_SUSPENSO, 
			        hd.RUBRO_CONTABLE_OPERACION, 
			        hd.FECHA_DEVENGADO,
			        hd.INTERES_DEVENG_VIGENTE_CONT AS IMPORTE_DEVENGADO_CON_STOP, 
			        hd.INTERES_DEVENG_VIGENTE_CONT + hd.INTERES_DEVENG_SUSPENSO_CONT AS IMPORTE_DEVENGADO_SIN_STOP, 
			        hd.INTERES_DEVENG_SUSPENSO_CONT AS IMPORTE_DEBITAR,
			        e.RUBROSALIDA AS rubro_debitar,        
			        s.EMPRESA, 
			        s.SUCURSAL_EMPRESA        
			    FROM SALDOS s
			    JOIN HISTORICO_DEVENGAMIENTO hd ON s.JTS_OID = hd.SALDO_JTS_OID AND hd.FECHA = s.C1813
			    JOIN ESQUEMACONTABLE e ON e.RUBRO = s.c1804 AND e.D_DC = 'C'
			    WHERE s.TZ_LOCK = 0 
			      AND s.c1785 = 5 
			      AND s.c1604 &lt; 0 
			      AND s.c1728 &lt;&gt; 'C'
			      AND s.c1813 = ?
			      AND (s.ATIVO_PROBLEMATICO IS NULL OR s.ATIVO_PROBLEMATICO = '') 
			      AND s.C1729 = 'S'
			      AND DATEDIFF(DAY, s.c1628, ?) BETWEEN 60 AND 90 
			      AND e.CAMPO = (SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_INTERES_DEVENGADO_VIGENTE')
			      AND hd.INTERES_DEVENG_SUSPENSO_CONT &gt; 0
			    UNION ALL 
			    SELECT 
			        hd.FECHA , 
			        s.jts_oid,
			        'MORA' AS TIPO_IMPORTE,
			        s.SUCURSAL, 
			        s.PRODUCTO,
			        s.CUENTA, 
			        s.MONEDA, 
			        s.OPERACION,
			        s.ORDINAL,
			        s.c1628, 
			        s.ATIVO_PROBLEMATICO,
			        hd.ESTADO_SUSPENSO, 
			        hd.RUBRO_CONTABLE_OPERACION, 
			        hd.FECHA_DEVENGADO,
			        hd.MORA_DEVENG_VIGENTE_CONT AS IMPORTE_DEVENGADO_CON_STOP, 
			        hd.MORA_DEVENG_VIGENTE_CONT + hd.MORA_DEVENG_SUSPENSO_CONT AS IMPORTE_DEVENGADO_SIN_STOP, 
			        hd.MORA_DEVENG_SUSPENSO_CONT AS IMPORTE_DEBITAR,
			        e.RUBROSALIDA AS rubro_debitar,        
			        s.EMPRESA, 
			        s.SUCURSAL_EMPRESA
			    FROM SALDOS s
			    JOIN HISTORICO_DEVENGAMIENTO hd ON s.JTS_OID = hd.SALDO_JTS_OID AND hd.FECHA = s.C1813
			    JOIN ESQUEMACONTABLE e ON e.RUBRO = s.c1804 AND e.D_DC = 'C'
			    WHERE s.TZ_LOCK = 0 
			      AND s.c1785 = 5 
			      AND s.c1604 &lt; 0
			      AND s.c1728 &lt;&gt; 'C'
			      AND s.c1813 = ?
			      AND (s.ATIVO_PROBLEMATICO IS NULL OR s.ATIVO_PROBLEMATICO = '')
			      AND s.C1729 = 'S'
			      AND DATEDIFF(DAY, s.c1628, ?) BETWEEN 60 AND 90
			      AND e.CAMPO = (SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_MORA_DEVENGADA_VIGENTE')
			      AND hd.MORA_DEVENG_SUSPENSO_CONT &gt; 0
				UNION ALL 
				SELECT 
					hd.FECHA , 
					s.jts_oid,
					'IPF' AS TIPO_IMPORTE,
					s.SUCURSAL, 
					s.PRODUCTO,
					s.CUENTA, 
					s.MONEDA, 
					s.OPERACION,
					s.ORDINAL,
					s.c1628, 
					s.ATIVO_PROBLEMATICO,
					hd.ESTADO_SUSPENSO, 
					hd.RUBRO_CONTABLE_OPERACION, 
					hd.FECHA_DEVENGADO,
					hd.IPF_DEVENG_VIGENTE_CONT AS IMPORTE_DEVENGADO_CON_STOP, 
					hd.IPF_DEVENG_VIGENTE_CONT + hd.IPF_DEVENG_SUSPENSO_CONT AS IMPORTE_DEVENGADO_SIN_STOP, 
					hd.IPF_DEVENG_SUSPENSO_CONT AS IMPORTE_DEBITAR,
					e.RUBROSALIDA AS rubro_debitar,		
					s.EMPRESA, 
					s.SUCURSAL_EMPRESA 
				FROM SALDOS s
			    JOIN HISTORICO_DEVENGAMIENTO hd ON s.JTS_OID = hd.SALDO_JTS_OID AND hd.FECHA = s.C1813
			    JOIN ESQUEMACONTABLE e ON e.RUBRO = s.c1804 AND e.D_DC = 'C'
				WHERE s.TZ_LOCK = 0 
				AND s.c1785 = 5 
				AND s.c1604 &lt; 0 
				AND s.c1728 &lt;&gt; 'C' 
				AND s.c1813 = ?   	
				AND ( s.ATIVO_PROBLEMATICO is NULL or s.ATIVO_PROBLEMATICO = ' ' ) 
				AND s.C1729 = 'S' 
				AND DATEDIFF(DAY, s.c1628, ?) BETWEEN 60 AND 90 
				AND e.CAMPO = (SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_IPF_DEVENGADO_VIGENTE')
				AND hd.IPF_DEVENG_SUSPENSO_CONT &gt; 0
				UNION ALL 
				SELECT 
					hd.FECHA , 
					s.jts_oid,
					'IPFMORATORIO' AS TIPO_IMPORTE,
					s.SUCURSAL, 
					s.PRODUCTO,
					s.CUENTA, 
					s.MONEDA, 
					s.OPERACION,
					s.ORDINAL,
					s.c1628, 
					s.ATIVO_PROBLEMATICO,
					hd.ESTADO_SUSPENSO, 
					hd.RUBRO_CONTABLE_OPERACION, 
					hd.FECHA_DEVENGADO,
					hd.IPF_MORATORIO_DEV_VIG_CONT AS IMPORTE_DEVENGADO_CON_STOP, 
					hd.IPF_MORATORIO_DEV_VIG_CONT + hd.IPF_MORATORIO_DEV_SUSP_CONT AS IMPORTE_DEVENGADO_SIN_STOP, 
					hd.IPF_MORATORIO_DEV_SUSP_CONT AS IMPORTE_DEBITAR,
					e.RUBROSALIDA AS rubro_debitar,		
					s.EMPRESA, 
					s.SUCURSAL_EMPRESA
				FROM  SALDOS s
			    JOIN HISTORICO_DEVENGAMIENTO hd ON s.JTS_OID = hd.SALDO_JTS_OID AND hd.FECHA = s.C1813
			    JOIN ESQUEMACONTABLE e ON e.RUBRO = s.c1804 AND e.D_DC = 'C'
				WHERE s.TZ_LOCK = 0 
				AND s.c1785 = 5 
				AND s.c1604 &lt; 0  
				AND s.c1728 &lt;&gt; 'C'
				AND s.c1813 = ?
				AND ( s.ATIVO_PROBLEMATICO is NULL or s.ATIVO_PROBLEMATICO = ' ' )
				AND s.C1729 = 'S'
				AND DATEDIFF(DAY, s.c1628, ?) BETWEEN 60 AND 90
				AND e.CAMPO = (SELECT VALOR FROM PARAMETROS_JTS WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_IPF_MORATORIO_DEV_VIGENTE')
				AND hd.IPF_MORATORIO_DEV_SUSP_CONT &gt; 0			        
			</sentence>
			<attribute>
		        <attribute-name>fecha</attribute-name>
		        <column-name>FECHA</column-name>
		 		<column-type>Timestamp</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>jtsOidSaldo</attribute-name>
		        <column-name>JTS_OID_SALDO</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>tipoImporte</attribute-name>
		        <column-name>TIPO_IMPORTE</column-name>
		 		<column-type>String</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>sucursal</attribute-name>
		        <column-name>SUCURSAL</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>producto</attribute-name>
		        <column-name>PRODUCTO</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>cuenta</attribute-name>
		        <column-name>CUENTA</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>moneda</attribute-name>
		        <column-name>MONEDA</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>operacion</attribute-name>
		        <column-name>OPERACION</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>ordinal</attribute-name>
		        <column-name>ORDINAL</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>fechaVencimiento</attribute-name>
		        <column-name>FECHA_VENCIMIENTO</column-name>
		 		<column-type>Timestamp</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>ativoProblema</attribute-name>
		        <column-name>ATIVO_PROBLEMATICO</column-name>
		 		<column-type>String</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>estadoSuspenso</attribute-name>
		        <column-name>ESTADO_SUSPENSO</column-name>
		 		<column-type>String</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>rubroContableDevengado</attribute-name>
		        <column-name>RUBRO_CONTABLE_OPERACION</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>fechaDevengado</attribute-name>
		        <column-name>FECHA_DEVENGADO</column-name>
		 		<column-type>Timestamp</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>importeDevengadoConStop</attribute-name>
		        <column-name>IMPORTE_DEVENGADO_CON_STOP</column-name>
		 		<column-type>double</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>importeDevengadoSinStop</attribute-name>
		        <column-name>IMPORTE_DEVENGADO_SIN_STOP</column-name>
		 		<column-type>double</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>importeADebitar</attribute-name>
		        <column-name>IMPORTE_DEBITAR</column-name>
		 		<column-type>double</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>rubroADebitar</attribute-name>
		        <column-name>rubro_debitar</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>empresaSaldo</attribute-name>
		        <column-name>EMPRESA</column-name>
		 		<column-type>long</column-type>
		    </attribute>
		    <attribute>
		        <attribute-name>sucursalEmpresaSaldo</attribute-name>
		        <column-name>SUCURSAL_EMPRESA</column-name>
		 		<column-type>String</column-type>
		    </attribute>
	    </query>
		"""
		 this.addCommand(new AgregarModificarQuery(archivoMap, querie))
		 }
	 }
	 
	 private adicionarProcesoReclasificaciones(){
		def archivo = "processes.py"
		def procesos = """
	    pdef = ProcessDefinition("Reclasificacion Ingresos NIIF9", "topsystems.automaticprocess.processmanager.WorkManager")
	    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WorkDescriptorReclasificacionIngresosNIIF9");
	    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BusinessWorkerReclasificacionIngresosNIIF9");
	    pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
		pdef.addConstant("rangoCommit","100");
		pdef.addConstant("cantidadHilos","10");
		pdef.addConstant("isStopable","true");
		pdef.addConstant("applySchemes","true");
		pdef.addConstant("isSumarizable","true");
		pdef.addConstant("offLine","true");
		pdef.addConstant("enqueue","false");
		pdef.addParameter("FECHA",4,"dd/MM/yyyy",0);
		pdefs.addProcess(pdef) 
		"""
		this.addCommand(new AgregarModificarProceso(archivo, procesos))
		def archivoGroups = "groups.py"
		def detalleGroups = """
		    #PA Reclasificacion Ingresos NIIF9
		    gdef = GroupDefinition("PA - Reclasificacion Ingresos NIIF9")
		    gdef.registerProcess("Reclasificacion Ingresos NIIF9")
		    gdefs.addGroup(gdef)
		"""
		this.addCommand( new AgregarModificarGrupo(archivoGroups, detalleGroups))
	 }
	 
	 private adicionarServicioReclasificaciones(){
		 def archivo = "ReclasificacionesDeploy.xml"
		 def servicio = """
		    <service>
		        <name>topsystems:processManager:WorkDescriptor=WorkDescriptorReclasificacionIngresosNIIF9</name>
		        <code>topsystems.automaticprocess.ReclasificacionIngresosNIIF9.WorkDescriptorReclasificacionIngresosNIIF9</code>
		        <code-instruction/>
		        <movement-filler/>
		    </service>
		    <service>
		        <name>topsystems:processManager:BusinessWorker=BusinessWorkerReclasificacionIngresosNIIF9</name>
		        <code>topsystems.automaticprocess.ReclasificacionIngresosNIIF9.BusinessWorkerReclasificacionIngresosNIIF9</code>
		        <code-instruction/>
		        <movement-filler/>
		    </service>
		"""
		 this.addCommand(new AgregarModificarServicios(archivo, servicio))
	 }

}
