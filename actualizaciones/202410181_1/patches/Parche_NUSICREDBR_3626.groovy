package Base

import topsystems.actualizador.command.ActualizacionCommand
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.CarpetasActualizador
import topsystems.actualizador.parche.Parche

class Parche_NUSICREDBR_3626 extends Parche {

	void sobreElParche() {
		this.notas_del_autor = "Parche_NUSICREDBR_3626"
		this.opcional = false
	}
	
	void definicionDeParametros(){
		this.addParameterNewOperationNumber("OPERACION_RECALIFICACION_INGRESOS_NIIF9", " Nro. para la operacion del PA Recalificaciones")
		this.addParameterPATitleOpe("TITULO_OPERACION_RECALIFICACION_INGRESOS_NIIF9", "Nro. titulo de operacion para el PA Recalificaciones.")
	}

	@Override
	protected void comandos() {
		adicionarProceso()
		adicionarServicio()
		adicionarDatamapping()
		nuevaEstructuraBD()
		insertarBaseDato()
	}
	
	private adicionarProceso(){
		def archivo = "processes.py"
		def procesos = """
	    pdef = ProcessDefinition("Reclasificacion Ingresos Renegociados NIIF9", "topsystems.automaticprocess.processmanager.WorkManager")
	    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WorkDescriptorReclasificacionIngresosNIIF9Renegociados");
	    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BusinessWorkerReclasificacionIngresosNIIF9Renegociados");
	    pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
		pdef.addConstant("rangoCommit","100");
		pdef.addConstant("cantidadHilos","10");
		pdef.addConstant("isStopable","true");
		pdef.addConstant("applySchemes","true");
		pdef.addConstant("isSumarizable","true");
		pdef.addConstant("offLine","true");
		pdef.addConstant("enqueue","false");
		pdef.addParameter("FECHA",4,"dd/MM/yyyy",0);
		pdefs.addProcess(pdef) 
		"""
		this.addCommand(new AgregarModificarProceso(archivo, procesos))
		def archivoGroups = "groups.py"
		def detalleGroups = """
		    #PA Reclasificacion Ingresos Renegociados NIIF9
		    gdef = GroupDefinition("PA - Reclasificacion Ingresos Renegociados NIIF9")
		    gdef.registerProcess("Reclasificacion Ingresos Renegociados NIIF9")
		    gdefs.addGroup(gdef)
		"""
		this.addCommand( new AgregarModificarGrupo(archivoGroups, detalleGroups))
	}
	
	private adicionarServicio(){
		def archivo = "ReclasificacionIngresosNIIF9Deploy.xml"
		def servicio = """
		    <service>
		        <name>topsystems:processManager:WorkDescriptor=WorkDescriptorReclasificacionIngresosNIIF9Renegociados</name>
		        <code>topsystems.automaticprocess.ReclasificacionIngresosNIIF9.WorkDescriptorReclasificacionIngresosNIIF9Renegociados</code>
		        <code-instruction/>
		        <movement-filler/>
		    </service>
		    <service>
		        <name>topsystems:processManager:BusinessWorker=BusinessWorkerReclasificacionIngresosNIIF9Renegociados</name>
		        <code>topsystems.automaticprocess.ReclasificacionIngresosNIIF9.BusinessWorkerReclasificacionIngresosNIIF9Renegociados</code>
		        <code-instruction/>
		        <movement-filler/>
		    </service>
		"""
		this.addCommand(new AgregarModificarServicios(archivo, servicio))		
	}
	
	private adicionarDatamapping(){
		def archivoMapp = "RastreabilidadeIFRS9Mapping.xml"
		def clase = """
		<class>
	    <class-name>topsystems.automaticprocess.ReclasificacionIngresosNIIF9.RastreabilidadeIFRS9</class-name>
	    <entity-name>core.vo_RastreabilidadeIFRS9</entity-name>
	    <table-name>RASTREABILIDADE_IFRS9</table-name>
	    <database-name>TOP/CLIENTES</database-name>
	    <attribute>
	        <attribute-name>fecha</attribute-name>
	        <column-name>FECHA</column-name>
	        <key>primary</key>
	    </attribute>
	    <attribute>
	        <attribute-name>jtsOidSaldo</attribute-name>
	        <column-name>JTS_OID_SALDO</column-name>
	        <key>primary</key>
	    </attribute>
	    <attribute>
	        <attribute-name>tipoImporte</attribute-name>
	        <column-name>TIPO_IMPORTE</column-name>
	        <key>primary</key>
	    </attribute>
	    <attribute>
	        <attribute-name>sucursal</attribute-name>
	        <column-name>SUCURSAL</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>producto</attribute-name>
	        <column-name>PRODUCTO</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>cuenta</attribute-name>
	        <column-name>CUENTA</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>moneda</attribute-name>
	        <column-name>MONEDA</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>operacion</attribute-name>
	        <column-name>OPERACION</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>ordinal</attribute-name>
	        <column-name>ORDINAL</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>fechaVencimiento</attribute-name>
	        <column-name>FECHA_VENCIMIENTO</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>ativoProblema</attribute-name>
	        <column-name>ATIVO_PROBLEMATICO</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>estadoSuspenso</attribute-name>
	        <column-name>ESTADO_SUSPENSO</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>rubroContableDevengado</attribute-name>
	        <column-name>RUBRO_CONTABLE_DEVENGADO</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>fechaDevengado</attribute-name>
	        <column-name>FECHA_DEVENGADO</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>importeDevengadoConStop</attribute-name>
	        <column-name>IMPORTE_DEVENGADO_CON_STOP</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>importeDevengadoSinStop</attribute-name>
	        <column-name>IMPORTE_DEVENGADO_SIN_STOP</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>importeADebitar</attribute-name>
	        <column-name>IMPORTE_A_DEBITAR</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>rubroADebitar</attribute-name>
	        <column-name>RUBRO_A_DEBITAR</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>empresaSaldo</attribute-name>
	        <column-name>EMPRESA_SALDO</column-name>
	    </attribute>
	    <attribute>
	        <attribute-name>sucursalEmpresaSaldo</attribute-name>
	        <column-name>SUCURSAL_EMPRESA_SALDO</column-name>
	    </attribute>
	    </class>
		"""
		this.addCommand( new AgregarModificarEntidad(archivoMapp, clase))
		// Agregar o sustituir una query (o varias) de DataMapping
		def archivoMap = "RastreabilidadeIFRS9Mapping.xml"
		def baseOra = esBaseDeDatos("ORACLE")
		if(baseOra){
		def queries = """
	    <query>
	        <class-name>topsystems.automaticprocess.ReclasificacionIngresosNIIF9.RastreabilidadeIFRS9</class-name>
	        <query-name>query.ReclasificacionIngresosNIIF9</query-name>
	        <database-name>TOP/CLIENTES</database-name>
	        <sentence>
			SELECT s.jts_oid AS JTS_OID_SALDO,
		    COALESCE(eInt.RUBROSALIDA, 0) AS RUBRO_SALIDA_INT,
		    COALESCE(eMora.RUBROSALIDA, 0) AS RUBRO_SALIDA_MORA,
		    COALESCE(eIpf.RUBROSALIDA, 0) AS RUBRO_SALIDA_IPF,
		    COALESCE(eIpfMora.RUBROSALIDA, 0) AS RUBRO_SALIDA_IPF_MORA
			FROM
			    SALDOS s
			    LEFT JOIN ESQUEMACONTABLE eInt
			        ON eInt.RUBRO = s.c1804 AND eInt.D_DC = 'C' AND eInt.CAMPO = (SELECT 
						CASE 
							WHEN REGEXP_LIKE(VALOR, '^[0-9]') THEN VALOR 
							ELSE '0' 
						END AS VALOR_NUMERO
					FROM 
						PARAMETROS_JTS 
					WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_INTERES_DEVENGADO_VIGENTE')
			    LEFT JOIN ESQUEMACONTABLE eMora
			        ON eMora.RUBRO = s.c1804 AND eMora.D_DC = 'C' AND eMora.CAMPO = (SELECT 
						CASE 
							WHEN REGEXP_LIKE(VALOR, '^[0-9]') THEN VALOR 
							ELSE '0' 
						END AS VALOR_NUMERO
					FROM 
						PARAMETROS_JTS 
					WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_MORA_DEVENGADA_VIGENTE')
			    LEFT JOIN ESQUEMACONTABLE eIpf
			        ON eIpf.RUBRO = s.c1804 AND eIpf.D_DC = 'C' AND eIpf.CAMPO = (SELECT 
						CASE 
							WHEN REGEXP_LIKE(VALOR, '^[0-9]') THEN VALOR 
							ELSE '0' 
						END AS VALOR_NUMERO
					FROM 
						PARAMETROS_JTS 
					WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_IPF_DEVENGADO_VIGENTE')
			    LEFT JOIN ESQUEMACONTABLE eIpfMora
			        ON eIpfMora.RUBRO = s.c1804 AND eIpfMora.D_DC = 'C' AND eIpfMora.CAMPO = (SELECT 
						CASE 
							WHEN REGEXP_LIKE(VALOR, '^[0-9]') THEN VALOR 
							ELSE '0' 
						END AS VALOR_NUMERO
					FROM 
						PARAMETROS_JTS 
					WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_IPF_MORATORIO_DEV_VIGENTE')
			WHERE s.PRODUCTO IN (SELECT p.C6250 FROM PRODUCTOS p WHERE p.RENEGOCIADO = 'S' AND p.TZ_LOCK = 0)  
			    AND s.TZ_LOCK = 0 
			    AND s.c1785 = 5 
			    AND s.c1604 &lt; 0 
			    AND (s.ATIVO_PROBLEMATICO IS NULL OR s.ATIVO_PROBLEMATICO = ' ')
			    AND (eInt.RUBROSALIDA IS NOT NULL OR eMora.RUBROSALIDA IS NOT NULL OR eIpf.RUBROSALIDA IS NOT NULL OR eIpfMora.RUBROSALIDA IS NOT NULL)  				        
			</sentence>
	        <attribute>
	            <attribute-name>jtsOidSaldo</attribute-name>
	            <column-name>JTS_OID_SALDO</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>rubroSalidaInt</attribute-name>
	            <column-name>RUBRO_SALIDA_INT</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>rubroSalidaMora</attribute-name>
	            <column-name>RUBRO_SALIDA_MORA</column-name>
	            <column-type>long</column-type>
	        </attribute>
			<attribute>
	            <attribute-name>rubroSalidaIpf</attribute-name>
	            <column-name>RUBRO_SALIDA_IPF</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>rubroSalidaIpfMora</attribute-name>
	            <column-name>RUBRO_SALIDA_IPF_MORA</column-name>
	            <column-type>long</column-type>
	        </attribute>
	    </query>
		"""
		this.addCommand(new AgregarModificarQuery(archivoMap, queries))
		}else{
		def queries = """
	    <query>
	        <class-name>topsystems.automaticprocess.ReclasificacionIngresosNIIF9.RastreabilidadeIFRS9</class-name>
	        <query-name>query.ReclasificacionIngresosNIIF9</query-name>
	        <database-name>TOP/CLIENTES</database-name>
	        <sentence>
			SELECT s.jts_oid AS JTS_OID_SALDO,
		    COALESCE(eInt.RUBROSALIDA, 0) AS RUBRO_SALIDA_INT,
		    COALESCE(eMora.RUBROSALIDA, 0) AS RUBRO_SALIDA_MORA,
		    COALESCE(eIpf.RUBROSALIDA, 0) AS RUBRO_SALIDA_IPF,
		    COALESCE(eIpfMora.RUBROSALIDA, 0) AS RUBRO_SALIDA_IPF_MORA
			FROM
			    SALDOS s
			    LEFT JOIN ESQUEMACONTABLE eInt
			        ON eInt.RUBRO = s.c1804 AND eInt.D_DC = 'C' AND eInt.CAMPO = (SELECT 
					    CASE 
					        WHEN TRY_CAST(VALOR AS INT) IS NOT NULL THEN VALOR 
					        ELSE '0' 
					    END AS VALOR_NUMERO
					FROM 
					    PARAMETROS_JTS 
					WHERE 
					    funcionalidad = 'DEVENGAMIENTOCONT' 
					    AND PARAMETRO = 'PIVOT_INTERES_DEVENGADO_VIGENTE')
			    LEFT JOIN ESQUEMACONTABLE eMora
			        ON eMora.RUBRO = s.c1804 AND eMora.D_DC = 'C' AND eMora.CAMPO = (SELECT 
					    CASE 
					        WHEN TRY_CAST(VALOR AS INT) IS NOT NULL THEN VALOR 
					        ELSE '0' 
					    END AS VALOR_NUMERO
					FROM 
						PARAMETROS_JTS 
					WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_MORA_DEVENGADA_VIGENTE')
			    LEFT JOIN ESQUEMACONTABLE eIpf
			        ON eIpf.RUBRO = s.c1804 AND eIpf.D_DC = 'C' AND eIpf.CAMPO = (SELECT 
					    CASE 
					        WHEN TRY_CAST(VALOR AS INT) IS NOT NULL THEN VALOR 
					        ELSE '0' 
					    END AS VALOR_NUMERO
					FROM 
						PARAMETROS_JTS 
					WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_IPF_DEVENGADO_VIGENTE')
			    LEFT JOIN ESQUEMACONTABLE eIpfMora
			        ON eIpfMora.RUBRO = s.c1804 AND eIpfMora.D_DC = 'C' AND eIpfMora.CAMPO = (SELECT 
					    CASE 
					        WHEN TRY_CAST(VALOR AS INT) IS NOT NULL THEN VALOR 
					        ELSE '0' 
					    END AS VALOR_NUMERO
					FROM 
						PARAMETROS_JTS 
					WHERE FUNCIONALIDAD = 'DEVENGAMIENTOCONT' AND PARAMETRO = 'PIVOT_IPF_MORATORIO_DEV_VIGENTE')
			WHERE s.PRODUCTO IN (SELECT p.C6250 FROM PRODUCTOS p WHERE p.RENEGOCIADO = 'S' AND p.TZ_LOCK = 0)  
			    AND s.TZ_LOCK = 0 
			    AND s.c1785 = 5 
			    AND s.c1604 &lt; 0 
			    AND (s.ATIVO_PROBLEMATICO IS NULL OR s.ATIVO_PROBLEMATICO = ' ')
			    AND (eInt.RUBROSALIDA IS NOT NULL OR eMora.RUBROSALIDA IS NOT NULL OR eIpf.RUBROSALIDA IS NOT NULL OR eIpfMora.RUBROSALIDA IS NOT NULL)  				        
			</sentence>
	        <attribute>
	            <attribute-name>jtsOidSaldo</attribute-name>
	            <column-name>JTS_OID_SALDO</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>rubroSalidaInt</attribute-name>
	            <column-name>RUBRO_SALIDA_INT</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>rubroSalidaMora</attribute-name>
	            <column-name>RUBRO_SALIDA_MORA</column-name>
	            <column-type>long</column-type>
	        </attribute>
			<attribute>
	            <attribute-name>rubroSalidaIpf</attribute-name>
	            <column-name>RUBRO_SALIDA_IPF</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>rubroSalidaIpfMora</attribute-name>
	            <column-name>RUBRO_SALIDA_IPF_MORA</column-name>
	            <column-type>long</column-type>
	        </attribute>
	    </query>
		"""
		this.addCommand(new AgregarModificarQuery(archivoMap, queries))
		}
	}
	
	private insertarBaseDato(){
		def sentencia = """
		INSERT INTO OPERACIONES
		(TITULO, IDENTIFICACION, NOMBRE, DESCRIPCION, MNEMOTECNICO, AUTORIZACION, FORMULARIOPRINCIPAL, PROXOPERACION, ESTADO, TZ_LOCK, COPIAS, SUBOPERACION, PERMITEBAJA, COMPORTAMIENTOENCIERRE, REQUIERECONTRASENA, PERMITECONCURRENTE, PERMITEESTADODIFERIDO, ICONO_TITULO, ESTILO)
		VALUES(${this.P("TITULO_OPERACION_RECALIFICACION_INGRESOS_NIIF9")}, ${this.P("OPERACION_RECALIFICACION_INGRESOS_NIIF9")}, 'PA: RECLASIFICACIONINGRESONIIF9', 'Carga la tabla RASTREABILIDADE_IFRS9', ${this.P("OPERACION_RECALIFICACION_INGRESOS_NIIF9")}, 'N', 0, 0, 'P', 0, NULL, NULL, NULL, 'N', 'N', 'S', 'S', NULL, NULL);
		
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
		VALUES ('RECLASIFICACION_INGRESO_NIIF9', 'DESCRIPCION', 'Reclasificacion Ingresos NIIF9', 0);
			
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
		VALUES ('RECLASIFICACION_INGRESO_NIIF9', 'OPERACION', '${this.P("OPERACION_RECALIFICACION_INGRESOS_NIIF9")}', 0);
		
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
		VALUES ('RECLASIFICACION_INGRESO_NIIF9', 'CANAL', '0', 0);
		"""
		def descripcion = "INSERT [PA: Reclasificacion Ingresos NIIF9 y Reclasificacion Ingresos NIIF9 Renegociados]"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))		
	}
		
	protected void nuevaEstructuraBD(){
	  def baseOra = esBaseDeDatos("ORACLE")
	  if(baseOra){
	   nuevaEstructuraBDOracle()   
	  }else{
	   nuevaEstructuraBDSqlServer()   
	  }
	 }
	 
	protected void nuevaEstructuraBDOracle(){
	  def sentencia = """
	   -- HM begin sentence
	   DECLARE
	       v_count NUMBER;
	   BEGIN
	       SELECT COUNT(*)
	       INTO v_count
	       FROM USER_TABLES
	       WHERE TABLE_NAME = 'RASTREABILIDADE_IFRS9';
	   
	       IF v_count = 0 THEN
	           EXECUTE IMMEDIATE '
	      CREATE TABLE RASTREABILIDADE_IFRS9(
	       FECHA       DATE NOT NULL, 
	       JTS_OID_SALDO     NUMBER (10) NOT NULL,
	       TIPO_IMPORTE     VARCHAR2 (15) NOT NULL, --Opciones validas: INTERES, MORA, IPF, IPFMORATORIO
	       SUCURSAL       NUMBER (5) DEFAULT (0) NOT NULL,
	       PRODUCTO      NUMBER (5) DEFAULT (0) NOT NULL,
	       CUENTA        NUMBER (12) DEFAULT (0) NOT NULL,
	       MONEDA       NUMBER (4) DEFAULT (0) NOT NULL,
	       OPERACION      NUMBER (12) DEFAULT (0) NOT NULL,
	       ORDINAL       NUMBER (6) DEFAULT (0) NOT NULL,
	       FECHA_VENCIMIENTO    DATE,
	       ATIVO_PROBLEMATICO    VARCHAR2 (1),   
	       ESTADO_SUSPENSO     VARCHAR2 (1),
	       RUBRO_CONTABLE_DEVENGADO  NUMBER (15),
	       FECHA_DEVENGADO     DATE,
	       IMPORTE_DEVENGADO_CON_STOP  NUMBER (15,2),
	       IMPORTE_DEVENGADO_SIN_STOP  NUMBER (15,2),
	       IMPORTE_A_DEBITAR    NUMBER (15,2),
	       RUBRO_A_DEBITAR     NUMBER (15),
	       EMPRESA_SALDO     NUMBER (4),
	       SUCURSAL_EMPRESA_SALDO   VARCHAR2 (5),
	       CONSTRAINT PK_RASTREABILIDADE_IFRS9 PRIMARY KEY (FECHA, JTS_OID_SALDO, TIPO_IMPORTE)
	      )';
	    END IF;
	   END;
	   -- HM end sentence
	  """
	  this.addCommand(new ActualizarBaseDeDatosOracle(sentencia))
	 }
	 
	protected void nuevaEstructuraBDSqlServer(){
	  def sentencia = """
	  -- HM begin sentence
	   IF NOT EXISTS (
	       SELECT * FROM INFORMATION_SCHEMA.TABLES 
	       WHERE TABLE_NAME = 'RASTREABILIDADE_IFRS9'
	   )
	   BEGIN
	    CREATE TABLE RASTREABILIDADE_IFRS9(
	     FECHA       DATETIME NOT NULL, 
	     JTS_OID_SALDO     NUMERIC (10) NOT NULL,
	     TIPO_IMPORTE     VARCHAR (15) NOT NULL, --Opciones validas: INTERES, MORA, IPF, IPFMORATORIO
	     SUCURSAL       NUMERIC (5) DEFAULT (0) NOT NULL,
	     PRODUCTO      NUMERIC (5) DEFAULT (0) NOT NULL,
	     CUENTA        NUMERIC (12) DEFAULT (0) NOT NULL,
	     MONEDA       NUMERIC (4) DEFAULT (0) NOT NULL,
	     OPERACION      NUMERIC (12) DEFAULT (0) NOT NULL,
	     ORDINAL       NUMERIC (6) DEFAULT (0) NOT NULL,
	     FECHA_VENCIMIENTO    DATETIME,
	     ATIVO_PROBLEMATICO    VARCHAR (1),   
	     ESTADO_SUSPENSO     VARCHAR (1),
	     RUBRO_CONTABLE_DEVENGADO  NUMERIC (15),
	     FECHA_DEVENGADO     DATETIME,
	     IMPORTE_DEVENGADO_CON_STOP  NUMERIC (15,2),
	     IMPORTE_DEVENGADO_SIN_STOP  NUMERIC (15,2),
	     IMPORTE_A_DEBITAR    NUMERIC (15,2),
	     RUBRO_A_DEBITAR     NUMERIC (15),
	     EMPRESA_SALDO     NUMERIC (4),
	     SUCURSAL_EMPRESA_SALDO   VARCHAR (5),
	     CONSTRAINT PK_RASTREABILIDADE_IFRS9 PRIMARY KEY (FECHA, JTS_OID_SALDO, TIPO_IMPORTE)
	    )
	   END
	  -- HM begin sentence
	  """
	  this.addCommand(new ActualizarBaseDeDatosSQLServer(sentencia))
	 }
	 
}
