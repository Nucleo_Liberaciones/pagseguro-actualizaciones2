import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.configuration.Client
import topsystems.actualizador.configuration.app.ConfigurationAppManager
import topsystems.actualizador.parche.Parche;

class Patch_NUBANKIBR_543 extends Parche{



	@Override
	protected void comandos() {

		aplicarUpdate();
	}
	
	private void aplicarUpdate(){
		def sentencia = """
UPDATE saldos
SET FECHA_CALCULADO_SUSPENSO = (
  SELECT MAX(h.fecha)
  FROM historico_devengamiento h
  WHERE h.estado_suspenso = 'V'
    AND h.saldo_jts_oid = saldos.jts_oid
)
WHERE EXISTS (
  SELECT 1
  FROM historico_devengamiento h
  WHERE h.estado_suspenso = 'V'
    AND saldos.c1785 = 5
    AND saldos.C1729 = 'S'
    AND saldos.tz_lock = 0
    AND saldos.C1604 < 0
    AND saldos.jts_oid = h.saldo_jts_oid)
 """
	def descripcion = "Actualiza SALDOS.FECHA_CALCULADO_SUSPENSO con la ultima fecha del devengado en vigente"  
	this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	
	}
	

}