import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Patch_NUCRDSIS_67 extends Parche {

	void definicionDeParametros(){
		this.addParameterNewFreeField("NroCampoTipoCalculoIPF", "Nro. del Campo del PRODUCTO que Indica el Tipo de Cálculo de IPF")	
	}		
	@Override
	protected void comandos() {
	
		//----------------------------------------------------------------------------------------------------------------//
		// Actualización de la Base de Datos en Oracle BANKING
		def sentencia = """
			ALTER TABLE PRODUCTOS ADD TIPO_CALCULO_IPF VARCHAR2(1) DEFAULT('R');
		"""
		def descripcion = "Se agrega el Tipo de IPF a la tabla de Productos"
		ActualizarBaseDeDatosOracle varActualizarBankingOracle = new ActualizarBaseDeDatosOracle(sentencia, descripcion)
		varActualizarBankingOracle.soloEjecutarSi(this.esProducto("banking"))
		this.addCommand(varActualizarBankingOracle)
		//----------------------------------------------------------------------------------------------------------------//
		
		//----------------------------------------------------------------------------------------------------------------//		
		// Actualización de la Base de Datos en Oracle Microfinanzas
		sentencia = """
			ALTER TABLE CO_PRODUCTOS ADD TIPO_CALCULO_IPF VARCHAR2(1) DEFAULT('R');
		"""
		descripcion = "Se agrega el Tipo de IPF a la tabla de Productos"
		ActualizarBaseDeDatosOracle varActualizarMicrofinanzasOracle = new ActualizarBaseDeDatosOracle(sentencia, descripcion)
		varActualizarMicrofinanzasOracle.soloEjecutarSi(this.esProducto("microfinance"))
		this.addCommand(varActualizarMicrofinanzasOracle)
		//----------------------------------------------------------------------------------------------------------------//
		
		//----------------------------------------------------------------------------------------------------------------//
		// Actualización de la Base de Datos en SQLServer BANKING
		sentencia = """
			ALTER TABLE PRODUCTOS ADD TIPO_CALCULO_IPF VARCHAR(1) DEFAULT('R') WITH VALUES;
		"""
		descripcion = "Se agrega el Tipo de IPF a la tabla de Productos"
		ActualizarBaseDeDatosSQLServer varActualizarBankingSQLServer = new ActualizarBaseDeDatosSQLServer(sentencia, descripcion)
		varActualizarBankingSQLServer.soloEjecutarSi(this.esProducto("banking"))
		this.addCommand(varActualizarBankingSQLServer)
		//----------------------------------------------------------------------------------------------------------------//
		
		//----------------------------------------------------------------------------------------------------------------//		
		// Actualización de la Base de Datos en SQLServer Microfinanzas
		sentencia = """
			ALTER TABLE CO_PRODUCTOS ADD TIPO_CALCULO_IPF VARCHAR(1) DEFAULT('R') WITH VALUES;
		"""
		descripcion = "Se agrega el Tipo de IPF a la tabla de Productos"
		ActualizarBaseDeDatosSQLServer varActualizarMicrofinanzasSQLServer = new ActualizarBaseDeDatosSQLServer(sentencia, descripcion)
		varActualizarMicrofinanzasSQLServer.soloEjecutarSi(this.esProducto("microfinance"))
		this.addCommand(varActualizarMicrofinanzasSQLServer)
		//----------------------------------------------------------------------------------------------------------------//	
		 
		//----------------------------------------------------------------------------------------------------------------//
		// Actualización de Base de Datos Genéricas
		sentencia = """
			INSERT INTO CONCEPTOS (CODIGO, DESCRIPCION, CAMPOV25)
			VALUES (1354, 'Tipo Calculo IPF', 0);

			INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
			VALUES ("""+this.P("NroCampoTipoCalculoIPF")+""", NULL, 0, 'Tipo Calculo IPF', 'TipoCalcIPF', 1, 'A', 0, NULL, 0, 1354, 0, 0, 0, 0, 1, 65, 'TIPO_CALCULO_IPF', 0, NULL);

			INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA)
			VALUES ("""+this.P("NroCampoTipoCalculoIPF")+""", 'E', 'Sobre el Remanente Capital', 'R', 'R');

			INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA)
			VALUES ("""+this.P("NroCampoTipoCalculoIPF")+""", 'E', 'Sobre el Capital Cuota', 'C', 'C');

			INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA)
			VALUES ("""+this.P("NroCampoTipoCalculoIPF")+""", 'E', 'Sobre el Monto Cuota', 'M', 'M');

			INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA)
			VALUES ("""+this.P("NroCampoTipoCalculoIPF")+""", 'P', 'Sobre o Saldo Capital', 'R', 'R');

			INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA)
			VALUES ("""+this.P("NroCampoTipoCalculoIPF")+""", 'P', 'Sobre o Capital Quota', 'C', 'C');

			INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA)
			VALUES ("""+this.P("NroCampoTipoCalculoIPF")+""", 'P', 'Sobre o Total Cuota', 'M', 'M');
		"""
		descripcion = "Actualización de Conceptos, Diccionario y Opciones por Tipo Cálculo IPF"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
		//----------------------------------------------------------------------------------------------------------------//
		
	}
}
