import topsystems.TopazException
import topsystems.actualizador.command.archivos.CopiarDesdeZip
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.processmanager.BorrarGrupo
import topsystems.actualizador.command.processmanager.BorrarProceso
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche


class Parche_NUBANKIBR_482 extends Parche {
	
	void definicionDeParametros(){
		
		//Generados por el sistema
		this.addParameterPATitleOpe("TITULO_OPERACION", "Numero de Titulo para la operacion asociada al proceso");
		this.addParameterNewOperationNumber("NUMERO_OPERACION_PROCESO", "Numero de operación para el proceso: Contabilizacion por Esquema Contable");
		this.addParameterNewFreeField("CAMPO_PIVOT", "Campo pivot a utilizar");
		this.addParameterQuery("CAMPO_MOD_NEG", "Campo modelo negocios de Saldos", "STRING", "SELECT NUMERODECAMPO FROM DICCIONARIO WHERE upper(CAMPO) = 'MODELO_NEGOCIOS' AND TABLA = 21")
	}

	@Override
	protected void comandos() throws TopazException {
		
		agregarDataMappingQuery();
		agregarServicios();
		agregarQueryExtorno();
		adicionarProcesoExt();
		adicionarProcesoCont();
		adicionarGrupo();
		actualizarParametrosJts();
		actualizarOperacion();
		actualizarDiccionario();
		
		if(isSqlServer()){
			actualizarBDSql();
		} else {
			actualizarBDOracle();
		}
		
	}
	
	private agregarDataMappingQuery(){
		
		this.addCommand new CopiarDesdeZip("ContPorEsqContMapping.xml", "\${DATAMAPPINGS}/")
	}
	
	private agregarServicios(){
		
		this.addCommand new CopiarDesdeZip("ContPorEsqContDeploy.xml", "\${TOPAZ_SERVICES}/")
	}
	
	private void agregarQueryExtorno(){
		// Agregar o sustituir una query (o varias) de DataMapping
		def archivoMapp = "ExtornProcessMapping.xml"
		def queries = """
			<query>
				<class-name>topsystems.automaticprocess.Extorno.objectsdomain.AsientosExtornar</class-name>
				<query-name>query.vo_extornoContabilizacionEsquemasContables</query-name>
				<database-name>TOP/CLIENTES</database-name>
				<sentence>
					select ASIENTO, SUCURSAL, FECHA, OPERACION 
					from ASIENTOS_A_EXTORNAR
					where  procesado = 'N' and
					operacion = (select valor from parametros_jts where 
					funcionalidad = 'CONTABILIZACION_POR_ESQUEMAS_CONTABLES' and parametro = 'OPERACION') and 
					fecha &lt;= (select fechaproceso  from parametros)
				</sentence>
				<attribute>
					<attribute-name>numeroAsiento</attribute-name>
					<column-name>ASIENTO</column-name>
					<column-type>long</column-type>
				</attribute>
				<attribute>
					<attribute-name>sucursal</attribute-name>
					<column-name>SUCURSAL</column-name>
					<column-type>long</column-type>
				</attribute>
				<attribute>
					<attribute-name>fechaProceso</attribute-name>
					<column-name>FECHA</column-name>
					<column-type>Timestamp</column-type>
				</attribute>
				<attribute>
					<attribute-name>operacion</attribute-name>
					<column-name>OPERACION</column-name>
					<column-type>long</column-type>
				</attribute>
			</query>"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}
	
	private	adicionarProcesoExt(){
		def archivo = "processes.py"
		def procesos = """
						# Proceso de Extorno de Contabilizacion por esquemas contables
						pdef = ProcessDefinition("Extorno Contabilizacion por Esquemas Contables","topsystems.automaticprocess.processmanager.WorkManager")
						pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WorkDescriptorExtornProcess");
						pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BusinessWorkerExtornProcess");
						pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
						pdef.addConstant("rangoCommit","1");
						pdef.addConstant("cantidadHilos","10");
						pdef.addConstant("isStopable","true");
						pdef.addConstant("offLine","true");
						pdef.addConstant("enqueue","false");
						pdef.addConstant("marcaAjuste","0");
						pdef.addConstant("queryAsientosExtornar","query.vo_extornoContabilizacionEsquemasContables");
						pdef.addConstant("queryFechaValor","query.vo_extornofechaValorHoy");
						pdef.addConstant("condicion","");
						pdef.addConstant("UTILIZA_CAMBIO_DEL_DIA","true"); 
						pdef.addConstant("applyCodigoTransaccion","true"); 
						pdefs.addProcess(pdef)
					   """
		
		this.addCommand(new AgregarModificarProceso(archivo, procesos))
	}
	
	private	adicionarProcesoCont(){
		def archivo = "processes.py"
		def procesos = """
						# Proceso Contabilizacion por esquemas contables
						pdef = ProcessDefinition("Contabilizacion por Esquemas Contables", "topsystems.automaticprocess.processmanager.WorkManager")
						pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WDContabilizacionPorEsquemaContable");
						pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BWContabilizacionPorEsquemaContable");
						pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
						pdef.addConstant("queryAEjecutar","query.SaldoAContabilizarPorEsqCont");
						pdef.addConstant("PIVOT","${this.P('CAMPO_PIVOT')}");
						pdef.addConstant("MODELO_NEGOCIO","${this.P('CAMPO_MOD_NEG')}");
						pdef.addConstant("rangoCommit","500");
						pdef.addConstant("cantidadHilos","10");
						pdefs.addProcess(pdef)
					   """
		
		this.addCommand(new AgregarModificarProceso(archivo, procesos))
	}
	
	private adicionarGrupo(){
		def archivo = "groups.py"
		def grupo = """
					gdef = GroupDefinition("CON - Contabilizacion por Esquemas Contables")
					gdef.registerProcess("Extorno Contabilizacion por Esquemas Contables")
					gdef.registerProcess("Contabilizacion por Esquemas Contables")
					gdefs.addGroup(gdef)
					"""
		this.addCommand( new AgregarModificarGrupo(archivo, grupo))
	}
	
	private actualizarParametrosJts(){
		
		def sentencia = """
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CONTABILIZACION_POR_ESQUEMAS_CONTABLES', 'CANAL', '0', 0);
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CONTABILIZACION_POR_ESQUEMAS_CONTABLES', 'DESCRIPCION', 'Contabilizacion por Esquemas Contables', 0);
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CONTABILIZACION_POR_ESQUEMAS_CONTABLES', 'OPERACION', """+ this.P("NUMERO_OPERACION_PROCESO")+""", 0);
						"""
		def descripcion = "Insertando parametros proceso"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	private void actualizarOperacion(){
		def sentencia = """
						INSERT INTO OPERACIONES (TITULO, IDENTIFICACION, NOMBRE, DESCRIPCION, MNEMOTECNICO, AUTORIZACION, FORMULARIOPRINCIPAL, PROXOPERACION, ESTADO, TZ_LOCK, COPIAS, SUBOPERACION, PERMITEBAJA, COMPORTAMIENTOENCIERRE, REQUIERECONTRASENA, PERMITECONCURRENTE, PERMITEESTADODIFERIDO)
						VALUES (${this.P("TITULO_OPERACION")} , ${this.P("NUMERO_OPERACION_PROCESO")} , 'PA: CON - Contabilizacion por Esquemas Contables', 'CON - Contabilizacion por Esquemas Contables', """+ this.P("NUMERO_OPERACION_PROCESO")+""", 'N', NULL, NULL, 'P', 0, NULL, 0, 'S', 'N', 'N', 'S', 'S');
						"""
		def descripcion = "Actualizar OPERACIONES"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	private void actualizarDiccionario(){
		def sentencia = """
							INSERT INTO DICCIONARIO
							(NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
							VALUES("""+ this.P("CAMPO_PIVOT")+""", NULL, NULL, 'PIVOT_CONT_ESQ_CONT', 'PIVOT_CONT_ESQ_CONT', 15, 'N', 2, 'I', 0, 0, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL)
						"""
		def descripcion = "Actualizar DICCIONARIO"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	@Override
	protected boolean isSqlServer() {
		return super.isSqlServer();
	}
	
	private actualizarBDOracle(){
		def sentencia = """
						CREATE TABLE RUBRO_CONTABILIZACION_POR_ESQUEMAS
						(TZ_LOCK NUMBER(15,0) DEFAULT (0) NOT NULL,
						 RUBRO_CONTABLE VARCHAR2(15),
						 CONSTRAINT PK_RUBRO_CONTABILIZACION_POR_ESQUEMAS PRIMARY KEY (RUBRO_CONTABLE));
						"""
		def descripcion = "Se agrega tabla"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	private actualizarBDSql(){
		def sentencia = """
						CREATE TABLE RUBRO_CONTABILIZACION_POR_ESQUEMAS
						(TZ_LOCK NUMERIC(15,0) DEFAULT (0) NOT NULL,
						 RUBRO_CONTABLE VARCHAR(15),
						 CONSTRAINT PK_RUBRO_CONTABILIZACION_POR_ESQUEMAS PRIMARY KEY (RUBRO_CONTABLE));
						"""
		def descripcion = "Se agrega tabla"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
}