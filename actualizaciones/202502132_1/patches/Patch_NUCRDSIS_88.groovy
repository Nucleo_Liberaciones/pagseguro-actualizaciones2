package topsystems.actualizador.parche

import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.configuration.CarpetasServidor

class Patch_NUCRDSIS_88 extends Parche{
	
	private isAGB = false;
	
	void definicionDeParametros(){
		isAGB = this.isAGB();
		}

	@Override
	protected void comandos() {

		if (!isAGB){
		def archivo = "CobranzaAutomaticaDeploy.xml"
		def servicios = """
	<service>
	  <name>topsystems:processManager:WorkDescriptor=WorkDescriptorCobranzaAutomatica</name>
	  <code>topsystems.automaticprocess.cobranzaautomatica.WorkDescriptorCobranzaAutomaticaBR</code>
	</service>
		"""
		this.addCommand( new AgregarModificarServicios(archivo, servicios))
		}
		
		def sentencia = """
update PARAMETROS_PREJUIZO 
set PIVOTCOBRANZAINTERES = (select p.VALOR from  PARAMETROS_JTS p  where  p.FUNCIONALIDAD  = 'COBRANZA_AUTOMATICA'  AND  p.PARAMETRO = 'PIVOT_PREVISION_PAGO_INTERESES') ,
PIVOTCOBRANZAMORA =  (select p.VALOR from  PARAMETROS_JTS p  where  p.FUNCIONALIDAD  = 'COBRANZA_AUTOMATICA'  AND  p.PARAMETRO = 'PIVOT_PREVISION_PAGO_MORA') ,
PIVOTCOBRANZAMULTA = (select p.VALOR from  PARAMETROS_JTS p  where  p.FUNCIONALIDAD  = 'COBRANZA_AUTOMATICA'  AND  p.PARAMETRO = 'PIVOT_PREVISION_PAGO_MULTA') , 
PIVOTCOBRANZATOTAL = (select p.VALOR from  PARAMETROS_JTS p  where  p.FUNCIONALIDAD  = 'COBRANZA_AUTOMATICA'  AND  p.PARAMETRO = 'PIVOT_PREVISION_PAGO_TOTAL')  ;

		"""
		this.addCommand( new ActualizarBaseDeDatos(sentencia))
		
		sentencia = """
DELETE from PARAMETROS_JTS  where  FUNCIONALIDAD  = 'COBRANZA_AUTOMATICA'  AND PARAMETRO IN ('PIVOT_PREVISION_PAGO_INTERESES','PIVOT_PREVISION_PAGO_MULTA','PIVOT_PREVISION_PAGO_MORA','PIVOT_PREVISION_PAGO_TOTAL');

		"""
				this.addCommand( new ActualizarBaseDeDatos(sentencia))
	}
	
	private boolean isAGB(){
		def jarName = "agb_br_tb";
		def directoryLib = new File(CarpetasServidor.SERVER_LIB.getExpandedPath());

		if(directoryLib.exists() && directoryLib.isDirectory()){
			File[] filesJars = directoryLib.listFiles({ File dir, String file ->
				file.startsWith(jarName) && file.endsWith(".jar")
			} as FilenameFilter
			)
			if( filesJars.length > 0 ){
				return true;
			}
		}
		return false;
	}

}