import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.BorrarAtributoEntidad
import topsystems.actualizador.command.properties.BorrarPropiedad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche;

class Patch_NUBANKIBR_564_LB extends Parche{



	@Override
	protected void comandos() {

		aplicarEnBase();
		
		//------------- Borrar un atributo de una entidad de DataMapping
		def archivoMapp = "DevengamientoMapping.xml"
		def entidad = "core.vo_SaldoDevengar"
		def atributo = "FECHA_CALCULADO_SUSPENSO"  
		this.addCommand( new BorrarAtributoEntidad(archivoMapp, entidad, atributo))
		//--------------------------------------------------------------------------------------------------------------------------------//
		
		//--------------------------------------------------------------------------------------------------------------------------------//
		archivoMapp = "DevengamientoMapping.xml"
		entidad = "core.vo_SaldoDevengar"
		atributo = """
			<attribute>
            	<attribute-name>SAL_FECHA_CALCULADO_SUSPENSO</attribute-name>
            	<column-name>FECHA_CALCULADO_SUSPENSO</column-name>
        	</attribute>
		"""
		this.addCommand(new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
		//--------------------------------------------------------------------------------------------------------------------------------//
		
						
		//---- Borrar property reemplazada por concepto 2082 -------------------------------------------
		def archivo = "jts.properties"
		def propiedad = "topaz.campo.saldos.fecha.calculado.suspenso"
		this.addCommand(new BorrarPropiedad(archivo, propiedad))
		//--------------------------------------------------------------------------------------------------------------------------------//
	}
	
	private void aplicarEnBase(){
		
		def sentencia = """ 
INSERT INTO CONCEPTOS
(CODIGO, DESCRIPCION, CAMPOV25)
VALUES(2082, 'FECHA_CALCULADO_SUSPENSO', 0);

UPDATE DICCIONARIO SET CONCEPTO=2082 WHERE CAMPO ='FECHA_CALCULADO_SUSPENSO' AND TABLA=21;
 """
	def descripcion = "Concepto 2082 FECHA_CALCULADO_SUSPENSO " 
	this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))	
		
	}
		
}