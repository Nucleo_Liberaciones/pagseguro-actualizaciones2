import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.parche.Parche

class Patch_NUBANKIBR_447 extends Parche{

	void definicionDeParametros(){
	
	}

	@Override
	protected void comandos() {
		
 // Agregar o sustituir una query (o varias) de DataMapping
def archivoMapp = "CaidaDiferidosMapping.xml"
def queries = """
    <query>
        <class-name>topsystems.topaz.server.objectsdomain.Asientos</class-name>
        <query-name>query.CaidaDiferidos</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence> 
				SELECT ASIENTO, FECHAPROCESO, SUCURSAL, HORAFIN FROM ASIENTOS 
				WHERE ESTADO = 68 AND FECHAPROCESO = (SELECT FECHAPROCESO FROM PARAMETROS) ORDER BY HORAFIN 
        	</sentence>
        <attribute>
            <attribute-name>transactionID</attribute-name>
            <column-name>ASIENTO</column-name>
            <column-type>log</column-type>
        </attribute>
        <attribute>
            <attribute-name>processDate</attribute-name>
            <column-name>FECHAPROCESO</column-name>
            <column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>branch</attribute-name>
            <column-name>SUCURSAL</column-name>
            <column-type>log</column-type>
        </attribute>
        <attribute>
	    	<attribute-name>finishTime</attribute-name>
			<column-name>HORAFIN</column-name>
			<column-type>Timestamp</column-type>
        </attribute>
    </query>
    <query>
        <class-name>topsystems.topaz.server.objectsdomain.Asientos</class-name>
        <query-name>query.CaidaDiferidosCtaCapital</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence> 
				SELECT ASIENTO, FECHAPROCESO, SUCURSAL, HORAFIN FROM ASIENTOS 
				WHERE ESTADO = 68 AND FECHAPROCESO = (SELECT FECHAPROCESO FROM PARAMETROS) AND OPERACION = 2302 ORDER BY HORAFIN 
        	</sentence>
        <attribute>
            <attribute-name>transactionID</attribute-name>
            <column-name>ASIENTO</column-name>
            <column-type>log</column-type>
        </attribute>
        <attribute>
            <attribute-name>processDate</attribute-name>
            <column-name>FECHAPROCESO</column-name>
            <column-type>Date</column-type>
        </attribute>
        <attribute>
            <attribute-name>branch</attribute-name>
            <column-name>SUCURSAL</column-name>
            <column-type>log</column-type>
        </attribute>
		<attribute>
	    	<attribute-name>finishTime</attribute-name>
			<column-name>HORAFIN</column-name>
			<column-type>Timestamp</column-type>
        </attribute>
    </query>
"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))




	
	}

}