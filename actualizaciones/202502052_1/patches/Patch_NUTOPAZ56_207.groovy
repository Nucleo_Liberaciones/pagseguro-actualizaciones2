package topsystems.actualizador.parche

import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.servicios.BorrarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.CarpetasServidor
import topsystems.actualizador.configuration.Client
import topsystems.actualizador.configuration.app.ConfigurationAppManager
import topsystems.actualizador.parche.Parche;

class Patch_NUTOPAZ56_207 extends Parche{


	@Override
	protected void comandos() {
		

		
		// Agregar o sustituir uno o varios servicios en un mismo archivo
		def archivoServ = "DevengamientoDeploy.xml"
		def servicios = """
   <service>
      <name>topsystems:processManager:WorkDescriptor=WorkDescriptorCalculoDevengamiento</name>
      <code>topsystems.automaticprocess.devengamiento.calculo.WorkDescriptorCalculoDevengamiento</code>
      <code-instruction></code-instruction>
      <movement-filler></movement-filler>
   </service>
   <service>
      <name>topsystems:processManager:BusinessWorker=BusinessWorkerCalculoDevengamiento</name>
      <code>topsystems.automaticprocess.devengamiento.calculo.BusinessWorkerCalculoDevengamiento</code>
      <code-instruction></code-instruction>
      <movement-filler></movement-filler>
   </service>
   <service>
      <name>topsystems:processManager:WorkDescriptor=WorkDescriptorContabilizacionDevengamiento</name>
      <code>topsystems.automaticprocess.devengamiento.contabilizacion.WorkDescriptorContabilizacionDevengamiento</code>
      <code-instruction></code-instruction>
      <movement-filler></movement-filler>
   </service>
   <service>
      <name>topsystems:processManager:BusinessWorker=BusinessWorkerContabilizacionDevengamiento</name>
      <code>topsystems.automaticprocess.devengamiento.contabilizacion.BusinessWorkerContabilizacionDevengamiento</code>
      <code-instruction></code-instruction>
      <movement-filler></movement-filler>
   </service>
   <service>
      <name>topsystems:service:core=DevengamientoLoanUtil</name>
      <code>topsystems.topaz.server.businesslogic.devengamientoutil.DevengamientoLoanUtilBankBr</code>
      <code-instruction></code-instruction>
      <movement-filler></movement-filler>
   </service>
   <service>
      <name>topsystems:service:core=DevengamientoDPFUtil</name>
      <code>topsystems.topaz.server.businesslogic.devengamientoutil.DevengamientoDPFUtilBankBr</code>
      <code-instruction></code-instruction>
      <movement-filler></movement-filler>
   </service>
   <service>
      <name>topsystems:service:core=DevengamientoPlazoWDLogic</name>
      <code>topsystems.automaticprocess.devengamiento.DevengamientoPlazoWDLogicBankBr</code>
      <code-instruction></code-instruction>
      <movement-filler></movement-filler>
   </service>
"""
		this.addCommand( new AgregarModificarServicios(archivoServ, servicios))

	}
	
}