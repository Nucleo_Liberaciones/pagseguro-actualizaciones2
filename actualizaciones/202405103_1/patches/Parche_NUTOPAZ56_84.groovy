package TOPAZ_56

import topsystems.actualizador.command.archivos.CopiarDesdeZip
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.dataserver.ModificarConsultaQuery
import topsystems.actualizador.command.mbeans.AgregarDependsMBeans
import topsystems.actualizador.command.mbeans.AgregarModificarMBeans
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.properties.BorrarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.CarpetasServidor
import topsystems.actualizador.parche.Parche

class Parche_NUTOPAZ56_84 extends Parche {

	
	void sobreElParche() {
		this.notas_del_autor = "Parche_NUTOPAZ56_84"
		this.opcional = false
	}	
	
	@Override
	protected void comandos() {		
		addProperty()
	}
	
	
	private void addProperty(){
		// Agregar una propiedad o cambiar el valor a una existente
		def archivo = "controlservers.properties"
		def propiedad = "topaz.controlservers.enable.write.binnacle"
		def valor = "true"
		def comentario = "Habilita grabar la bitacora cuando se da de alta/baja un registro en la servercontrol"
		this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, valor, comentario))

	}
	

}
