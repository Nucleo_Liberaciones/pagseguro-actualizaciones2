import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche;

class Parche_NUAILOS_246 extends Parche {

	void definicionDeParametros(){
	}
	
	@Override
	protected void comandos() {
		//----------------------------------------------------------------------------------------------------------------//
		// Actualizaci�n de la Base de Datos con sentencias gen�ricas
		def sentencia = """			
			UPDATE PRODUCTOS SET C3361='N' WHERE C6252=6 AND C3361='S';
		"""
		def descripcion = "Por Defecto, en L�nea Base los Productos de Tipo Descuento (Tipo 6) NO permiten Pagos Parciales"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	
		//----------------------------------------------------------------------------------------------------------------//
	}
}