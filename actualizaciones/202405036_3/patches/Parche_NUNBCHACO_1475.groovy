package chaco

import topsystems.TopazException
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche

class Parche_NUNBCHACO_1475 extends Parche{

	@Override
	protected void comandos() throws TopazException {
		insertarParametrosJTS();
	}
	
	private void insertarParametrosJTS(){
		def descripcion = "Parametro que indica si en el concepto del movimiento de d�bito debe indicar el n�mero de operaci�n o la cuenta"
		def sentencia = """
			INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
			VALUES ('COBRANZA_AUTOMATICA', 'OPERACION_EN_CONCEPTO_DEBITO', 'N', 0);
		"""
		this.addCommand(new ActualizarBaseDeDatos(sentencia, descripcion))				
	}

}
