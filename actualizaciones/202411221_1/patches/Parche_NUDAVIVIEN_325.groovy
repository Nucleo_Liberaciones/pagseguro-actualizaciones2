import topsystems.TopazException
import topsystems.actualizador.command.archivos.BorrarArchivo
import topsystems.actualizador.command.archivos.CopiarDesdeZip
import topsystems.actualizador.parche.Parche
import topsystems.actualizador.configuration.CarpetasServidor

class Parche_NUDAVIVIEN_325 extends Parche{

	@Override
	protected void comandos() throws TopazException {
		
		String pathJbossModules = CarpetasServidor.APP_HOME.getExpandedPath().concat(File.separator).concat("modules")

		String pathSpringModule = pathJbossModules
		.concat(File.separator).concat("org")
		.concat(File.separator).concat("springframework")
		.concat(File.separator).concat("spring")
		.concat(File.separator).concat("main")

		File fileStructureSpringModule = new File(pathSpringModule)
		if(fileStructureSpringModule.exists()){
			
			// Borrar archivo
			this.addCommand new BorrarArchivo("\${APP_HOME}/modules/org/springframework/spring/main/spring-beans-5.0.9.RELEASE.jar")
			this.addCommand new BorrarArchivo("\${APP_HOME}/modules/org/springframework/spring/main/spring-core-5.0.9.RELEASE.jar")
			this.addCommand new BorrarArchivo("\${APP_HOME}/modules/org/springframework/spring/main/spring-web-5.0.9.RELEASE.jar")

			
			this.addCommand new CopiarDesdeZip("module.xml", "\${APP_HOME}/modules/org/springframework/spring/main/module.xml")
			this.addCommand new CopiarDesdeZip("spring-beans-5.3.39.jar", "\${APP_HOME}/modules/org/springframework/spring/main")
			this.addCommand new CopiarDesdeZip("spring-core-5.3.39.jar", "\${APP_HOME}/modules/org/springframework/spring/main")
			this.addCommand new CopiarDesdeZip("spring-web-5.3.39.jar", "\${APP_HOME}/modules/org/springframework/spring/main")
			
		}
		
		
	}

}
