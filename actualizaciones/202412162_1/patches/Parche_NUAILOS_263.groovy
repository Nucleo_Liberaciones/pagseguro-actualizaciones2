import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.parche.Parche;

class Parche_NUAILOS_263 extends Parche {

	@Override
	protected void comandos() {

		//----------------------------------------------------------------------------------------------------------------//
		// Agregar o sustituir uno (o varios) atributos de una entidad de DataMapping
		def archivoMapp = "DevengamientoMapping.xml"
		def entidad = "core.vo_HistoricoDevengamiento"
		def atributo = """
	        <attribute>
	            <attribute-name>intCompMoraDevengadoUltProc</attribute-name>
	            <column-name>ICMORA_DEVENGADO_ULT_PROC</column-name>
	        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
		//----------------------------------------------------------------------------------------------------------------//
	}
}