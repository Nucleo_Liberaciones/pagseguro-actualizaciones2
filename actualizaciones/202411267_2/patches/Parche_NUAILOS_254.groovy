package ailos

import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.parche.Parche

class Parche_NUAILOS_254 extends Parche {
	
	@Override
	protected void comandos() {
		modificarDeploy();
		modificarConstanteProceso();
	}
	
	protected void modificarDeploy(){
		def archivo = "ExposicionSobregiirosDeploy.xml"
		def servicios = """
		<service>
			<name>topsystems:processManager:BusinessWorker=BusinessWorkerExposicionSobregiros</name>
			<code>topsystems.automaticprocess.exposicionsobregiros.BusinessWorkerExposicionSobregirosBr</code>
		</service>
		"""
		this.addCommand( new AgregarModificarServicios(archivo, servicios))
	}
	
		
	private void modificarConstanteProceso() {
		def archivoPro = "processes.py"
		def nombreProceso = "ExposicionSobregiros"
		def constParamProc = """ pdef.addConstant("nroCampo1662Saldo","1662"); """		
		this.addCommand( new AgregarModificarPropiedadesProceso(archivoPro, nombreProceso, constParamProc))
	}

}
