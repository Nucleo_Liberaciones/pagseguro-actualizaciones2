package Parches

import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Patch_NUBNB_9 extends Parche {



	 void definicionDeParametros(){ 
		
			 this.addParameterNewFreeField("CAMPO_SUCURSAL_NUMERADOR", "Numero de campo sucursal numerador")
			 
	     }

		 void comandos() {
	

	def sentencia31 = """
insert into diccionario (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
values (${this.P("CAMPO_SUCURSAL_NUMERADOR")}, ' ', 0, 'Suc Numer', 'Sucursal Numerador', 5, 'N', 0, 'I', 0, 1539, 0, 0, 0, 0, 0, 0, 'SucNum', 0, '');

insert into conceptos (CODIGO, DESCRIPCION, CAMPOV25)
values (${this.P("CAMPO_SUCURSAL_NUMERADOR")}, 'Sucursal Numerador', 0);
	"""
	def descripcion31 = " " // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatos(sentencia31, descripcion31))
		



		
		 }
		 

	}
