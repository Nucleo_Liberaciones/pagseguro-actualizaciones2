import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche
import topsystems.actualizador.configuration.CarpetasServidor

class Parche_NUAGIBANK_929 extends Parche {

	boolean isAgibank=false
	
	@Override
	protected void comandos() throws TopazException {
		isAgibank = this.isAgibank();
		if (isAgibank){
			insertDB();
		}
		agregarAtributoVOPrepagoAgenda();
		agregarAtributoVOPrepago();
		agregarAtributoVOHistorico();
	}


	protected void insertDB(){
		if(isOracle()){
			def sentencia = """
			ALTER TABLE SCL_PREPAGOS_AGENDA ADD NUMERO_BENEFICIO VARCHAR2(20);
			ALTER TABLE SCL_PREPAGOS_AGENDA ADD JTS_OID NUMBER(15);

			ALTER TABLE SCL_PREPAGOS ADD JTS_OID NUMBER(15);
			
			ALTER TABLE SCL_HISTORICO_MOV_PREPAGOS ADD JTS_OID NUMBER(15);
		"""
	def descripcion = "Sobre la sentencia" // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
		}else if(isSqlServer()){
			def sentencia = """
			ALTER TABLE SCL_PREPAGOS_AGENDA ADD NUMERO_BENEFICIO VARCHAR(20);
			ALTER TABLE SCL_PREPAGOS_AGENDA ADD JTS_OID NUMERIC(15, 0);
			
			ALTER TABLE SCL_PREPAGOS ADD JTS_OID NUMERIC(15, 0);
			
			ALTER TABLE SCL_HISTORICO_MOV_PREPAGOS ADD JTS_OID NUMERIC(15, 0);
		"""
	def descripcion = "Sobre la sentencia" // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
			
			
		}
	}
	
	
	protected void agregarAtributoVOPrepagoAgenda(){
			// Agregar o sustituir uno (o varios) atributos de una entidad de DataMapping
	def archivoMapp = "PagosAutomaticosMapping.xml"
	def entidad = "core.vo_PrepagoAgenda"
	def atributo = """
        <attribute>
            <attribute-name>nroBeneficio</attribute-name>
            <column-name>NUMERO_BENEFICIO</column-name>
        </attribute>
        <attribute>
            <attribute-name>jtsOid</attribute-name>
            <column-name>JTS_OID</column-name>
        </attribute>
	"""
	this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))

	}
	
	protected void agregarAtributoVOPrepago(){
		// Agregar o sustituir uno (o varios) atributos de una entidad de DataMapping
	def archivoMapp = "PagosAutomaticosMapping.xml"
	def entidad = "core.vo_Prepago"
	def atributo = """
        <attribute>
            <attribute-name>jtsOid</attribute-name>
            <column-name>JTS_OID</column-name>
        </attribute>
	"""
	this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))

	}
	
	protected void agregarAtributoVOHistorico(){
		// Agregar o sustituir uno (o varios) atributos de una entidad de DataMapping
	def archivoMapp = "PagosAutomaticosMapping.xml"
	def entidad = "core.vo_HistoricoMovimientosPrepago"
	def atributo = """
        <attribute>
            <attribute-name>jtsOid</attribute-name>
            <column-name>JTS_OID</column-name>
        </attribute>
	"""
	this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))

	}
	
	private boolean isAgibank(){
		def jarName = "agb_br_tb";
		def directoryLib = new File(CarpetasServidor.SERVER_LIB.getExpandedPath());
	
		if(directoryLib.exists() && directoryLib.isDirectory()){
			File[] filesJars = directoryLib.listFiles({ File dir, String file ->
				file.startsWith(jarName) && file.endsWith(".jar")
			} as FilenameFilter
			)
			if( filesJars.length > 0 ){
				return true;
			}
		}
		return false;
	}
	
}
