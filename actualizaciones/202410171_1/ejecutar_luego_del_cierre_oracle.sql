--Después del cierre del primer día hábil del mes siguiente (día de cobro), cuando se ejecuta el PA de Cobro IOF Sobregiros.
UPDATE SALDOS S
SET S.C1621 = S.C1621 + INTERVAL '1' DAY
WHERE S.JTS_OID IN (SELECT JTS_OID FROM IOF_SALDOS);


DROP TABLE IOF_SALDOS;