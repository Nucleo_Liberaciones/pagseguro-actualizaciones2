--Después del cierre del primer día hábil del mes siguiente (día de cobro), cuando se ejecuta el PA de Cobro IOF Sobregiros.
UPDATE S
SET S.C1621 = DATEADD(DAY, 1, S.C1621)
FROM SALDOS S
WHERE S.JTS_OID IN (SELECT JTS_OID FROM IOF_SALDOS);

DROP TABLE IOF_SALDOS;