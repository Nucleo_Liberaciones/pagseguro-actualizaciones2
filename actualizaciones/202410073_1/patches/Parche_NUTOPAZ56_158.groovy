import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Parche_NUTOPAZ56_158 extends Parche {

	@Override
	protected void comandos() throws TopazException {
		addAttributeToEntity();
		addColumnSQLSentence();
		addColumnOracleSentence();
		setPriorityDefaultZero();
	}
	
	private void addAttributeToEntity() {
		// Agregar o sustituir uno (o varios) atributos de una entidad de DataMapping
def archivoMapp = "ChargesMapping.xml"
def entidad = "core.vo_TaxesPerCharge"
def atributo = """
        <attribute>
            <attribute-name>priority</attribute-name>
            <column-name>PRIORIDAD</column-name>
        </attribute>
"""
this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))

	}
	
	private void addColumnSQLSentence() {
		def sentencia = """
		alter table CI_IMPUESTOS_X_CARGO ADD PRIORIDAD NUMERIC(2,0) DEFAULT 0;
"""
	def descripcion = "Agregar columna Prioridad a la tabla CI_IMPUESTOS_X_CARGO" // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
	
	}
	
	private void addColumnOracleSentence() {
		def sentencia = """
		ALTER TABLE CI_IMPUESTOS_X_CARGO ADD PRIORIDAD NUMBER(2) DEFAULT 0;
"""
	def descripcion = "Agregar columna Prioridad a la tabla CI_IMPUESTOS_X_CARGO" // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	
	}
	
	private void setPriorityDefaultZero() {
		def sentencia = """
		UPDATE CI_IMPUESTOS_X_CARGO SET PRIORIDAD = 0;
 """
	def descripcion = "Setear el campo PRIORIDAD en 0" // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	
	}

}
