import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche;

class Parche_NUBANKIBR_508 extends Parche{

	void definicionDeParametros(){
		
	}

	@Override
	protected void comandos() {

	//----------------------------------------------------------------------------------------------------------------//
	// Agregar o sustituir uno (o varios) atributos de una entidad de DataMapping
	def archivoMapp = "DevengamientoMapping.xml"
	def entidad = "core.vo_HistoricoDevengamiento"
	def atributo = """
        <attribute>
            <attribute-name>fechaCalculadoSuspenso</attribute-name>
            <column-name>FECHA_CALCULADO_SUSPENSO</column-name>
        </attribute>		
		<attribute>
            <attribute-name>motivoSuspenso</attribute-name>
            <column-name>MOTIVO_SUSPENSO</column-name>
        </attribute>	
		<attribute>
            <attribute-name>moraPercibida</attribute-name>
            <column-name>MORA_PERCIBIDA</column-name>
        </attribute>		
		<attribute>
            <attribute-name>ipfMoratorioPercibido</attribute-name>
            <column-name>IPFMORATORIO_PERCIBIDO</column-name>
        </attribute>		
	"""
	this.addCommand(new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
	
	//----------------------------------------------------------------------------------------------------------------//

	// Actualización de la Base de Datos en Oracle
	if (esBaseDeDatos("ORACLE")) {
		def sentencia = """
			alter table HISTORICO_DEVENGAMIENTO add FECHA_CALCULADO_SUSPENSO	 DATE null;
			alter table HISTORICO_DEVENGAMIENTO add MOTIVO_SUSPENSO 			 VARCHAR2(9) DEFAULT(' ') ;
			alter table HISTORICO_DEVENGAMIENTO add MORA_PERCIBIDA  			 number(15, 2) default (0);
			alter table HISTORICO_DEVENGAMIENTO add IPFMORATORIO_PERCIBIDO  	 number(15, 2) default (0);
		"""
		this.addCommand( new ActualizarBaseDeDatosOracle(sentencia))
	} else {
		// Actualización de la Base de Datos en SQLServer
		def sentencia = """
			alter table HISTORICO_DEVENGAMIENTO add FECHA_CALCULADO_SUSPENSO	  DATETIME;
			alter table HISTORICO_DEVENGAMIENTO add MOTIVO_SUSPENSO			      VARCHAR(9) default (' ');
			alter table HISTORICO_DEVENGAMIENTO add MORA_PERCIBIDA				  numeric(15, 2) default (0) with values;
			alter table HISTORICO_DEVENGAMIENTO add IPFMORATORIO_PERCIBIDO		  numeric(15, 2) default (0) with values;
		"""
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia))
	}
	//----------------------------------------------------------------------------------------------------------------//

	}

}