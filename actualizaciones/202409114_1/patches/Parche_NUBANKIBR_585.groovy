import topsystems.TopazException
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche

class Parche_NUBANKIBR_585 extends Parche{

	@Override
	protected void comandos() throws TopazException {
		agregarConstante();
		scriptBaseDatos();
	}
	
	
	protected agregarConstante(){
		// Agregar constantes y parametros a un proceso
		def archivo = "processes.py"
		def nombreProceso = "Saldos D y M Reaplicacion de movimientos"
		def constParamProc = """
		pdef.addConstant("GENERA_CON_SALDOS_DIARIOS","false");
		"""
	this.addCommand( new AgregarModificarPropiedadesProceso(archivo, nombreProceso, constParamProc))
		// Agregar constantes y parametros a un proceso
		 archivo = "processes.py"
		 nombreProceso = "Saldos D y M Reaplicacion de movimientos_Procesa todos"
		 constParamProc = """ 
		pdef.addConstant("GENERA_CON_SALDOS_DIARIOS","false");
		"""
this.addCommand( new AgregarModificarPropiedadesProceso(archivo, nombreProceso, constParamProc))

	}
	
	
	protected scriptBaseDatos(){
		def sentencia = """
		DELETE FROM CON_SALDOS_DIARIOS_ACTUALIZAR;
		"""
	def descripcion = "Sobre la sentencia" // La descripción es opcional
	this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	
	}

}
