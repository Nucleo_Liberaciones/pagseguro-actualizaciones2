package Paquetes

import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Parche_NUBANKIBR_505 extends Parche {

	void sobreElParche() {
		this.notas_del_autor = "NUBANKIBR_505"
		this.opcional = false
	}
	

	@Override
	protected void comandos() {
		addColumnInTableEvent();
	}
	
	private void addColumnInTableEvent(){
		if(this.isOracle()){
			addColumnInTableEventOracle()
		}else if(this.isSqlServer()){
			addColumnInTableEventSqlServer()
		}
	}

	private void addColumnInTableEventOracle(){
	def sentencia = """
		-- HM begin sentence 
			DECLARE
			    v_table_exists NUMBER := 0;
			    v_column_exists NUMBER := 0;
			BEGIN
			    SELECT COUNT(*) INTO v_table_exists
			    FROM all_tables
			    WHERE table_name = 'CRE_RENEGOCIACIONES';
			
			    IF v_table_exists = 0 THEN
			        EXECUTE IMMEDIATE '
			            CREATE TABLE CRE_RENEGOCIACIONES
			            (
			                JTS_OID_NEGOCIADO NUMBER(10) DEFAULT 0 NOT NULL,
			                JTS_OID_NUEVO NUMBER(10) DEFAULT 0 NOT NULL,
			                ID_NEGOCIACION NUMBER(10) DEFAULT 0,
			                ID_CONDONACIONES NUMBER(10) DEFAULT 0,
			                FECHA_PROCESO DATE,
			                SUCURSAL NUMBER(10) DEFAULT 0,
			                ASIENTO NUMBER(10) DEFAULT 0,
			                TZ_LOCK NUMBER(15) DEFAULT 0 NOT NULL,
							VALOR_CANCELACION NUMBER(15, 2),
							TEJ_ORIGINAL NUMBER(11, 7),
							PONDERACION NUMBER(11, 7),
							RESULTADO_REESTRUCTURA NUMBER(15, 2),
							REESTRUCTURA VARCHAR2(1),
			                CONSTRAINT PK_CRE_RENEGOCIACIONES_01 PRIMARY KEY (JTS_OID_NEGOCIADO, JTS_OID_NUEVO)
			            )';
			    ELSE
			        SELECT COUNT(*) INTO v_column_exists
			        FROM all_tab_columns
			        WHERE column_name = 'VALOR_CANCELACION'
			        AND table_name = 'CRE_RENEGOCIACIONES';
			
			        IF v_column_exists = 0 THEN
			            EXECUTE IMMEDIATE 'ALTER TABLE CRE_RENEGOCIACIONES ADD VALOR_CANCELACION NUMBER(15, 2)';
			        END IF;
			
			        SELECT COUNT(*) INTO v_column_exists
			        FROM all_tab_columns
			        WHERE column_name = 'TEJ_ORIGINAL'
			        AND table_name = 'CRE_RENEGOCIACIONES';
			
			        IF v_column_exists = 0 THEN
			            EXECUTE IMMEDIATE 'ALTER TABLE CRE_RENEGOCIACIONES ADD TEJ_ORIGINAL NUMBER(11, 7)';
			        END IF;
			
			        SELECT COUNT(*) INTO v_column_exists
			        FROM all_tab_columns
			        WHERE column_name = 'PONDERACION'
			        AND table_name = 'CRE_RENEGOCIACIONES';
			
			        IF v_column_exists = 0 THEN
			            EXECUTE IMMEDIATE 'ALTER TABLE CRE_RENEGOCIACIONES ADD PONDERACION NUMBER(11, 7)';
			        END IF;
			
			        SELECT COUNT(*) INTO v_column_exists
			        FROM all_tab_columns
			        WHERE column_name = 'RESULTADO_REESTRUCTURA'
			        AND table_name = 'CRE_RENEGOCIACIONES';
			
			        IF v_column_exists = 0 THEN
			            EXECUTE IMMEDIATE 'ALTER TABLE CRE_RENEGOCIACIONES ADD RESULTADO_REESTRUCTURA NUMBER(15, 2)';
			        END IF;
			
			        SELECT COUNT(*) INTO v_column_exists
			        FROM all_tab_columns
			        WHERE column_name = 'REESTRUCTURA'
			        AND table_name = 'CRE_RENEGOCIACIONES';
			
			        IF v_column_exists = 0 THEN
			            EXECUTE IMMEDIATE 'ALTER TABLE CRE_RENEGOCIACIONES ADD REESTRUCTURA VARCHAR2(1)';
			        END IF;
			    END IF;
			END;
			-- HM end sentence
		"""
		def descripcion = "Add columns in table CRE_RENEGOCIACIONES"
		this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	
	}
	
	private void addColumnInTableEventSqlServer(){
		def sentencia = """
		-- HM begin sentence 
			IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'CRE_RENEGOCIACIONES')
			BEGIN
			    CREATE TABLE CRE_RENEGOCIACIONES
			    (
			        JTS_OID_NEGOCIADO INT DEFAULT 0 NOT NULL,
			        JTS_OID_NUEVO INT DEFAULT 0 NOT NULL,
			        ID_NEGOCIACION INT DEFAULT 0,
			        ID_CONDONACIONES INT DEFAULT 0,
			        FECHA_PROCESO DATE,
			        SUCURSAL INT DEFAULT 0,
			        ASIENTO INT DEFAULT 0,
			        TZ_LOCK BIGINT DEFAULT 0 NOT NULL,
			        VALOR_CANCELACION DECIMAL(15, 2),
			        TEJ_ORIGINAL DECIMAL(11, 7),
			        PONDERACION DECIMAL(11, 7),
			        RESULTADO_REESTRUCTURA DECIMAL(15, 2),
			        REESTRUCTURA CHAR(1),
			        CONSTRAINT PK_CRE_RENEGOCIACIONES_01 PRIMARY KEY (JTS_OID_NEGOCIADO, JTS_OID_NUEVO)
			    )
			END
			ELSE
			BEGIN
			    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'VALOR_CANCELACION' AND TABLE_NAME = 'CRE_RENEGOCIACIONES')
			    BEGIN
			        ALTER TABLE CRE_RENEGOCIACIONES ADD VALOR_CANCELACION DECIMAL(15, 2);
			    END;
			
			    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'TEJ_ORIGINAL' AND TABLE_NAME = 'CRE_RENEGOCIACIONES')
			    BEGIN
			        ALTER TABLE CRE_RENEGOCIACIONES ADD TEJ_ORIGINAL DECIMAL(11, 7);
			    END;
			
			    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'PONDERACION' AND TABLE_NAME = 'CRE_RENEGOCIACIONES')
			    BEGIN
			        ALTER TABLE CRE_RENEGOCIACIONES ADD PONDERACION DECIMAL(11, 7);
			    END;
			
			    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'RESULTADO_REESTRUCTURA' AND TABLE_NAME = 'CRE_RENEGOCIACIONES')
			    BEGIN
			        ALTER TABLE CRE_RENEGOCIACIONES ADD RESULTADO_REESTRUCTURA DECIMAL(15, 2);
			    END;
			
			    IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'REESTRUCTURA' AND TABLE_NAME = 'CRE_RENEGOCIACIONES')
			    BEGIN
			        ALTER TABLE CRE_RENEGOCIACIONES ADD REESTRUCTURA CHAR(1);
			    END;
			END;
			-- HM end sentence
		"""
		def descripcion = "Add columns in table CRE_RENEGOCIACIONES"
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
	
	}
	
}

