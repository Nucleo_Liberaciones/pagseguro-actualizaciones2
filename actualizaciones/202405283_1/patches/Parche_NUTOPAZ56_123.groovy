package nucleo

import topsystems.TopazException
import topsystems.actualizador.command.dataserver.BorrarAtributoEntidad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Parche_NUTOPAZ56_123 extends Parche{

	@Override
	protected void comandos() throws TopazException {
		addBackUpTableBD()
		ejecutarUpdateBD()
		removeAttributeFromMapping()
	}
	
	protected void addBackUpTableBD(){
		def baseOra = esBaseDeDatos("ORACLE")
		if(baseOra){
			def sentencia = """
			CREATE TABLE MODIFICACION_C1692_RSP
			  AS (SELECT 
			  			s.jts_oid,
			  			s.C1692 AS Rubro_Original, 
			  			s.C1730 AS RUBRO_CONTABLE, 
			  			s.C1728, 
			  			sd.rubrocontable AS Rubro_Saldo_Diario
					FROM saldos s
					INNER JOIN grl_saldos_diarios sd ON s.JTS_OID = sd.SALDOS_JTS_OID AND s.C1620 = sd.FECHA
					WHERE (s.c1692 = 0 OR s.c1692 IS NULL) AND
					s.TZ_LOCK = 0 AND
					EXISTS (SELECT sd.rubrocontable
						FROM grl_saldos_diarios sd
						WHERE s.JTS_OID = sd.SALDOS_JTS_OID AND s.C1620 = sd.FECHA) )
			"""
			this.addCommand( new ActualizarBaseDeDatosOracle(sentencia))
		}else{
			def sentencia = """
				SELECT 
					s.JTS_OID,
				  	s.C1692 AS RUBRO_ORIGINAL, 
				  	s.C1730 AS RUBRO_CONTABLE, 
				  	s.C1728, 
				  	sd.RUBROCONTABLE AS RUBRO_SALDO_DIARIO
				INTO MODIFICACION_C1692_RSP
				FROM saldos s
				INNER JOIN grl_saldos_diarios sd ON s.JTS_OID = sd.SALDOS_JTS_OID AND s.C1620 = sd.FECHA
				WHERE (s.c1692 = 0 OR s.c1692 IS NULL) AND
				s.TZ_LOCK = 0 AND
				EXISTS (SELECT sd.rubrocontable
					FROM grl_saldos_diarios sd
					WHERE s.JTS_OID = sd.SALDOS_JTS_OID AND s.C1620 = sd.FECHA);
			"""
			this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia))
		}
	}
	
	private void removeAttributeFromMapping(){
		def archivoMapp = "categorizacionPorCuotaMapping.xml"
		def entidad = "core.vo_SaldoCategorizar"
		def atributo = "SAL_RUBRO_ORIGINAL"	
		this.addCommand(new BorrarAtributoEntidad(archivoMapp, entidad, atributo))
	}

	protected void ejecutarUpdateBD(){
		def baseOra = esBaseDeDatos("ORACLE")
		if(baseOra){
			def sentencia = """
			UPDATE saldos s
			SET s.c1692 = (SELECT sd.rubrocontable
			                   	FROM grl_saldos_diarios sd
			                    WHERE s.JTS_OID = sd.SALDOS_JTS_OID AND s.C1620 = sd.FECHA)
			WHERE (s.c1692 = 0 OR s.c1692 IS NULL)
			AND s.TZ_LOCK = 0
			AND EXISTS (SELECT sd.rubrocontable
			                   	FROM grl_saldos_diarios sd
			                    WHERE s.JTS_OID = sd.SALDOS_JTS_OID AND s.C1620 = sd.FECHA)
			"""
			this.addCommand( new ActualizarBaseDeDatosOracle(sentencia))
		}else{
			def sentencia = """
				UPDATE s
				SET c1692 = sd.rubrocontable
				FROM saldos s
				INNER JOIN grl_saldos_diarios sd
				ON s.JTS_OID = sd.SALDOS_JTS_OID AND s.C1620 = sd.FECHA
				WHERE (s.c1692 = 0 OR s.c1692 IS NULL)
				AND s.TZ_LOCK = 0
				AND EXISTS (SELECT sd.rubrocontable
									   FROM grl_saldos_diarios sd
									WHERE s.JTS_OID = sd.SALDOS_JTS_OID AND s.C1620 = sd.FECHA)
			"""
			this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia))
		}
	}

}
