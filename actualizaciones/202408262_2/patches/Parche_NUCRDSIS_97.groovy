package Base

import topsystems.actualizador.command.ActualizacionCommand
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.CarpetasActualizador
import topsystems.actualizador.configuration.CarpetasServidor
import topsystems.actualizador.parche.Parche

class Parche_NUCRDSIS_97 extends Parche {

	void sobreElParche() {
		this.notas_del_autor = "Parche_NUCRDSIS_97"
		this.opcional = false
	}

	@Override
	protected void comandos() {
		addNewParametroSqlServer()
		addNewParametroOracle()
	}
	
	private void addNewParametroSqlServer(){
		def sentencia = """
		-- HM begin sentence
			If not exists (select VALOR from PARAMETROS_JTS where upper(FUNCIONALIDAD)='CATEGORIZACION' and upper(PARAMETRO) = 'PIVOT_IPF_DEVENGADO_VIGENTE' )

			INSERT INTO PARAMETROS_JTS
			(FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
			VALUES(N'CATEGORIZACION', N'PIVOT_IPF_DEVENGADO_VIGENTE', N'0000', 0);
			
			If not exists (select VALOR from PARAMETROS_JTS where upper(FUNCIONALIDAD)='CATEGORIZACION' and upper(PARAMETRO) = 'PIVOT_DEVENGADO_IPFMORATORIO_VIGENTE' )
			
			INSERT INTO PARAMETROS_JTS
			(FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
			VALUES(N'CATEGORIZACION', N'PIVOT_DEVENGADO_IPFMORATORIO_VIGENTE', N'0000', 0);
		-- HM end sentence
		"""
		def descripcion = "Add PARAMETROS_JTS en SqlServer"
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
	}
	
	private void addNewParametroOracle(){
	    def sentencia = """
	    -- HM begin sentence
	        DECLARE
	            v_count NUMBER;
	        BEGIN
	            SELECT COUNT(*) INTO v_count 
	            FROM PARAMETROS_JTS 
	            WHERE UPPER(FUNCIONALIDAD) = 'CATEGORIZACION' AND UPPER(PARAMETRO) = 'PIVOT_IPF_DEVENGADO_VIGENTE';
	            
	            IF v_count = 0 THEN
	                INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
	                VALUES ('CATEGORIZACION', 'PIVOT_IPF_DEVENGADO_VIGENTE', '0000', 0);
	            END IF;
	
	            SELECT COUNT(*) INTO v_count 
	            FROM PARAMETROS_JTS 
	            WHERE UPPER(FUNCIONALIDAD) = 'CATEGORIZACION' AND UPPER(PARAMETRO) = 'PIVOT_DEVENGADO_IPFMORATORIO_VIGENTE';
	            
	            IF v_count = 0 THEN
	                INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
	                VALUES ('CATEGORIZACION', 'PIVOT_DEVENGADO_IPFMORATORIO_VIGENTE', '0000', 0);
	            END IF;
	        END;
	    -- HM end sentence
	    """;
	    def descripcion = "Add PARAMETROS_JTS en Oracle";
	    this.addCommand(new ActualizarBaseDeDatosOracle(sentencia, descripcion));
	}
	
}
