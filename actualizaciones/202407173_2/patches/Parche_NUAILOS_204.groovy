package AILOS

import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.parche.Parche

class Parche_NUAILOS_204 extends Parche {

	private final String CONCEPT_NUMBER = 3214;
	private final String FIELD_PARAM_NAME = "CAMPO_CONCEPTO_" + CONCEPT_NUMBER;

	@Override
	void definicionDeParametros(){
		this.addParameterNewFreeField(FIELD_PARAM_NAME, "Campo topaz para el nuevo concepto " + CONCEPT_NUMBER)
	}

	@Override
	void sobreElParche() {
		this.notas_del_autor = "Parche_NUAILOS_204"
		this.opcional = false
	}

	@Override
	protected void comandos() {
		addConceptCommand()
		addDiccionarioCommand()
		addColumnEventToProcessCommand()
		changeEventToProcessDatamappingCommand()
	}

	private void addConceptCommand(){
		def sentencia = """
		INSERT INTO CONCEPTOS (CODIGO, DESCRIPCION, CAMPOV25) VALUES(${CONCEPT_NUMBER}, 'kafka partition key', 0);
		"""
		def descripcion = "Agregando nuevo concepto "+ CONCEPT_NUMBER
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}

	private void addDiccionarioCommand(){
		def fieldNumber = this.P(FIELD_PARAM_NAME);
		def sentencia = """
		INSERT INTO DICCIONARIO(NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
		VALUES(${fieldNumber}, ' ', 0, 'kafka partition key', 'kafka partition key event', 350, 'A', 0, NULL, 0, ${CONCEPT_NUMBER}, 0, 0, 0, 0, 0, 0, NULL, 0, NULL);
		"""
		def descripcion = "Agregando nuevo campo en diccionario para el concepto " + CONCEPT_NUMBER
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	private void addColumnEventToProcessCommand(){
		def sentencia = """
		ALTER TABLE EVENTOS_A_PROCESAR ADD PARTITION_KEY VARCHAR(350)
		"""
		def descripcion = "Agregando nueva columna PARTITION_KEY a eventos a procesar "
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	private void changeEventToProcessDatamappingCommand(){
		def archivoMapp = "EventoTransaccionMapping.xml"
		def entidad = "core.vo_EventoProcesar"
		def atributo = """
			<attribute>
	            <attribute-name>partitionKey</attribute-name>
	            <column-name>PARTITION_KEY</column-name>
	        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
	}

}
