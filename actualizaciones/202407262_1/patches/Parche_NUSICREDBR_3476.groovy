package sicredi;

import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche;

public class Parche_NUSICREDBR_3476 extends Parche {
	
	void sobreElParche() {
		this.notas_del_autor = "Parche_NUSICREDBR_3476"
		this.opcional = false
	}
	
	void definicionDeParametros(){
		this.addParameterNewFreeField("numeroDeCampoDIAS_PRORROGA", "N�mero de campo libre para ingresar la cantidad de d�as a prorrogar")
		this.addParameterNewFreeField("numeroDeCampoTASA_PRORROGA", "N�mero de campo libre para ingresar la tasa de inter�s de la pr�rroga")
		this.addParameterNewFreeField("numeroDeCampoINTERES_PENDIENTE", "N�mero de campo libre para los intereses pendientes")
	}
	
	@Override
	protected void comandos() {
		insertCamposConceptos()
	}
	
	private void insertCamposConceptos(){
		def sentencia = """
			INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO)
			VALUES (${this.P('numeroDeCampoDIAS_PRORROGA')}, ' ', 0, 'Dias Prorrogados', 'Dias Prorrogados', 10, 'N', 0, NULL, 0, 2801, 0, 0, 0, 0, 0, 0, 'PF_CANTIDAD_DIAS', 0);
			
			INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO)
			VALUES (${this.P('numeroDeCampoTASA_PRORROGA')}, ' ', 0, 'Tasa Interes Prorroga', 'Tasa Interes Prorroga', 11, 'N', 7, 'I', 0, 2802, 0, 0, 0, 0, 0, 0, 'PF_TASA_PRORROGA', 0);
			
			INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO)
			VALUES (${this.P('numeroDeCampoINTERES_PENDIENTE')}, ' ', 0, 'Int. Pendiente Prorroga', 'Int. Pendiente Prorroga', 15, 'N', 2, 'I', 0, 2803, 0, 0, 0, 0, 0, 0, 'PF_INTERES_PENDIENTE', 0);
			
			INSERT INTO CONCEPTOS (CODIGO, DESCRIPCION, CAMPOV25)
			VALUES (2801, 'PF_CANTIDAD_DIAS', ${this.P('numeroDeCampoDIAS_PRORROGA')});
			
			INSERT INTO CONCEPTOS (CODIGO, DESCRIPCION, CAMPOV25)
			VALUES (2802, 'PF_TASA_PRORROGA', ${this.P('numeroDeCampoTASA_PRORROGA')});
			
			INSERT INTO CONCEPTOS (CODIGO, DESCRIPCION, CAMPOV25)
			VALUES (2803, 'PF_INTERES_PENDIENTE', ${this.P('numeroDeCampoINTERES_PENDIENTE')});
		"""
		def descripcion = "Inserando nuevos conceptos y sus registros en diccionario"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	


}
