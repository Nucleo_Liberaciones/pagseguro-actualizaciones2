import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.parche.Parche;

class Parche_NUCRDSIS_76 extends Parche {

	@Override
	protected void comandos() {

		//----------------------------------------------------------------------------------------------------------------//
		// Agregar o sustituir uno (o varios) atributos de una entidad de DataMapping
		def archivoMapp = "CreditosMapping.xml"
		def entidad = "core.vo_DeudaPorCuota"
		def atributo = """
	        <attribute>
	            <attribute-name>interesIpf</attribute-name>
	            <column-name>INTERES_IPF</column-name>
	        </attribute>
	        <attribute>
	            <attribute-name>moraIpf</attribute-name>
	            <column-name>MORA_IPF</column-name>
	        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
		//----------------------------------------------------------------------------------------------------------------//
		
		//----------------------------------------------------------------------------------------------------------------//
		// Actualización de la Base de Datos en Oracle
		def sentencia = """
			ALTER TABLE CRE_DEUDA_POR_CUOTA ADD INTERES_IPF NUMBER (15, 2)     DEFAULT (0);
			ALTER TABLE CRE_DEUDA_POR_CUOTA ADD MORA_IPF    NUMBER (15, 2)     DEFAULT (0);
        """
		def descripcion = "Se agrega INTERES_IPF y MORA_IPF a CRE_DEUDA_POR_CUOTA."
		this.addCommand(new ActualizarBaseDeDatosOracle(sentencia, descripcion))
		//----------------------------------------------------------------------------------------------------------------//
		
		//----------------------------------------------------------------------------------------------------------------//
		// Actualización de la Base de Datos en SQLServer
		sentencia = """
			ALTER TABLE CRE_DEUDA_POR_CUOTA ADD INTERES_IPF NUMERIC (15, 2)     DEFAULT (0) WITH VALUES;
			ALTER TABLE CRE_DEUDA_POR_CUOTA ADD MORA_IPF    NUMERIC (15, 2)     DEFAULT (0) WITH VALUES;
        """
		descripcion = "Se agrega INTERES_IPF y MORA_IPF a CRE_DEUDA_POR_CUOTA."
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
		//----------------------------------------------------------------------------------------------------------------//
	}
}