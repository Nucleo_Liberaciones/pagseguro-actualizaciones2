import topsystems.TopazException
import topsystems.actualizador.command.archivos.CopiarDesdeZip
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.processmanager.BorrarGrupo
import topsystems.actualizador.command.processmanager.BorrarProceso
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche


class Parche_NUTOPAZ56_61 extends Parche {
	
	@Override
	protected void comandos() throws TopazException {
		
		agregarDataMappingQuery();
		
	}
	
	private void agregarDataMappingQuery(){
		// Agregar o sustituir una query (o varias) de DataMapping
		def archivoMapp = "BandejaEntradaContableMapping.xml"
		def queries = """
				<query>
					<class-name>topsystems.automaticprocess.bandejaDeMovimientos.valueobjects.QuerySucursal</class-name>
					<query-name>query.TablaSucursales</query-name>
					<database-name>TOP/CLIENTES</database-name>
					<sentence>
						SELECT SUCURSAL FROM SUCURSALESSC
					</sentence>
					<attribute>
						<attribute-name>number</attribute-name>
						<column-name>SUCURSAL</column-name>
						<column-type>long</column-type>
					</attribute>
				</query>
				"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}

}