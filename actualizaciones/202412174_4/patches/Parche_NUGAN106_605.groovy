package GANADERO

import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Parche_NUGAN106_605 extends Parche {

	void sobreElParche() {
		this.notas_del_autor = "Parche_NUGAN106_605"
		this.opcional = false
	}

	@Override
	protected void comandos() {
		addNewSaltColumn()
		addProperty()
	}

	private void addProperty(){
		// Agregar una propiedad o cambiar el valor a una existente
		def archivo = "jts.properties" // Si el archivo no existe, se crea.
		def propiedad = "topaz.security.user.password.enable.salt"
		def valor = "false"
		def comentario = "Habilita el uso del salt en las contraseņas default(false)"
		this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, valor, comentario))
	}

	private void addNewSaltColumn(){
		if(this.isOracle()){
			addNewSaltColumnOracle()
		}else if(this.isSqlServer()){
			addNewSaltColumnSqlServer()
		}else{
			throw new RuntimeException("Database is not valid");
		}
	}

	private void addNewSaltColumnOracle(){
		def sentencia = """
		ALTER TABLE USUARIOS ADD SALT VARCHAR2(250);
		ALTER TABLE USUARIOS ADD CONTRASENIASUSADAS_SALT VARCHAR2(250);
		 """
		def descripcion = "Add salt column in usuarios table"
		this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	}

	private void addNewSaltColumnSqlServer(){
		def sentencia = """
		ALTER TABLE USUARIOS ADD SALT VARCHAR(250);
		ALTER TABLE USUARIOS ADD CONTRASENIASUSADAS_SALT VARCHAR(250);
		"""
		def descripcion = "Add salt column in usuarios table"
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))

	}
}
