package Parches

import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.parche.Parche

class Parche_NUBANKIBR_416 extends Parche{

	@Override
	protected void comandos() throws TopazException {
		modificarMapping();
	}
	
	protected void modificarMapping(){
		// Agregar o sustituir una entidad de DataMapping
	def archivoMapp = "categorizacionPorCuotaMapping.xml"
	def entidades = """
		<class>
	        <class-name>topsystems.automaticprocess.categorizaciondecartera.SaldoCategorizar</class-name>
	        <entity-name>core.vo_SaldoCategorizar</entity-name>
	        <table-name>SALDOS</table-name>
	        <database-name>TOP/CLIENTES</database-name>
	        <attribute>
	            <attribute-name>JTS_OID</attribute-name>
	            <column-name>JTS_OID</column-name>
	            <key>primary</key>
	        </attribute>
	        <attribute>
	            <attribute-name>SAL_DIAS_ATRASO</attribute-name>
	            <column-name>C1649</column-name>
	        </attribute>
	        <attribute>
	            <attribute-name>SAL_FECHA_ULTIMA_CATEGORIZACION</attribute-name>
	            <column-name>C3877</column-name>
	        </attribute>
	        <attribute>
	            <attribute-name>SAL_TIPO_CATEGORIZACION</attribute-name>
	            <column-name>C1833</column-name>
	        </attribute>
	        <attribute>
	            <attribute-name>SAL_RUBRO_OPERATIVO</attribute-name>
	            <column-name>C1804</column-name>
	        </attribute>
	        <attribute>
	            <attribute-name>SAL_RUBRO_CONTABLE</attribute-name>
	            <column-name>C1730</column-name>
	        </attribute>
	        <attribute>
	            <attribute-name>SAL_ESTADO_ATRASO</attribute-name>
	            <column-name>C1728</column-name>
	        </attribute>
	        <attribute>
	            <attribute-name>SAL_FECHA_CASTIGADO</attribute-name>
	            <column-name>FECHA_CASTIGADO</column-name>
	        </attribute>
	        <attribute>
	            <attribute-name>SAL_ULT_FECHA_EN_VIGENTE</attribute-name>
	            <column-name>ULT_FECHA_VIGENTE</column-name>
	        </attribute>
			<attribute>
	            <attribute-name>SAL_RUBRO_ORIGINAL</attribute-name>
	            <column-name>C1692</column-name>
	        </attribute>
	    </class>
		"""
	this.addCommand( new AgregarModificarEntidad(archivoMapp, entidades))

	}
	
	

}
