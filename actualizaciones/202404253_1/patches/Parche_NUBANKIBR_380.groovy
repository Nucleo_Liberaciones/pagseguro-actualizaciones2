import topsystems.TopazException
import topsystems.actualizador.command.archivos.CopiarDesdeZip
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.processmanager.BorrarGrupo
import topsystems.actualizador.command.processmanager.BorrarProceso
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche


class Parche_NUBANKIBR_380 extends Parche {
	
	void definicionDeParametros(){
		
		//Generados por el sistema
		this.addParameterPATitleOpe("TITULO_OPERACION", "Numero de Titulo para la operacion asociada al proceso")
		this.addParameterNewOperationNumber("NUMERO_OPERACION_PROCESO", "Numero de operación para el proceso: Calculo Prevision")
	}

	@Override
	protected void comandos() throws TopazException {
		
		copiarFichero();
		copiarServicio();
		adicionarProceso();
		adicionarGrupo();
		actualizarParametrosJts();
		actualizarOperacion();
		
		if(isSqlServer()){
			agregarDataMappingQuerySql();
			actualizarBD1Sql();
		} else {
			agregarDataMappingQueryOracle();
			actualizarBD1Oracle();
		}
		
	}
	
	private copiarFichero(){
		
		this.addCommand new CopiarDesdeZip("PrevisionesMapping.xml", "\${DATAMAPPINGS}/")
	}
	
	private copiarServicio(){
		
		this.addCommand new CopiarDesdeZip("PrevisionesDeploy.xml", "\${TOPAZ_SERVICES}/")
	}
	
	private	adicionarProceso(){
		def archivo = "processes.py"
		def procesos = """
						# Proceso Calculo de Previsiones
					    pdef = ProcessDefinition("Calculo Previsiones", "topsystems.automaticprocess.processmanager.WorkManager")
					    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WDCalculoPrevisiones");
					    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BWCalculoPrevisiones");
					    pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
						pdef.addConstant("rangoCommit","500");
					    pdef.addConstant("cantidadHilos","10");
					    pdefs.addProcess(pdef)
					   """
		
		this.addCommand(new AgregarModificarProceso(archivo, procesos))
	}
	
	private adicionarGrupo(){
		def archivo = "groups.py"
		def grupo = """
					 gdef = GroupDefinition("CRE - Calculo Previsiones")
				     gdef.registerProcess("Calculo Previsiones")
				     gdefs.addGroup(gdef)
					"""
		this.addCommand( new AgregarModificarGrupo(archivo, grupo))
	}
	
	private actualizarParametrosJts(){
		
		def sentencia = """
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CALCULO_DE_PREVISIONES_BR', 'CANAL', '0', 0)
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CALCULO_DE_PREVISIONES_BR', 'DESCRIPCION', 'Calcula previsiones', 0)	
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CALCULO_DE_PREVISIONES_BR', 'OPERACION', """+ this.P("NUMERO_OPERACION_PROCESO")+""", 0)
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CALCULO_DE_PREVISIONES_BR', 'CONSIDERAR_MULTA', 'S', 0)
						"""
		def descripcion = "Insertando parametros proceso"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	private void actualizarOperacion(){
		def sentencia = """
						INSERT INTO OPERACIONES (TITULO, IDENTIFICACION, NOMBRE, DESCRIPCION, MNEMOTECNICO, AUTORIZACION, FORMULARIOPRINCIPAL, PROXOPERACION, ESTADO, TZ_LOCK, COPIAS, SUBOPERACION, PERMITEBAJA, COMPORTAMIENTOENCIERRE, REQUIERECONTRASENA, PERMITECONCURRENTE, PERMITEESTADODIFERIDO)
						VALUES (${this.P("TITULO_OPERACION")} , ${this.P("NUMERO_OPERACION_PROCESO")} , 'PA: CRE - Calculo Previsiones', 'CRE - Calculo Previsiones', """+ this.P("NUMERO_OPERACION_PROCESO")+""", 'N', NULL, NULL, 'P', 0, NULL, 0, 'S', 'N', 'N', 'S', 'S')
						"""
		def descripcion = "Actualizar OPERACIONES"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	@Override
	protected boolean isSqlServer() {
		return super.isSqlServer();
	}

	private actualizarBD1Oracle(){
		def sentencia = """
						CREATE TABLE PREVISIONES_AUXILIAR_CONTAB
						(SALDOS_JTS_OID NUMBER(12) NOT NULL,
						 RUBRO_CONTABLE_ANT NUMBER(12) DEFAULT 0,
						 SALDO_CONTABLE_ANT NUMBER(15,2) DEFAULT 0,
						 POR_PD_ANT NUMBER(8,4) DEFAULT 0,
						 POR_LGD_ANT NUMBER(8,4)DEFAULT 0,
						 PORCENTAJE_PREV_ANT NUMBER(8,4) DEFAULT 0,
						 PREVISION_ANT NUMBER(15,2) DEFAULT 0,
						 FECHA_CALCULADO_ANT DATE DEFAULT NULL,
						 FECHA_CONTABILIZADO_ANT DATE DEFAULT NULL,
						 RUBRO_CONTABLE NUMBER(12) DEFAULT 0,
						 SALDO_CONTABLE NUMBER(15,2) DEFAULT 0,
						 POR_PD NUMBER(8,4) DEFAULT 0,
						 POR_LGD NUMBER(8,4) DEFAULT 0,
						 PORCENTAJE_PREV NUMBER(8,4) DEFAULT 0,
						 PREVISION NUMBER(15,2) DEFAULT 0,
						 FECHA_CALCULADO DATE DEFAULT NULL,
						 FECHA_CONTABILIZADO DATE DEFAULT NULL,
						 RUBRO_CONTABLE_PEA NUMBER(12) DEFAULT 0,
						 SALDO_CONTABLE_PEA NUMBER(15,2) DEFAULT 0,
						 POR_PD_PEA NUMBER(8,4) DEFAULT 0,
						 POR_LGD_PEA NUMBER(8,4) DEFAULT 0,
						 PORCENTAJE_PREV_PEA NUMBER(8,4) DEFAULT 0,
						 PREVISION_PEA NUMBER(15,2) DEFAULT 0,
						 FECHA_CALCULADO_PEA DATE DEFAULT NULL,
						 FECHA_CONTABILIZADO_PEA DATE DEFAULT NULL,
						
						CONSTRAINT PK_PREVISIONES_AUXILIAR_CONTAB PRIMARY KEY (SALDOS_JTS_OID)
						)
						"""
		def descripcion = "Se agrega tabla"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	private actualizarBD1Sql(){
		def sentencia = """
						CREATE TABLE PREVISIONES_AUXILIAR_CONTAB
						(SALDOS_JTS_OID NUMERIC(12) NOT NULL,
						 RUBRO_CONTABLE_ANT NUMERIC(12) DEFAULT 0,
						 SALDO_CONTABLE_ANT NUMERIC(15,2) DEFAULT 0,
						 POR_PD_ANT NUMERIC(8,4) DEFAULT 0,
						 POR_LGD_ANT NUMERIC(8,4)DEFAULT 0,
						 PORCENTAJE_PREV_ANT NUMERIC(8,4) DEFAULT 0,
						 PREVISION_ANT NUMERIC(15,2) DEFAULT 0,
						 FECHA_CALCULADO_ANT DATE DEFAULT NULL,
						 FECHA_CONTABILIZADO_ANT DATE DEFAULT NULL,
						 RUBRO_CONTABLE NUMERIC(12) DEFAULT 0,
						 SALDO_CONTABLE NUMERIC(15,2) DEFAULT 0,
						 POR_PD NUMERIC(8,4) DEFAULT 0,
						 POR_LGD NUMERIC(8,4) DEFAULT 0,
						 PORCENTAJE_PREV NUMERIC(8,4) DEFAULT 0,
						 PREVISION NUMERIC(15,2) DEFAULT 0,
						 FECHA_CALCULADO DATE DEFAULT NULL,
						 FECHA_CONTABILIZADO DATE DEFAULT NULL,
						 RUBRO_CONTABLE_PEA NUMERIC(12) DEFAULT 0,
						 SALDO_CONTABLE_PEA NUMERIC(15,2) DEFAULT 0,
						 POR_PD_PEA NUMERIC(8,4) DEFAULT 0,
						 POR_LGD_PEA NUMERIC(8,4) DEFAULT 0,
						 PORCENTAJE_PREV_PEA NUMERIC(8,4) DEFAULT 0,
						 PREVISION_PEA NUMERIC(15,2) DEFAULT 0,
						 FECHA_CALCULADO_PEA DATE DEFAULT NULL,
						 FECHA_CONTABILIZADO_PEA DATE DEFAULT NULL,
						
						CONSTRAINT PK_PREVISIONES_AUXILIAR_CONTAB PRIMARY KEY (SALDOS_JTS_OID)
						)
						"""
		def descripcion = "Se agrega tabla"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}

	private agregarDataMappingQuerySql(){
		def archivoMapp = "PrevisionesMapping.xml"
		def queries = """
			<query>
				<class-name>topsystems.automaticprocess.prevision.objectdomain.ObjetoSaldoPrevision</class-name>
				<query-name>query.objetoSaldoPrevision</query-name>
				<database-name>TOP/CLIENTES</database-name>
				<sentence>
					SELECT S.C1803 AS numeroCliente,
						   S.JTS_OID,
						   SUM(abs(S.C1604)) AS SALDO_CAPITAL,
						   SUM(H.INTERES_DEVENG_VIGENTE_CONT + H.INTERES_DEVENG_VENCIDO_CONT) AS INTERES_DEV_NO_SUSPENDIDO,
						   SUM(H.MORA_DEVENG_VIGENTE_CONT + H.MORA_DEVENG_VENCIDO_CONT) AS MORA_NO_SUSPENDIDA,
						   SUM (-S.GIO_IN_TOTAL + H.GIO_IN_DEVENG_VIGENTE_CONT + H.GIO_IN_DEVENG_VENCIDO_CONT
								+ ABS(S.GIO_GT_TOTAL) - H.GIO_GT_DEVENG_VIGENTE_CONT - H.GIO_GT_DEVENG_VENCIDO_CONT) 
								AS DEV_GASTOS_INGRESOS_ORIG,
						   SUM(G.SALDO_GASTO) AS SALDO_GASTO,
						   S.C1730 AS RUBRO_CONTABLE,
						   E.POR_PD,
						   E.POR_LGD
					FROM SALDOS S
					INNER JOIN PLANCTAS P ON S.C1730=P.C6326 AND PREVISIONA &gt; 0
					LEFT JOIN (SELECT * 
							   FROM HISTORICO_DEVENGAMIENTO 
							   WHERE TZ_LOCK=0 
							   AND FECHA= ? ) H ON H.SALDO_JTS_OID = S.JTS_OID -- Fecha de proceso
					LEFT JOIN (SELECT * 
							   FROM GASTOS_POR_CUOTA 
							   WHERE TZ_LOCK=0 
							   AND PREVISIONA='S' 
							   AND SALDO_GASTO &gt; 0) G  ON G.SALDOS_JTS_OID = S.JTS_OID
					INNER JOIN PORCENTAGEM_PERDAS_ESPERADAS E ON E.codigocliente = S.C1803
					WHERE s.TZ_LOCK = 0
					AND p.TZ_LOCK=0
					AND ((s.C1604  &lt;= 0) OR (s.C1604 = 0 
												AND DATEPART(MONTH FROM S.C1626) = DATEPART(MONTH FROM ? ) -- Fecha de proceso
												AND DATEPART(YEAR FROM S.C1626) = DATEPART(YEAR FROM ? ))) -- Fecha de proceso
					AND (s.C1803 &gt; 0)
					AND e.fecha = (SELECT MAX(FECHA) AS FECHA 
								   FROM PORCENTAGEM_PERDAS_ESPERADAS e1 
								   WHERE e1.CODIGOCLIENTE=s.C1803 
								   AND e.TZ_LOCK=0 
								   AND POR_LGD IS NOT null)
					GROUP BY C1803,JTS_OID, C1730,POR_PD,POR_LGD,RUBRO_CONTABLE_OPERACION
				</sentence>
				<attribute>
					<attribute-name>nroCliente</attribute-name>
					<column-name>numeroCliente</column-name>
					<column-type>long</column-type>
				</attribute>
				<attribute>
					<attribute-name>jtsOid</attribute-name>
					<column-name>JTS_OID </column-name>
					<column-type>long</column-type>
				</attribute>
				<attribute>
					<attribute-name>capital</attribute-name>
					<column-name>SALDO_CAPITAL </column-name>
					<column-type>BigDecimal</column-type>
				</attribute>
				<attribute>
					<attribute-name>intDevNoSuspendido</attribute-name>
					<column-name>INTERES_DEV_NO_SUSPENDIDO </column-name>
					<column-type>BigDecimal</column-type>
				</attribute>
				<attribute>
					<attribute-name>moraNoSuspendida</attribute-name>
					<column-name>MORA_NO_SUSPENDIDA </column-name>
					<column-type>BigDecimal</column-type>
				</attribute>
				<attribute>
					<attribute-name>devGastosIngresoOrig</attribute-name>
					<column-name>DEV_GASTOS_INGRESOS_ORIG </column-name>
					<column-type>BigDecimal</column-type>
				</attribute>
				<attribute>
					<attribute-name>saldoGasto</attribute-name>
					<column-name>SALDO_GASTO </column-name>
					<column-type>BigDecimal</column-type>
				</attribute>
				<attribute>
					<attribute-name>rubroContable</attribute-name>
					<column-name>RUBRO_CONTABLE </column-name>
					<column-type>long</column-type>
				</attribute>
				<attribute>
					<attribute-name>porcentajePD</attribute-name>
					<column-name>POR_PD </column-name>
					<column-type>double</column-type>
				</attribute>
				<attribute>
					<attribute-name>porcentajeLGD</attribute-name>
					<column-name>POR_LGD </column-name>
					<column-type>Double</column-type>
				</attribute>
			</query>"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}

	private agregarDataMappingQueryOracle(){
		def archivoMapp = "PrevisionesMapping.xml"
		def queries = """
			<query>
				<class-name>topsystems.automaticprocess.prevision.objectdomain.ObjetoSaldoPrevision</class-name>
				<query-name>query.objetoSaldoPrevision</query-name>
				<database-name>TOP/CLIENTES</database-name>
				<sentence>
					SELECT S.C1803 AS numeroCliente,
						   S.JTS_OID,
						   SUM(abs(S.C1604)) AS SALDO_CAPITAL,
						   SUM(H.INTERES_DEVENG_VIGENTE_CONT + H.INTERES_DEVENG_VENCIDO_CONT) AS INTERES_DEV_NO_SUSPENDIDO,
						   SUM(H.MORA_DEVENG_VIGENTE_CONT + H.MORA_DEVENG_VENCIDO_CONT) AS MORA_NO_SUSPENDIDA,
						   SUM (-S.GIO_IN_TOTAL + H.GIO_IN_DEVENG_VIGENTE_CONT + H.GIO_IN_DEVENG_VENCIDO_CONT
								+ ABS(S.GIO_GT_TOTAL) - H.GIO_GT_DEVENG_VIGENTE_CONT - H.GIO_GT_DEVENG_VENCIDO_CONT) 
								AS DEV_GASTOS_INGRESOS_ORIG,
						   SUM(G.SALDO_GASTO) AS SALDO_GASTO,
						   S.C1730 AS RUBRO_CONTABLE,
						   E.POR_PD,
						   E.POR_LGD
					FROM SALDOS S
					INNER JOIN PLANCTAS P ON S.C1730=P.C6326 AND PREVISIONA &gt; 0
					LEFT JOIN (SELECT * 
							   FROM HISTORICO_DEVENGAMIENTO 
							   WHERE TZ_LOCK=0 
							   AND FECHA= ? ) H ON H.SALDO_JTS_OID = S.JTS_OID -- Fecha de proceso
					LEFT JOIN (SELECT * 
							   FROM GASTOS_POR_CUOTA 
							   WHERE TZ_LOCK=0 
							   AND PREVISIONA='S' 
							   AND SALDO_GASTO &gt; 0) G  ON G.SALDOS_JTS_OID = S.JTS_OID
					INNER JOIN PORCENTAGEM_PERDAS_ESPERADAS E ON E.codigocliente = S.C1803
					WHERE s.TZ_LOCK = 0
					AND p.TZ_LOCK=0
					AND ((s.C1604  &lt;= 0) OR (s.C1604 = 0 
												AND EXTRACT(MONTH FROM S.C1626) = EXTRACT(MONTH FROM ? ) -- Fecha de proceso
												AND EXTRACT(YEAR FROM S.C1626) = EXTRACT(YEAR FROM ? ))) -- Fecha de proceso
					AND (s.C1803 &gt; 0)
					AND e.fecha = (SELECT MAX(FECHA) AS FECHA 
								   FROM PORCENTAGEM_PERDAS_ESPERADAS e1 
								   WHERE e1.CODIGOCLIENTE=s.C1803 
								   AND e.TZ_LOCK=0 
								   AND POR_LGD IS NOT null)
					GROUP BY C1803,JTS_OID, C1730,POR_PD,POR_LGD,RUBRO_CONTABLE_OPERACION
				</sentence>
				<attribute>
					<attribute-name>nroCliente</attribute-name>
					<column-name>numeroCliente</column-name>
					<column-type>long</column-type>
				</attribute>
				<attribute>
					<attribute-name>jtsOid</attribute-name>
					<column-name>JTS_OID </column-name>
					<column-type>long</column-type>
				</attribute>
				<attribute>
					<attribute-name>capital</attribute-name>
					<column-name>SALDO_CAPITAL </column-name>
					<column-type>BigDecimal</column-type>
				</attribute>
				<attribute>
					<attribute-name>intDevNoSuspendido</attribute-name>
					<column-name>INTERES_DEV_NO_SUSPENDIDO </column-name>
					<column-type>BigDecimal</column-type>
				</attribute>
				<attribute>
					<attribute-name>moraNoSuspendida</attribute-name>
					<column-name>MORA_NO_SUSPENDIDA </column-name>
					<column-type>BigDecimal</column-type>
				</attribute>
				<attribute>
					<attribute-name>devGastosIngresoOrig</attribute-name>
					<column-name>DEV_GASTOS_INGRESOS_ORIG </column-name>
					<column-type>BigDecimal</column-type>
				</attribute>
				<attribute>
					<attribute-name>saldoGasto</attribute-name>
					<column-name>SALDO_GASTO </column-name>
					<column-type>BigDecimal</column-type>
				</attribute>
				<attribute>
					<attribute-name>rubroContable</attribute-name>
					<column-name>RUBRO_CONTABLE </column-name>
					<column-type>long</column-type>
				</attribute>
				<attribute>
					<attribute-name>porcentajePD</attribute-name>
					<column-name>POR_PD </column-name>
					<column-type>double</column-type>
				</attribute>
				<attribute>
					<attribute-name>porcentajeLGD</attribute-name>
					<column-name>POR_LGD </column-name>
					<column-type>Double</column-type>
				</attribute>
			</query>"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}

}