import topsystems.TopazException
import topsystems.actualizador.command.archivos.CopiarDesdeZip
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.processmanager.BorrarGrupo
import topsystems.actualizador.command.processmanager.BorrarProceso
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche


class Parche_NUBANKIBR_383 extends Parche {
	
	void definicionDeParametros(){
		
		//Generados por el sistema
		this.addParameterPATitleOpe("TITULO_OPERACION", "Numero de Titulo para la operacion asociada al proceso")
		this.addParameterNewOperationNumber("NUMERO_OPERACION_PROCESO", "Numero de operación para el proceso: Contabilizacion Prevision")
		this.addParameterNewFreeField("NRO_PIVOT_ALTA", "Numero de campo libre para ser utilizado como pivot");
		this.addParameterNewFreeField("NRO_PIVOT_BAJA", "Numero de campo libre para ser utilizado como pivot");
		this.addParameterNewFreeField("NRO_PIVOT_GANANCIA", "Numero de campo libre para ser utilizado como pivot");
		this.addParameterNewFreeField("NRO_PIVOT_PERDIDA", "Numero de campo libre para ser utilizado como pivot");
	}

	@Override
	protected void comandos() throws TopazException {
		
		agregarDataMappingQuery();
		agregarDataMappingEntidad();
		modificarServicios();
		adicionarProceso();
		adicionarGrupo();
		actualizarParametrosJts();
		actualizarOperacion();
		
		if(isSqlServer()){
			actualizarBD1Sql();
		} else {
			actualizarBD1Oracle();
		}
		
	}
	
	private void agregarDataMappingQuery(){
		// Agregar o sustituir una query (o varias) de DataMapping
		def archivoMapp = "PrevisionesMapping.xml"
		def queries = """
						<query>
							<class-name>topsystems.automaticprocess.prevision.objectdomain.ObjetoSaldoAContabilizar</class-name>
							<query-name>query.SaldoAContabilizar</query-name>
							<database-name>TOP/CLIENTES</database-name>
							<sentence>
								SELECT S.C1604,
									   S.JTS_OID
								FROM PREVISIONES_AUXILIAR_CONTAB P 
									 INNER JOIN SALDOS S ON P.SALDOS_JTS_OID=S.JTS_OID
								WHERE S.TZ_LOCK=0 
								AND (P.FECHA_CONTABILIZADO IS NULL 
									 OR (P.FECHA_CONTABILIZADO &lt; ? and S.C1604 = 0)) -- Fecha de proceso 
							</sentence>
							<attribute>
								<attribute-name>saldo</attribute-name>
								<column-name>C1604</column-name>
								<column-type>BigDecimal</column-type>
							</attribute>
							<attribute>
								<attribute-name>jtsOid</attribute-name>
								<column-name>JTS_OID</column-name>
								<column-type>Long</column-type>
							</attribute>
						</query>
						"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}
	
	private agregarDataMappingEntidad(){
		// Agregar o sustituir una entidad de DataMapping
		def archivoMapp = "PrevisionesMapping.xml"
		def entidades = """
					   	<class>
							<class-name>topsystems.automaticprocess.prevision.objectdomain.PrevisionesHistorico</class-name>
							<entity-name>core.vo_PrevisionesHistorico</entity-name>
							<table-name>PREVISIONES_HISTORICO</table-name>
							<database-name>TOP/CLIENTES</database-name>
								<attribute>
									<attribute-name>fechaProceso</attribute-name>
									<column-name>FECHA_PROCESO</column-name>
									<key>primary</key>
								</attribute>
								<attribute>
									<attribute-name>saldosJtsOid</attribute-name>
									<column-name>SALDOS_JTS_OID</column-name>
									<key>primary</key>
								</attribute>
								<attribute>
									<attribute-name>rubroContable</attribute-name>
									<column-name>RUBRO_CONTABLE</column-name>
								</attribute>
								<attribute>
									<attribute-name>saldoContable</attribute-name>
									<column-name>SALDO_CONTABLE</column-name>
								</attribute>
								<attribute>
									<attribute-name>porcentajePD</attribute-name>
									<column-name>POR_PD</column-name>
								</attribute>
								<attribute>
									<attribute-name>porcentajeLGD</attribute-name>
									<column-name>POR_LGD</column-name>
								</attribute>
								<attribute>
									<attribute-name>porcentajePrevision</attribute-name>
									<column-name>PORCENTAJE_PREV</column-name>
								</attribute>
								<attribute>
									<attribute-name>prevision</attribute-name>
									<column-name>PREVISION</column-name>
								</attribute>
								<attribute>
									<attribute-name>asiento</attribute-name>
									<column-name>ASIENTO</column-name>
								</attribute>								
								<attribute>
									<attribute-name>sucursal</attribute-name>
									<column-name>SUCURSAL</column-name>
								</attribute>
						</class>
					"""
		this.addCommand( new AgregarModificarEntidad(archivoMapp, entidades))
	}
	
	private void modificarServicios(){
		
		def archivo = "PrevisionesDeploy.xml"
		def servicios = """
						<service>
							<name>topsystems:processManager:WorkDescriptor=WDContabilizacionPrevisiones</name>
							<code>topsystems.automaticprocess.prevision.contabilizacion.WDContabilizacionPrevisiones</code>
						</service>
						<service>
							<name>topsystems:processManager:BusinessWorker=BWContabilizacionPrevisiones</name>
							<code>topsystems.automaticprocess.prevision.contabilizacion.BWContabilizacionPrevisiones</code>
						</service>
						"""
		this.addCommand( new AgregarModificarServicios(archivo, servicios))
	}
	
	private	adicionarProceso(){
		def archivo = "processes.py"
		def procesos = """
						# Proceso Contabilizacion de Previsiones
					    pdef = ProcessDefinition("Contabilizacion Previsiones", "topsystems.automaticprocess.processmanager.WorkManager")
					    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WDContabilizacionPrevisiones");
					    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BWContabilizacionPrevisiones");
					    pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
						pdef.addConstant("rangoCommit","500");
					    pdef.addConstant("cantidadHilos","10");
					    pdefs.addProcess(pdef)
					   """
		
		this.addCommand(new AgregarModificarProceso(archivo, procesos))
	}
	
	private adicionarGrupo(){
		def archivo = "groups.py"
		def grupo = """
					 gdef = GroupDefinition("CRE - Contabilizacion Previsiones")
				     gdef.registerProcess("Contabilizacion Previsiones")
				     gdefs.addGroup(gdef)
					"""
		this.addCommand( new AgregarModificarGrupo(archivo, grupo))
	}
	
	private actualizarParametrosJts(){
		
		def sentencia = """
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CONTABILIZACION_DE_PREVISIONES_BR', 'CANAL', '0', 0)
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CONTABILIZACION_DE_PREVISIONES_BR', 'DESCRIPCION', 'Contabiliza previsiones', 0)
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CONTABILIZACION_DE_PREVISIONES_BR', 'OPERACION', """+ this.P("NUMERO_OPERACION_PROCESO")+""", 0)
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CONTABILIZACION_DE_PREVISIONES_BR', 'PIVOT_ALTA', """+ this.P("NRO_PIVOT_ALTA")+""", 0)
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CONTABILIZACION_DE_PREVISIONES_BR', 'PIVOT_BAJA', """+ this.P("NRO_PIVOT_BAJA")+""", 0)
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CONTABILIZACION_DE_PREVISIONES_BR', 'PIVOT_GANANCIA', """+ this.P("NRO_PIVOT_GANANCIA")+""", 0)
						INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
						VALUES ('CONTABILIZACION_DE_PREVISIONES_BR', 'PIVOT_PERDIDA', """+ this.P("NRO_PIVOT_PERDIDA")+""", 0)
						"""
		def descripcion = "Insertando parametros proceso"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	private void actualizarOperacion(){
		def sentencia = """
						INSERT INTO OPERACIONES (TITULO, IDENTIFICACION, NOMBRE, DESCRIPCION, MNEMOTECNICO, AUTORIZACION, FORMULARIOPRINCIPAL, PROXOPERACION, ESTADO, TZ_LOCK, COPIAS, SUBOPERACION, PERMITEBAJA, COMPORTAMIENTOENCIERRE, REQUIERECONTRASENA, PERMITECONCURRENTE, PERMITEESTADODIFERIDO)
						VALUES (${this.P("TITULO_OPERACION")} , ${this.P("NUMERO_OPERACION_PROCESO")} , 'PA: CRE - Contabilizacion Previsiones', 'CRE - Contabilizacion Previsiones', """+ this.P("NUMERO_OPERACION_PROCESO")+""", 'N', NULL, NULL, 'P', 0, NULL, 0, 'S', 'N', 'N', 'S', 'S')
						"""
		def descripcion = "Actualizar OPERACIONES"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	@Override
	protected boolean isSqlServer() {
		return super.isSqlServer();
	}

	private actualizarBD1Oracle(){
		def sentencia = """
						CREATE TABLE PREVISIONES_HISTORICO
							(FECHA_PROCESO DATE NOT NULL,
							SALDOS_JTS_OID NUMBER(12) NOT NULL,
							RUBRO_CONTABLE NUMBER(12) NOT NULL,
							SALDO_CONTABLE NUMBER(15,2) NOT NULL,
							POR_PD NUMBER(8,4) NOT NULL,
							POR_LGD NUMBER(8,4)NOT NULL,
							PORCENTAJE_PREV NUMBER(8,4) NOT NULL,
							PREVISION NUMBER(15,2) NOT NULL,
							ASIENTO NUMBER(10,0) NOT NULL,
							SUCURSAL NUMBER(10,0) NOT NULL,
							CONSTRAINT PK_PREVISIONES_HIST PRIMARY KEY (FECHA_PROCESO, SALDOS_JTS_OID)
							)
						"""
		def descripcion = "Se agrega tabla"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
	private actualizarBD1Sql(){
		def sentencia = """
						CREATE TABLE PREVISIONES_HISTORICO
							(FECHA_PROCESO DATE NOT NULL,
							SALDOS_JTS_OID NUMERIC(12) NOT NULL,
							RUBRO_CONTABLE NUMERIC(12) NOT NULL,
							SALDO_CONTABLE NUMERIC(15,2) NOT NULL,
							POR_PD NUMERIC(8,4) NOT NULL,
							POR_LGD NUMERIC(8,4)NOT NULL,
							PORCENTAJE_PREV NUMERIC(8,4) NOT NULL,
							PREVISION NUMERIC(15,2) NOT NULL,
							ASIENTO NUMERIC(10,0) NOT NULL,
							SUCURSAL NUMERIC(10,0) NOT NULL,
							CONSTRAINT PK_PREVISIONES_HIST PRIMARY KEY (FECHA_PROCESO, SALDOS_JTS_OID)
							)
						"""
		def descripcion = "Se agrega tabla"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
}