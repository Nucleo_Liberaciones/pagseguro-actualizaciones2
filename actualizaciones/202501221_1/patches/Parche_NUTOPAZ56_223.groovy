import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.configuration.CarpetasServidor
import topsystems.actualizador.parche.Parche

class Parche_NUTOPAZ56_223 extends Parche {
	
	void sobreElParche() {
		this.notas_del_autor = "Parche_NUTOPAZ56_223"
		this.opcional = false
	}		
	
	@Override
	protected void comandos() {	
		if(!isApplied_NUSACSICRE257()){
			addProperty()
		}
		
	}	
	
	private void addProperty(){
		// Agregar una propiedad o cambiar el valor a una existente
		def archivo = "jts.properties"
		this.addCommand( new AgregarModificarPropiedad(archivo, "topaz.codigo.transaccion.time.out.commit", "0", "Define el codigo de transaccion para el evento por timeout. Para homogeneizar se utiliza el codigo 99001  (si es cero, no se envia evento)"))
		
		def propiedad = "topaz.service.time.out.aftercommit.return.error.enable"
		this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, "false", "Habilita a que el servicio retorne error una vez enviado el evento de reversa por timeout"))
	}
	
	
	private boolean isApplied_NUSACSICRE257(){		
		Properties prop = loadPropertyFile();		
		return prop.getProperty("topaz.service.time.out.aftercommit.return.error.enable") != null;		
	}

	private Properties loadPropertyFile(){	
		Properties msclProp = new Properties()
		File msclFile = new File(CarpetasServidor.TOPAZ_PROPS.getExpandedPath() + File.separator + "jts.properties");
		grabarLog(String.format("Buscando archivo [%s]", msclFile.getAbsolutePath()))
				
		if(msclFile.exists()){			
			msclProp.load(new FileInputStream(msclFile))
		}
		
		return msclProp
	} 
}
