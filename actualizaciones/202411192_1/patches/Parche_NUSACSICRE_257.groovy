package TOPAZ_56

import topsystems.actualizador.command.archivos.CopiarDesdeZip
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.dataserver.ModificarConsultaQuery
import topsystems.actualizador.command.mbeans.AgregarDependsMBeans
import topsystems.actualizador.command.mbeans.AgregarModificarMBeans
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.properties.BorrarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.CarpetasServidor
import topsystems.actualizador.parche.Parche

class Parche_NUSACSICRE_257 extends Parche {

	
	void sobreElParche() {
		this.notas_del_autor = "Parche_NUSACSICRE_257"
		this.opcional = false
	}	
	
	@Override
	protected void comandos() {	
		changeProperty()
		addProperty()
	}
		
	private void addProperty(){
		// Agregar una propiedad o cambiar el valor a una existente
		def archivo = "jts.properties"		
		this.addCommand( new AgregarModificarPropiedad(archivo, "topaz.codigo.transaccion.time.out.commit", "0", ""))
		
		def propiedad = "topaz.service.time.out.aftercommit.return.error.enable"
		this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, "false", "Habilita a que el servicio retorne error una vez enviado el evento de reversa por timeout"))
	}
	
	private void changeProperty(){
		def archivo = "jts.properties"
		this.addCommand( new AgregarModificarPropiedad(archivo, "topaz.codigo.transaccion.time.out.commit", "0", "Define el codigo de transaccion para el evento por timeout. Para homogeneizar se utiliza el codigo 99001  (si es cero, no se envia evento)"))
	}
	

}
