package Base

import topsystems.actualizador.command.ActualizacionCommand
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.configuration.CarpetasActualizador
import topsystems.actualizador.parche.Parche

class Parche_NUSICREDBR_3618 extends Parche {

	void sobreElParche() {
		this.notas_del_autor = "Parche_NUSICREDBR_3618"
		this.opcional = false
	}

	@Override
	protected void comandos() {
		addProperty()
	}
	private void addProperty(){
		def archivo = "loans.properties"
		def propiedad = "topaz.loans.fecha.inicio.ifrs9"
		def comentario = "Propiedad que indica la Fecha a partir de la cual, se ajusta el comportamiento para renegociados IFRS9"
		def defaultValue = "20250101"
		this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, defaultValue, comentario))
	}
}
