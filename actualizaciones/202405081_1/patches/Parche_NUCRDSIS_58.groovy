import topsystems.TopazException
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.parche.Parche
import topsystems.actualizador.configuration.CarpetasServidor

class Parche_NUCRDSIS_58 extends Parche {

	@Override
	protected void comandos() throws TopazException {
			if(!isSicredi()) {
				actualizarServicioAcreditacionCheques();
			}
	}

	private boolean isSicredi(){
		def jarName = "sic_br_tb";
		def directoryLib = new File(CarpetasServidor.SERVER_LIB.getExpandedPath());
	
		if(directoryLib.exists() && directoryLib.isDirectory()){
			File[] filesJars = directoryLib.listFiles({ File dir, String file ->
				file.startsWith(jarName) && file.endsWith(".jar")
			} as FilenameFilter
			)
			if( filesJars.length > 0 ){
				return true;
			}
		}
		return false;
	}

	private void actualizarServicioAcreditacionCheques() {
		// Agregar o sustituir uno o varios servicios en un mismo archivo
def archivo = "clearing/ClearingDeploy.xml"
def servicios = """
	<service>
		<name>topsystems:processManager:BusinessWorker=BusinessWorkerAcreditacionCheques</name>
		<code>topsystems.automaticprocess.clearing.acreditacionchequesBR.BusinessWorkerAcreditacionChequesBR</code>
	</service>

"""
this.addCommand( new AgregarModificarServicios(archivo, servicios))

	}

}
