package chaco

import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Parche_NUNBCHACO_1610 extends Parche{
	
	void definicionDeParametros(){
		this.addParameterNewFreeField("CAMPO_PUBLICA_PIVOT", "Campo para la tabla CI_IMPUESTOS")
	}

	@Override
	protected void comandos() {
		modificarEntity()
		modificarEstructura()
		insert()
	}
	
	private void modificarEntity(){ 
		def archivoMapp = "ChargesMapping.xml"
		def entidad = "core.vo_Taxes"
		def atributo = """
			<attribute>
				<attribute-name>pivotTaxAmountFieldNumber</attribute-name>
				<column-name>CAMPO_PUBLICA_PIVOT</column-name>
			</attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
	}
	
	private void modificarEstructura(){
		if (esBaseDeDatos("ORACLE")) {
			def sentenciaORA = """
			ALTER TABLE CI_IMPUESTOS 
			ADD CAMPO_PUBLICA_PIVOT NUMBER(5) DEFAULT 0 NOT NULL;
			"""
			this.addCommand( new ActualizarBaseDeDatosOracle(sentenciaORA))
		} else {
			def sentenciaSQL = """
			ALTER TABLE CI_IMPUESTOS 
			ADD CAMPO_PUBLICA_PIVOT NUMERIC(5,0) DEFAULT 0 NOT NULL;
			"""
			this.addCommand( new ActualizarBaseDeDatosSQLServer(sentenciaSQL))
		}
	}
	
	private insert(){
		if (esBaseDeDatos("ORACLE")) {
			def sentenciaORA = """
			INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
			VALUES (${this.P("CAMPO_PUBLICA_PIVOT")}, NULL, 0, 'CAMPO_PUBLICA_PIVOT', 'CAMPO_PUBLICA_PIVOT', 5, 'N', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 196, 'CAMPO_PUBLICA_PIVOT', 0, NULL);
			"""
			def descripcion = "Se crean los inserts necesarios para el proceso"
			this.addCommand( new ActualizarBaseDeDatosOracle(sentenciaORA, descripcion))
		} else {
			def sentenciaSQL = """
			INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA, encript)
			VALUES (${this.P("CAMPO_PUBLICA_PIVOT")}, NULL, 0, 'CAMPO_PUBLICA_PIVOT', 'CAMPO_PUBLICA_PIVOT', 5, 'N', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 196, 'CAMPO_PUBLICA_PIVOT', 0, NULL, NULL);
			"""
			def descripcion = "Se crean los inserts necesarios para el proceso"
			this.addCommand( new ActualizarBaseDeDatosSQLServer(sentenciaSQL, descripcion))
		}
	}

}
