import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.parche.Parche;

class Parche_NUCRDSIS_82 extends Parche {

	void definicionDeParametros(){
		this.addParameterNewFreeField("INTERESIPF", "Número de campo para columna INTERESIPF de la tabla CRE_DEUDA_POR_CUOTA")
		this.addParameterNewFreeField("MORAIPF", "Número de campo para columna MORAIPF de la tabla CRE_DEUDA_POR_CUOTA")
		this.addParameterQuery("numeroDescriptorCRE_DEUDA_POR_CUOTA", "Numero de descriptor tabla CRE_DEUDA_POR_CUOTA", "INTEGER", "SELECT IDENTIFICACION FROM DESCRIPTORES WHERE UPPER(NOMBREFISICO)='CRE_DEUDA_POR_CUOTA'")
	}
	
	@Override
	protected void comandos() {

		//----------------------------------------------------------------------------------------------------------------//
		// Agregar o sustituir uno (o varios) atributos de una entidad de DataMapping
		def archivoMapp = "CreditosMapping.xml"
		def entidad = "core.vo_DeudaPorCuota"
		def atributo = """
	        <attribute>
	            <attribute-name>interesIpf</attribute-name>
	            <column-name>INTERESIPF</column-name>
	        </attribute>
	        <attribute>
	            <attribute-name>moraIpf</attribute-name>
	            <column-name>MORAIPF</column-name>
	        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
		//----------------------------------------------------------------------------------------------------------------//
		
		//----------------------------------------------------------------------------------------------------------------//
		// Actualización de la Base de Datos en Oracle
		def sentencia = """
			ALTER TABLE CRE_DEUDA_POR_CUOTA RENAME COLUMN INTERES_IPF TO INTERESIPF;
			ALTER TABLE CRE_DEUDA_POR_CUOTA RENAME COLUMN MORA_IPF TO MORAIPF;
        """
		def descripcion = "Renombra campos en la tabla CRE_DEUDA_DEVENGADA_POR_CUOTA."
		this.addCommand(new ActualizarBaseDeDatosOracle(sentencia, descripcion))
		//----------------------------------------------------------------------------------------------------------------//
		
		//----------------------------------------------------------------------------------------------------------------//
		// Actualización de la Base de Datos en SQLServer	
		sentencia = """
			exec sp_rename 'CRE_DEUDA_POR_CUOTA.INTERES_IPF', 'INTERESIPF', 'COLUMN';
			exec sp_rename 'CRE_DEUDA_POR_CUOTA.MORA_IPF', 'MORAIPF', 'COLUMN';
        """
		descripcion = "Renombra campos en la tabla CRE_DEUDA_DEVENGADA_POR_CUOTA."
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
		//----------------------------------------------------------------------------------------------------------------//
		
		//----------------------------------------------------------------------------------------------------------------//
		// Actualización de la Base de Datos con sentencias genéricas
		sentencia = """
			DELETE FROM DICCIONARIO WHERE CAMPO='INTERES_IPF' AND TABLA='${this.P("numeroDescriptorCRE_DEUDA_POR_CUOTA")}';
	
			DELETE FROM DICCIONARIO WHERE CAMPO='MORA_IPF' AND TABLA='${this.P("numeroDescriptorCRE_DEUDA_POR_CUOTA")}';

			INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
			VALUES ('${this.P("INTERESIPF")}', ' ', 0, 'InteresIpf', 'InteresIpf', 15, 'N', 2, 'I', 0, 0, 0, 0, 0, 0, 0, '${this.P("numeroDescriptorCRE_DEUDA_POR_CUOTA")}', 'INTERESIPF', 0, NULL);
			
			INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA)
			VALUES ('${this.P("MORAIPF")}', ' ', 0, 'MoraIpf', 'MoraIpf', 15, 'N', 2, 'I', 0, 0, 0, 0, 0, 0, 0, '${this.P("numeroDescriptorCRE_DEUDA_POR_CUOTA")}', 'MORAIPF', 0, NULL);
		"""
		descripcion = "Inserta los campos de CRE_DEUDA_DEVENGADA_POR_CUOTA, con el nuevo nombre en el Diccionario. Si ya existían con el nombre previo, los elimina."
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	
		//----------------------------------------------------------------------------------------------------------------//
	}
}