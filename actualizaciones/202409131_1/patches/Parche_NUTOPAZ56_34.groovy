package TOPAZ_56
import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.app.ConfigurationAppManager
import topsystems.actualizador.exception.HakunaException
import topsystems.actualizador.parche.Parche;

class Parche_NUTOPAZ56_34 extends Parche {
	
	@Override
	void sobreElParche() {
		this.notas_del_autor = "Parche_NUTOPAZ56_34"
		this.opcional = false
	}

	@Override
	protected void comandos() throws TopazException{
		
		addProperty()
		addNewFieldToParametrosMapping()
		addNewColumnInParametrosTable()
	}
	
	private void addProperty(){
		// Agregar una propiedad o cambiar el valor a una existente
		def archivo = "jts.properties" 
		def propiedad = "topaz.prontocambiarfecha.field.enable"
		def valor = "false"
		def comentario = "Habilita el uso del campo prontocambiarfecha en los procesos de marca inicio/fin del dia y tambien en el proceso de cambio de fecha de proceso"
		this.addCommand( new AgregarModificarPropiedad(archivo, propiedad, valor, comentario))

	}
	
	
	private void addNewFieldToParametrosMapping(){
		// Agregar o sustituir uno (o varios) atributos de una entidad de DataMapping
		def archivoMapp = "systemdatamapping.xml"
		def entidad = "topsystems.topaz.server.core.system.Parametros"
		def atributo = """
		<attribute>
			<attribute-name>prontoCambiarFecha</attribute-name>
			<column-name>PRONTOCAMBIARFECHA</column-name>
		</attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
	}
	
	
	private void addNewColumnInParametrosTable(){
		if(this.isOracle()){
			addNewColumnInParametrosTableOracle()
		}else if(this.isSqlServer()){
			addNewColumnInParametrosTableSQLServer()
		}else{
			throw new HakunaException("Database not implemented. Database type "+ ConfigurationAppManager.getInstance().getDbms().getTipo() + " not impemented")
		}
	
	}
	
	private void addNewColumnInParametrosTableOracle(){
		def sentencia = """ALTER TABLE PARAMETROS ADD PRONTOCAMBIARFECHA NUMBER(1,0) DEFAULT(0)"""
		def descripcion = "Add new column PRONTOCAMBIARFECHA in table PARAMETROS"
		this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	}
	
	private void addNewColumnInParametrosTableSQLServer(){
		def sentencia = """ALTER TABLE PARAMETROS ADD PRONTOCAMBIARFECHA NUMERIC(1,0) DEFAULT(0)"""
		def descripcion = "Add new column PRONTOCAMBIARFECHA in table PARAMETROS"
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
	}
}