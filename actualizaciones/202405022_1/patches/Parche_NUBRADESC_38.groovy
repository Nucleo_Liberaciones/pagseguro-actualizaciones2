package Parches

import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.parche.Parche

class Parche_NUBRADESC_38 extends Parche {

	@Override
	protected void comandos() throws TopazException {
	
		modificarQuery();
		addConstantPeriodico();
		addConstantReintento();
	}
	
	protected void modificarQuery(){
		// Agregar o sustituir una query (o varias) de DataMapping
def archivoMapp = "FundsTransferMapping.xml"
def queries = """
<query>
        <class-name>topsystems.automaticprocess.transferenciaperiodicas.TransferenciaPeriodica</class-name>
        <query-name>query.TransferenciaPeriodica</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
     	SELECT 
			JTS_ORIGEN,
			JTS_DESTINO,
			IMPORTE_TRANSFERENCIA,
			FECHA_INICIO,
			PERIODICIDAD,
			FECHA_ULT_TRANSF_PROGRAMADA,
			DETALLE_ULT_PROCESO,
			REINTENTO
	   	FROM 
			VTA_TRANSFERENCIA_PERIODICA
	  	WHERE 
			TZ_LOCK = 0 
			AND (FECHA_ULT_TRANSF_PROGRAMADA IS NULL  OR FECHA_ULT_TRANSF_PROGRAMADA &lt;= (SELECT FECHAPROCESO FROM PARAMETROS) 
			AND FECHA_INICIO &lt;= (SELECT FECHAPROCESO FROM PARAMETROS)
		    AND FECHA_LIMITE &gt; (SELECT FECHAPROCESO FROM PARAMETROS)
		    AND REINTENTO = 0)
		    OR (FECHA_LIMITE &gt;= (SELECT FECHAPROCESO FROM PARAMETROS)
			AND REINTENTO = 1)
			ORDER BY JTS_ORIGEN
		</sentence>
        <attribute>
            <attribute-name>jtsOidCuentaOrigen</attribute-name>
            <column-name>JTS_ORIGEN</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>jtsOidCuentaDestino</attribute-name>
            <column-name>JTS_DESTINO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>importeFijo</attribute-name>
            <column-name>IMPORTE_TRANSFERENCIA</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaAlta</attribute-name>
            <column-name>FECHA_INICIO</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>codigoPeriodicidad</attribute-name>
            <column-name>PERIODICIDAD</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>fecha_ultima_transferencia</attribute-name>
            <column-name>FECHA_ULT_TRANSF_PROGRAMADA</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>codigoReintento</attribute-name>
            <column-name>REINTENTO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>detalle_proceso</attribute-name>
            <column-name>DETALLE_ULT_PROCESO</column-name>
            <column-type>String</column-type>
        </attribute>
    </query>
"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))

	}
	
	
	private void addConstantPeriodico(){
		// Agregar constantes y parametros a un proceso 
		def archivo = "processes.py"
		def nombreProceso = "Transferencias de fondos Reintento"
		def constParamProc = """
		pdef.addConstant("applyCodigoTransaccion","true");
		"""
		this.addCommand( new AgregarModificarPropiedadesProceso(archivo, nombreProceso, constParamProc))
		
	}
	
	private void addConstantReintento(){
		// Agregar constantes y parametros a un proceso
		def archivo = "processes.py"
		def nombreProceso = "Transferencias de fondos Periodica"
		def constParamProc = """
		pdef.addConstant("applyCodigoTransaccion","true");
		"""
		this.addCommand( new AgregarModificarPropiedadesProceso(archivo, nombreProceso, constParamProc))
	}

}
