import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Patch_NUTOPAZ56_52 extends Parche {
	
	void sobreElParche() { // Este m�todo es opcional
		 this.notas_del_autor = ""
		 this.opcional = false
	}
	 
	void definicionDeParametros(){
				
	}

	void comandos() {	
		addAtributeToClassMapping();
		addDataBaseObjects();		
	} 
	
	private void addAtributeToClassMapping(){
		// Agregar o sustituir uno (o varios) atributos de una entidad de DataMapping
		def archivoMapp = "BitacoraWSMapping.xml"
		def entidad = "core.vo_BitacoraWS"
		def atributo = """
		<attribute>
            <attribute-name>serverIp</attribute-name>
            <column-name>SERVERIP</column-name>
        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))		
	}
	
	private void addDataBaseObjects(){
		if(isOracle()){
			oracleScript()
		}else{
			sqlServerScript()
		}
	}
	
	private void oracleScript(){
		def sentencia = """
			ALTER TABLE BITACORA_WS ADD SERVERIP VARCHAR2(40);
		"""
		def descripcion = "Campo para guardar la IP del servidor que atendio el servicio." // La descripci�n es opcional
		this.addCommand( new ActualizarBaseDeDatosOracle(sentencia, descripcion))
	
	}
	
	private void sqlServerScript(){
		def sentencia = """
			ALTER TABLE BITACORA_WS ADD SERVERIP VARCHAR(40);
		"""
		def descripcion = "Campo para guardar la IP del servidor que atendio el servicio." // La descripci�n es opcional
		this.addCommand( new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
	}
}
