package bna
import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.servicios.AgregarModificarParametrosServicio
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.parche.Parche

class Parche_NUBNABO105_46 extends Parche {

	@Override
	protected void comandos() throws TopazException {
		agregarServicios();
		agregarQueryServicios();
	}
	
	private void agregarServicios() {
		def fileName = """interesesVistaSaldoOPromedioDeploy.xml""";
		def serviceName = """topsystems:processManager:BusinessWorker=BWPagoIVSegunPromedioOSaldo""";
		def parametros = """
			<parameter>
            	<name>P</name>
            	<value>topsystems:service:vista:intereses=AlgoritmoPagoSegunPromedioClienteOSaldoNumerales</value>
         	</parameter>
			""";
			this.addCommand(new AgregarModificarParametrosServicio(fileName, serviceName, parametros));
		
		def servicios ="""
			<service>
				<name>topsystems:service:vista:intereses=AlgoritmoPagoSegunPromedioClienteOSaldoNumerales</name>
				<code>topsystems.automaticprocess.interesesvistasaldoopromedio.algoritmos.AlgoritmoPagoSegunPromedioClienteOSaldoNumerales</code>
				<parameters>
				   <parameter>
					  <name>QUERY_NAME</name>
					  <value>query.NumeralesAlgoritmosPagoCliente</value>
				   </parameter>
				</parameters>
			</service>
			""";
		this.addCommand( new AgregarModificarServicios(fileName, servicios));
	}
	
	private void agregarQueryServicios() {
		def fileName = """interesesVistaSalodOPromedioMapping.xml""";
        def query = """
			<query>
				<class-name>topsystems.accounting.objectsdomain.SaldoDiarioExtend</class-name>
				<query-name>query.NumeralesAlgoritmosPagoCliente</query-name>
				<database-name>TOP/CLIENTES</database-name>
				<sentence>
				SELECT SD.FECHA,
				       SD.SALDO_CON_FECHA_VALOR_PAGO,
				       SD.SALDOS_JTS_OID
				FROM GRL_SALDOS_DIARIOS SD
				JOIN SALDOS S ON SD.SALDOS_JTS_OID = S.JTS_OID
				WHERE SD.FECHA BETWEEN ? AND ?
				      AND S.C1803 = ?
				      AND S.C1785 = ?
				      AND S.MONEDA = ?
				      AND S.TZ_LOCK = 0
				      AND S.C1651 = ' '
				ORDER BY SD.SALDOS_JTS_OID, SD.FECHA ASC
				</sentence>
				<attribute>
					<attribute-name>fecha</attribute-name>
					<column-name>FECHA</column-name>
					<column-type>Timestamp</column-type>
				</attribute>
				<attribute>
					<attribute-name>saldo_fecha_valor_pago</attribute-name>
					<column-name>SALDO_CON_FECHA_VALOR_PAGO</column-name>
					<column-type>double</column-type>
				</attribute>
				<attribute>
					<attribute-name>saldoJtsOid</attribute-name>
					<column-name>SALDOS_JTS_OID</column-name>
					<column-type>double</column-type>
				</attribute>
			</query>
			""";
		this.addCommand( new AgregarModificarQuery(fileName, query))
	}		
}
