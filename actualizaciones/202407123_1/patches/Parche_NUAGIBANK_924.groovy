import topsystems.TopazException
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.parche.Parche
import topsystems.actualizador.configuration.CarpetasServidor

class Parche_NUAGIBANK_924 extends Parche {
	
	boolean isAgibank=false

	@Override
	protected void comandos() throws TopazException {
		isAgibank = this.isAgibank();
		if (isAgibank){
		addConstante();
		}else{
		addConstante2();
		}
		
		
	}
	
	void definicionDeParametros(){
		isAgibank = this.isAgibank();
		if (isAgibank){
		this.addParameterQuery("TIPO_DOC_BENEFICIARIO", "Campo de la tabla SCL_DETALLE_CREDITO", "INTEGER", "SELECT NUMERODECAMPO FROM DICCIONARIO WHERE UPPER(CAMPO)='TIPO_DOC_BENEFICIARIO' AND TABLA = (SELECT IDENTIFICACION  FROM DESCRIPTORES D WHERE UPPER(D.NOMBREFISICO) = 'SCL_DETALLE_CREDITOS')")
		this.addParameterQuery("DOCUMENTO_BENEFICIARIO", "Campo de la tabla SCL_DETALLE_CREDITO", "INTEGER", "SELECT NUMERODECAMPO FROM DICCIONARIO WHERE UPPER(CAMPO)='DOCUMENTO_BENEFICIARIO' AND TABLA = (SELECT IDENTIFICACION  FROM DESCRIPTORES D WHERE UPPER(D.NOMBREFISICO) = 'SCL_DETALLE_CREDITOS')")
		}
	}
	
	protected addConstante(){
		// Agregar constantes y parametros a un proceso
		def archivo = "processes.py"
		def nombreProceso = "Actualizar Agenda Prepago"
		def constParamProc = """
		pdef.addConstant("tipoConta","CONTA_CORRENTE_PF");
		pdef.addConstant("atributosDetalleCreditoParaEvento","${this.P('TIPO_DOC_BENEFICIARIO')}:tipoDocBeneficiario;${this.P('DOCUMENTO_BENEFICIARIO')}:documBeneficiario"); 
		"""
		this.addCommand( new AgregarModificarPropiedadesProceso(archivo, nombreProceso, constParamProc))
	}
	
	protected addConstante2(){
		// Agregar constantes y parametros a un proceso
		def archivo = "processes.py"
		def nombreProceso = "Actualizar Agenda Prepago"
		def constParamProc = """
		pdef.addConstant("tipoConta","");
		pdef.addConstant("atributosDetalleCreditoParaEvento",""); 
		"""
		this.addCommand( new AgregarModificarPropiedadesProceso(archivo, nombreProceso, constParamProc))
	}
	
	private boolean isAgibank(){
		def jarName = "agb_br_tb";
		def directoryLib = new File(CarpetasServidor.SERVER_LIB.getExpandedPath());
	
		if(directoryLib.exists() && directoryLib.isDirectory()){
			File[] filesJars = directoryLib.listFiles({ File dir, String file ->
				file.startsWith(jarName) && file.endsWith(".jar")
			} as FilenameFilter
			)
			if( filesJars.length > 0 ){
				return true;
			}
		}
		return false;
	}
}

