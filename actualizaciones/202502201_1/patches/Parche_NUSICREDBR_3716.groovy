package sicredi

import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoQuery
import topsystems.actualizador.command.dataserver.ModificarConsultaQuery
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Parche_NUSICREDBR_3716 extends Parche{

	@Override
	protected void comandos() throws TopazException {
		newDataBaseFields()
		modifyMappingQuery()
		addNewAttributeToClass()
	}
	
	
	private void addNewAttributeToClass(){
		def archivoMapp = "categorizacionPorCuotaMapping.xml"
		def entidad = "core.vo_CategoriaCartera"
		def atributo = """
	        <attribute>
	            <attribute-name>comportamiento</attribute-name>
	            <column-name>COMPORTAMIENTO</column-name>
	        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
		atributo = """
	        <attribute>
	            <attribute-name>consideraFinDeMes</attribute-name>
	            <column-name>CONSIDERA_FIN_DE_MES</column-name>
	        </attribute>
		"""
		this.addCommand( new AgregarModificarAtributoEntidad(archivoMapp, entidad, atributo))
	}

	private void modifyMappingQuery(){
		def archivoMapp = "categorizacionPorCuotaMapping.xml"
		def queryName = "query.vo_categoriasCartera"
		def query = """
						SELECT cat.codigo_atraso_destino     	  AS codigoAtrasoDestino,
						       cat.incumplimientos            	  AS limiteCantidadIncumplimientos,
						       cat.rubro_origen               	  AS rubroOrigen,
						       cat.rubro_destino              	  AS rubroDestino,
						       cat.moneda                     	  AS moneda,
						       cat.producto_destino           	  AS productoDestino,
						       cat.tipo_categorizacion_auto   	  AS tipoCategorizacionAutomatica,
						       cat.tipo_categorizacion_manual 	  AS tipoCategorizacionForzada,
						       cod.nivel_atraso               	  AS nivelAtraso,
						       cat.periodo_generacion_cuota       AS periodoGeneracionCuota,
						       cat.criterio                       AS criterio,
							   cat.CAMPO_PARA_CALC_ATRASO_CATEG   AS campoParaCalcAtrasoCateg,
							   cat.TIPO_CALENDARIO                AS tipoCalendario,
							   cat.COMPORTAMIENTO				  AS COMPORTAMIENTO,
							   cat.CONSIDERA_FIN_DE_MES			  AS CONSIDERA_FIN_DE_MES
						FROM   cr_categorias_cartera     cat,
						       cr_codigos_atraso_cartera cod
						WHERE  cat.codigo_atraso_destino = cod.cod_atraso
		"""
		def queryAttribute = """
	        <attribute>
	            <attribute-name>comportamiento</attribute-name>
	            <column-name>COMPORTAMIENTO</column-name>
				<column-type>String</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>consideraFinDeMes</attribute-name>
	            <column-name>CONSIDERA_FIN_DE_MES</column-name>
				<column-type>String</column-type>
	        </attribute>
		"""
		this.addCommand( new ModificarConsultaQuery(archivoMapp, queryName, query))
		this.addCommand( new AgregarModificarAtributoQuery(archivoMapp, queryName, queryAttribute));
	}

	protected void newDataBaseFields(){
		def descripcion = "Agregando campos COMPORTAMIENTO y CONSIDERA_FIN_DE_MES en tabla CR_CATEGORIAS_CARTERA"
		def baseOra = esBaseDeDatos("ORACLE")
		if(baseOra){
			def sentencia = """
				ALTER TABLE CR_CATEGORIAS_CARTERA ADD COMPORTAMIENTO VARCHAR2 (1) DEFAULT ('P');
				ALTER TABLE CR_CATEGORIAS_CARTERA ADD CONSIDERA_FIN_DE_MES VARCHAR2 (1) DEFAULT ('N');
			"""
			this.addCommand(new ActualizarBaseDeDatosOracle(sentencia, descripcion))
		}else{
			def sentencia = """
				ALTER TABLE CR_CATEGORIAS_CARTERA
					ADD COMPORTAMIENTO VARCHAR(1) DEFAULT 'P',
					    CONSIDERA_FIN_DE_MES VARCHAR(1) DEFAULT 'N';
				GO;					
				UPDATE CR_CATEGORIAS_CARTERA SET COMPORTAMIENTO = 'P', CONSIDERA_FIN_DE_MES = 'N';
			"""
			this.addCommand(new ActualizarBaseDeDatosSQLServer(sentencia, descripcion))
		}
	}
	
		
}
