import topsystems.TopazException
import topsystems.actualizador.command.dataserver.ModificarConsultaQuery
import topsystems.actualizador.parche.Parche

class Parche_NUBRADESC_45 extends Parche{

	@Override
	protected void comandos() throws TopazException {
		newQuery();
		
	}
	
	protected void newQuery(){
		// Sustituir consulta SQL de query de DataMapping
def archivoMapp = "FundsTransferMapping.xml"
def query = "query.TransferenciaPeriodica"
def consultaSQL = """
		SELECT 
			JTS_ORIGEN,
			JTS_DESTINO,
			IMPORTE_TRANSFERENCIA,
			FECHA_INICIO,
			PERIODICIDAD,
			FECHA_ULT_TRANSF_PROGRAMADA,
			DETALLE_ULT_PROCESO,
			REINTENTO
	   	FROM 
			VTA_TRANSFERENCIA_PERIODICA
	  	WHERE 
			TZ_LOCK = 0 
			AND ((FECHA_ULT_TRANSF_PROGRAMADA IS NULL  OR FECHA_ULT_TRANSF_PROGRAMADA &lt;= (SELECT FECHAPROCESO FROM PARAMETROS)) 
			AND FECHA_INICIO &lt;= (SELECT FECHAPROCESO FROM PARAMETROS)
		    AND FECHA_LIMITE &gt; (SELECT FECHAPROCESO FROM PARAMETROS)
		    AND REINTENTO = 0)
		    OR (FECHA_LIMITE &gt;= (SELECT FECHAPROCESO FROM PARAMETROS)
			AND REINTENTO = 1)
			ORDER BY JTS_ORIGEN
"""
this.addCommand( new ModificarConsultaQuery(archivoMapp, query, consultaSQL))
	}

}
