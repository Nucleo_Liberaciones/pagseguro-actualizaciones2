package TOPAZ_56

import java.io.File

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory

import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.transform.OutputKeys
import javax.xml.transform.Transformer
import javax.xml.transform.TransformerFactory
import javax.xml.transform.dom.DOMSource
import javax.xml.transform.stream.StreamResult

import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList

import topsystems.actualizador.command.ActualizacionCommand
import topsystems.actualizador.command.ActualizacionCommandResult
import topsystems.actualizador.command.archivos.CopiarDesdeZip
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.dataserver.ModificarConsultaQuery
import topsystems.actualizador.command.groovy.ComandoGroovy
import topsystems.actualizador.command.mbeans.AgregarDependsMBeans
import topsystems.actualizador.command.mbeans.AgregarModificarMBeans
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.properties.BorrarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.CarpetasServidor
import topsystems.actualizador.exception.HakunaException
import topsystems.actualizador.parche.Parche
import topsystems.actualizador.simulation.Simulacion
import topsystems.actualizador.undo.UndoManager
import topsystems.actualizador.util.FileUtil
import topsystems.actualizador.util.PathUtil
import topsystems.actualizador.util.zip.ZipManager

class Parche_NUNBCHACO_1775 extends Parche {

	
	void sobreElParche() {
		this.notas_del_autor = "Parche_NUNBCHACO_1775"
		this.opcional = false
	}	
	
	@Override
	protected void comandos() {	
		createModule()
		addModule()
	}
		
	private void createModule(){
		def title = "Creando directorio para el modulo topaz_commons_imaging"
		def summary = "Creando directorio para el modulo topaz_commons_imaging"
		def modulePath = CarpetasServidor.APP_HOME.getExpandedPath()+"/modules/topaz_commons_imaging/main/"
		
		def ejecutar = { ComandoGroovy comando ->
			File file = new File(modulePath)
			file.mkdirs()
		}
		
		def simular = {
			return "Creando directorio "+modulePath+"  para el modulo topaz_commons_imaging"
		}
		
		this.addCommand(new ComandoGroovy(title, summary, ejecutar, simular))
		
		
		this.addCommand( new CopiarDesdeZip("commons-imaging-1.0.0-alpha5.jar", modulePath))
		this.addCommand( new CopiarDesdeZip("commons-io-2.18.0.jar", modulePath))
		this.addCommand( new CopiarDesdeZip("module.xml", modulePath))
	}
	
	private void addModule(){
		this.addCommand(new AddModuleTopazCommand("topaz_commons_imaging"))
	}
	
	public class AddModuleTopazCommand extends ActualizacionCommand {
		
		private static Log log = LogFactory.getLog(AddModuleTopazCommand.class)
		private final String moduleName
		private final  filePath = CarpetasServidor.TOPAZ_HOME.getExpandedPath() + File.separator + "META-INF" + File.separator + "jboss-deployment-structure.xml"
	
		public AddModuleTopazCommand(String moduleName) {
			this.moduleName = moduleName;
		}
	
		@Override
		public String getSummary() {
			return String.format("Modificando archivo [%s]", filePath);
		}
	
		@Override
		public ActualizacionCommandResult execute() {
			ActualizacionCommandResult result = new ActualizacionCommandResult();
			
			File file = new File(filePath)
			if(!file.exists()){
				throw new HakunaException(String.format("No se encontro el archivo [%s]", filePath));
			}
						
			try {
				
				UndoManager.instance.registerUpdateAndBackUpFile(this, filePath);
				
				Document doc = loadFile(file);
				
				NodeList dependencies = doc.getElementsByTagName("dependencies");
				if (dependencies.getLength() > 0) {
					Node dependenciesNode = null;
					
					for (int i = 0; i < dependencies.getLength(); i++) {
						Node node = dependencies.item(i);
						if (node instanceof Element) {
							dependenciesNode = node;
							break;
						}
					}
					
					if(dependenciesNode != null){
						Element newModule = doc.createElement("module");
						newModule.setAttribute("name", moduleName);
						dependenciesNode.appendChild(newModule);
						
						writeFile(doc, file);
					}
				}
								
			} catch (Throwable e) {
				String error = String.format("Se produjo un error al ejecutar el comando AddModuleTopazCommand. Archivo: '%s' Error: '%s'",
						filePath, e.getMessage());
				log.error(error, e);
				return new ActualizacionCommandResult(error);
			}
			
			return result;
		}
	
		@Override
		public ActualizacionCommandResult simulation() {
			try {
				
				String textoSimulacion = String.format("Agregar el modulo topaz_commons_imaging al archivo '%s'.", filePath);
				Simulacion.getInstance().addTexto(this, textoSimulacion);		
				return new ActualizacionCommandResult();
		
			} catch (Throwable e) {
				String error = String.format("Se produjo un error durante la simulación del comando AddModuleTopazCommand.Error: %s", e.getMessage());
				log.error(error, e);
				return new ActualizacionCommandResult(error);
			}
		}
	
		@Override
		public String getTitulo() {
			return "Agregando modulo nuevo " + moduleName;
		}
				
		private Document loadFile(File file){
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = builderFactory.newDocumentBuilder();
			return builder.parse(file);
		}
		
		private void writeFile(Document doc, File file){
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			
			DOMSource source = new DOMSource(doc);
			transformer.transform(source, new StreamResult(new FileOutputStream(file)));
		}
	}	
}
