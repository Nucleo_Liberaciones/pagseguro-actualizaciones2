import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.parche.Parche;

class Parche_NUSICREDBR_3402 extends Parche{

	void definicionDeParametros(){
	}

	@Override
	protected void comandos() {

		//----------------------------------------------------------------------------------------------------------------//
		// Agregar o sustituir uno o varios servicios en un mismo archivo
		def archivo = "DevengamientoDeploy.xml"
		def servicios = """
		    <service>
		        <name>topsystems:processManager:WorkDescriptor=WorkDescriptorCalculoDevengamiento</name>
		        <code>topsystems.automaticprocess.devengamiento.calculo.WorkDescriptorCalculoDevengamientoBankBr</code>
		        <code-instruction/>
		        <movement-filler/>
	       </service>
		"""
		this.addCommand( new AgregarModificarServicios(archivo, servicios))
		//----------------------------------------------------------------------------------------------------------------//
	}

}