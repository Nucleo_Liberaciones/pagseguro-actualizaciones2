import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.dataserver.ModificarConsultaQuery
import topsystems.actualizador.parche.Parche
import topsystems.actualizador.configuration.CarpetasServidor

class Parche_NUSICREDBR_3351 extends Parche{

	private isAccounting_br = false;
	private isAccounting_co = false;


	void definicionDeParametros(){
		
	}

	@Override
	protected void comandos() throws TopazException {
		isAccounting_br = this.isAccountingBr();
		isAccounting_co = this.isAccountingCo();
		if(isAccounting_br){
			adicionarDatamappingBR();
			modificarQueryJoinVistaPlazoBR();
		} 
		else if (!isAccounting_co){
			adicionarDatamappingBase();
			modificarQueryJoinVistaPlazoLineaBase();		
		}
		
	
		
		
	}
	
	private adicionarDatamappingBR(){
		def archivoMapp = "HistoricoMapping.xml"
def queries = """
	<query>
         <class-name>topsystems.automaticprocess.historicocliente.valueobjects.QueryJoinVistaPlazo</class-name>
        <query-name>query.QueryJoinVistaPlazoTodos</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
		SELECT SALDO_JTS_OID, SIGNO, FECHA_VALOR, MONTO, AJUSTE, MONTOMN,
				       FECHAPROCESO,
				       ASIENTO,
				       SUCURSAL,
				       TRREAL,
				       ORDINAL,
				       TRVIRTUAL,
				       TRTIPO,
				       RUBRO,
				       EQRESULTADOME,
				       MONEDA,
				       SUCURSAL_SALDO,
					   EMPRESA,
	   				   SUCURSAL_EMPRESA								   
				FROM (			
				SELECT M.SALDO_JTS_OID, M.DEBITOCREDITO AS SIGNO,
				       M.FECHAVALOR AS FECHA_VALOR,
				       M.CAPITALREALIZADO AS MONTO,
				       M.MARCAAJUSTE AS AJUSTE,
				       M.EQUIVALENTEMN AS MONTOMN,
				       M.FECHAPROCESO,
				       M.ASIENTO,
				       M.SUCURSAL,
				       M.TRREAL,
				       M.ORDINAL,
				       M.TRVIRTUAL,
				       M.TRTIPO,	
				       M.RUBROCONTABLE AS RUBRO,
				       M.EQUIVALENTEME AS EQRESULTADOME,
				       M.MONEDA,
				       M.SUCURSAL_CUENTA AS SUCURSAL_SALDO,
	   				   M.EMPRESA,
	   				   M.SUCURSAL_EMPRESA_CUENTA AS SUCURSAL_EMPRESA							   
				  FROM MOVIMIENTOS_CONTABLES M,
				       ASIENTOS A
				 WHERE M.FECHAPROCESO = A.FECHAPROCESO
				   AND M.FECHAPROCESO &lt;= ?
				   AND M.SUCURSAL = A.SUCURSAL
				   AND M.ASIENTO = A.ASIENTO
				   AND A.ESTADO = 77
				   AND M.DEBITOCREDITO IN ('C','D')
				   AND M.FECHAVALOR &lt; M.FECHAPROCESO 
				   AND ESTADOFECHAVALOR = 0		
			UNION ALL   
				SELECT M.SALDO_JTS_OID, M.DEBITOCREDITO AS SIGNO,
				       M.FECHAVALOR AS FECHA_VALOR,
				       M.CAPITALREALIZADO *-1 AS MONTO ,
				       M.MARCAAJUSTE AS AJUSTE,
				       M.EQUIVALENTEMN *-1  AS MONTOMN,
				       M.FECHAPROCESO,
				       M.ASIENTO,
				       M.SUCURSAL,
				       M.TRREAL,
				       M.ORDINAL,
				       M.TRVIRTUAL,
				       M.TRTIPO,
				       M.RUBROCONTABLE AS RUBRO,
				       M.EQUIVALENTEME AS EQRESULTADOME,
				       M.MONEDA,
				       M.SUCURSAL_CUENTA AS SUCURSAL_SALDO,
					   M.EMPRESA,
	   				   M.SUCURSAL_EMPRESA_CUENTA AS SUCURSAL_EMPRESA
				  FROM MOVIMIENTOS_CONTABLES M,
				       ASIENTOS A
				 WHERE M.FECHAPROCESO = A.FECHAPROCESO
				       AND M.FECHAPROCESO &lt;= ?
				       AND M.SUCURSAL = A.SUCURSAL
				       AND M.ASIENTO = A.ASIENTO
				       AND A.ESTADO = 42
				       AND M.DEBITOCREDITO IN ('C','D')
				       AND M.FECHAVALOR &lt; M.FECHAPROCESO 
				       AND M.ESTADOFECHAVALOR = 1 ) T 			         
		 ORDER BY SALDO_JTS_OID, FECHAPROCESO
		</sentence>
        <attribute>
            <attribute-name>saldo_jts_oid</attribute-name>
            <column-name>SALDO_JTS_OID</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>signo</attribute-name>
            <column-name>SIGNO</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>fecha_valor</attribute-name>
            <column-name>FECHA_VALOR</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>monto</attribute-name>
            <column-name>MONTO</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>ajuste</attribute-name>
            <column-name>AJUSTE</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>montoMN</attribute-name>
            <column-name>MONTOMN</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>fecha_procesado</attribute-name>
            <column-name>FECHAPROCESO</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>asiento</attribute-name>
            <column-name>ASIENTO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>sucursal</attribute-name>
            <column-name>SUCURSAL</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>trReal</attribute-name>
            <column-name>TRREAL</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>ordinal</attribute-name>
            <column-name>ORDINAL</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>trVirtual</attribute-name>
            <column-name>TRVIRTUAL</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>trTipo</attribute-name>
            <column-name>TRTIPO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>rubro</attribute-name>
            <column-name>RUBRO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>eqResultadoME</attribute-name>
            <column-name>EQRESULTADOME</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>moneda</attribute-name>
            <column-name>MONEDA</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>sucursalSaldo</attribute-name>
            <column-name>SUCURSAL_SALDO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>empresa</attribute-name>
            <column-name>EMPRESA</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>sucursalEmpresa</attribute-name>
            <column-name>SUCURSAL_EMPRESA</column-name>
            <column-type>String</column-type>
        </attribute>
    </query>
"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))

	}
	
	private modificarQueryJoinVistaPlazoBR(){
// Sustituir consulta SQL de query de DataMapping
def archivoMapp = "HistoricoMapping.xml"
def query = "query.QueryJoinVistaPlazo"
def consultaSQL = """
SELECT SALDO_JTS_OID, SIGNO, FECHA_VALOR, MONTO, AJUSTE, MONTOMN,
				       FECHAPROCESO,
				       ASIENTO,
				       SUCURSAL,
				       TRREAL,
				       ORDINAL,
				       TRVIRTUAL,
				       TRTIPO,
				       RUBRO,
				       EQRESULTADOME,
				       MONEDA,
				       SUCURSAL_SALDO,
					   EMPRESA,
	   				   SUCURSAL_EMPRESA								   
				FROM (			
				SELECT M.SALDO_JTS_OID, M.DEBITOCREDITO AS SIGNO,
				       M.FECHAVALOR AS FECHA_VALOR,
				       M.CAPITALREALIZADO AS MONTO,
				       M.MARCAAJUSTE AS AJUSTE,
				       M.EQUIVALENTEMN AS MONTOMN,
				       M.FECHAPROCESO,
				       M.ASIENTO,
				       M.SUCURSAL,
				       M.TRREAL,
				       M.ORDINAL,
				       M.TRVIRTUAL,
				       M.TRTIPO,	
				       M.RUBROCONTABLE AS RUBRO,
				       M.EQUIVALENTEME AS EQRESULTADOME,
				       M.MONEDA,
				       M.SUCURSAL_CUENTA AS SUCURSAL_SALDO,
	   				   M.EMPRESA,
	   				   M.SUCURSAL_EMPRESA_CUENTA AS SUCURSAL_EMPRESA							   
				  FROM MOVIMIENTOS_CONTABLES M,
				       ASIENTOS A
				 WHERE M.FECHAPROCESO = A.FECHAPROCESO
				   AND M.FECHAPROCESO = ? 
				   AND M.SUCURSAL = A.SUCURSAL
				   AND M.ASIENTO = A.ASIENTO
				   AND A.ESTADO = 77
				   AND M.DEBITOCREDITO IN ('C','D')
				   AND M.FECHAVALOR &lt; M.FECHAPROCESO 
				   AND ESTADOFECHAVALOR = 0		
			UNION ALL   
				SELECT M.SALDO_JTS_OID, M.DEBITOCREDITO AS SIGNO,
				       M.FECHAVALOR AS FECHA_VALOR,
				       M.CAPITALREALIZADO *-1 AS MONTO ,
				       M.MARCAAJUSTE AS AJUSTE,
				       M.EQUIVALENTEMN *-1  AS MONTOMN,
				       M.FECHAPROCESO,
				       M.ASIENTO,
				       M.SUCURSAL,
				       M.TRREAL,
				       M.ORDINAL,
				       M.TRVIRTUAL,
				       M.TRTIPO,
				       M.RUBROCONTABLE AS RUBRO,
				       M.EQUIVALENTEME AS EQRESULTADOME,
				       M.MONEDA,
				       M.SUCURSAL_CUENTA AS SUCURSAL_SALDO,
					   M.EMPRESA,
	   				   M.SUCURSAL_EMPRESA_CUENTA AS SUCURSAL_EMPRESA
				  FROM MOVIMIENTOS_CONTABLES M,
				       ASIENTOS A
				 WHERE M.FECHAPROCESO = A.FECHAPROCESO
				       AND M.FECHAPROCESO = ?
				       AND M.SUCURSAL = A.SUCURSAL
				       AND M.ASIENTO = A.ASIENTO
				       AND A.ESTADO = 42
				       AND M.DEBITOCREDITO IN ('C','D')
				       AND M.FECHAVALOR &lt; M.FECHAPROCESO 
				       AND M.ESTADOFECHAVALOR = 1 ) T 			         
		 ORDER BY SALDO_JTS_OID, FECHAPROCESO
"""
this.addCommand( new ModificarConsultaQuery(archivoMapp, query, consultaSQL))

	}
	
	
	
	
	
	
	private adicionarDatamappingBase(){
		def archivoMapp = "HistoricoMapping.xml"
def queries = """
	<query>
         <class-name>topsystems.automaticprocess.historicocliente.valueobjects.QueryJoinVistaPlazo</class-name>
        <query-name>query.QueryJoinVistaPlazoTodos</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>
		SELECT SALDO_JTS_OID, SIGNO, FECHA_VALOR, MONTO, AJUSTE, MONTOMN,
				       FECHAPROCESO,
				       ASIENTO,
				       SUCURSAL,
				       TRREAL,
				       ORDINAL,
				       TRVIRTUAL,
				       TRTIPO,
				       RUBRO,
				       EQRESULTADOME,
				       MONEDA,
				       SUCURSAL_SALDO							   
				FROM (			
				SELECT M.SALDO_JTS_OID, M.DEBITOCREDITO AS SIGNO,
				       M.FECHAVALOR AS FECHA_VALOR,
				       M.CAPITALREALIZADO AS MONTO,
				       M.MARCAAJUSTE AS AJUSTE,
				       M.EQUIVALENTEMN AS MONTOMN,
				       M.FECHAPROCESO,
				       M.ASIENTO,
				       M.SUCURSAL,
				       M.TRREAL,
				       M.ORDINAL,
				       M.TRVIRTUAL,
				       M.TRTIPO,	
				       M.RUBROCONTABLE AS RUBRO,
				       M.EQUIVALENTEME AS EQRESULTADOME,
				       M.MONEDA,
				       M.SUCURSAL_CUENTA AS SUCURSAL_SALDO							   
				  FROM MOVIMIENTOS_CONTABLES M,
				       ASIENTOS A
				 WHERE M.FECHAPROCESO = A.FECHAPROCESO
				   AND M.FECHAPROCESO &lt; = ? 
				   AND M.SUCURSAL = A.SUCURSAL
				   AND M.ASIENTO = A.ASIENTO
				   AND A.ESTADO = 77
				   AND M.DEBITOCREDITO IN ('C','D')
				   AND M.FECHAVALOR &lt; M.FECHAPROCESO 
				   AND ESTADOFECHAVALOR = 0		
			UNION ALL   
				SELECT M.SALDO_JTS_OID, M.DEBITOCREDITO AS SIGNO,
				       M.FECHAVALOR AS FECHA_VALOR,
				       M.CAPITALREALIZADO *-1 AS MONTO ,
				       M.MARCAAJUSTE AS AJUSTE,
				       M.EQUIVALENTEMN *-1  AS MONTOMN,
				       M.FECHAPROCESO,
				       M.ASIENTO,
				       M.SUCURSAL,
				       M.TRREAL,
				       M.ORDINAL,
				       M.TRVIRTUAL,
				       M.TRTIPO,
				       M.RUBROCONTABLE AS RUBRO,
				       M.EQUIVALENTEME AS EQRESULTADOME,
				       M.MONEDA,
				       M.SUCURSAL_CUENTA AS SUCURSAL_SALDO
				  FROM MOVIMIENTOS_CONTABLES M,
				       ASIENTOS A
				 WHERE M.FECHAPROCESO = A.FECHAPROCESO
				       AND M.FECHAPROCESO &lt;= ?
				       AND M.SUCURSAL = A.SUCURSAL
				       AND M.ASIENTO = A.ASIENTO
				       AND A.ESTADO = 42
				       AND M.DEBITOCREDITO IN ('C','D')
				       AND M.FECHAVALOR &lt; M.FECHAPROCESO 
				       AND M.ESTADOFECHAVALOR = 1 ) T 			         
		 ORDER BY SALDO_JTS_OID, FECHAPROCESO
		</sentence>
        <attribute>
            <attribute-name>saldo_jts_oid</attribute-name>
            <column-name>SALDO_JTS_OID</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>signo</attribute-name>
            <column-name>SIGNO</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>fecha_valor</attribute-name>
            <column-name>FECHA_VALOR</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>monto</attribute-name>
            <column-name>MONTO</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>ajuste</attribute-name>
            <column-name>AJUSTE</column-name>
            <column-type>String</column-type>
        </attribute>
        <attribute>
            <attribute-name>montoMN</attribute-name>
            <column-name>MONTOMN</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>fecha_procesado</attribute-name>
            <column-name>FECHAPROCESO</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>asiento</attribute-name>
            <column-name>ASIENTO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>sucursal</attribute-name>
            <column-name>SUCURSAL</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>trReal</attribute-name>
            <column-name>TRREAL</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>ordinal</attribute-name>
            <column-name>ORDINAL</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>trVirtual</attribute-name>
            <column-name>TRVIRTUAL</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>trTipo</attribute-name>
            <column-name>TRTIPO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>rubro</attribute-name>
            <column-name>RUBRO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>eqResultadoME</attribute-name>
            <column-name>EQRESULTADOME</column-name>
            <column-type>BigDecimal</column-type>
        </attribute>
        <attribute>
            <attribute-name>moneda</attribute-name>
            <column-name>MONEDA</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>sucursalSaldo</attribute-name>
            <column-name>SUCURSAL_SALDO</column-name>
            <column-type>long</column-type>
        </attribute>
    </query>
"""
this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
		
	}
	
	private modificarQueryJoinVistaPlazoLineaBase(){
		// Sustituir consulta SQL de query de DataMapping
def archivoMapp = "HistoricoMapping.xml"
def query = "query.QueryJoinVistaPlazo"
def consultaSQL = """
SELECT SALDO_JTS_OID, SIGNO, FECHA_VALOR, MONTO, AJUSTE, MONTOMN,
				       FECHAPROCESO,
				       ASIENTO,
				       SUCURSAL,
				       TRREAL,
				       ORDINAL,
				       TRVIRTUAL,
				       TRTIPO,
				       RUBRO,
				       EQRESULTADOME,
				       MONEDA,
				       SUCURSAL_SALDO							   
				FROM (			
				SELECT M.SALDO_JTS_OID, M.DEBITOCREDITO AS SIGNO,
				       M.FECHAVALOR AS FECHA_VALOR,
				       M.CAPITALREALIZADO AS MONTO,
				       M.MARCAAJUSTE AS AJUSTE,
				       M.EQUIVALENTEMN AS MONTOMN,
				       M.FECHAPROCESO,
				       M.ASIENTO,
				       M.SUCURSAL,
				       M.TRREAL,
				       M.ORDINAL,
				       M.TRVIRTUAL,
				       M.TRTIPO,	
				       M.RUBROCONTABLE AS RUBRO,
				       M.EQUIVALENTEME AS EQRESULTADOME,
				       M.MONEDA,
				       M.SUCURSAL_CUENTA AS SUCURSAL_SALDO							   
				  FROM MOVIMIENTOS_CONTABLES M,
				       ASIENTOS A
				 WHERE M.FECHAPROCESO = A.FECHAPROCESO
				   AND M.FECHAPROCESO  = ? 
				   AND M.SUCURSAL = A.SUCURSAL
				   AND M.ASIENTO = A.ASIENTO
				   AND A.ESTADO = 77
				   AND M.DEBITOCREDITO IN ('C','D')
				   AND M.FECHAVALOR &lt; M.FECHAPROCESO 
				   AND ESTADOFECHAVALOR = 0		
			UNION ALL   
				SELECT M.SALDO_JTS_OID, M.DEBITOCREDITO AS SIGNO,
				       M.FECHAVALOR AS FECHA_VALOR,
				       M.CAPITALREALIZADO *-1 AS MONTO ,
				       M.MARCAAJUSTE AS AJUSTE,
				       M.EQUIVALENTEMN *-1  AS MONTOMN,
				       M.FECHAPROCESO,
				       M.ASIENTO,
				       M.SUCURSAL,
				       M.TRREAL,
				       M.ORDINAL,
				       M.TRVIRTUAL,
				       M.TRTIPO,
				       M.RUBROCONTABLE AS RUBRO,
				       M.EQUIVALENTEME AS EQRESULTADOME,
				       M.MONEDA,
				       M.SUCURSAL_CUENTA AS SUCURSAL_SALDO
				  FROM MOVIMIENTOS_CONTABLES M,
				       ASIENTOS A
				 WHERE M.FECHAPROCESO = A.FECHAPROCESO
				       AND M.FECHAPROCESO = ?
				       AND M.SUCURSAL = A.SUCURSAL
				       AND M.ASIENTO = A.ASIENTO
				       AND A.ESTADO = 42
				       AND M.DEBITOCREDITO IN ('C','D')
				       AND M.FECHAVALOR &lt; M.FECHAPROCESO 
				       AND M.ESTADOFECHAVALOR = 1 ) T 			         
		 ORDER BY SALDO_JTS_OID, FECHAPROCESO
"""
this.addCommand( new ModificarConsultaQuery(archivoMapp, query, consultaSQL))

	}
	
	private boolean isAccountingBr(){
		def jarName = "accounting_br";
		def directoryLib = new File(CarpetasServidor.SERVER_LIB.getExpandedPath());

		if(directoryLib.exists() && directoryLib.isDirectory()){
			File[] filesJars = directoryLib.listFiles({ File dir, String file ->
				file.startsWith(jarName) && file.endsWith(".jar")
			} as FilenameFilter
			)
			if( filesJars.length > 0 ){
				return true;
			}
		}
		return false;
	}
	
	private boolean isAccountingCo(){
		def jarName = "accounting_co";
		def directoryLib = new File(CarpetasServidor.SERVER_LIB.getExpandedPath());

		if(directoryLib.exists() && directoryLib.isDirectory()){
			File[] filesJars = directoryLib.listFiles({ File dir, String file ->
				file.startsWith(jarName) && file.endsWith(".jar")
			} as FilenameFilter
			)
			if( filesJars.length > 0 ){
				return true;
			}
		}
		return false;
	}
}
