package BRADESCARD

import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.parche.Parche

class Parche_NUBRADESC_15 extends Parche {
	
	
 void definicionDeParametros(){ 
	// Busca un nuevo campo libre
	this.addParameterNewFreeField("JTS_ORIGEN", "Campo para la tabla VTA_TRANSFERENCIA_PERIODICA")
	this.addParameterNewFreeField("JTS_DESTINO", "Campo para la tabla VTA_TRANSFERENCIA_PERIODICA")
	this.addParameterNewFreeField("IMPORTE_TRANSFERENCIA", "Campo para la tabla VTA_TRANSFERENCIA_PERIODICA")
	this.addParameterNewFreeField("FECHA_INICIO", "Campo para la tabla VTA_TRANSFERENCIA_PERIODICA")
	this.addParameterNewFreeField("FECHA_LIMITE", "Campo para la tabla VTA_TRANSFERENCIA_PERIODICA")
	this.addParameterNewFreeField("PERIODICIDAD", "Campo para la tabla VTA_TRANSFERENCIA_PERIODICA")
	this.addParameterNewFreeField("REINTENTO", "Campo para la tabla VTA_TRANSFERENCIA_PERIODICA")
	this.addParameterNewFreeField("FECHA_ULT_TRANSF_PROGRAMADA", "Campo para la tabla VTA_TRANSFERENCIA_PERIODICA")
	this.addParameterNewFreeField("DETALLE_ULT_PROCESO", "Campo para la tabla VTA_TRANSFERENCIA_PERIODICA")
	this.addParameterNewFreeField("CUENTA_ORIGEN", "Campo para la vista VW_TRANSFERENCIA_FALLIDA")
	this.addParameterNewFreeField("CLIENTE_ORIGEN", "Campo para la vista VW_TRANSFERENCIA_FALLIDA")
	this.addParameterNewFreeField("EMPRESA_SALDO", "Campo para la vista VW_TRANSFERENCIA_FALLIDA")
	this.addParameterNewFreeField("DETALLE_ULT_PROCESO_VW", "Campo para la vista VW_TRANSFERENCIA_FALLIDA")
	
	//PARA OBTENER EL NUMERO DE TITULO DE OPERACION
	this.addParameterQuery("TITULO_PA_ANTERIOR", "descripcion del parametro", "INTEGER", "SELECT TITULO FROM DESCRIPTORES WHERE upper(NOMBREFISICO) = 'CV_TRANSFERENCIA'")
	

	this.addParameterNewOperationNumber("OPERACION_PERIODICA", "Operacion para Parametro_JTS")
	this.addParameterNewOperationNumber("OPERACION_PERIODICA_EVENTO", "Operacion para Evento")
	
	
	this.addParameterNewFreeDescriptor("DESCRIPTOR_PERIODICA", "Este campo es para el descriptor")
	this.addParameterNewFreeDescriptor("DESCRIPTOR_PERIODICA_VISTA", "Este campo es para el descriptor")
	
	this.addParameterPATitleOpe("NRO_OPERACIO_PA"," Numero de titulo de los Procesos Automaticos")
	
 }

	@Override
	protected void comandos() throws TopazException {
		if(isOracle()){
			insertTableOracle();
		}else{
			insertTableSql();
		}
		inserts();
		agregarGroup();
		agregarGroupR();
		agregarProcess();
		agregarProcessR();
		agregarProcessEvento()
		agregarServices();
		agregarQueryDeEntradaEntity();
		agregarQueryDeEntrada();
		agregarQueryDeEntradaP();
	}
	private insertTableOracle(){
		def sentencia = """
		CREATE TABLE VTA_TRANSFERENCIA_PERIODICA (
			JTS_ORIGEN NUMBER (10),
			JTS_DESTINO NUMBER (10),
			IMPORTE_TRANSFERENCIA NUMBER (15,2),
			FECHA_INICIO DATE,
			FECHA_LIMITE DATE,
			PERIODICIDAD VARCHAR (1),
			REINTENTO NUMBER (1),
			FECHA_ULT_TRANSF_PROGRAMADA DATE,
			DETALLE_ULT_PROCESO VARCHAR (1),
			TZ_LOCK NUMBER (15),
			CONSTRAINT PK_PERIODICA PRIMARY KEY (JTS_ORIGEN,JTS_DESTINO,FECHA_INICIO));
		"""
		def descripcion = "Se crea la tabla VTA_TRANSFERENCIA_PERIODICA para oracle"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	private insertTableSql(){
		def sentencia = """
		CREATE TABLE VTA_TRANSFERENCIA_PERIODICA (
		    JTS_ORIGEN NUMERIC (10),
		    JTS_DESTINO NUMERIC (10),
		    IMPORTE_TRANSFERENCIA NUMERIC (15,2),
		    FECHA_INICIO DATETIME,
		    FECHA_LIMITE DATETIME,
		    PERIODICIDAD VARCHAR (1),
		    REINTENTO NUMERIC (1),
		    FECHA_ULT_TRANSF_PROGRAMADA DATETIME,
			DETALLE_ULT_PROCESO VARCHAR (1),
		    TZ_LOCK NUMERIC (15),
		    CONSTRAINT PK_PERIODICA PRIMARY KEY (JTS_ORIGEN,JTS_DESTINO,FECHA_INICIO)
		);
		"""
		def descripcion = "Se crea la tabla VTA_TRANSFERENCIA_PERIODICA para oracle"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	private inserts(){
		def sentencia = """
		CREATE VIEW VW_TRANSFERENCIA_FALLIDA AS
		SELECT S.CUENTA,S.C1803,S.EMPRESA,T.DETALLE_ULT_PROCESO
		FROM VTA_TRANSFERENCIA_PERIODICA T JOIN SALDOS S ON S.JTS_OID = T.JTS_ORIGEN
		WHERE T.REINTENTO = 1;

		INSERT INTO DESCRIPTORES (TITULO, IDENTIFICACION, TIPODEARCHIVO, DESCRIPCION, GRUPODELMAPA, NOMBREFISICO, TIPODEDBMS, LARGODELREGISTRO, INICIALIZACIONDELREGISTRO, BASE, SELECCION, ACEPTA_MOVS_DIFERIDO) VALUES
		(${this.P("TITULO_PA_ANTERIOR")}, ${this.P("DESCRIPTOR_PERIODICA")}, NULL, 'Transferencia_Periodica', 3, 'VTA_TRANSFERENCIA_PERIODICA', 'D', NULL, NULL, 'Top/Clientes', NULL, 'N');


		INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA) VALUES
		(${this.P("JTS_ORIGEN")}, ' ', 0, 'JTS_OID de origen', 'JTS_OID de origen', 10, 'N', 0, NULL, 0, 0, 0, 0, 0, 0, 0, ${this.P("DESCRIPTOR_PERIODICA")}, 'JTS_ORIGEN', 0, NULL);

		INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA) VALUES
		(${this.P("JTS_DESTINO")}, ' ', 0, 'JTS_OID de destino', 'JTS_OID de destino', 10, 'N', 0, NULL, 0, 0, 0, 0, 0, 0, 0, ${this.P("DESCRIPTOR_PERIODICA")}, 'JTS_DESTINO', 0, NULL);

		INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA) VALUES
		(${this.P("IMPORTE_TRANSFERENCIA")}, ' ', 0, 'Importe transferencia', 'Importe transferencia', 15, 'N', 2, NULL, 0, 0, 0, 0, 0, 0, 0, ${this.P("DESCRIPTOR_PERIODICA")}, 'IMPORTE_TRANSFERENCIA', 0, NULL);

		INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA) VALUES
		(${this.P("FECHA_INICIO")}, ' ', 0, 'Fecha de inicio de trasferencia', 'Fecha de inicio', 8, 'F', 0, NULL, 0, 0, 0, 0, 0, 0, 0, ${this.P("DESCRIPTOR_PERIODICA")}, 'FECHA_INICIO', 0, NULL);

		INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA) VALUES
		(${this.P("FECHA_LIMITE")}, ' ', 0, 'Fecha limite de transferencia', 'Fecha limite', 8, 'F', 0, NULL, 0, 0, 0, 0, 0, 0, 0, ${this.P("DESCRIPTOR_PERIODICA")}, 'FECHA_LIMITE', 0, NULL);

		INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA) VALUES
		(${this.P("PERIODICIDAD")}, ' ', 0, 'Periodicidad de la transferencia', 'Periodicidad de la transferencia', 1, 'V', 0, NULL, 0, 0, 0, 0, 0, 0, 0, ${this.P("DESCRIPTOR_PERIODICA")}, 'PERIODICIDAD', 0, NULL);

		INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA) VALUES
		(${this.P("REINTENTO")}, ' ', 0, 'Reintento para la transferencia', 'Reintento para la transferencia', 1, 'N', 0, NULL, 0, 0, 0, 0, 0, 0, 0, ${this.P("DESCRIPTOR_PERIODICA")}, 'REINTENTO', 0, NULL);

		INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA) VALUES
		(${this.P("FECHA_ULT_TRANSF_PROGRAMADA")}, ' ', 0, 'Fecha ult transf prog', 'Fecha ult transf prog', 8, 'F', 0, NULL, 0, 0, 0, 0, 0, 0, 0, ${this.P("DESCRIPTOR_PERIODICA")}, 'FECHA_ULT_TRANSF_PROGRAMADA', 0, NULL);

		INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA) VALUES
		(${this.P("DETALLE_ULT_PROCESO")}, ' ', 0, 'Estado ult proc', 'Estado ult proc', 1, 'A', 0, NULL, 0, 0, 0, 0, 0, 0, 1, ${this.P("DESCRIPTOR_PERIODICA")}, 'DETALLE_ULT_PROCESO', 0, NULL);


		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("PERIODICIDAD")}, 'E', 'Diaria', 'D', 'D');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("PERIODICIDAD")}, 'E', 'Semanal', 'S', 'S');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("PERIODICIDAD")}, 'E', 'Quincenal', 'Q', 'Q');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("PERIODICIDAD")}, 'E', 'Mensual', 'M', 'M');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("PERIODICIDAD")}, 'E', 'Ultimo dia del mes', 'U', 'U');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("PERIODICIDAD")}, 'P', 'Diaria', 'D', 'D');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("PERIODICIDAD")}, 'P', 'Semanal', 'S', 'S');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("PERIODICIDAD")}, 'P', 'Quincenal', 'Q', 'Q');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("PERIODICIDAD")}, 'P', 'Mensual', 'M', 'M');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("PERIODICIDAD")}, 'P', 'Ultimo dia del mes', 'U', 'U');

		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("REINTENTO")}, 'E', 'Registro para reintento', '1', '1');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("REINTENTO")}, 'P', 'Registro para reintento', '1', '1');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("REINTENTO")}, 'E', 'Registro para preiodico', '0', '0');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("REINTENTO")}, 'P', 'Registro para preiodico', '0', '0');

		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("DETALLE_ULT_PROCESO")}, 'E', 'Error por disponibilidad', 'D', 'D');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("DETALLE_ULT_PROCESO")}, 'E', 'Error por bloqueo', 'B', 'B');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("DETALLE_ULT_PROCESO")}, 'E', 'Sin error', ' ', ' ');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("DETALLE_ULT_PROCESO")}, 'P', 'Error por disponibilidad', 'D', 'D');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("DETALLE_ULT_PROCESO")}, 'P', 'Error por bloqueo', 'B', 'B');
		INSERT INTO OPCIONES (NUMERODECAMPO, IDIOMA, DESCRIPCION, OPCIONINTERNA, OPCIONDEPANTALLA) VALUES(${this.P("DETALLE_ULT_PROCESO")}, 'P', 'Sin error', ' ', ' ');

		INSERT INTO OPERACIONES (TITULO, IDENTIFICACION, NOMBRE, DESCRIPCION, MNEMOTECNICO, AUTORIZACION, FORMULARIOPRINCIPAL, PROXOPERACION, ESTADO, TZ_LOCK, COPIAS, SUBOPERACION, PERMITEBAJA, COMPORTAMIENTOENCIERRE, REQUIERECONTRASENA, PERMITECONCURRENTE, PERMITEESTADODIFERIDO, ICONO_TITULO, ESTILO) VALUES
		(${this.P("NRO_OPERACIO_PA")}, ${this.P("OPERACION_PERIODICA")}, 'Traspaso entre Cuentas Periodico', 'Traspaso entre Cuentas Periodico', ${this.P("OPERACION_PERIODICA")}, 'N', 500, NULL, 'P', 0, NULL, 1, 'S', 'I', 'N', 'S', 'S', NULL, 0);

		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK) VALUES('TRASPASO_PERIODICO', 'OPERACION', '${this.P("OPERACION_PERIODICA")}', 0);	
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK) VALUES('TRASPASO_PERIODICO', 'DESCRIPCION', 'Traspaso entre cuentas periodico', 0);	
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK) VALUES('TRASPASO_PERIODICO', 'CANAL', '0', 0);	

		INSERT INTO DESCRIPTORES (TITULO, IDENTIFICACION, TIPODEARCHIVO, DESCRIPCION, GRUPODELMAPA, NOMBREFISICO, TIPODEDBMS, LARGODELREGISTRO, INICIALIZACIONDELREGISTRO, BASE, SELECCION, ACEPTA_MOVS_DIFERIDO) VALUES
		(${this.P("TITULO_PA_ANTERIOR")}, ${this.P("DESCRIPTOR_PERIODICA_VISTA")}, NULL, 'Vista_Transferencia_fallida', 3, 'VW_TRANSFERENCIA_FALLIDA', 'D', NULL, NULL, 'Top/Clientes', NULL, 'N');

		INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA) VALUES
		(${this.P("CUENTA_ORIGEN")}, ' ', 0, 'Cuenta origen', 'Cuenta origen', 15, 'N', 0, NULL, 0, 0, 0, 0, 0, 0, 0, ${this.P("DESCRIPTOR_PERIODICA_VISTA")}, 'CUENTA', 0, NULL);

		INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA) VALUES
		(${this.P("CLIENTE_ORIGEN")}, ' ', 0, 'Codigo Cliente', 'Codigo Cliente', 15, 'N', 0, NULL, 0, 0, 0, 0, 0, 0, 0, ${this.P("DESCRIPTOR_PERIODICA_VISTA")}, 'C1803', 0, NULL);

		INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA) VALUES
		(${this.P("EMPRESA_SALDO")}, ' ', 0, 'Empresa Saldo', 'Empresa Saldo', 4, 'N', 0, NULL, 0, 0, 0, 0, 0, 0, 0, ${this.P("DESCRIPTOR_PERIODICA_VISTA")}, 'EMPRESA', 0, NULL);

		INSERT INTO DICCIONARIO (NUMERODECAMPO, USODELCAMPO, REFERENCIA, DESCRIPCION, PROMPT, LARGO, TIPODECAMPO, DECIMALES, EDICION, CONTABILIZA, CONCEPTO, CALCULO, VALIDACION, TABLADEVALIDACION, TABLADEAYUDA, OPCIONES, TABLA, CAMPO, BASICO, MASCARA) VALUES
		(${this.P("DETALLE_ULT_PROCESO_VW")}, ' ', 0, 'Estado ult proc', 'Estado ult proc', 1, 'A', 0, NULL, 0, 0, 0, 0, 0, 0, 0, ${this.P("DESCRIPTOR_PERIODICA_VISTA")}, 'DETALLE_ULT_PROCESO', 0, NULL);

		INSERT INTO INDICES (NUMERODEARCHIVO, NUMERODEINDICE, DESCRIPCION, CLAVESREPETIDAS, CAMPO1, CAMPO2, CAMPO3, CAMPO4, CAMPO5, CAMPO6, CAMPO7, CAMPO8, CAMPO9, CAMPO10, CAMPO11, CAMPO12, CAMPO13, CAMPO14, CAMPO15, CAMPO16, CAMPO17, CAMPO18, CAMPO19, CAMPO20)
		VALUES (${this.P("DESCRIPTOR_PERIODICA_VISTA")}, 1, 'i1 VW_TRANSFERENCIA_FALLIDA', 0, ${this.P("CUENTA_ORIGEN")}, ${this.P("CLIENTE_ORIGEN")}, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

		INSERT INTO OPERACIONES (TITULO, IDENTIFICACION, NOMBRE, DESCRIPCION, MNEMOTECNICO, AUTORIZACION, FORMULARIOPRINCIPAL, PROXOPERACION, ESTADO, TZ_LOCK, COPIAS, SUBOPERACION, PERMITEBAJA, COMPORTAMIENTOENCIERRE, REQUIERECONTRASENA, PERMITECONCURRENTE, PERMITEESTADODIFERIDO, ICONO_TITULO, ESTILO) VALUES
		(${this.P("NRO_OPERACIO_PA")}, ${this.P("OPERACION_PERIODICA_EVENTO")}, 'Evento Cuentas fallida', 'Evento Cuentas fallida', ${this.P("OPERACION_PERIODICA_EVENTO")}, 'N', 500, NULL, 'P', 0, NULL, 1, 'S', 'I', 'N', 'S', 'S', NULL, 0);

"""
		def descripcion = "Se crean los inserts necesarios para el proceso"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}


	private agregarGroup(){
		// Agregar o sustituir un grupo
		def archivo = "groups.py"
		def grupo = """
    gdef = GroupDefinition("VTA - Transferencia entre Cuentas Periodica")
    gdef.registerProcess("Transferencias de fondos Periodica")
    gdef.setCanBeRootGroup("true")
    gdef.setSingleton("false")
    gdefs.addGroup(gdef)
"""
		this.addCommand( new AgregarModificarGrupo(archivo, grupo))

	}

	private agregarGroupR(){
		// Agregar o sustituir un grupo
		def archivo = "groups.py"
		def grupo = """
    gdef = GroupDefinition("VTA - Transferencia entre Cuentas Reintento")
    gdef.registerProcess("Transferencias de fondos Reintento")
    gdef.registerProcess("Evento Generico transferencia fallida")
    gdef.setCanBeRootGroup("true")
    gdef.setSingleton("false")
    gdefs.addGroup(gdef)
"""
		this.addCommand( new AgregarModificarGrupo(archivo, grupo))

	}

	private agregarProcess(){
		// Agregar o sustituir un proceso
		def archivo = "processes.py"
		def procesos = """
# Transferencias periodicas entre Cuentas
    pdef = ProcessDefinition("Transferencias de fondos Periodica", "topsystems.automaticprocess.processmanager.WorkManager")
    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WorkDescriptorTransferenciasPeriodicas");
    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BusinessWorkerTransferenciasPeriodicas");
	pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
    pdef.addConstant("rangoCommit","500");
    pdef.addConstant("cantidadHilos","10");
    pdef.addConstant("isStopable","true");
    pdef.addConstant("reintento","false");
    pdefs.addProcess(pdef)
"""
		this.addCommand(new AgregarModificarProceso(archivo, procesos))

	}

	private agregarProcessR(){
		// Agregar o sustituir un proceso
		def archivo = "processes.py"
		def procesos = """
    pdef = ProcessDefinition("Transferencias de fondos Reintento", "topsystems.automaticprocess.processmanager.WorkManager")
    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WorkDescriptorTransferenciasPeriodicas");
    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BusinessWorkerTransferenciasPeriodicas");
	pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
    pdef.addConstant("rangoCommit","500");
    pdef.addConstant("cantidadHilos","10");
    pdef.addConstant("isStopable","true");
    pdef.addConstant("reintento","true");
    pdefs.addProcess(pdef)
"""
		this.addCommand(new AgregarModificarProceso(archivo, procesos))

	}
	
	private agregarProcessEvento(){
		// Agregar o sustituir un proceso
def archivo = "processes.py"
def procesos = """
    pdef = ProcessDefinition("Evento Generico transferencia fallida", "topsystems.automaticprocess.processmanager.WorkManager")
    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WorkDescriptorEnviarEventosGenerico");
    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BusinessWorkerEnviarEventosGenerico");
    pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
    pdef.addConstant("rangoCommit","500");
    pdef.addConstant("cantidadHilos","10");
    pdef.addConstant("isStopable","true");
    pdef.addConstant("applySchemes","true");
    pdef.addConstant("isSumarizable","true");
    pdef.addConstant("offLine","true");
    pdef.addConstant("descriptor","${this.P("DESCRIPTOR_PERIODICA_VISTA")}");
    pdef.addConstant("codigoTransaccion","0");
    pdef.addConstant("nroOperacion","${this.P("OPERACION_PERIODICA_EVENTO")}");
    pdef.addConstant("fieldNumberEmpresa","${this.P("EMPRESA_SALDO")}");
    pdef.addConstant("enqueue","false");
    pdefs.addProcess(pdef)
"""
this.addCommand(new AgregarModificarProceso(archivo, procesos))

	}

	
	private agregarServices(){
def archivo = "fundsTransfer/fundsTransferDeploy.xml"
def servicios = """
<service>
<name>topsystems:processManager:WorkDescriptor=WorkDescriptorTransferenciasPeriodicas</name>
<code>topsystems.automaticprocess.transferenciaperiodicas.WorkDescriptorTransferenciasPeriodicas</code>
</service>
<service>
<name>topsystems:processManager:BusinessWorker=BusinessWorkerTransferenciasPeriodicas</name>
<code>topsystems.automaticprocess.transferenciaperiodicas.BusinessWorkerTransferenciasPeriodicas</code>
</service>
"""
this.addCommand( new AgregarModificarServicios(archivo, servicios))
 
	}
	private agregarQueryDeEntradaEntity(){
		// Agregar o sustituir una entidad de DataMapping
		def archivoMapp = "FundsTransferMapping.xml"
		def entidades = """
		<class>
		        <class-name>topsystems.automaticprocess.transferenciaperiodicas.TransferenciaPeriodica</class-name>
		        <entity-name>core.vo_transfPeriodica</entity-name>
		        <table-name>VTA_TRANSFERENCIA_PERIODICA</table-name>
		        <database-name>TOP/CLIENTES</database-name>
		    <attribute>
            	<attribute-name>jtsOidCuentaOrigen</attribute-name>
	            <column-name>JTS_ORIGEN</column-name>
	            <key>primary</key>
	        </attribute>
	        <attribute>
	            <attribute-name>jtsOidCuentaDestino</attribute-name>
	            <column-name>JTS_DESTINO</column-name>
	            <key>primary</key>
	        </attribute>
			<attribute>
	            <attribute-name>fechaAlta</attribute-name>
	            <column-name>FECHA_INICIO</column-name>
	            <key>primary</key>
	        </attribute>
	        <attribute>
	            <attribute-name>importeFijo</attribute-name>
	            <column-name>IMPORTE_TRANSFERENCIA</column-name>
	        </attribute>
	        <attribute>
	            <attribute-name>codigoPeriodicidad</attribute-name>
	            <column-name>PERIODICIDAD</column-name>
	        </attribute>
			<attribute>
	            <attribute-name>fecha_ultima_transferencia</attribute-name>
	            <column-name>FECHA_ULT_TRANSF_PROGRAMADA</column-name>
	        </attribute>
			<attribute>
	            <attribute-name>detalle_proceso</attribute-name>
	            <column-name>DETALLE_ULT_PROCESO</column-name>
	        </attribute>
	        <attribute>
	            <attribute-name>codigoReintento</attribute-name>
	            <column-name>REINTENTO</column-name>
	        </attribute>
		</class>
		"""
	this.addCommand( new AgregarModificarEntidad(archivoMapp, entidades))
	}


	private agregarQueryDeEntrada(){
		def archivoMapp = "FundsTransferMapping.xml"
		def queries = """
	<query>
        <class-name>topsystems.automaticprocess.transferenciaperiodicas.TransferenciaPeriodica</class-name>
        <query-name>query.TransferenciaPeriodica</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>

     SELECT 
			JTS_ORIGEN,
			JTS_DESTINO,
			IMPORTE_TRANSFERENCIA,
			FECHA_INICIO,
			PERIODICIDAD,
			FECHA_ULT_TRANSF_PROGRAMADA,
			DETALLE_ULT_PROCESO,
			REINTENTO
	   FROM 
			VTA_TRANSFERENCIA_PERIODICA
	  WHERE 
			TZ_LOCK = 0 
			AND (FECHA_ULT_TRANSF_PROGRAMADA IS NULL 
			AND FECHA_INICIO &lt;= (SELECT FECHAPROCESO FROM PARAMETROS)
		    AND FECHA_LIMITE &gt; (SELECT FECHAPROCESO FROM PARAMETROS)
		    AND REINTENTO = 0)
		    OR (FECHA_ULT_TRANSF_PROGRAMADA &lt;= (SELECT FECHAPROCESO FROM PARAMETROS)
		    AND FECHA_INICIO &lt;= (SELECT FECHAPROCESO FROM PARAMETROS)
		    AND FECHA_LIMITE &gt;= (SELECT FECHAPROCESO FROM PARAMETROS)
			AND (REINTENTO = 0 OR REINTENTO = 1))
			ORDER BY JTS_ORIGEN
		</sentence>
        <attribute>
            <attribute-name>jtsOidCuentaOrigen</attribute-name>
            <column-name>JTS_ORIGEN</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>jtsOidCuentaDestino</attribute-name>
            <column-name>JTS_DESTINO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>importeFijo</attribute-name>
            <column-name>IMPORTE_TRANSFERENCIA</column-name>
            <column-type>double</column-type>
        </attribute>
        <attribute>
            <attribute-name>fechaAlta</attribute-name>
            <column-name>FECHA_INICIO</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
		<attribute>
            <attribute-name>codigoPeriodicidad</attribute-name>
            <column-name>PERIODICIDAD</column-name>
            <column-type>String</column-type>
        </attribute>
		<attribute>
            <attribute-name>fecha_ultima_transferencia</attribute-name>
            <column-name>FECHA_ULT_TRANSF_PROGRAMADA</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>codigoReintento</attribute-name>
            <column-name>REINTENTO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>detalle_proceso</attribute-name>
            <column-name>DETALLE_ULT_PROCESO</column-name>
            <column-type>String</column-type>
        </attribute>
    </query>
"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}

	private agregarQueryDeEntradaP(){
		def archivoMapp = "FundsTransferMapping.xml"
		def queries = """
	<query>
        <class-name>topsystems.automaticprocess.transferenciaperiodicas.TransferenciaPeriodica</class-name>
        <query-name>query.TransferenciaPeriodicaR</query-name>
        <database-name>TOP/CLIENTES</database-name>
        <sentence>

	  SELECT 
			JTS_ORIGEN,
			JTS_DESTINO,
			IMPORTE_TRANSFERENCIA,
		 	FECHA_INICIO,
			PERIODICIDAD,
			FECHA_ULT_TRANSF_PROGRAMADA,
			DETALLE_ULT_PROCESO,
			REINTENTO
	   FROM
			VTA_TRANSFERENCIA_PERIODICA
	  WHERE 
			TZ_LOCK = 0
			AND REINTENTO = 1
			AND FECHA_LIMITE &gt;= (SELECT FECHAPROCESO FROM PARAMETROS)
			ORDER BY JTS_ORIGEN
		</sentence>
        <attribute>
            <attribute-name>jtsOidCuentaOrigen</attribute-name>
            <column-name>JTS_ORIGEN</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>jtsOidCuentaDestino</attribute-name>
            <column-name>JTS_DESTINO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>importeFijo</attribute-name>
            <column-name>IMPORTE_TRANSFERENCIA</column-name>
            <column-type>double</column-type>
        </attribute>
		<attribute>
            <attribute-name>fechaAlta</attribute-name>
            <column-name>FECHA_INICIO</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>codigoPeriodicidad</attribute-name>
            <column-name>PERIODICIDAD</column-name>
            <column-type>String</column-type>
        </attribute>
		<attribute>
            <attribute-name>fecha_ultima_transferencia</attribute-name>
            <column-name>FECHA_ULT_TRANSF_PROGRAMADA</column-name>
            <column-type>Timestamp</column-type>
        </attribute>
        <attribute>
            <attribute-name>codigoReintento</attribute-name>
            <column-name>REINTENTO</column-name>
            <column-type>long</column-type>
        </attribute>
        <attribute>
            <attribute-name>detalle_proceso</attribute-name>
            <column-name>DETALLE_ULT_PROCESO</column-name>
            <column-type>String</column-type>
        </attribute>
    </query>
"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}
	
}
