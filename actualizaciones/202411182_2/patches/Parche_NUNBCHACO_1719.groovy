package chaco

import topsystems.TopazException
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche

class Parche_NUNBCHACO_1719 extends Parche {

	@Override
	protected void comandos() throws TopazException {
		crearParametrosJTS()
	}
	
	private void crearParametrosJTS(){
		def descripcion = "Parametro que indica si es opcional cobrar los cargos generados por el evento de d�bito"
		def sentencia = """
			INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
			VALUES ('COBRANZA_AUTOMATICA', 'COBRO_EVENTO_DEBITO_OPCIONAL', 'N', 0);
		"""
		this.addCommand(new ActualizarBaseDeDatos(sentencia, descripcion))
	}
}
