import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche;

class Parche_NUCREDINKA_1064 extends Parche{

	void definicionDeParametros(){

	}

	@Override
	protected void comandos() {
		
		//----------------------------------------------------------------------------------------------------------------//
		// SQL GEN�RICO
		def sentencia = """
			INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
			VALUES ('CANCELACION_DPF', 'MARCA_AJUSTE_X_ASIENTO_FCH_VALOR_MES_ANTERIOR', 'S', 0)
		"""
		this.addCommand( new ActualizarBaseDeDatos(sentencia))
		//----------------------------------------------------------------------------------------------------------------//
	}

}