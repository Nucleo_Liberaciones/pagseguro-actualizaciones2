import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.parche.Parche

class Parche_NUSICREDBR_3512 extends Parche {

	void definicionDeParametros(){
	}

	@Override
	protected void comandos() {
		//----------------------------------------------------------------------------------------------------------------//
		// Sentencias SQL Genéricas
		def sentencia = """
			UPDATE PARAMETROS_JTS SET VALOR='91'
			WHERE FUNCIONALIDAD='DEVENGAMIENTO' AND PARAMETRO = 'CANTIDAD_DIAS_X_DIAS_ATRASO'; 
		"""
		def descripcion = "Sentencias SQL Genéricas"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion));
	}

}