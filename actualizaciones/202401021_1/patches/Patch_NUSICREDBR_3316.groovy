package Parches

import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.processmanager.AgregarModificarPropiedadesProceso
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Patch_NUSICREDBR_3316 extends Parche {

	
	void definicionDeParametros(){

	
	}
	
	
void sobreElParche() { // Este metodo es opcional
	 this.notas_del_autor = "Patch_NUSICREDBR-3316"
	 this.opcional = false
}
	 
void comandos() {

// Agregar o sustituir un proceso
def archivo = "processes.py"
def procesos = """
    # PROCESO: Extorno con Sucursal
    pdef = ProcessDefinition("Extorno con Sucursal", "topsystems.automaticprocess.extorno.ExtornProcess")
    pdef.addParameter("FECHA_VALOR",4,"dd/MM/yyyy",0);
    pdef.addParameter("esAjusteParam",3,"",0);
    pdef.addParameter("sucursalProceso",1,"",0);
    pdefs.addProcess(pdef)  
"""
this.addCommand(new AgregarModificarProceso(archivo, procesos))


// Agregar o sustituir un grupo
def archivo3 = "groups.py"
def grupo3 = """
gdef = GroupDefinition("PROCESO: Extorno con Sucursal") 
gdef.registerProcess("Extorno con Sucursal")
gdefs.addGroup(gdef)
"""
this.addCommand( new AgregarModificarGrupo(archivo3, grupo3))


}
			

		
	
			
			
}
