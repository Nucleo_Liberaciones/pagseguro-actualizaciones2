package brasil

import topsystems.TopazException
import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Parche_NUSICREDBR_3526 extends Parche{
	
	void definicionDeParametros(){
		this.addParameterNewOperationNumber("NRO_OPE_PA", "Nro. de operaci�n para asociar al proceso autom�tico")
		this.addParameterPATitleOpe("NUMERO_TITULO_OPE", "Ingrese el n�mero del t�tulo para la operaci�n del proceso")
	}

	@Override
	protected void comandos() throws TopazException {
		addParametersJtsBD()
		crearQueryMapping()
		nuevaEstructuraBD()
		crearClaseMapping()
		creaProceso()
		crearGrupo()
		agregarModificarDeploy()
	}
	
	protected void addParametersJtsBD(){
		def sentencia = """
			INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
			VALUES ('SUSTITUCION_DEVENGADO', 'OPERACION', '${this.P('NRO_OPE_PA')}', 0);
			
			INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
			VALUES ('SUSTITUCION_DEVENGADO', 'DESCRIPCION', 'SUSTITUCION DEVENGADO VIGENTE DIA D', 0);
			
			INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
			VALUES ('SUSTITUCION_DEVENGADO', 'CANAL', '0', 0);

			INSERT INTO OPERACIONES (TITULO, IDENTIFICACION, NOMBRE, DESCRIPCION, MNEMOTECNICO, AUTORIZACION, FORMULARIOPRINCIPAL, PROXOPERACION, ESTADO, TZ_LOCK, COPIAS, SUBOPERACION, PERMITEBAJA, COMPORTAMIENTOENCIERRE, REQUIERECONTRASENA, PERMITECONCURRENTE, PERMITEESTADODIFERIDO)
			VALUES ('${this.P('NUMERO_TITULO_OPE')}', '${this.P('NRO_OPE_PA')}', 'Sustitucion Devengado VigenteDiaD', 'Sustitucion Devengado VigenteDiaD', '${this.P('NRO_OPE_PA')}', 'N', NULL, NULL, 'P', 0, NULL, 0, 'S', 'N', 'N', 'S', 'N');

		"""
		this.addCommand(new ActualizarBaseDeDatos(sentencia))
	}
	
	protected void crearQueryMapping(){
		def archivoMapp = "SustitucionDevengadoVigenteDiaD.xml"
		def queries = """
	    <query>
	        <class-name>topsystems.topaz.server.businesslogic.sustituciondevengadovigente.PrestamoSustituirDevengadoVigente</class-name>
	        <query-name>query.vo_prestamos_sustituir_devengado</query-name>
	        <database-name>TOP/CLIENTES</database-name>
	        <sentence>
				SELECT a.JTS_OID, a.fecha_90, hd.fecha_devengado, hd.interes_devengado_NP, hd.MORA_DEVENGADA, hd.IPF_DEVENGADO, hd.IPF_MORATORIO_DEV, hd.MORA_TASA_INT_DEVENGADA, hd.ICMORA_DEVENGADO
				FROM 	
					(
					SELECT s.JTS_OID, max(p.C2300) cuota, max(psc.C2302_ORIGINAL) vto_susp, max(psc.C2302_ORIGINAL) + 90 AS fecha_90
					FROM SALDOS s , PLANPAGOS p , PLANPAGOS psc 
					WHERE s.TZ_LOCK = 0 AND s.c1785 = 5 AND s.c1604 &lt; 0 
						AND s.c1728 &lt;&gt; 'C' 
						AND s.c1813 = ( SELECT FECHAPROCESOANTERIOR FROM parametros )
						AND s.c1628 &lt; s.c1813-59 
						AND s.c1729 = 'S' 
						AND s.c1628 &lt; s.c1813-90 
					AND s.JTS_OID = p.SALDO_JTS_OID AND p.C2300 &lt;= s.ULTIMA_CUOTA_CON_SALDO 
					AND p.C2309 + p.C2310 = 0 AND p.c2313 &gt; s.FCAMBIOESTADOSUSPENSO 
					AND s.JTS_OID = psc.SALDO_JTS_OID AND psc.C2300 = p.C2300 + 1
					AND p.C2313 - psc.C2302_ORIGINAL &lt;= 90 
					GROUP BY s.JTS_OID 
					UNION all
					
					SELECT s.JTS_OID, p.C2300 cuota, p.C2302_ORIGINAL vto_susp, p.C2302_ORIGINAL + 90 AS fecha_90
					FROM SALDOS s , PLANPAGOS p 
					WHERE s.TZ_LOCK = 0 AND s.c1785 = 5 AND s.c1604 &lt; 0 
						AND s.c1728 &lt;&gt; 'C' 
						AND s.c1813 = ( SELECT FECHAPROCESOANTERIOR FROM parametros )
						AND s.c1628 &lt; s.c1813-59 
						AND s.c1729 = 'S' 
						AND s.c1628 &lt; s.c1813-90 
					AND s.JTS_OID = p.SALDO_JTS_OID AND p.C2300 &lt;= s.ULTIMA_CUOTA_CON_SALDO 
					AND s.FCAMBIOESTADOSUSPENSO - p.C2302_ORIGINAL BETWEEN 56 AND 64
					AND s.JTS_OID NOT IN 
						(
						SELECT DISTINCT s.JTS_OID
						FROM SALDOS s , PLANPAGOS p , PLANPAGOS psc 
						WHERE s.TZ_LOCK = 0 AND s.c1785 = 5 AND s.c1604 &lt; 0 
							AND s.c1728 &lt;&gt; 'C' 
							AND s.c1813 = ( SELECT FECHAPROCESOANTERIOR FROM parametros )
							AND s.c1628 &lt; s.c1813-59 
							AND s.c1729 = 'S' 
							AND s.c1628 &lt; s.c1813-90 
						AND s.JTS_OID = p.SALDO_JTS_OID AND p.C2300 &lt;= s.ULTIMA_CUOTA_CON_SALDO 
						AND p.C2309 + p.C2310 = 0 AND p.c2313 &gt; s.FCAMBIOESTADOSUSPENSO 
						AND s.JTS_OID = psc.SALDO_JTS_OID AND psc.C2300 = p.C2300 + 1
						AND p.C2313 - psc.C2302_ORIGINAL &lt;= 90 
						)
					) a
				LEFT JOIN HISTORICO_DEVENGAMIENTO hd
				ON a.JTS_OID = hd.saldo_jts_oid
				AND a.FECHA_90 = hd.FECHA_DEVENGADO
			</sentence>
	        <attribute>
	            <attribute-name>JTS_OID</attribute-name>
	            <column-name>JTS_OID</column-name>
	            <column-type>long</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fecha90</attribute-name>
	            <column-name>fecha_90</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>fechaDevengado</attribute-name>
	            <column-name>fecha_devengado</column-name>
	            <column-type>Date</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>interesDevengado</attribute-name>
	            <column-name>interes_devengado_NP</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>moraDevengada</attribute-name>
	            <column-name>MORA_DEVENGADA</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>ipfDevengado</attribute-name>
	            <column-name>IPF_DEVENGADO</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>ipfMoratorioDevengado</attribute-name>
	            <column-name>IPF_MORATORIO_DEV</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>moraTasaIntDevengada</attribute-name>
	            <column-name>MORA_TASA_INT_DEVENGADA</column-name>
	            <column-type>double</column-type>
	        </attribute>
	        <attribute>
	            <attribute-name>icMoraDevengada</attribute-name>
	            <column-name>ICMORA_DEVENGADO</column-name>
	            <column-type>double</column-type>
	        </attribute>
	    </query>
		"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, queries))
	}

	protected void nuevaEstructuraBD(){
		def baseOra = esBaseDeDatos("ORACLE")
		if(baseOra){
			def sentencia = """
				CREATE TABLE BS_SUSTITUCION_DEVENGADO (
					SALDOS_JTS_OID 					NUMBER (10) NOT NULL,
					FECHA							DATE NOT NULL,
					INTERES_DEVENGADO				NUMBER (15,2) NOT NULL,
					MORA_DEVENGADA					NUMBER (15,2) NOT NULL,
					IPF_DEVENGADO					NUMBER (15,2) NOT NULL,
					IPF_MORATORIO_DEV				NUMBER (15,2) NOT NULL,
					MORA_TASA_INT_DEVENGADA			NUMBER (15,2) NOT NULL,
					ICMORA_DEVENGADO				NUMBER (15,2) NOT NULL,
					MORA_PERCIBIDA					NUMBER (15,2) NOT NULL,
					IPF_MORATORIO_PERCIBIDO			NUMBER (15,2) NOT NULL,
					INTERES_COMP_MORA_PERCIBIDO		NUMBER (15,2) NOT NULL,		
					FECHA_SUSPENSO					DATE NOT NULL,
					FECHA_VIGENTE					DATE NOT NULL,
					CONSTRAINT BS_SUSTITUCION_DEVENGADO PRIMARY KEY (SALDOS_JTS_OID, FECHA)
				);			
			"""
			this.addCommand(new ActualizarBaseDeDatosOracle(sentencia))
		}else{
			def sentencia = """
				CREATE TABLE BS_SUSTITUCION_DEVENGADO (
					SALDOS_JTS_OID 					NUMERIC (10) NOT NULL,
					FECHA							DATETIME NOT NULL,
					INTERES_DEVENGADO				NUMERIC (15,2) NOT NULL,
					MORA_DEVENGADA					NUMERIC (15,2) NOT NULL,
					IPF_DEVENGADO					NUMERIC (15,2) NOT NULL,
					IPF_MORATORIO_DEV				NUMERIC (15,2) NOT NULL,
					MORA_TASA_INT_DEVENGADA			NUMERIC (15,2) NOT NULL,
					ICMORA_DEVENGADO				NUMERIC (15,2) NOT NULL,
					MORA_PERCIBIDA					NUMERIC (15,2) NOT NULL,
					IPF_MORATORIO_PERCIBIDO			NUMERIC (15,2) NOT NULL,
					INTERES_COMP_MORA_PERCIBIDO		NUMERIC (15,2) NOT NULL,	
					FECHA_SUSPENSO					DATETIME NOT NULL,
					FECHA_VIGENTE					DATETIME NOT NULL,
					CONSTRAINT PK_BS_SUSTITUCION_DEVENGADO PRIMARY KEY (SALDOS_JTS_OID, FECHA)
				)
				GO;
			"""
			this.addCommand(new ActualizarBaseDeDatosSQLServer(sentencia))
		}
	}
	
	protected void crearClaseMapping(){
		def archivoMapp = "SustitucionDevengadoVigenteDiaD.xml"
		def entidades = """
			<class>
		        <class-name>topsystems.topaz.server.businesslogic.sustituciondevengadovigente.PrestamoSustituirDevengadoVigente</class-name>
		        <entity-name>core.vo_PrestamoSustituirDevengadoVigente</entity-name>
		        <table-name>BS_SUSTITUCION_DEVENGADO</table-name>
		        <database-name>TOP/CLIENTES</database-name>
		        <attribute>
		            <attribute-name>JTS_OID</attribute-name>
		            <column-name>SALDOS_JTS_OID</column-name>
		            <key>primary</key>
		        </attribute>
		        <attribute>
		            <attribute-name>fecha90</attribute-name>
		            <column-name>FECHA</column-name>
		        </attribute>
		        <attribute>
		            <attribute-name>interesDevengado</attribute-name>
		            <column-name>INTERES_DEVENGADO</column-name>
		        </attribute>
		        <attribute>
		            <attribute-name>moraDevengada</attribute-name>
		            <column-name>MORA_DEVENGADA</column-name>
		        </attribute>
		        <attribute>
		            <attribute-name>ipfDevengado</attribute-name>
		            <column-name>IPF_DEVENGADO</column-name>
		        </attribute>
		        <attribute>
		            <attribute-name>ipfMoratorioDevengado</attribute-name>
		            <column-name>IPF_MORATORIO_DEV</column-name>
		        </attribute>
		        <attribute>
		            <attribute-name>moraTasaIntDevengada</attribute-name>
		            <column-name>MORA_TASA_INT_DEVENGADA</column-name>
		        </attribute>
		        <attribute>
		            <attribute-name>icMoraDevengada</attribute-name>
		            <column-name>ICMORA_DEVENGADO</column-name>
		        </attribute>
		        <attribute>
		            <attribute-name>moraPercibida</attribute-name>
		            <column-name>MORA_PERCIBIDA</column-name>
		        </attribute>
		        <attribute>
		            <attribute-name>ipfMoratorioPercibido</attribute-name>
		            <column-name>IPF_MORATORIO_PERCIBIDO</column-name>
		        </attribute>
		        <attribute>
		            <attribute-name>intCompensatorioMoraPercibido</attribute-name>
		            <column-name>INTERES_COMP_MORA_PERCIBIDO</column-name>
		        </attribute>
		        <attribute>
		            <attribute-name>fechaPasajeSuspenso</attribute-name>
		            <column-name>FECHA_SUSPENSO</column-name>
		        </attribute>
		        <attribute>
		            <attribute-name>ultimaFechaVigente</attribute-name>
		            <column-name>FECHA_VIGENTE</column-name>
		        </attribute>
			</class>
		"""
		this.addCommand( new AgregarModificarEntidad(archivoMapp, entidades))
	}

	protected void creaProceso(){
		// Agregar o sustituir un proceso
		def archivo = "processes.py"
		def procesos = """
		# PROCESO: CRE - SUSTITUCION DEVENGADO VIGENTE DIA D
	    pdef = ProcessDefinition("Sustitucion devengado vigente dia D", "topsystems.automaticprocess.processmanager.WorkManager")
	    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WorkDescriptorSustitucionDevengadoVigenteDiaD");
	    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BusinessWorkerSustitucionDevengadoVigenteDiaD");
	    pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
		pdef.addConstant("rangoCommit","100");
		pdef.addConstant("cantidadHilos","10");
		pdef.addConstant("grabarSaldo","true");
	    pdefs.addProcess(pdef)
	    """
		this.addCommand(new AgregarModificarProceso(archivo, procesos))
	}
	
	protected void crearGrupo(){
		def archivo = "groups.py"
		def grupo = """
		# PROCESO: CRE - SUSTITUCION DEVENGADO VIGENTE DIA D
	    gdef = GroupDefinition("CRE - SUSTITUCION DEVENGADO VIGENTE DIA D")
	    gdef.registerProcess("Sustitucion devengado vigente dia D")
	    gdef.setCanBeRootGroup("true")
	    gdef.setSingleton("false")
	    gdefs.addGroup(gdef)
		"""
		this.addCommand( new AgregarModificarGrupo(archivo, grupo))
	}
	
	private void agregarModificarDeploy(){
		def archivo = "SustitucionDevengadoVigenteDiaDDeploy.xml"
		def servicios = """
			<service>
				<name>topsystems:processManager:WorkDescriptor=WorkDescriptorSustitucionDevengadoVigenteDiaD</name>
				<code>topsystems.topaz.server.businesslogic.sustituciondevengadovigente.WorkDescriptorSustitucionDevengadoVigenteDiaD</code>
			</service>
			<service>
				<name>topsystems:processManager:BusinessWorker=BusinessWorkerSustitucionDevengadoVigenteDiaD</name>
				<code>topsystems.topaz.server.businesslogic.sustituciondevengadovigente.BusinessWorkerSustitucionDevengadoVigenteDiaD</code>
			</service>
		"""
		this.addCommand( new AgregarModificarServicios(archivo, servicios))
	}
	
}
