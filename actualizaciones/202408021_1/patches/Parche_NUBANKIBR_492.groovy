package BANKIBR

import topsystems.actualizador.command.ActualizacionCommand
import topsystems.actualizador.command.dataserver.AgregarModificarAtributoEntidad
import topsystems.actualizador.command.properties.AgregarModificarPropiedad
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.configuration.CarpetasActualizador
import topsystems.actualizador.configuration.CarpetasServidor
import topsystems.actualizador.configuration.app.ConfigurationAppManager
import topsystems.actualizador.parche.Parche

class Parche_NUBANKIBR_492 extends Parche {
	
	private static final String PROPERTY_FILE_NAME = "userfunctions.properties";

	void sobreElParche() {
		this.notas_del_autor = "Parche_NUBANKIBR_492"
		this.opcional = false
	}

	@Override
	protected void comandos() {
		addProperty()
	}

	private void addProperty(){
		Properties userFuctionProperty = loadFileUserFunctionProperty(PROPERTY_FILE_NAME);
		int count = getFunctionSize(userFuctionProperty);
		int nextCount = count + 1;

		// Change property userfunctions.count
		this.addCommand( new AgregarModificarPropiedad(PROPERTY_FILE_NAME, "userfunctions.count", String.valueOf(nextCount)));

		// Add new property
		def propiedad = "userfunctions.name."+ nextCount;
		this.addCommand( new AgregarModificarPropiedad(PROPERTY_FILE_NAME, propiedad, "topsystems.util.formevaluator.userfunctions.br.GenerateCheckDigitFunction"));

	}

	private int getFunctionSize(Properties userFuctionProperty){
		
		Integer countReaded = userFuctionProperty.get("userfunctions.count");
		if(countReaded == null){
			countReaded = 0
		}

		int countCalculate = 0;
		Enumeration<String> keys = userFuctionProperty.keys();
		while (keys.hasMoreElements()) {
			String clave = keys.nextElement();
			if(clave.startsWith("userfunctions.name.")){
				countCalculate ++;
			}
		}

		if(countReaded != countCalculate){
			return countCalculate;
		}

		return countReaded;
	}


	private Properties loadFileUserFunctionProperty(String fileName){

		String filePath = CarpetasServidor.TOPAZ_PROPS.getExpandedPath() + File.separator + fileName;
		InputStream fileInputStream = null;
		try{
			fileInputStream = new FileInputStream(filePath);
			Properties prop = new Properties();
			prop.load(fileInputStream);
			return prop;
		} catch (Exception ex) {
			throw new RuntimeException("Can't read file:"+ filePath +" Error: "+ ex.getMessage(), ex);
		} finally{
			if(fileInputStream != null){
				try{
					fileInputStream.close();
				}catch (IOException ex) {
					ex.printStackTrace();
				}
			}
		}
	}
}
