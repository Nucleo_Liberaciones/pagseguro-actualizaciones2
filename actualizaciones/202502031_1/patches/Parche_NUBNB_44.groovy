package Base


import java.sql.Date

import topsystems.actualizador.command.dataserver.AgregarModificarEntidad
import topsystems.actualizador.command.dataserver.AgregarModificarQuery
import topsystems.actualizador.command.processmanager.AgregarModificarGrupo
import topsystems.actualizador.command.processmanager.AgregarModificarProceso
import topsystems.actualizador.command.servicios.AgregarModificarServicios
import topsystems.actualizador.command.sql.ActualizarBaseDeDatos
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosOracle
import topsystems.actualizador.command.sql.ActualizarBaseDeDatosSQLServer
import topsystems.actualizador.parche.Parche

class Parche_NUBNB_44 extends Parche {

	void sobreElParche() {
		this.notas_del_autor = "Parche_NUBNB_44"
		this.opcional = false
	}

	void definicionDeParametros(){
		this.addParameterNewOperationNumber("OPERACION_SALDOS_DIARIOS_POUPANCA_DET", " Nro. para la operacion del PA Saldos Diarios Poupanca Detallado.")
		this.addParameterPATitleOpe("TITULO_SALDOS_DIARIOS_POUPANCA_DET", "Nro. titulo de operacion para el PA Saldos Diarios Poupanca Detallado.")
	}
	
	@Override
	protected void comandos() {
		modificarProcessesGroups()
		nuevaEstructuraBD()
		adicionarEntidadMapping()
		adicionarServicio()
		adicionarQueryMapping()
		insertarBaseDato()
	}
	
	private adicionarEntidadMapping(){
		def archivoMapp = "poupancamapping.xml"
		def clase = """
        <class>
		    <class-name>topsystems.automaticprocess.poupanca.sumarizasaldos.objectsdomain.SaldosDiariosPoupancaDetallado</class-name>
		    <entity-name>vo_SaldosDiariosPoupancaDetallado</entity-name>
		    <table-name>GRL_SDOS_DIARIOS_POUPANCA_DET</table-name>
		    <database-name>TOP/CLIENTES</database-name>  
		    <attribute>
		        <attribute-name>saldosJtsOid</attribute-name>
		        <column-name>SALDOS_JTS_OID</column-name>
		        <key>primary</key>
		    </attribute>
		    <attribute>
		        <attribute-name>mes</attribute-name>
		        <column-name>MES</column-name>
		        <key>primary</key>
		    </attribute>
		    <attribute>
		        <attribute-name>diaAniversario</attribute-name>
		        <column-name>DIA_ANIVERSARIO</column-name>
		        <key>primary</key>
		    </attribute>
		    <attribute>
		        <attribute-name>saldo</attribute-name>
		        <column-name>SALDO</column-name>
		    </attribute>
		    <attribute>
		        <attribute-name>interesBasico</attribute-name>
		        <column-name>INTERES_BASICO</column-name>
		    </attribute>
		    <attribute>
		        <attribute-name>interesAdicional</attribute-name>
		        <column-name>INTERES_ADICIONAL</column-name>
		    </attribute>
		    <attribute>
		        <attribute-name>saldoAConfirmar</attribute-name>
		        <column-name>SALDO_A_CONFIRMAR</column-name>
		    </attribute>
		    <attribute>
		        <attribute-name>saldoDiaAnterior</attribute-name>
		        <column-name>SALDO_DIA_ANTERIOR</column-name>
		    </attribute>
		    <attribute>
		        <attribute-name>saldoDiaAnteriorConfirmar</attribute-name>
		        <column-name>SALDO_DIA_ANTERIOR_CONFIRMAR</column-name>
		    </attribute>
		    <attribute>
		        <attribute-name>poupanca2012</attribute-name>
		        <column-name>POUPANCA_2012</column-name>
		    </attribute>
	</class>
    """
	this.addCommand(new AgregarModificarEntidad(archivoMapp, clase))
	}
	
	private adicionarQueryMapping(){
		def archivoMapp = "poupancamapping.xml"
		def query = """
	    <query>
				<class-name>topsystems.automaticprocess.poupanca.sumarizasaldos.SaldosDiariosPoupancaDetallado</class-name>
				<query-name>query.SaldosDiariosPoupancaDetallado</query-name>
				<database-name>TOP/CLIENTES</database-name>
				<sentence>	
					SELECT 
					    s.saldos_jts_oid, 
					    s.MES, 
					    s.DIA_ANIVERSARIO, 
					    s.SALDO, 
					    s.INTERES_BASICO_DEVENGADO, 
					    s.INTERES_ADICIONAL_DEVENGADO, 
					    s.SALDO_A_CONFIRMAR,
					    s.SALDO_DIA_ANTERIOR, 
					    s.SALDO_DIA_ANTERIOR_CONFIRMAR, 
					    'N' AS poupanca2012 
					FROM SALDOS_POUPANCA s WHERE (s.SALDO_DIA_ANTERIOR + s.SALDO_DIA_ANTERIOR_CONFIRMAR + s.INTERES_BASICO_DEVENGADO + s.INTERES_ADICIONAL_DEVENGADO) != 0 -- condicion de que la suma de saldo e intereses sea distinta de cero
					UNION ALL
					SELECT 
					    sp.saldos_jts_oid, 
					    sp.MES, 
					    sp.DIA_ANIVERSARIO, 
					    sp.SALDO, 
					    sp.INTERES_BASICO_DEVENGADO, 
					    sp.INTERES_ADICIONAL_DEVENGADO, 
					    0 AS SALDO_A_CONFIRMAR,
					    sp.SALDO_DIA_ANTERIOR, 
					    0 AS SALDO_DIA_ANTERIOR_CONFIRMAR, 
					    'S' AS poupanca2012
					FROM SALDOS_POUPANCA_2012 sp WHERE (sp.SALDO_DIA_ANTERIOR + sp.INTERES_BASICO_DEVENGADO + sp.INTERES_ADICIONAL_DEVENGADO) != 0 -- condicion de que la suma de saldo e intereses sea distinta de cero
                </sentence>
				<attribute>
					<attribute-name>saldosJtsOid</attribute-name>
					<column-name>saldos_jts_oid</column-name>
					<column-type>long</column-type>
				</attribute>
				<attribute>
					<attribute-name>mes</attribute-name>
					<column-name>MES</column-name>
					<column-type>int</column-type>
				</attribute>
				<attribute>
					<attribute-name>diaAniversario</attribute-name>
					<column-name>DIA_ANIVERSARIO</column-name>
					<column-type>int</column-type>
				</attribute>
				<attribute>
					<attribute-name>saldo</attribute-name>
					<column-name>SALDO</column-name>
					<column-type>double</column-type>
				</attribute>
				<attribute>
					<attribute-name>interesBasico</attribute-name>
					<column-name>INTERES_BASICO_DEVENGADO</column-name>
					<column-type>double</column-type>
				</attribute>
				<attribute>
					<attribute-name>interesAdicional</attribute-name>
					<column-name>INTERES_ADICIONAL_DEVENGADO</column-name>
					<column-type>double</column-type>
				</attribute>
				<attribute>
					<attribute-name>saldoAConfirmar</attribute-name>
					<column-name>SALDO_A_CONFIRMAR</column-name>
					<column-type>double</column-type>
				</attribute>
				<attribute>
					<attribute-name>saldoDiaAnterior</attribute-name>
					<column-name>SALDO_DIA_ANTERIOR</column-name>
					<column-type>double</column-type>
				</attribute>
				<attribute>
					<attribute-name>saldoDiaAnteriorConfirmar</attribute-name>
					<column-name>SALDO_DIA_ANTERIOR_CONFIRMAR</column-name>
					<column-type>double</column-type>
				</attribute>
				<attribute>
					<attribute-name>poupanca2012</attribute-name>
					<column-name>poupanca2012</column-name>
					<column-type>String</column-type>
				</attribute>
        </query>
		"""
		this.addCommand( new AgregarModificarQuery(archivoMapp, query))
	}
	
	private adicionarServicio(){
		def archivo = "PoupancaService.xml"
		def servicio = """
				<service>
					<name>topsystems:processManager:WorkDescriptor=WorkDescriptorSaldosDiariosPoupancaDetallado</name>
					<code>topsystems.automaticprocess.poupanca.sumarizasaldos.WorkDescriptorSaldosDiariosPoupancaDetallado</code>
				</service>
				<service>
					<name>topsystems:processManager:BusinessWorker=BusinessWorkerSaldosDiariosPoupancaDetallado</name>
					<code>topsystems.automaticprocess.poupanca.sumarizasaldos.BusinessWorkerSaldosDiariosPoupancaDetallado</code>
				</service>
			"""
		this.addCommand( new AgregarModificarServicios(archivo, servicio))
	}
	
	private modificarProcessesGroups(){
		def archivo = "processes.py"
		def procesos = """
	    pdef = ProcessDefinition("Saldos Diarios Poupanca Detallado", "topsystems.automaticprocess.processmanager.WorkManager")
	    pdef.addConstant("workDescriptorName","topsystems:processManager:WorkDescriptor=WorkDescriptorSaldosDiariosPoupancaDetallado");
	    pdef.addConstant("businessWorkName","topsystems:processManager:BusinessWorker=BusinessWorkerSaldosDiariosPoupancaDetallado");
	    pdef.addConstant("resultHandlerName","topsystems:processManager:ResultHandler=ResultHandlerDefault");
		pdef.addConstant("rangoCommit","100");
		pdef.addConstant("cantidadHilos","10");
		pdef.addConstant("isStopable","true");
		pdef.addConstant("applySchemes","true");
		pdef.addConstant("isSumarizable","true");
		pdef.addConstant("offLine","true");
		pdef.addConstant("enqueue","false");
		pdefs.addProcess(pdef) 
		"""
		this.addCommand(new AgregarModificarProceso(archivo, procesos))
		def archivoGroups = "groups.py"
		def detalleGroups = """
		    #PA Saldos Diarios Poupanca Detallado
		    gdef = GroupDefinition("PA - Saldos Diarios Poupanca Detallado")
		    gdef.registerProcess("Saldos Diarios Poupanca Detallado")
		    gdefs.addGroup(gdef)
		"""
		this.addCommand( new AgregarModificarGrupo(archivoGroups, detalleGroups))
	}

	protected void nuevaEstructuraBD(){
		def baseOra = esBaseDeDatos("ORACLE")
		if(baseOra){
			nuevaEstructuraBDOracle()
		}else{
			nuevaEstructuraBDSqlServer()
		}
	}

	protected void nuevaEstructuraBDOracle(){
		def sentencia = """
		-- HM begin sentence
				CREATE TABLE GRL_SDOS_DIARIOS_POUPANCA_DET (
				    TZ_LOCK                     NUMBER(15) NOT NULL,
				    SALDOS_JTS_OID              NUMBER(15) NOT NULL,
				    FECHA                       DATE NOT NULL,
				    MES                         NUMBER(2) DEFAULT 0 NOT NULL,
				    DIA_ANIVERSARIO             NUMBER(2) DEFAULT 0 NOT NULL,
				    SALDO                       NUMBER(15,2) DEFAULT 0,
				    INTERES_BASICO              NUMBER(15,2) DEFAULT 0,
				    INTERES_ADICIONAL           NUMBER(15,2) DEFAULT 0,
				    SALDO_A_CONFIRMAR           NUMBER(15,2) DEFAULT 0,
				    SALDO_DIA_ANTERIOR          NUMBER(15,2) DEFAULT 0,
				    SALDO_DIA_ANTERIOR_CONFIRMAR NUMBER(15,2) DEFAULT 0,
				    POUPANCA_2012               VARCHAR2(1) DEFAULT 'N',
				    CONSTRAINT PK_SDOS_DIARIOS PRIMARY KEY (SALDOS_JTS_OID, FECHA, MES, DIA_ANIVERSARIO,POUPANCA_2012)
				)
				PARTITION BY RANGE (FECHA) 
			INTERVAL (NUMTODSINTERVAL(1, 'DAY'))
			(
    		PARTITION p_start VALUES LESS THAN (TO_DATE('01/01/2025', 'dd/mm/yyyy'))
			)
		-- HM end sentence;
	  """
		this.addCommand(new ActualizarBaseDeDatosOracle(sentencia))
	}

	protected void nuevaEstructuraBDSqlServer(){
		def sentencia = """
			-- HM begin sentence
			CREATE TABLE GRL_SDOS_DIARIOS_POUPANCA_DET (
			    TZ_LOCK                     NUMERIC(15) NOT NULL,
			    SALDOS_JTS_OID              NUMERIC(15) NOT NULL,
			    FECHA                       DATE NOT NULL,
			    MES                         NUMERIC(2) DEFAULT 0 NOT NULL,
			    DIA_ANIVERSARIO             NUMERIC(2) DEFAULT 0 NOT NULL,
			    SALDO                       NUMERIC(15,2) DEFAULT 0,
			    INTERES_BASICO              NUMERIC(15,2) DEFAULT 0,
			    INTERES_ADICIONAL           NUMERIC(15,2) DEFAULT 0,
			    SALDO_A_CONFIRMAR           NUMERIC(15,2) DEFAULT 0,
			    SALDO_DIA_ANTERIOR          NUMERIC(15,2) DEFAULT 0,
			    SALDO_DIA_ANTERIOR_CONFIRMAR NUMERIC(15,2) DEFAULT 0,
			    POUPANCA_2012               VARCHAR(1) DEFAULT 'N',
			    PRIMARY KEY (SALDOS_JTS_OID, FECHA, MES, DIA_ANIVERSARIO,POUPANCA_2012)
			)
			-- HM begin sentence;
	  """
		this.addCommand(new ActualizarBaseDeDatosSQLServer(sentencia))
	}

	private insertarBaseDato(){
		def sentencia = """
		INSERT INTO OPERACIONES
		(TITULO, IDENTIFICACION, NOMBRE, DESCRIPCION, MNEMOTECNICO, AUTORIZACION, FORMULARIOPRINCIPAL, PROXOPERACION, ESTADO, TZ_LOCK, COPIAS, SUBOPERACION, PERMITEBAJA, COMPORTAMIENTOENCIERRE, REQUIERECONTRASENA, PERMITECONCURRENTE, PERMITEESTADODIFERIDO, ICONO_TITULO, ESTILO)
		VALUES(${this.P("TITULO_SALDOS_DIARIOS_POUPANCA_DET")}, ${this.P("OPERACION_SALDOS_DIARIOS_POUPANCA_DET")}, 'PA: SALDOS_DIARIOS_POUPANCA_DET', 'Carga la tabla GRL_SDOS_DIARIOS_POUPANCA_DET', ${this.P("OPERACION_SALDOS_DIARIOS_POUPANCA_DET")}, 'N', 0, 0, 'P', 0, NULL, NULL, NULL, 'N', 'N', 'S', 'S', NULL, NULL);
		
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
		VALUES ('SALDOS_DIARIOS_POUPANCA_DET', 'OPERACION', '${this.P("OPERACION_SALDOS_DIARIOS_POUPANCA_DET")}', 0);
		
		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
		VALUES ('SALDOS_DIARIOS_POUPANCA_DET', 'CANAL', '0', 0);

		INSERT INTO PARAMETROS_JTS (FUNCIONALIDAD, PARAMETRO, VALOR, TZ_LOCK)
		VALUES ('SALDOS_DIARIOS_POUPANCA_DET', 'DESCRIPCION', 'Saldos Diarios Poupanca Detallado', 0);
			
		"""
		def descripcion = "INSERT [PA: Saldos Diarios Poupanca Detallado]"
		this.addCommand( new ActualizarBaseDeDatos(sentencia, descripcion))
	}
	
}
